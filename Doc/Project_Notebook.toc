\contentsline {section}{\numberline {1}Formal Game Proposal}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Game Description}{3}{subsection.1.1}
\contentsline {paragraph}{Game Loop}{3}{section*.2}
\contentsline {subsubsection}{\numberline {1.1.1}Setting}{3}{subsubsection.1.1.1}
\contentsline {subsubsection}{\numberline {1.1.2}Environment}{3}{subsubsection.1.1.2}
\contentsline {subsubsection}{\numberline {1.1.3}Orbit Mechanics}{3}{subsubsection.1.1.3}
\contentsline {subsubsection}{\numberline {1.1.4}Asteroid Base}{4}{subsubsection.1.1.4}
\contentsline {paragraph}{Movement/Controls}{4}{section*.3}
\contentsline {paragraph}{Modules}{4}{section*.5}
\contentsline {subsubsection}{\numberline {1.1.5}Resources and Collection}{5}{subsubsection.1.1.5}
\contentsline {subsubsection}{\numberline {1.1.6}Gameplay}{5}{subsubsection.1.1.6}
\contentsline {subsubsection}{\numberline {1.1.7}Narration}{5}{subsubsection.1.1.7}
\contentsline {subsubsection}{\numberline {1.1.8}HUD}{6}{subsubsection.1.1.8}
\contentsline {subsubsection}{\numberline {1.1.9}Menus}{6}{subsubsection.1.1.9}
\contentsline {subsubsection}{\numberline {1.1.10}Graphics}{6}{subsubsection.1.1.10}
\contentsline {subsection}{\numberline {1.2}Technical Achievement}{10}{subsection.1.2}
\contentsline {subsubsection}{\numberline {1.2.1}Engine}{10}{subsubsection.1.2.1}
\contentsline {subsubsection}{\numberline {1.2.2}Physics simulation}{10}{subsubsection.1.2.2}
\contentsline {subsection}{\numberline {1.3}"Big Idea" Bullseye}{10}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Assessment}{10}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}Development Schedule}{12}{subsection.1.5}
\contentsline {section}{\numberline {2}Prototype}{13}{section.2}
\contentsline {subsection}{\numberline {2.1}Prototyping Goals}{13}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Setup}{13}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Game Loop}{13}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Game Rules}{14}{subsection.2.4}
\contentsline {subsubsection}{\numberline {2.4.1}General rules}{14}{subsubsection.2.4.1}
\contentsline {subsubsection}{\numberline {2.4.2}Movement phase}{14}{subsubsection.2.4.2}
\contentsline {subsubsection}{\numberline {2.4.3}Building phase}{14}{subsubsection.2.4.3}
\contentsline {subsubsection}{\numberline {2.4.4}Energy phase}{15}{subsubsection.2.4.4}
\contentsline {subsubsection}{\numberline {2.4.5}Orbital transfer}{15}{subsubsection.2.4.5}
\contentsline {subsubsection}{\numberline {2.4.6}Random Cards}{15}{subsubsection.2.4.6}
\contentsline {subsubsection}{\numberline {2.4.7}Physics}{15}{subsubsection.2.4.7}
\contentsline {subsection}{\numberline {2.5}Results}{15}{subsection.2.5}
\contentsline {section}{\numberline {3}Interim Report}{17}{section.3}
\contentsline {subsection}{\numberline {3.1}Implementation}{17}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Physics engine}{17}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}Collision}{17}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}Orbit mechanics}{18}{subsubsection.3.2.2}
\contentsline {subsection}{\numberline {3.3}Entity Component System}{18}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Render Engine}{19}{subsection.3.4}
\contentsline {subsubsection}{\numberline {3.4.1}Object Rendering}{19}{subsubsection.3.4.1}
\contentsline {subsubsection}{\numberline {3.4.2}Postprocessing}{20}{subsubsection.3.4.2}
\contentsline {paragraph}{Extras:}{21}{figure.caption.15}
\contentsline {subsubsection}{\numberline {3.4.3}UI}{21}{subsubsection.3.4.3}
\contentsline {subsection}{\numberline {3.5}InputEngine}{22}{subsection.3.5}
\contentsline {section}{\numberline {4}Alpha Report}{23}{section.4}
\contentsline {subsection}{\numberline {4.1}Gameplay}{23}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}Orbit mechanics}{23}{subsubsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.2}Resources}{23}{subsubsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.3}Base building}{23}{subsubsection.4.1.3}
\contentsline {subsubsection}{\numberline {4.1.4}Asteroids}{23}{subsubsection.4.1.4}
\contentsline {subsection}{\numberline {4.2}Renderer}{23}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}Additions}{24}{subsubsection.4.2.1}
\contentsline {paragraph}{Camera.}{24}{section*.18}
\contentsline {paragraph}{Bilateral blur.}{24}{section*.19}
\contentsline {paragraph}{Particles.}{24}{section*.20}
\contentsline {subsubsection}{\numberline {4.2.2}Improvements}{25}{subsubsection.4.2.2}
\contentsline {paragraph}{UI.}{25}{section*.21}
\contentsline {paragraph}{Cube map rendering.}{25}{section*.22}
\contentsline {subsection}{\numberline {4.3}InputEngine}{25}{subsection.4.3}
\contentsline {subsubsection}{\numberline {4.3.1}Changes}{25}{subsubsection.4.3.1}
\contentsline {paragraph}{InputWrapper.}{25}{section*.23}
\contentsline {paragraph}{InputEngine.}{26}{section*.24}
\contentsline {subsubsection}{\numberline {4.3.2}Additions}{26}{subsubsection.4.3.2}
\contentsline {paragraph}{InputEngine.}{26}{section*.25}
\contentsline {paragraph}{UIComponents.}{26}{section*.26}
\contentsline {section}{\numberline {5}Playtesting}{27}{section.5}
\contentsline {subsection}{\numberline {5.1}Organization}{27}{subsection.5.1}
\contentsline {subsubsection}{\numberline {5.1.1}Questions}{27}{subsubsection.5.1.1}
\contentsline {subsubsection}{\numberline {5.1.2}Answers}{27}{subsubsection.5.1.2}
\contentsline {subsubsection}{\numberline {5.1.3}Changes}{28}{subsubsection.5.1.3}
\contentsline {section}{\numberline {6}Release}{29}{section.6}
\contentsline {subsection}{\numberline {6.1}Changes since Playtesting}{29}{subsection.6.1}
\contentsline {paragraph}{Engine Structure}{29}{section*.28}
\contentsline {paragraph}{Balancing}{29}{section*.29}
\contentsline {paragraph}{Particle System}{29}{section*.30}
\contentsline {subsection}{\numberline {6.2}State of the Game / What We Achieved}{29}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Evaluation}{30}{subsection.6.3}
\contentsline {subsection}{\numberline {6.4}Personal Impressions}{31}{subsection.6.4}
