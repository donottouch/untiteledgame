﻿#include "pch.h"
#include "GameMain.h"
#include "Common\DirectXHelper.h"
#include "Content/ShaderStructures.h"
#include "Components/Rendering/StandardMaterial.h"
#include "Components/Rendering/OrbitMaterial.h"
#include "Components/Rendering/ParticleMaterial.h"
#include "Components/CameraComponent.h"
#include "Components/Rendering/ParticleRenderComponent.h"
#include "Content/ShaderStructures.h"
#include "Components/Behaviours/Player.h"
#include "Components/Behaviours/Asteroid.h"
#include "Components/Behaviours/CameraController.h"
#include "Components/Rendering/StandardParticleSystem.h"
#include "InputTestScript.h"
#include <functional>

//#define USE_SAMPLE_RENDERER

using namespace Game;
using namespace Windows::Foundation;
using namespace Windows::System::Threading;
using namespace Concurrency;

// Die Vorlage "DirectX 12-Anwendung" wird unter https://go.microsoft.com/fwlink/?LinkID=613670&clcid=0x407 dokumentiert.

// Lädt und initialisiert die Anwendungsobjekte, wenn die Anwendung geladen wird.
GameMain::GameMain()
{
	InitConfig();

	//Init Meshes
	m_MeshPlane = Renderer::Mesh::GetQuadMesh();
	m_MeshCube = Renderer::Mesh::GetCubeMesh();
	m_MeshSphere = Renderer::Mesh::GetSphereMesh(8);

	{
		m_AsteroidMesh = new Renderer::Mesh();
		m_AsteroidMesh->LoadFromFile("Asteroid.obj", true);

		m_BaseMesh = new Renderer::Mesh();
		m_BaseMesh->LoadFromFile("Base.obj", true);

	}
	


	//Init Materials
	{
		m_MatWireframe = new Renderer::StandardMaterial(L"SampleVertexShader.cso", nullptr, L"SamplePixelShader.cso", D3D12_FILL_MODE_WIREFRAME, D3D12_CULL_MODE_BACK, false, false);
		m_MatFlat = new Renderer::StandardMaterial(L"Flat_VS.cso", L"FlatNormal_GS.cso", L"Flat_PS.cso", D3D12_FILL_MODE_SOLID, D3D12_CULL_MODE_BACK);
		Renderer::StandardMaterial* occluder = new Renderer::StandardMaterial(L"Flat_VS.cso", L"FlatNormal_GS.cso", L"Flat_PS.cso", D3D12_FILL_MODE_SOLID, D3D12_CULL_MODE_BACK, true, false);
		occluder->SetBlendState(Renderer::discardNormalDepth);
		occluder->SetBlendStateNormalDepth(Renderer::discardNormalDepth);
		m_MatOccluder = occluder;
		m_MatParticle = new Renderer::ParticleMaterial(L"Particle_VS.cso", L"Particle_GS.cso", L"Particle_PS.cso"); //, 
		//m_Renderer->InitializeMaterial(m_MatWireframe);

		m_ParticleSystemType = std::make_shared<Renderer::StandardParticleSystem>(L"ParticleSpawn_CS.cso", L"ParticleUpdate_CS.cso", L"ParticleStats_CS.cso");
	}

	// Init engines
	{
		m_Physics = std::make_unique<PhysicEngine>();
		m_InputEngine = std::make_unique<InputEngine::InputEngine>(Windows::UI::Core::CoreWindow::GetForCurrentThread());//new InputEngine::InputWrapper(Windows::UI::Core::CoreWindow::GetForCurrentThread());
		m_InputEngine->setCoordinateScale(m_Config->GetFloat("fButtonScaling", 1.0));
		m_Renderer = std::make_unique<Renderer::RenderEngine>(m_Config);
	}

	// Create root;
	Renderer::MeshParams style;
	m_SceneRoot = std::make_unique<GameObject>(this, XMFLOAT3(0, 0, 0));
	m_SceneRoot->setName("GameRoot");
	
	// Create Main Camera
	{
		m_CameraPivot = new GameObject(this);
		m_CameraPivot->setName("Camera Pivot");
		m_SceneRoot->addChild(m_CameraPivot);

		GameObject* cameraPivotX = new GameObject(this);
		cameraPivotX->setName("Camera Pivot X");
		m_CameraPivot->addChild(cameraPivotX);

		GameObject* camera = new GameObject(this, XMFLOAT3(0, 6.0f, 0));
		camera->setName("Camera");
		cameraPivotX->addChild(camera);
		camera->getTransform()->RotateLocal(XMFLOAT3(0, static_cast<float>(-0.5*M_PI), 0));
		camera->getTransform()->RotateLocal(XMFLOAT3(static_cast<float>(0.5*M_PI), 0, 0));

		Behavior::CameraController* cameraCtrl = new Behavior::CameraController(m_Config);
		camera->addComponent(cameraCtrl);

		Renderer::CameraComponent* cameraComp = new Renderer::CameraComponent();
		camera->addComponent(cameraComp);
		m_Renderer->SetMainCamera(cameraComp);
	}

	
	//*/
	// TODO: Timereinstellungen ändern, wenn Sie etwas Anderes möchten als den standardmäßigen variablen Zeitschrittmodus.
	// z.B. für eine Aktualisierungslogik mit festen 60FPS-Zeitschritten Folgendes aufrufen:
	/*
	m_timer.SetFixedTimeStep(true);
	m_timer.SetTargetElapsedSeconds(1.0 / 60);
	*/

	initTestScene();
}

// Erstellt und initialisiert die Renderer.
void GameMain::CreateRenderers(const std::shared_ptr<DX::DeviceResources>& deviceResources)
{
	// TODO: Dies durch Ihre App-Inhaltsinitialisierung ersetzen.
#ifdef USE_SAMPLE_RENDERER
	m_sceneRenderer = std::unique_ptr<Sample3DSceneRenderer>(new Sample3DSceneRenderer(deviceResources));
#else
	m_Renderer->Initialize(deviceResources);
#endif

	OnWindowSizeChanged();
}

// Aktualisiert den Anwendungszustand ein Mal pro Frame.
void GameMain::Update()
{

	// Die Szeneobjekte aktualisieren.
	m_timer.Tick([&]()
	{
		const float dTime = static_cast<float>(m_timer.GetElapsedSeconds());
		const float radPerSecond = 0.2f;
		const float radStep = radPerSecond * dTime;

		// ULTRA HACKY WAY TO GET TIME WARP FACTOR
		const float timeWarp = m_SceneRoot.get()->getComponent<OrbitManager>()->getTimeWarpFactor();
		const float dT = dTime * timeWarp;

		//m_CameraPivot->getTransform()->RotateLocal(XMFLOAT3(0, radStep, 0));

		m_Physics.get()->computeCollisions();

#ifdef USE_SAMPLE_RENDERER
		m_sceneRenderer->Update(m_timer);
#else
		m_Renderer->Update(m_timer, timeWarp);
		for each (Renderer::ParticleRenderComponent* p in m_ParticleComponents) {
			p->OnUpdate(dT);
		}
		m_Renderer->UpdateParticleSystems(m_ParticleComponents);
#endif
		//Tick all Behaviours
		for each (Behaviour* b in otherComps)
		{
			b->OnUpdate(dTime);
		}

		// delete flagged objects
		deleteGameObjects();
		m_InputEngine->tick(&uiComps);
	});
}

DX::StepTimer GameMain::getTimer() {
	return m_timer;
}


// Rendert den aktuellen Frame dem aktuellen Anwendungszustand entsprechend.
// Gibt True zurück, wenn der Frame gerendert wurde und angezeigt werden kann.
bool GameMain::Render()
{
	// Nicht versuchen, etwas vor dem ersten Update zu rendern.
	if (m_timer.GetFrameCount() == 0)
	{
		return false;
	}

	// Die Szeneobjekte rendern.
	// TODO: Dies durch die Inhaltsrenderingfunktionen Ihrer App ersetzen.
	// TODO: RenderEngine daten übermitteln, evtl liste updaten/verarbeiten
#ifdef USE_SAMPLE_RENDERER
	return m_sceneRenderer->Render();
#else
	if (m_Renderer != nullptr && m_Renderer->IsInitialized()) {
		for (auto itr = m_RCInitPending.begin(); itr != m_RCInitPending.end(); ++itr) {
			RegisterComponent(*itr);
		}
		m_RCInitPending.clear();
	}
	m_Renderer->Render(m_RenderComponents, lightComps);
	return true;
#endif
}

bool GameMain::RegisterComponent(Renderer::RenderComponent* component) {
#ifndef USE_SAMPLE_RENDERER
	if (m_Renderer == nullptr || !m_Renderer->IsInitialized()) {
		std::cout << "Renderer not yet initialized!" << std::endl;
		m_RCInitPending.emplace_back(component);
		return true;
	}
	else {
		if (!component->IsLoadingComplete()) m_Renderer->InitializeRenderComponent(component);
		if (componentInstanceOf<Renderer::ParticleRenderComponent*>(component)) {
			m_ParticleComponents.emplace(dynamic_cast<Renderer::ParticleRenderComponent*>(component));
		}
		return m_RenderComponents.emplace(component).second;

	}
#else
	return false;
#endif
}

// Aktualisiert den Anwendungszustand, wenn sich die Fenstergröße ändert (z. B. Änderung der Geräteausrichtung)
void GameMain::OnWindowSizeChanged()
{
	// TODO: Dies durch Ihre größenabhängige Initialisierung Ihres App-Inhalts ersetzen.
	// ToDO: Poke Renderer
	//m_Renderer->ResizeWindow(0,0);
#ifdef USE_SAMPLE_RENDERER
	m_sceneRenderer->CreateWindowSizeDependentResources();
#else
	m_Renderer->ResizeWindow();
	m_InputEngine->handleResizeWindow(&uiComps);
#endif
}

// Benachrichtigt die App, dass sie gesperrt wird.
void GameMain::OnSuspending()
{
	// TODO: Dies durch Ihre App-Sperrlogik ersetzen.

	// Die Prozesslebensdauer-Verwaltung kann gesperrte Apps jederzeit beenden, daher ist es
	// eine bewärte Vorgehensweise, jeden Zustand zu speichern, der es ermöglicht, die App an dem Punkt neu zu starten, an dem sie beendet wurde.

#ifdef USE_SAMPLE_RENDERER
	m_sceneRenderer->SaveState();
#endif

	// Wenn die Anwendung Videospeicherbelegungen nutzt, die einfach neu erstellt werden können,
	// sollten Sie in Erwägung ziehen, diesen Arbeitsspeicher freizugeben, um ihn anderen Anwendungen zur Verfügung zu stellen.
}

// Benachrichtigt die App, dass sie nicht länger gesperrt ist.
void GameMain::OnResuming()
{
	// TODO: Dies durch Ihre App-Fortsetzungslogik ersetzen.
}

// Weist Renderer darauf hin, dass die Geräteressourcen freigegeben werden m・sen.
void GameMain::OnDeviceRemoved()
{
	// TODO: Jeden erforderlichen Anwendungs- oder Rendererzustand speichern und den Renderer freigeben.
	// und die Ressourcen, die nicht länger gültig sind.
	// ToDO: Poke renderEngine
#ifdef USE_SAMPLE_RENDERER
	m_sceneRenderer->SaveState();
	m_sceneRenderer = nullptr;
#else
	m_Renderer = nullptr;
#endif
}

void GameMain::flagForDeletion(GameObject* go) {
	list<GameObject*> children = go->getAllChildren();

	// check if one of the children is already contained
	list<GameObject*>::iterator childIt;
	for (childIt = children.begin(); childIt != children.end(); ++childIt) {

		list<GameObject*>::iterator deleteIt;
		for (deleteIt = LflaggedForDeletion.begin(); deleteIt != LflaggedForDeletion.end(); /* nothing */) {
			// check if pointer contained
			bool found = (std::find(childIt, children.end(), *deleteIt) != children.end());
			if (found) {
				deleteIt = LflaggedForDeletion.erase(deleteIt);
			}
			else {
				++deleteIt;
			}
		}
	}

	LflaggedForDeletion.push_back(go);
}

void GameMain::deleteGameObjects() {
	// delete all elements flagged for deletion
	for (auto&& child : LflaggedForDeletion) {
		delete child;
	}
	LflaggedForDeletion.clear();
}

void GameMain::logToConsole(string input) {
	std::wstring ws(input.begin(), input.end());
	wstringstream s;
	s.str(ws);
	OutputDebugString(s.str().c_str());
}

void Game::GameMain::InitConfig() {
	m_Config = std::make_shared<ConfigParser::Config>();

	//TODO set default values

	//parse
	if (ConfigParser::Parse("DarkRip.ini", m_Config.get())) {
		logToConsole("parsed ini\n");
	}
	else {
		logToConsole("failed to parse ini\n");
	}
}

PhysicEngine* GameMain::getPhysicsEngine() {
	return m_Physics.get();
}

void GameMain::initTestScene() {
	Renderer::MeshParams style;
	//Renderer::Mesh* monkeyMesh = new Renderer::Mesh();
	//monkeyMesh->LoadFromFile("monkey.obj", true); //Execution dir is btw \untiteledgame\x64\Debug\Game\AppX
	////create monkey object
	//GameObject* monkey = new GameObject(this, XMFLOAT3(0, 0, 2));
	//monkey->setName("Monkey");
	//m_SceneRoot->addChild(monkey);
	//monkey->getTransform()->Rotate(XMFLOAT3(0, 3.14f, 0.5f));

	//Renderer::RenderComponent* monkeyRC = new Renderer::MeshRenderComponent(*monkeyMesh, *m_MatFlat, style);
	//monkey->addComponent(monkeyRC);

	GameObject* backgroundParticles = new GameObject(this);
	backgroundParticles->setName("Background Particles");
	m_SceneRoot->addChild(backgroundParticles); //->getAllChildren().back()->getAllChildren().front()
	backgroundParticles->getTransform()->setScale(XMFLOAT3(0.72f, 0.72f, 0.72f));
	style.colorDiffuse = XMFLOAT4(0,0,0,0);// (0.f, 0.9f, 1.0f, 1);
	style.colorEmissive = XMFLOAT4(0, 0, 0, 0);
	Renderer::RenderComponent* blackHoleOccluder = new Renderer::MeshRenderComponent(*m_MeshSphere, *m_MatOccluder, style);
	backgroundParticles->addComponent(blackHoleOccluder);
	
	{//background stars
		uint32_t numBackgroundParticles = 6000;
		Renderer::ParticleSystemParameters PSparams = {};
		PSparams.particleBufferSize = numBackgroundParticles;
		PSparams.dragCoefficient = 0.1f;
		PSparams.emitterType = Renderer::PARTICLE_EMITTER_TYPE_IN_SPHERE;
		PSparams.emitterRadius = XMFLOAT2(35, 75);
		PSparams.size = XMFLOAT2(0.04f, 0.07f);
		PSparams.color1 = XMFLOAT4(1, 0.8, 0.5, 5);
		PSparams.color2 = XMFLOAT4(0.8, 0.4, 1, 5);
		PSparams.flags = Renderer::PARTICLE_FLAG_WARP_TIME | Renderer::PARTICLE_FLAG_COLLISION;
		Renderer::ParticleRenderComponent* particles = new Renderer::ParticleRenderComponent(*m_MatParticle, m_ParticleSystemType, PSparams);
		particles->Active(false);
		particles->SpawnParticles(numBackgroundParticles);
		backgroundParticles->addComponent(particles);
	}
	{//black hole jets
		uint32_t jetBufferSize = 900;
		float jetEmissionRate = 150.0f;
		Renderer::ParticleSystemParameters PSparams;
		ZeroMemory(&PSparams, sizeof(Renderer::ParticleSystemParameters));
		PSparams.particleBufferSize = jetBufferSize;
		PSparams.dragCoefficient = 0.0f;
		PSparams.emitterType = Renderer::PARTICLE_EMITTER_TYPE_ON_HEMISPHERE;
		PSparams.emitterRadius = XMFLOAT2(0.02f, 0.02f);
		PSparams.emissionVel = XMFLOAT2(0.03f, 0.07f);
		PSparams.emissionVelType = Renderer::EMISSION_VEL_FLAG_X | Renderer::EMISSION_VEL_FLAG_Y | Renderer::EMISSION_VEL_FLAG_Z | Renderer::EMISSION_VEL_FLAG_RADIAL;
		PSparams.lifetime = XMFLOAT2(2.5f, 4.0f);
		PSparams.size = XMFLOAT2(0.003f, 0.01f);
		PSparams.color1 = XMFLOAT4(0, 0.3, 1, 8);
		PSparams.color2 = XMFLOAT4(0.2, 0, 1, 4);
		PSparams.flags = Renderer::PARTICLE_FLAG_WARP_TIME;
		{//Jet upwards

			PSparams.emissionVelLin = XMFLOAT4(0, 1.0f, 0, 1);
			Renderer::ParticleRenderComponent* particles = new Renderer::ParticleRenderComponent(*m_MatParticle, m_ParticleSystemType, PSparams);
			particles->SetSpawnRate(jetEmissionRate);
			backgroundParticles->addComponent(particles);
		}
		{//Jet downwards
			PSparams.emissionVelLin = XMFLOAT4(0, -1.05f, 0, 1);
			Renderer::ParticleRenderComponent* particles = new Renderer::ParticleRenderComponent(*m_MatParticle, m_ParticleSystemType, PSparams);
			particles->SetSpawnRate(jetEmissionRate);
			backgroundParticles->addComponent(particles);
		}
	}
	{//incoming particles
		uint32_t jetBufferSize = 300; //~ emission rate * max life time
		float jetEmissionRate = 6.0f;
		Renderer::ParticleSystemParameters PSparams;
		ZeroMemory(&PSparams, sizeof(Renderer::ParticleSystemParameters));
		PSparams.particleBufferSize = jetBufferSize;
		PSparams.dragCoefficient = 0.01f;
		PSparams.emitterType = Renderer::PARTICLE_EMITTER_TYPE_ON_CIRCLE;
		PSparams.emitterRadius = XMFLOAT2(10.0f, 20.0f);
		PSparams.randomPos = 6.5f;
		PSparams.randomVel = 0.4f;
		//PSparams.emissionVel = XMFLOAT2(0.03f, 0.07f);
		//PSparams.emissionVelType = Renderer::EMISSION_VEL_FLAG_X | Renderer::EMISSION_VEL_FLAG_Y | Renderer::EMISSION_VEL_FLAG_Z | Renderer::EMISSION_VEL_FLAG_RADIAL;
		PSparams.lifetime = XMFLOAT2(9.5f, 11.0f);
		PSparams.size = XMFLOAT2(0.012f, 0.014f);
		PSparams.color1 = XMFLOAT4(0, 0.3, 1, 12);
		PSparams.color2 = XMFLOAT4(0.2, 0, 1, 6);
		PSparams.forceFalloff = 2.0f;
		PSparams.flags = Renderer::PARTICLE_FLAG_WARP_TIME | Renderer::PARTICLE_FLAG_COLLISION | Renderer::PARTICLE_FLAG_FORCE;

		Renderer::ParticleRenderComponent* particles = new Renderer::ParticleRenderComponent(*m_MatParticle, m_ParticleSystemType, PSparams);
		particles->SetSpawnRate(jetEmissionRate);
		backgroundParticles->addComponent(particles);
		
	}


	// create Orbit manager
	OrbitManager* orbitManager = new OrbitManager(m_MeshSphere, m_MeshPlane, m_AsteroidMesh, m_BaseMesh, m_MatWireframe, m_MatFlat, m_MatOrbit);
	m_SceneRoot.get()->addComponent(orbitManager);


	/*Renderer::UIRectComponent* frame = new Renderer::UIRectComponent();
	frame->SetAnchor(XMFLOAT2(1, 1));
	frame->SetOffset(XMINT2(-160, -160));
	frame->SetSize(XMINT2(140, 140));
	frame->SetLineWidth(2);
	frame->SetColor(UIColor);
	m_SceneRoot->addComponent(frame);

	frame = new Renderer::UIRectComponent();
	frame->SetAnchor(XMFLOAT2(1, 1));
	frame->SetOffset(XMINT2(-308, -160));
	frame->SetSize(XMINT2(140, 140));
	frame->SetLineWidth(2);
	frame->SetColor(UIColor);
	m_SceneRoot->addComponent(frame);

	frame = new Renderer::UIRectComponent();
	frame->SetAnchor(XMFLOAT2(1, 1));
	frame->SetOffset(XMINT2(-456, -160));
	frame->SetSize(XMINT2(140, 140));
	frame->SetLineWidth(2);
	frame->SetColor(UIColor);
	m_SceneRoot->addComponent(frame);*/

	
	

	//Input Test
	/*
	GameObject* inTest = new GameObject(this, XMFLOAT3(0, 0, 4));
	inTest->setName("InputTestObject");
	m_SceneRoot->addChild(inTest); //->getAllChildren().back()->getAllChildren().front()
	inTest->getTransform()->setScale(XMFLOAT3(1.0f, 1.0f, 1.0f));

	style.colorDiffuse = XMFLOAT4(1, 1, 1, 1);// (0.f, 0.9f, 1.0f, 1);
	style.colorEmissive = XMFLOAT4(0,0,0, 0);
	Renderer::RenderComponent* inRC = new Renderer::MeshRenderComponent(*m_MeshSphere, *m_MatFlat, style);
	inTest->addComponent(inRC);


	// create test button
	InputTestScript* tS = new InputTestScript(this);
	inTest->addComponent(tS);
	UIComponent::Rect r = UIComponent::Rect();
	r.upperLeft = XMINT2(700, 700);
	r.lowerRight = XMINT2(780, 720);
	UIButtonComponent* UITests = new UIButtonComponent(r, L"Toggle", 20.0f);

	std::function<void()> cbf = std::bind(&InputTestScript::unS, tS); //Bind object to callback non static callback
	UITests->setClickedCallback(cbf);

	m_SceneRoot->addComponent(UITests);

	*/
}

bool GameMain::addComponent(Component* c) {
	// check for render component
	if (componentInstanceOf<Renderer::RenderComponent*>(c)) {
		// if not already contained, add it
		if (listContainsComponent<Renderer::RenderComponent*>(c, &renderComps) == false) {
			renderComps.push_back(dynamic_cast<Renderer::RenderComponent*>(c));
		}
		RegisterComponent(dynamic_cast<Renderer::RenderComponent*>(c));
		return true;
	}
	else if (componentInstanceOf<Renderer::LightComponent*>(c)) {
		// if not already contained, add it
		if (listContainsComponent<Renderer::LightComponent*>(c, &lightComps) == false) {
			lightComps.push_back(dynamic_cast<Renderer::LightComponent*>(c));
		}
		return true;
	}
	else if (componentInstanceOf<PhysicsComponent*>(c)) {
		// if not already contained, add it
		if (listContainsComponent<PhysicsComponent*>(c, &physicsComps) == false) {
			physicsComps.push_back(dynamic_cast<PhysicsComponent*>(c));
			m_Physics.get()->addPhysicsComponent(dynamic_cast<PhysicsComponent*>(c));
		}
		return true;
	}
	else if (componentInstanceOf<UIComponent*>(c)) {
		// if not already contained, add it
		if (listContainsComponent<UIComponent*>(c, &uiComps) == false) {
			uiComps.push_back(dynamic_cast<UIComponent*>(c));
		}
		return true;
	}
	else {
		// if not already contained, add it
		if (listContainsComponent<Component*>(c, &otherComps) == false) {
			otherComps.push_back(dynamic_cast<Component*>(c));
		}
		return true;
	}
	return false;
}

bool GameMain::removeComponent(Component* c) {
	// check for render component
	if (componentInstanceOf<Renderer::RenderComponent*>(c)) {
		// if contained, remove it
		if (listContainsComponent<Renderer::RenderComponent*>(c, &renderComps)) {
			Renderer::RenderComponent* rc = dynamic_cast<Renderer::RenderComponent*>(c);
			renderComps.remove(rc);
			m_RenderComponents.erase(rc);
			if (componentInstanceOf<Renderer::ParticleRenderComponent*>(rc)) {
				m_ParticleComponents.erase(dynamic_cast<Renderer::ParticleRenderComponent*>(rc));
			}
			m_RCInitPending.remove(rc);
		}
		return true;
	}
	else if (componentInstanceOf<Renderer::LightComponent*>(c)) {
		// if not already contained, add it
		if (listContainsComponent<Renderer::LightComponent*>(c, &lightComps)) {
			lightComps.remove(dynamic_cast<Renderer::LightComponent*>(c));
		}
		return true;
	}
	else if (componentInstanceOf<PhysicsComponent*>(c)) {
		// if contained, remove it
		if (listContainsComponent<PhysicsComponent*>(c, &physicsComps)) {
			physicsComps.remove(dynamic_cast<PhysicsComponent*>(c));
			m_Physics.get()->removePhysicsComponent(dynamic_cast<PhysicsComponent*>(c));
		}
		return true;
	}
	else if (componentInstanceOf<UIComponent*>(c)) {
		// if not already contained, add it
		if (listContainsComponent<UIComponent*>(c, &uiComps) == false) {
			uiComps.remove(dynamic_cast<UIComponent*>(c));
		}
		return true;
	}
	else {
		// if contained, remove it
		if (listContainsComponent<Component*>(c, &otherComps)) {
			otherComps.remove(dynamic_cast<Component*>(c));
		}
		return true;
	}
	return false;
}

template <class T>
bool GameMain::listContainsComponent(Component* c, list<T>* compList) {
	return (std::find(compList->begin(), compList->end(), c) != compList->end());
} 