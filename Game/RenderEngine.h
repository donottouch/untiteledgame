//based on https://www.3dgep.com/learning-directx12-2/
#pragma once

#ifndef RENDER_ENGINE
#define RENDER_ENGINE

#include "../pch.h"


// In order to define a function called CreateWindow, the Windows macro needs to
// be undefined.
#if defined(CreateWindow)
#undef CreateWindow
#endif

//#include <d3d12.h>
#include <dxgi1_6.h>
#include <d3dcompiler.h>
//#include <DirectXMath.h>
#include <Header/d3dx12.h>
//#include <wrl.h>
//#include <cstdint>
//#include <queue>
#include "..\Common\DeviceResources.h"
#include "Common/StepTimer.h"
#include "..\Common\DirectXHelper.h"
#include "Components/Rendering/RenderComponent.h"
//#include "Components/Rendering/UIRenderComponent.h"
//#include "Components/Rendering/UIRectComponent.h"
//#include "Components/Rendering/Mesh.h"
//#include "Components/Rendering/PostProcessing.h"
//#include "Components/Rendering/LightComponent.h"
#include "Content/ShaderStructures.h"
// STL Headers
//#include <cassert>
//#include <chrono>

#include "ConfigParser.h"

#define AA_METHOD_SMAA

using namespace Microsoft::WRL;

namespace Renderer {

	class UITextMaterial;
	class Font;
	class UIRectMaterial;
	class Texture;

	class Mesh;

	class CameraComponent;
	class LightComponent;
	class ParticleRenderComponent;

	class PostProcessing;

	class RenderEngine
	{
	public:
		RenderEngine(std::shared_ptr<ConfigParser::Config>& config);
		void Initialize(const std::shared_ptr<DX::DeviceResources>& deviceResources);
		bool IsInitialized();
		//update all render component buffers
		void Update(DX::StepTimer&, float timeWarp);
		void UpdateParticleSystems(std::unordered_set<ParticleRenderComponent*>&);
		void Render(std::unordered_set<RenderComponent*>&, std::list<LightComponent*>& ); //std::unordered_set<LightComponent*>&

		void ResizeWindow();
		void Flush();

		void InitializeMaterial(Material* mat);
		void InitializeRenderComponent(RenderComponent * renderComponent);

		void SetMainLight(const std::shared_ptr<LightComponent>& light) { m_GlobalLightComponent = light; };
		void SetMainCamera(CameraComponent* camera) { m_Camera = camera; }

		void RenderOpague(const  bool active) {m_RenderOpaque = active; }
		void RenderTransparent(const  bool active) { m_RenderTransparent = active; }
		void RenderTransparentAdditive(const  bool active) { m_RenderTransparentAdditive = active; }
		void SetTonemapMethod(const TonemapMethod tm, const bool active=true);
		void SetSMAA(const bool active) { m_SMAAActive = active; }
		void SetBloom(const bool active, UINT passes) { m_BloomActive = active; m_BloomBlurrPasses = passes; }
		void RenderUI(const bool active) { m_RenderUI = active; }
		void RenderFog(const bool active) { m_FogActive = active; }
		void SetFog(const FogParameters params, const float depthBias, const bool active = true) { m_FogActive = active; m_FogParameters = params; m_FogBlurDepthBias = depthBias; }
		~RenderEngine();
	private:

		void UpdateRenderComponents(std::unordered_set<RenderComponent*>*, ModelViewProjectionConstantBuffer* VPBuffer);
		void UpdateRenderComponents(std::vector<RenderComponent*>*, ModelViewProjectionConstantBuffer* VPBuffer, std::list<LightComponent*>& lightComponents);
		void UpdateGlobalBuffers();

		void CreateDefaultPipeline();
		void CreatePipelineState(Material* mat, DXGI_FORMAT format = DXGI_FORMAT_R16G16B16A16_FLOAT);
		void CreateCubeMap();
		void SetupAA();
		void SetupPostProcessing();
		void CreatePPPipeline(PostProcessing& pp, const UINT numParameters, const CD3DX12_ROOT_PARAMETER1* parameters, DXGI_FORMAT format= DXGI_FORMAT_R16G16B16A16_FLOAT);
		void LoadMeshToGPU(Mesh* mesh, const bool loadTriangles);
		void LoadComponentData(RenderComponent * renderComponent);
		void CreateGlobalConstantBuffers();



		void SetViewport();
		void SetCubemapViewport(XMFLOAT3 cubePosition);


		//Rendering
		void SortRenderComponents(std::unordered_set<RenderComponent*>*);
		void CullOpaque();
		void RenderOpaque();
		//void CullTransparent();
		//void RenderTransparent();
		//void RenderAdditive(); //for particles
		//post processing
		void PostProcessing(); //bloomfilter, blurrX/Y, tonemapping
		void RenderUI();
		void Present();

		inline void RenderDepthCube();

		inline void RenderComponents(std::vector<RenderComponent*>& renderComponents);
		inline void RenderComponentsDepth(std::vector<RenderComponent*>& renderComponents);
		inline void RenderFog();
		inline void RenderBloom();
		inline void RenderTonemapping();
		inline void RenderSMAA();

		//overrides dst and mid RTs
		inline void Blur(const DX::HDR_RTV src, const DX::HDR_RTV dst, const UINT passes, const DX::HDR_RTV mid = DX::HDR_RTV_BLOOM1);
		inline void BlurBilateral(const DX::HDR_RTV src, const DX::HDR_RTV normalDepth, const DX::HDR_RTV dst, const float depthBias, const UINT passes, const DX::HDR_RTV mid = DX::HDR_RTV_BLOOM1);
		//blends RT2 over RT1 using RT2s alpha: lerp(src1, src2, src2.a)
		inline void Blend(const DX::HDR_RTV src1, const DX::HDR_RTV src2, const DX::HDR_RTV dst);

		inline void RenderSDR();

		inline void DrawScreenQuad();

		//helpers
		void EnableDebugLayer();

		//const XMFLOAT4 c_clearColor = XMFLOAT4(0.1f, 0.1f, 0.15f, 1.0f);

		//window

		bool m_IsInitialized = false;

		std::shared_ptr<DX::DeviceResources> m_deviceResources;


		ComPtr<ID3D12GraphicsCommandList>			m_CommandList;
		ComPtr<ID3D12GraphicsCommandList>			m_ComputeCommandList;

		LPCWSTR m_DefaultVertexShader = L"SampleVertexShader.cso"; //pipeline config
		LPCWSTR m_DefaultPixelShader = L"SamplePixelShader.cso"; //pipeline config
		D3D12_FILL_MODE m_DefaultFillMode = D3D12_FILL_MODE_SOLID; //pipeline config
		D3D12_CULL_MODE m_DefaultCullMode = D3D12_CULL_MODE_BACK; //pipeline config
		D3D12_PRIMITIVE_TOPOLOGY_TYPE m_DefaultTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;//pipeline config: topology type
		ComPtr<ID3D12RootSignature>					m_DefaultRootSignature;
		ComPtr<ID3D12PipelineState>					m_DefaultPipelineState;
		UINT m_rtvDescriptorSize = 0;
		ComPtr<ID3D12DescriptorHeap>	m_rtvHeap;
		ComPtr<ID3D12DescriptorHeap>	m_dsvHeap;
		DX::HDR_RTV									m_MainRTV;
		DX::HDR_RTV									m_LastRTV;

		//render components
		bool										m_RenderOpaque;
		std::vector<RenderComponent*>				m_OpaqueComponents;
		bool										m_RenderTransparent;
		std::vector<RenderComponent*>				m_TransparentComponents;
		bool										m_RenderTransparentAdditive;
		std::vector<RenderComponent*>				m_TransparentAdditiveComponents;
		bool										m_RenderUI;
		std::vector<RenderComponent*>				m_UIOrbitComponents;
		std::vector<RenderComponent*>				m_UIRenderComponents;
		std::vector<RenderComponent*>				m_UIRectComponents;
		bool										m_ShadowsActive;

		//LightComponent*							m_mainLight;

		//HDR Pipeline
		ComPtr<ID3D12RootSignature>					m_HDRRootSignature;
		ComPtr<ID3D12PipelineState>					m_HDRPipelineConfig;
		const DXGI_FORMAT c_HDRRTVFormat = DXGI_FORMAT_R16G16B16A16_FLOAT;
		const DXGI_FORMAT c_HDRDSVFormat = DXGI_FORMAT_D32_FLOAT;

		//PostProcessing
		SMAARTMetrics								m_RTMetrics;
		Mesh*										m_ScreenQuad;
		bool										m_SMAAActive;
		std::unique_ptr<Renderer::PostProcessing>	m_SSAA;
		std::unique_ptr<Renderer::PostProcessing>	m_SMAAedge;
		std::unique_ptr<Renderer::PostProcessing>	m_SMAAweight;
		std::unique_ptr<Renderer::PostProcessing>	m_SMAA;
		std::unique_ptr<Renderer::Texture>			m_SMAAareatex;
		std::unique_ptr<Renderer::Texture>			m_SMAAsearchtex;

		bool										m_BloomActive;
		UINT										m_BloomBlurrPasses;
		BloomFilterParameters						m_BloomFilterParameters;
		std::unique_ptr<Renderer::PostProcessing>	m_BloomFilter;
		//BlurrParameters							m_BloomBlurrHParameters;
		std::unique_ptr<Renderer::PostProcessing>	m_BloomBlurrH;
		//BlurrParameters							m_BloomBlurrVParameters;
		std::unique_ptr<Renderer::PostProcessing>	m_BloomBlurrV;
		std::unique_ptr<Renderer::PostProcessing>	m_BlurBilateral;

		bool										m_FogActive;
		Renderer::FogParameters						m_FogParameters;
		float										m_FogBlurDepthBias;
		std::unique_ptr<Renderer::PostProcessing>	m_Fog;
		std::unique_ptr<Renderer::PostProcessing>	m_FogBlend;


		//Normal/SDR pipeline, also for final render from tonemapping
		bool										m_TonemappingActive;
		TonemapParameters							m_TonemapParameters;
		std::unique_ptr<Renderer::PostProcessing>	m_Tonemapping;
		std::unique_ptr<Renderer::PostProcessing>	m_Blit;
		//ComPtr<ID3D12RootSignature>					m_SDRRootSignature;
		//ComPtr<ID3D12PipelineState>					m_SDRPipelineConfig;
		//LPCWSTR	m_TonemapVS = L"Blit_VS.cso";
		//LPCWSTR	m_TonemapPS = L"Tonemap_PS.cso";

		//UI
		std::unique_ptr<UITextMaterial> m_TextMat;
		std::shared_ptr<UIRectMaterial> m_UIRectMat;
		Font* m_DefaultFont;


		//UINT m_DesciptorHeapSize;
		UINT m_RTVDescriptorSize;
		UINT m_CurrentBackBufferIdx;

		D3D12_RECT									m_scissorRect;
		D3D12_RECT									m_ShadowScissorRect;
		CameraComponent*							m_Camera;

		//global buffers
		Microsoft::WRL::ComPtr<ID3D12Resource>		m_CubeMapMVPBuffer;
		CubeMapViewProjectionConstantBuffer			m_CubeMapMVPBufferData;
		UINT8 *										m_MappedCubeMapMVPBuffer;
		UINT										m_CubeMapMVPBufferDescriptorOffset;

		Microsoft::WRL::ComPtr<ID3D12Resource>		m_MVPBuffer;
		ModelViewProjectionConstantBuffer			m_MVPBufferData;
		UINT8 *										m_MappedMVPBuffer;
		UINT										m_MVPBufferDescriptorOffset;

		std::shared_ptr<LightComponent>				m_GlobalLightComponent;
		Microsoft::WRL::ComPtr<ID3D12Resource>		m_GlobalLightBuffer;
		PointLightData								m_GlobalLightBufferData;
		UINT8 *										m_MappedGlobalLightBuffer;
		UINT										m_GlobalLightBufferDescriptorOffset;

		Microsoft::WRL::ComPtr<ID3D12Resource>		m_FrameConstantsBuffer;
		GloablConstantsBuffer						m_FrameConstantsBufferData;
		UINT8 *										m_MappedFrameConstantsBuffer;
		UINT										m_FrameConstantsBufferDescriptorOffset;

		//ModelViewProjectionConstantBuffer			m_ViewProjectionData;

	};
}

#endif