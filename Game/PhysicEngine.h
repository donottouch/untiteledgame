#include <direct.h>
#include <DirectXMath.h>
#include <list>
#include "Components/Physics/PhysicsComponent.h"
#include "Components/Physics/SphereCollider.h"
#include "Components/OrbitComponent.h"

using namespace std;
using namespace DirectX;

#ifndef M_PI
#define M_PI 3.14159265358979323846264338327950288
#endif

#ifndef M_MU
#define M_MU 3.986004418e14
#endif

#pragma once
class PhysicEngine
{
public:
	PhysicEngine();
	~PhysicEngine();

	/*
	Calculates if two spheres intersect. The first two parameters are the sphere centers. The last two the sphere radiuses (radiy, radiusi?).
	returns true if collision occured. False if not.
	*/
	bool checkCollision(XMVECTOR v3_1, XMVECTOR v3_2, double radius1, double radius2);
	
	// compute all collisions
	void computeCollisions();

	// returns true if the operation was succesfull
	bool addPhysicsComponent(PhysicsComponent* pc);
	// returns true if the operation was succesfull
	bool removePhysicsComponent(PhysicsComponent* pc);

private:
	std::vector<PhysicsComponent*> physicsComps;
	bool physicsCompsContainComponent(PhysicsComponent* c);


	

	double findLargestComponent(XMFLOAT3 vector);

	float dot(XMVECTOR a, XMVECTOR b);

	// computes acos; clamps value to be safe
	double safeAcos(double value);
};

