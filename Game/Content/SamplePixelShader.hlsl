// Pro-Pixel-Farbdaten an den Pixelshader �bergeben.
struct PixelShaderInput
{
	float4 pos : SV_POSITION;
	float3 normal : NORMAL;
	float2 uv : TEXCOORD0;
	float3 wPos : TEXCOORD1;
};
#include "../Shader/lib/ShaderStructures.hlsl"

ConstantBuffer<MeshParams> meshCB : register(b1);
ConstantBuffer<PointLightData> lightCB : register(b10);
ConstantBuffer<CubeMapViewProjectionConstantBuffer> cubeCB : register(b11);

// Eine Pass-Through-Funktion f�r die (interpolierten) Farbdaten.
float4 main(PixelShaderInput input) : SV_TARGET
{
	return meshCB.color;
}
