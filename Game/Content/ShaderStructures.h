﻿#pragma once

#ifndef SHADER_STRUCTURES
#define SHADER_STRUCTURES

//These structs must match the ones used in the shaders
#ifndef NUM_32BIT_CONSTANTS
#define NUM_32BIT_CONSTANTS(s) (sizeof(s) >> 2) // /4)
#endif


namespace Game
{
	struct ModelViewProjectionConstantBuffer {
		DirectX::XMFLOAT4X4 model;
		DirectX::XMFLOAT4X4 view;
		DirectX::XMFLOAT4X4 projection;
	};

	// Verwendet zum Senden von Pro-Vertex-Daten an den Vertex-Shader.
	struct VertexPositionColor {
		DirectX::XMFLOAT3 pos;
		DirectX::XMFLOAT3 color;
	};
}
namespace Renderer {
	//constants
#ifndef MAX_POINT_LIGHTS
	const uint32_t MAX_POINT_LIGHTS = 8;
#endif


	// Konstantenpuffer zum Senden von MVP-Matrizen an den Vertex-Shader verwendet.
	struct ModelViewProjectionConstantBuffer {
		//matrices
		DirectX::XMFLOAT4X4 model;
		DirectX::XMFLOAT4X4 view;
		DirectX::XMFLOAT4X4 projection;
		DirectX::XMFLOAT4X4 inv_model;
		DirectX::XMFLOAT4X4 inv_view;
		DirectX::XMFLOAT4X4 inv_projection;
		DirectX::XMFLOAT4X4 it_model;
		//lighting data
		DirectX::XMFLOAT4 lightPos[MAX_POINT_LIGHTS];
		DirectX::XMFLOAT4 lightColor[MAX_POINT_LIGHTS];
		uint32_t lightCount;
	};
	static const UINT c_alignedConstantBufferSize = (sizeof(ModelViewProjectionConstantBuffer) + 255) & ~255;
	struct CubeMapViewProjectionConstantBuffer {
		DirectX::XMFLOAT4X4 projection;
		DirectX::XMFLOAT4X4 viewPX;
		DirectX::XMFLOAT4X4 viewNX;
		DirectX::XMFLOAT4X4 viewPY;
		DirectX::XMFLOAT4X4 viewNY;
		DirectX::XMFLOAT4X4 viewPZ;
		DirectX::XMFLOAT4X4 viewNZ;
	};
	static const UINT c_alignedCBSizeCube = (sizeof(CubeMapViewProjectionConstantBuffer) + 255) & ~255;
	struct InverseModelViewProjectionConstantBuffer {
		DirectX::XMFLOAT4X4 inv_model;
		DirectX::XMFLOAT4X4 inv_view;
		DirectX::XMFLOAT4X4 inv_projection;
	};
	struct PointLightData {
		DirectX::XMFLOAT4 position;
		DirectX::XMFLOAT4 color;
		float ambient;
	};
	static const UINT c_alignedLightBufferSize = (sizeof(PointLightData) + 255) & ~255;
	struct GloablConstantsBuffer{
		DirectX::XMFLOAT4 time; //time, deltaTime, warpedTime, warpedDeltaTime;
		float remainingTime;
	};
	static const UINT c_alignedGlobalBufferSize = (sizeof(GloablConstantsBuffer) + 255) & ~255;
	//mesh prarameters
	struct MeshParams {
		DirectX::XMFLOAT4 colorDiffuse;
		DirectX::XMFLOAT4 colorEmissive;
	};
	// Verwendet zum Senden von Pro-Vertex-Daten an den Vertex-Shader.
	struct VertexData {
		DirectX::XMFLOAT3 pos;
		DirectX::XMFLOAT3 color;
	};
	static const D3D12_INPUT_ELEMENT_DESC VertexDataLayout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
	};
	//particles
	struct ParticleData {
		DirectX::XMFLOAT3 position;
		DirectX::XMFLOAT3 velocity;
		float size;
		float startTime;
		float lifeTime;
		uint32_t state;
		DirectX::XMFLOAT4 color;
	};
	struct ParticleState {
		uint32_t state;
	};
	struct ParticleStatistics {
		uint32_t active;
		uint32_t inactive;
	};
	enum PARTICLE_EMITTER_TYPE : uint32_t{
		PARTICLE_EMITTER_TYPE_ON_CIRCLE = 0u,
		PARTICLE_EMITTER_TYPE_IN_CIRCLE = 1u,
		PARTICLE_EMITTER_TYPE_ON_SPHERE = 4u,
		PARTICLE_EMITTER_TYPE_IN_SPHERE = 5u,
		PARTICLE_EMITTER_TYPE_ON_HEMISPHERE = 6u,
		PARTICLE_EMITTER_TYPE_IN_HEMISPHERE = 7u,
	};
	enum EMISSION_VEL_FLAG : uint32_t {
		EMISSION_VEL_FLAG_Z = 8u,
		EMISSION_VEL_FLAG_Y = 4u,
		EMISSION_VEL_FLAG_X = 2u,
		EMISSION_VEL_FLAG_RADIAL = 1u,
	};
	enum PARTICLE_FLAG : uint32_t {
		PARTICLE_FLAG_COLLISION = 1u,
		PARTICLE_FLAG_USE_WORLD_SPACE = 2u,
		PARTICLE_FLAG_FORCE = 4u,
		PARTICLE_FLAG_WARP_TIME = 8u,
	};
	struct ParticleSystemParameters {
		uint32_t particleBufferSize;
		uint32_t flags;
		DirectX::XMFLOAT2 emitterRadius; //min, max

		DirectX::XMFLOAT4 emissionVelLin; //x,y,z,pvar

		DirectX::XMFLOAT2 emissionVel; //min, max
		PARTICLE_EMITTER_TYPE emitterType;
		uint32_t emissionVelType;  //use z, use y, use x, radial

		DirectX::XMFLOAT2 lifetime;
		float forceFalloff;
		float dragCoefficient;

		DirectX::XMFLOAT4 color1;

		DirectX::XMFLOAT4 color2;

		DirectX::XMFLOAT3 constForce;
		float randomPos;

		DirectX::XMFLOAT2 size;
		float randomVel;
	};
	static const UINT c_alignedPSParamsBufferSize = (sizeof(ParticleSystemParameters) + 255) & ~255;
	struct ParticleUpdateParameters {
		uint32_t seed;
		uint32_t numForcePoints;
		uint32_t numImpulsePoints;
		uint32_t amountToSpawn;
	};
	static const UINT c_alignedParticleUpdateParametersBufferSize = (sizeof(ParticleUpdateParameters) + 255) & ~255;
	//SMAA
	struct SMAARTMetrics {
		float invWidth;
		float invHight;
		float width;
		float hight;
	};
	struct RTMetrics {
		DirectX::XMFLOAT4 rtmetrics;
	};
	//for text render
	//per instance (per character) data
	struct TextInstanceData {
		DirectX::XMFLOAT2 pos; //character upper-left position
		uint16_t texX, texY, texW, texH; //font glyph data
	};
	static const D3D12_INPUT_ELEMENT_DESC TextInstanceDataLayout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32_FLOAT     , 0, 0, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R16G16B16A16_UINT, 0, 8, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 }
	};
	//global shader data
	struct TextVertexParams {
		DirectX::XMFLOAT2 anchorUV;
		DirectX::XMINT2 offsetPix;
		DirectX::XMFLOAT2 invTexDim;
		float textSize;
		float textScale;
		float dstBorder;
		UINT srcBorder;
	};
	struct TextPixelParams {
		DirectX::XMFLOAT4 color;
		float heightRange;
	};
	struct RectVertexParams {
		DirectX::XMFLOAT2 anchorUV;
		DirectX::XMINT2 offsetPix;
		DirectX::XMINT2 sizePix;
		float border;
	};
	struct RectPixelParams {
		DirectX::XMFLOAT4 color;
		DirectX::XMFLOAT2 anchorUV;
		DirectX::XMINT2 offsetPix;
		DirectX::XMINT2 sizePix;
		float lineWidth;
		float borderWidth;
	};

	struct OrbitalElements {
		float e; // eccentricity
		float a; // semimajor axis
		float b; // semiminor axis

		//(vertical tilt of the ellipse with respect to the reference plane, measured at the ascending node)
		//float i; // inclination; THIS SHOULD ALWAYS STAY 0 -> everything stays in 2D
		//float Q; // Longitude of the ascending node; horizontally orients the ascending node of the ellipse
		float w; // Argument of periapsis defines the orientation of the ellipse in the orbital plane
		float v; // true anomaly; orbit angle; describes position in orbit
		// the true anomaly is measured from periapsis
		float ra; //distance to central body at apoapsis
		float rp; //distance to central body at periapsis
	};
	struct OrbitalOffsets {
		DirectX::XMFLOAT4 v;
	};

	struct LineStyle {
		DirectX::XMFLOAT4 color;
		float width;
	};
	struct BloomFilterParameters {
		float threshold;
	};
	struct BlurParameters {
		UINT direction;
		float depthBias;
	};
	struct FogParameters {
		float height;
		float maxDensity;
		float stepSize;
		UINT maxSteps;
		float blackHoleSize;
	};

	//https://github.com/jpvanoosten/LearningDirectX12/blob/master/Tutorial4/src/Tutorial4.cpp
	enum TonemapMethod : uint32_t
	{
		TM_Linear,
		TM_Reinhard,
		TM_ReinhardSq,
		TM_ACESFilmic,
	};

	struct TonemapParameters
	{
		TonemapParameters()
			: TonemapMethod(TM_Linear)
			, Exposure(0.0f)
			, MaxLuminance(1.0f)
			, K(1.0f)
			, A(0.22f)
			, B(0.3f)
			, C(0.1f)
			, D(0.2f)
			, E(0.01f)
			, F(0.3f)
			, LinearWhite(11.2f)
			, Gamma(2.2f)
			, BloomStrength(1.0f)
		{}

		// The method to use to perform tonemapping.
		TonemapMethod TonemapMethod;
		// Exposure should be expressed as a relative exposure value (-2, -1, 0, +1, +2 )
		float Exposure;

		// The maximum luminance to use for linear tonemapping.
		float MaxLuminance;

		// Reinhard constant. Generally this is 1.0.
		float K;

		// ACES Filmic parameters
		// See: https://www.slideshare.net/ozlael/hable-john-uncharted2-hdr-lighting/142
		float A; // Shoulder strength
		float B; // Linear strength
		float C; // Linear angle
		float D; // Toe strength
		float E; // Toe Numerator
		float F; // Toe denominator
		// Note E/F = Toe angle.
		float LinearWhite;
		float Gamma;
		float BloomStrength;
	};
}

#endif