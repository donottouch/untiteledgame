
#include "../Shader/lib/ShaderStructures.hlsl"

ConstantBuffer<ModelViewProjectionConstantBuffer> objectCB : register(b0);
ConstantBuffer<MeshParams> meshCB : register(b1);

// Pro-Vertex-Daten als Eingabe f�r den Vertex-Shader verwendet.
struct VertexShaderInput
{
	float3 pos : POSITION;
	float3 color : COLOR0;
};

// Pro-Pixel-Farbdaten an den Pixelshader �bergeben.
struct PixelShaderInput
{
	float4 pos : SV_POSITION;
	float3 normal : NORMAL;
	float2 uv : TEXCOORD0;
	float3 wPos : TEXCOORD1;
};

// Einfacher Shader f�r Vertex-Bearbeitung auf der GPU.
PixelShaderInput main(VertexShaderInput input)
{
	PixelShaderInput output;
	float4 pos = float4(input.pos, 1.0f);

	output.uv = pos.xy; //TODO

	// Die Position des Scheitelpunkts in den projektierten Raum transformieren.
	pos = mul(pos, objectCB.model);
	output.wPos = pos.xyz;

	pos = mul(pos, objectCB.view);
	pos = mul(pos, objectCB.projection);
	output.pos = pos;

	// Normal is set in GS
	output.normal = (float3)0;

	return output;
}
