
#include"pch.h"
#include "ConfigParser.h"

int ConfigParser::Parse(const std::string fileName, ConfigParser::Config* config){
	std::string line, key, value;
	std::ifstream configFile(fileName.c_str(), std::ifstream::in);
	
	if(configFile.is_open()){
		while (getline(configFile, line)){ //split for lines
			
			std::istringstream keyStream(line);
			std::getline(keyStream, key, ' ');
			
			const char type = key[0];
			
			switch(type){
			case '[': //header, not used yet
				break;
			case ';': //comment
				break;
			case 'b': //bool property
				std::getline(keyStream, value, ' ');
				config->AddBoolProperty(key, value=="true"? true : false);
				break;
			case 'i': //int property
				std::getline(keyStream, value, ' ');
				config->AddIntProperty(key, atoi(value.c_str()));
				break;
			case 'f': //float property
				std::getline(keyStream, value, ' ');
				config->AddFloatProperty(key, atof(value.c_str()));
				break;
			case 'c': //float property
				DirectX::XMFLOAT4 color;
				std::getline(keyStream, value, ' ');
				color.x = std::max(0.0f, static_cast<float>(atof(value.c_str())));
				std::getline(keyStream, value, ' ');
				color.y = std::max(0.0f, static_cast<float>(atof(value.c_str())));
				std::getline(keyStream, value, ' ');
				color.z = std::max(0.0f, static_cast<float>(atof(value.c_str())));
				std::getline(keyStream, value, ' ');
				color.w = std::max(0.0f, std::min(1.0f, static_cast<float>(atof(value.c_str()))));
				config->AddColorProperty(key, color);
				break;
			case 's': //string property
				std::getline(keyStream, value, ' ');
				config->AddStringProperty(key, value);
				break;
			default:
				break;
			}
			
		}
		return 1;
	}
	return 0;
}

bool ConfigParser::Config::GetBool(const std::string key, const bool default) const {
	try {
		return m_PropertiesBool.at(key);
	}catch(std::out_of_range){
		return default;
	}
}

int ConfigParser::Config::GetInt(const std::string key, const int default) const {
	try {
		return m_PropertiesInt.at(key);
	}
	catch (std::out_of_range) {
		return default;
	}
}

float ConfigParser::Config::GetFloat(const std::string key, const float default) const {
	try {
		return m_PropertiesFloat.at(key);
	}
	catch (std::out_of_range) {
		return default;
	}
}

DirectX::XMFLOAT4 ConfigParser::Config::GetColor(const std::string key, const DirectX::XMFLOAT4 default) const {
	try {
		return m_PropertiesColor.at(key);
	}
	catch (std::out_of_range) {
		return default;
	}
}

std::string ConfigParser::Config::GetString(const std::string key) const {
	try {
		return m_PropertiesString.at(key);
	}
	catch (std::out_of_range) {
		return "";
	}
}

int ConfigParser::Config::GetBool(const std::string key, bool * value) const {
	try {
		*value = m_PropertiesBool.at(key);
	}
	catch (std::out_of_range) {
		return 0;
	}
	return 1;
}

int ConfigParser::Config::GetInt(const std::string key, int * value) const {
	try {
		*value = m_PropertiesInt.at(key);
	}
	catch (std::out_of_range) {
		return 0;
	}
	return 1;
}

int ConfigParser::Config::GetFloat(const std::string key, float * value) const {
	try {
		*value = m_PropertiesFloat.at(key);
	}
	catch (std::out_of_range) {
		return 0;
	}
	return 1;
}

int ConfigParser::Config::GetColor(const std::string key, DirectX::XMFLOAT4 * value) const {
	try {
		DirectX::XMFLOAT4 color = m_PropertiesColor.at(key);
		value->x = color.x;
		value->y = color.y;
		value->z = color.z;
		value->w = color.w;
	}
	catch (std::out_of_range) {
		return 0;
	}
	return 1;
}

void ConfigParser::Config::AddBoolProperty(const std::string key, const bool value) {
	m_PropertiesBool[key] = value;
}

void ConfigParser::Config::AddIntProperty(const std::string key, const int value) {
	m_PropertiesInt[key] = value;
}

void ConfigParser::Config::AddFloatProperty(const std::string key, const float value) {
	m_PropertiesFloat[key] = value;
}

void ConfigParser::Config::AddColorProperty(const std::string key, const DirectX::XMFLOAT4 value) {
	m_PropertiesColor[key] = value;
}

void ConfigParser::Config::AddStringProperty(const std::string key, const std::string value) {
	m_PropertiesString[key] = value;
}
