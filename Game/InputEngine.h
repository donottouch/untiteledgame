#pragma once
#include "InputWrapper.h"
#include <queue>
#include "Components/UI/UIComponent.h"

namespace InputEngine {

	class InputEngine
	{
	public:
		InputEngine(Windows::UI::Core::CoreWindow ^ window);
		~InputEngine();

		InputWrapper^ inputWrapper;
		void tick(list<UIComponent*>* ui);

		void setCoordinateScale(const float scale) { m_CoordianteScale = 0.1> scale? 0.1:scale; }

		void handleResizeWindow(list<UIComponent*>* ui);
	private:
		float m_CoordianteScale = 1.0;
	};

	
}