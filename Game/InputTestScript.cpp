#include "pch.h"
#include "InputTestScript.h"

using namespace Windows::System;


bool s = false;
float range = 1.0f;

InputTestScript::InputTestScript(Game::GameMain * gameMain)
{
	this->m_gMain = gameMain;
}

InputTestScript::~InputTestScript()
{
}

void InputTestScript::unS() {
	s = !s;
}

void InputTestScript::OnUpdate(float dT)
{
	/*
	if (m_gMain->getInput()->mouseIsDown()) {
		s = !s;
	}
	*/
	if (s) {
		m_master->getTransform()->setLocalPosition(XMFLOAT3(0, 0, range));
	}
	else {

		m_master->getTransform()->setLocalPosition(XMFLOAT3(0, 0, 0));
	}

	switch (m_gMain->getInput()->getKey(VirtualKey::Add))
	{
	case 2:
	case 3:
		range += 0.1f; //TODO: we have to add deltatime to OnUpdate;
		m_master->getTransform()->setLocalPosition(XMFLOAT3(0, 0, range));
		break;
	default:
		break;
	}

	switch (m_gMain->getInput()->getKey(VirtualKey::Subtract))
	{
	case 2:
	case 3:
		range -= 0.1f; //TODO: we have to add deltatime to OnUpdate;
		m_master->getTransform()->setLocalPosition(XMFLOAT3(0, 0, range));
		break;
	default:
		break;
	}
}

void InputTestScript::OnStart()
{
}

void InputTestScript::OnDestroy()
{
}

void InputTestScript::OnCollision(GameObject::CollisionObject collision)
{
}
