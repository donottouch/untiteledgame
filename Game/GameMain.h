﻿#pragma once

#include "Common\StepTimer.h"
#include "Common\DeviceResources.h"
#include "Content\Sample3DSceneRenderer.h"
#include "RenderEngine.h"
#include "PhysicEngine.h"
#include "GameObject.h"
#include "Components/Transform.h"
#include "Components/Rendering/RenderComponent.h"
#include "Components/Rendering/LightComponent.h"
#include "Components/Physics/PhysicsComponent.h"
#include "Components/Rendering/MeshRenderComponent.h"
#include "Components/Rendering/OrbitRenderComponent.h"
#include "Components/Rendering/UIRenderComponent.h"
#include "Components/Rendering/Mesh.h"
#include "Components/Rendering/Material.h"
#include "Components/Rendering/OrbitMaterial.h"
#include "Components/Rendering/ParticleSystem.h"
#include "InputEngine.h"

#include "ConfigParser.h"

#include "Components/UI/UIButtonComponent.h"

#include "Components/OrbitComponent.h"
#include "Components/Behaviours/OrbitManager.h"

// Rendert Direct3D-Inhalte auf dem Bildschirm.
namespace Game
{
	class GameMain
	{
	public:
		GameMain();
		void CreateRenderers(const std::shared_ptr<DX::DeviceResources>& deviceResources);
		void Update();
		DX::StepTimer getTimer();
		bool Render();

		void OnWindowSizeChanged();
		void OnSuspending();
		void OnResuming();
		void OnDeviceRemoved();

		// returns true if the component was edded or already contained
		bool addComponent(Component* c);
		// returns true if the component was removed or not contained in the first place
		bool removeComponent(Component* c);

		InputEngine::InputWrapper^ getInput() { return m_InputEngine->inputWrapper; };

		PhysicEngine* getPhysicsEngine();
		ConfigParser::Config* getConfig() const { return m_Config.get(); }

		void flagForDeletion(GameObject* go);

		// logs strings to console
		void logToConsole(string input);

		GameObject* getSceneRoot() { return m_SceneRoot.get(); }

	private:
		std::shared_ptr<ConfigParser::Config> m_Config;
		void InitConfig();

		std::unique_ptr<Renderer::RenderEngine> m_Renderer;
		std::unique_ptr<PhysicEngine> m_Physics;
		std::unique_ptr<InputEngine::InputEngine> m_InputEngine;

		std::unique_ptr<GameObject> m_SceneRoot;
		//TODO build proper scene graph

		bool RegisterComponent(Renderer::RenderComponent*);
		std::list<Renderer::RenderComponent*> m_RCInitPending;
		std::unordered_set<Renderer::RenderComponent*> m_RenderComponents;
		std::unordered_set<Renderer::ParticleRenderComponent*> m_ParticleComponents;
		// TODO: Durch Ihre eigenen Inhaltsrenderer ersetzen.
		std::unique_ptr<Sample3DSceneRenderer> m_sceneRenderer;

		// Schleifentimer wird gerendert.
		DX::StepTimer m_timer;

		// List or renderComponets/ physicsComponents
		list<Renderer::RenderComponent*> renderComps;
		list<Renderer::LightComponent*> lightComps;
		list<PhysicsComponent*> physicsComps;
		list<UIComponent*> uiComps;
		list<Component*> otherComps;

		// list of objects for deletion
		list<GameObject*> LflaggedForDeletion;

		// deletes all gameObjects flagged for deletion
		void deleteGameObjects();

		// checks if the component is in the passed list
		template <class T>
		bool listContainsComponent(Component* c, list<T>* compList);

		// creates all GameObjects/ things for the test scene
		void initTestScene();

		template <class T>
		bool componentInstanceOf(Component* c) {
			return (dynamic_cast<T>(c) != nullptr);
		}

		GameObject* m_CameraPivot;

		//meshes
		Renderer::Mesh* m_MeshPlane;
		Renderer::Mesh* m_MeshCube;
		Renderer::Mesh* m_MeshSphere;
		Renderer::Mesh* m_AsteroidMesh;
		Renderer::Mesh* m_BaseMesh;

		//materials
		Renderer::Material* m_MatWireframe;
		Renderer::Material* m_MatFlat;
		Renderer::Material* m_MatOccluder;
		Renderer::Material* m_MatOrbit;
		Renderer::Material* m_MatParticle;

		//Particle system types (similar to material)
		std::shared_ptr<Renderer::ParticleSystem> m_ParticleSystemType;
	};
}