#pragma once
#include <map>
#include <functional> 
#include <set>
#include <queue>
#include "GameObject.h"

using namespace DirectX;
using namespace Windows::UI::Core;
using namespace Windows::UI::Input;


namespace InputEngine {

	public enum class KeyState : int
	{
		None = 0,
		Released = 1,
		Hold = 2,
		Pushed = 3
	};

	//public delegate void CallbackFunc(PointerEventArgs^);


	ref class InputWrapper
	{
	internal:
		InputWrapper(Windows::UI::Core::CoreWindow ^ window);


	public:

		//Event Handler Pointer
		void OnPointerPressed(Windows::UI::Core::CoreWindow ^ sender, Windows::UI::Core::PointerEventArgs^ args);
		void OnPointerMoved(Windows::UI::Core::CoreWindow ^ sender, Windows::UI::Core::PointerEventArgs^ args);
		void OnPointerReleased(Windows::UI::Core::CoreWindow ^ sender, Windows::UI::Core::PointerEventArgs^ args);
		void OnPointerExited(Windows::UI::Core::CoreWindow ^ sender, Windows::UI::Core::PointerEventArgs^ args);

		//Event Handler Mouse
		void OnWheelChanged(Windows::UI::Core::CoreWindow ^ sender, Windows::UI::Core::PointerEventArgs^ args);
		//void OnMouseMoved(Windows::Devices::Input::MouseDevice^ device, Windows::Devices::Input::MouseEventArgs^ args);

		//Event Handler KeyBoard
		void OnKeyDown(Windows::UI::Core::CoreWindow ^ sender, Windows::UI::Core::KeyEventArgs^ args);
		void OnKeyUp(Windows::UI::Core::CoreWindow ^ sender, Windows::UI::Core::KeyEventArgs^ args);

		//Gamepad Handler
		//void OnGamePadAdded(Windows::Gaming::Input::Gamepad^ gamepad);
		//void OnGamePadRemoved(Windows::Gaming::Input::Gamepad^ gamepad);

		void reset();

		bool mouseIsDown() { return mouseClicked; }
		float mouseDeltaX() { return mouseDeltaFrame.x; }
		float mouseDeltaY() { return mouseDeltaFrame.y; }
		int mouseWheelDelta() { return wheelDeltaFrame; };

		int getKey(Windows::System::VirtualKey k) { return (int)keys[k]; };

		size_t getClicksLenght() { return clicks.size(); };
		Windows::UI::Core::PointerEventArgs^ getClick(int index) { return  clicks[index]; };

	private:

		void setKeyDown(Windows::System::VirtualKey key);
		void setKeyUp(Windows::System::VirtualKey key);

		XMFLOAT2 mouseDeltaFrame;
		XMFLOAT2 mousePosition;
		bool mouseClicked;
		int wheelDeltaFrame;

		XMFLOAT2 mouseDeltaLast;
		XMFLOAT2 tmpMouseDeltaFrame;
		XMFLOAT2 tmpMousePosition;
		bool tmpMouseClicked;
		int tmpWheelDelta;

		std::map<Windows::System::VirtualKey, KeyState> keys;
		std::map<Windows::System::VirtualKey, KeyState> tmpKeys;

		std::vector<Windows::UI::Core::PointerEventArgs^> clicks;
		std::vector<Windows::UI::Core::PointerEventArgs^> tmpClicks;

	};

}