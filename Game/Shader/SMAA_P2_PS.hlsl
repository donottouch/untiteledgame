//Pass 2: Blend Weight Calculation
//Pixel Shader

Texture2D<float4> edgesTex : register (t0);
Texture2D<float4> areaTex : register (t1);
Texture2D<float4> searchTex : register (t2);
#define RT_MAIN edgesTex

#include "SMAA_INI.hlsl"
#include "SMAA.hlsl"

float4 main(float4 position : SV_POSITION,
			float2 texcoord : TEXCOORD0,
			float2 pixcoord : TEXCOORD1,
			float4 offset[3] : TEXCOORD2) : SV_Target0{
	return SMAABlendingWeightCalculationPS(texcoord, pixcoord, offset, edgesTex, areaTex, searchTex, (float4)0);
}
