
#include "../Shader/lib/ShaderStructures.hlsl"

ConstantBuffer<ModelViewProjectionConstantBuffer> objectCB : register(b0);
ConstantBuffer<MeshParams> meshCB : register(b1);

SimplePSInput main(SimpleVSInput IN) {
	SimplePSInput OUT = (SimplePSInput)0;

	float4 pos = float4(IN.position, 1.0f);

	OUT.uv = pos.xy; //TODO

	// Die Position des Scheitelpunkts in den projektierten Raum transformieren.
	pos = mul(pos, objectCB.model);
	OUT.positionW = pos;

	pos = mul(pos, objectCB.view);
	OUT.positionV = pos;

	pos = mul(pos, objectCB.projection);
	OUT.positionP = pos;
	OUT.position = pos;

	return OUT;
}