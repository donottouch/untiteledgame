#include "lib/ShaderStructures.hlsl"
#include "lib/ShaderLib.hlsl"
#include "lib/GPUParticles.hlsl"


void UpdateParticle(const uint idx) {
	if (!particleDataBuffer[idx].state) return;
	//check lifetime
	if (particleDataBuffer[idx].lifeTime > 0) {
		if (particleDataBuffer[idx].startTime + particleDataBuffer[idx].lifeTime < getTime()) {
			particleDataBuffer[idx].state = 0;
			return;
		}
	}

	float3 position = particleDataBuffer[idx].position;
	float3 velocity = particleDataBuffer[idx].velocity;

	//check position outside black hole
	if (HAS_PARTICLE_FLAG(PARTICLE_FLAG_COLLISION)) {
		if (dot(position, position) < 0.7*0.7) {
			particleDataBuffer[idx].state = 0;
			return;
		}
	}


	//FORCES
	float3 force = PSparamCB.constForce;

	//pointForce spinn test
#define ROTATING_POINT_FORCE
	float4 angVel = float4(0, 1, 0, -14);

	/*//pointForces
	for (uint i = 0; i < numForcePoints; i++) {
		float3 dir = position - forcePoints[i].xyz;
		float sqlen = dot(dir, dir);
		if (sqlen > 0) {
			float pointForce = pow(sqrt(sqlen) + 1, forceFalloff) * forcePoints[i].w;
			force += normalize(dir) * pointForce;
		}
#ifdef ROTATING_POINT_FORCE
		float3 proj = projectOnPlane(dir, angVel.xyz);
		if (dot(proj, proj) > 1) {
			proj *= 1.0f / (dot(proj, proj) + 1); //stabilize small distances
			float3 rotForce = cross(angVel.xyz * angVel.w, proj);
			force += rotForce;

		}
#endif
	}*/

	//hardcoded black hole force
	const float4 blackHoleForcePoint = float4(0, 0, 0, -100);
	float3 dir = position - blackHoleForcePoint.xyz;
	float sqlen = dot(dir, dir);
	if (sqlen > 0) {
		float pointForce = 1.0f / pow(sqrt(sqlen) + 1, PSparamCB.forceFalloff) * blackHoleForcePoint.w;
		force += normalize(dir) * pointForce;
	}

	float3 proj = projectOnPlane(dir, angVel.xyz);
	float coaltitude = acos(dot(angVel.xyz, dir) / (length(dir)));
	float frameDragStrength = sin(coaltitude);
	frameDragStrength *= frameDragStrength;
	float angularMomentum = frameDragStrength * angVel.w / (sqlen);
	float sina, cosa;
	sincos(angularMomentum* getDeltaTime(), sina, cosa);
	float3x3 m = float3x3(cosa, 0, sina, 0, 1, 0, -sina, 0, cosa);


	if (dot(proj, proj) > 1) {
		proj *= 1.0f / (dot(proj, proj) + 1); //stabilize small distances
		float3 rotForce = cross(angVel.xyz * angVel.w, proj);
		//force += rotForce;

	}

	/*//impulses
	float3 impulse;
	for (uint i = 0; i < numImpulsePoints; i++) {
		float3 dir = position - impulsePoints[i].xyz;
		float sqlen = dot(dir, dir);
		if (sqlen > 0) {
			float pointImpulse = pow(sqrt(sqlen) + 1, forceFalloff) * impulsePoints[i].w;
			impulse += normalize(dir) * pointImpulse;
		}
	}//*/


	//update vel with force
	//drag https://www.grc.nasa.gov/www/k-12/airplane/falling.html
	float drag = PSparamCB.dragCoefficient * dot(velocity, velocity);
	if (drag > 0) {
		velocity -= drag * normalize(velocity) * getDeltaTime();
	}
	if(HAS_PARTICLE_FLAG(PARTICLE_FLAG_FORCE))
		velocity += force * getDeltaTime();
	//velocity += impulse;

	//update pos with vel (use better scheme?)
	position += velocity * getDeltaTime();

	position = mul(position, m);
	velocity = mul(velocity, m);

	/*//collision
#define COLLDET_CONTINOUS //just to disable collision
	//TODO SSC
	float3 planePosition = float3(0, -5, 0);
	float3 planeNormal = float3(0, 1, 0);
	float particleRadius = 0.02;
	float particleBounce = 0.5;
#ifdef COLLDET_CONTINOUS
	//needs last position
#else
	//https://mathinsight.org/distance_point_plane
	float3 v = position - planePosition;
	float d = abs(dot(planeNormal, v));
	if (d <= particleRadius) { //collision
		//float3 normalVel = dot(velocity, planeNormal)*planeNormal;
		//velocity -= normalVel * (1 + particleBounce);
		velocity = reflectLossy(velocity, planeNormal, 1 - particleBounce);
	}
#endif
	//*/

	particleDataBuffer[idx].position = position;
	particleDataBuffer[idx].velocity = velocity;
}


[numthreads(WARP_SIZE, 1, 1)]
void main(uint3 id : SV_DispatchThreadID) {
	if (id.x >= PSparamCB.particleBufferSize) return;
	const uint idx = particleStateBuffer[id.x];
	UpdateParticle(idx);
}