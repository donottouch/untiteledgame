#include "lib/ShaderLib.hlsl"

//source: https://github.com/Microsoft/DirectX-Graphics-Samples/blob/master/MiniEngine/Core/Shaders/TextAntialiasPS.hlsl

struct TextPixelParams {
	float4 color;
	float heightRange;
};

Texture2D<float> SignedDistanceFieldTex : register(t0);
SamplerState LinearSampler : register(s0);
ConstantBuffer<TextPixelParams> pCB : register(b1);

struct PixelShaderInput {
	float4 pos : SV_POSITION;
	float2 uv : TEXCOORD0;
};

float GetAlpha(float2 uv) {
	//float a = saturate(SignedDistanceFieldTex.Sample(LinearSampler, uv) *pCB.heightRange + 0.5);
	float a = SignedDistanceFieldTex.Sample(LinearSampler, uv) *pCB.heightRange;
	return CelStep(a, 0);
}

float4 main(PixelShaderInput Input) : SV_Target
{
	float alpha = GetAlpha(Input.uv);
	return float4(pCB.color.rgb, pCB.color.a * (alpha));
}