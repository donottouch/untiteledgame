#include "lib/ShaderStructures.hlsl"

cbuffer ModelViewProjectionConstantBuffer : register(b0) {
	matrix model;
	matrix view;
	matrix projection;
};

SimplePSInput main(SimpleVSInput IN) {
	SimplePSInput OUT = (SimplePSInput)0;

	float4 pos = float4(IN.position, 1.0f);

	OUT.uv = pos.xy; //TODO

	// Die Position des Scheitelpunkts in den projektierten Raum transformieren.
	pos = mul(pos, model);
	OUT.positionW = pos;

	pos = mul(pos, view);
	OUT.positionV = pos;

	pos = mul(pos, projection);
	OUT.positionP = pos;
	OUT.position = pos;

	return OUT;
}