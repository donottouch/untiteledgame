// Use a real macro here for maximum performance!

struct SMAARTMetrics {
	float4 rtMetrics;
};
ConstantBuffer<SMAARTMetrics> rtMetricsCB : register(b0);
#define SMAA_RT_METRICS rtMetricsCB.rtMetrics

#if !defined(SMAA_RT_METRICS) // This is just for compilation-time syntax checking.
#if !defined(RT_MAIN)
#define SMAA_RT_METRICS float4(1.0 / 1280.0, 1.0 / 720.0, 1280.0, 720.0)
#else
#define SMAA_RT_METRICS rt_metrics()
#endif
#endif

#define SMAA_PRESET_ULTRA

#if defined(RT_MAIN)
float4 rt_metrics(){
	uint width, hight, level;
	RT_MAIN.GetDimensions(0, width, hight, level);
	return float4(1.0/width,1.0/hight,width, hight);
}
#endif