
#include "lib/ShaderStructures.hlsl"

ConstantBuffer<RectPixelParams> pCB : register(b1);
ConstantBuffer<RTMetrics> rtmCB : register(b2);
#define RT_METRICS rtmCB.rtmetrics
#include "lib/ShaderLib.hlsl"

//Texture2D<float> SignedDistanceFieldTex : register(t0);
//SamplerState LinearSampler : register(s0);

float4 main(BlitPSInput IN) : SV_Target
{
	const float2 posPix = UVToPixRTM(pCB.anchorUV).xy + pCB.offsetPix;
	const float2 relPos0 = IN.position.xy - (posPix - pCB.lineWidthPix*0.5);
	const float2 relPos1 = IN.position.xy - (posPix + pCB.lineWidthPix*0.5);
	const uint inInnerRect = isInRect(relPos1, pCB.sizePix - pCB.lineWidthPix);
	const uint inBorder = isInRect(relPos0, pCB.sizePix + pCB.lineWidthPix) && !inInnerRect;
	return pCB.color * inBorder + float4(0,0,0,0.84) * inInnerRect;
}