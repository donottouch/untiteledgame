#include "lib/ShaderStructures.hlsl"
#include "lib/ShaderLib.hlsl"

ColorNormalDepthPSOutput main(ParticlePSInput IN) : SV_TARGET{
	ColorNormalDepthPSOutput OUT = (ColorNormalDepthPSOutput)0;

	//float4 albedo = tex2D(_MainTex, IN.uv);
	float2 clipUV = UVToClip(IN.uv);
	float intensity = dot(clipUV, clipUV) <= 1;//1 - saturate(dot(clipUV, clipUV));

	//return float4 (1, 0, 1, 1);
	OUT.color = float4(IN.color.rgb, IN.color.a*intensity);
	OUT.normalDepth = float4(1,1,1, IN.positionP.z / IN.positionP.w);
	DiscardTransparent(OUT.color);
	return OUT;

}