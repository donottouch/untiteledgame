#include "lib/ShaderStructures.hlsl"
//#include "lib/ShaderLib.hlsl"


float main(DepthCubePSInput IN) : SV_TARGET{

	return saturate(IN.positionP.z / IN.positionP.w);
}