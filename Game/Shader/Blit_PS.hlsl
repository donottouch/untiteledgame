
#include "lib/ShaderStructures.hlsl"
#include "lib/ShaderLib.hlsl"

Texture2D<float4> HDRTexture : register(t0);

float4 main( BlitPSInput IN ) : SV_Target0{
	int3 texCoord = int3((int2)IN.position.xy,0);
	float4 HDR = HDRTexture.Load(texCoord);
	//return float4(HDR.r, HDR.g, HDR.a, 1);
	//return HDR.r<1? HDR:float4(0,0,0,1);
	return HDR;
}