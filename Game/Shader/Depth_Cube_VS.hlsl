#include "lib/ShaderStructures.hlsl"

ConstantBuffer<ModelViewProjectionConstantBuffer> objectCB : register(b0);

DepthCubeGSInput main(SimpleVSInput IN) { //, uint instID : SV_InstanceID
	DepthCubeGSInput OUT = (DepthCubeGSInput)0;

	float4 pos = float4(IN.position, 1.0f);

	pos = mul(pos, objectCB.model);
	OUT.positionW = pos;
	OUT.position = pos;

	return OUT;
}