//https://github.com/Microsoft/DirectX-Graphics-Samples/blob/master/MiniEngine/Core/Shaders/ScreenQuadVS.hlsl
#include "lib/ShaderStructures.hlsl"
#include "lib/ShaderLib.hlsl"

//Draw 4 vertices without vertex buffer
BlitPSInput main(uint vertID : SV_VertexID){
	BlitPSInput OUT = (BlitPSInput)0;
	//OUT.uv = float2(vertID & 1, (vertID >> 1) & 1);
	//OUT.position = float4(lerp(float2(-1, 1), float2(1, -1), OUT.uv), 0, 1);
	makeScreenQuad(vertID, OUT.position, OUT.uv);
	OUT.positionP = OUT.position;
	//float4 position = float4(IN.pos, 1);
	
	return OUT;
}