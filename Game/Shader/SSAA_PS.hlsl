Texture2D<float4> HDRTexture : register(t0);

float4 main(float4 Position : SV_Position) : SV_Target0
{
	uint width, height, superSample = 2;
	HDRTexture.GetDimensions(width, height);
	float3 HDR = (float3)0;
	int3 texCoord = int3((int2)Position.xy,0);
	for (uint x = 0; x < superSample;++x) {
		for (uint y = 0; y < superSample;++y) {
			HDR += HDRTexture.Load(texCoord * superSample  + int3(x,y,0)).rgb;
		}
	}
	HDR /= superSample * superSample;
	//return float4(texCoord/(float)width * 2,1);
	return float4(HDR, 1);
}