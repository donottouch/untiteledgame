#include "lib/ShaderStructures.hlsl"


ConstantBuffer<ModelViewProjectionConstantBuffer> mvpCB : register(b0);

[maxvertexcount(4)]
void main(point ParticleGSInput IN[1], inout TriangleStream<ParticlePSInput> outStream) { //, uint pid : SV_PrimitiveID
	const float3 up = float3(0, 1, 0); //float3(1, 0, 0);
	const float3 right = float3(1, 0, 0); //float3(0, 1, 0);
	const float3 fwd = float3(0, 0, 1); //float3(0, 1, 0);
	const float _ParticleSize = IN[0].size;

	ParticlePSInput o = (ParticlePSInput)0;
	o.color = IN[0].color;

	float3 position = IN[0].position.xyz + up * _ParticleSize - right * _ParticleSize;
	o.position = mul(float4(position, 1), mvpCB.projection);
	o.positionP = o.position;
	o.uv = float2(0, 0);
	outStream.Append(o);

	position = IN[0].position.xyz + up * _ParticleSize + right * _ParticleSize;
	o.position = mul(float4(position, 1), mvpCB.projection);
	o.positionP = o.position;
	o.uv = float2(1, 0);
	outStream.Append(o);

	position = IN[0].position.xyz - up * _ParticleSize - right * _ParticleSize;
	o.position = mul(float4(position, 1), mvpCB.projection);
	o.positionP = o.position;
	o.uv = float2(0, 1);
	outStream.Append(o);

	position = IN[0].position.xyz - up * _ParticleSize + right * _ParticleSize;
	o.position = mul(float4(position, 1), mvpCB.projection);
	o.positionP = o.position;
	o.uv = float2(1, 1);
	outStream.Append(o);
	outStream.RestartStrip();
}