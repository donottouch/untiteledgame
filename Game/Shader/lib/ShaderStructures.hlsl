//structures for constant buffers, have to be the same as in Content/ShaderStructures.h

#if !defined(MAX_POINT_LIGHTS)
#define MAX_POINT_LIGHTS 8
#endif

// CONSTANT BUFFER STRUCTURES
struct ModelViewProjectionConstantBuffer {
	matrix model;
	matrix view;
	matrix projection;
	matrix inv_model;
	matrix inv_view;
	matrix inv_projection;
	matrix it_model;
	//local point light data
	float4 lightPos[MAX_POINT_LIGHTS];
	float4 lightColor[MAX_POINT_LIGHTS];
	uint lightCount;
};
struct CubeMapViewProjectionConstantBuffer {
	matrix projection;
	matrix view[6];
	//matrix viewPX;
	//matrix viewNX;
	//matrix viewPY;
	//matrix viewNY;
	//matrix viewPZ;
	//matrix viewNZ;
};
struct InverseModelViewProjectionConstantBuffer {
	matrix inv_model;
	matrix inv_view;
	matrix inv_projection;
};

struct GloablConstantsBuffer{
	float4 time; //time, deltaTime, warpedTime, warpedDeltaTime;
	float remainingTime;
};

struct RTMetrics {
	float4 rtmetrics;
	float near;
	float far;
//	float invWidth;
//	float invHight;
//	float width;
//	float hight;
};

struct MeshParams {
	float4 color;
	float4 colorEmissive;
};
struct VertexData {
	float3 pos;
	float3 color;
};
//particles
struct ParticleData {
	float3 position;
	float3 velocity;
	float size;
	float startTime;
	float lifeTime;
	uint state;
	float4 color;
};
struct ParticleStatus {
	uint status;
};
struct ParticleStatistics {
	uint active;
	uint inactive;
};
struct ParticleSpawnParameters {
	uint spawnType;
};
struct ParticleSystemParameters {
	uint particleBufferSize;
	uint flags;
	float2 emitterRadius;

	float4 emissionVelLin; //x,y,z,pvar

	float2 emissionVel; //min, max
	uint emitterType;
	uint emissionVelType;  //use z, use y, use x, radial

	float2 lifetime;
	float forceFalloff;
	float dragCoefficient;

	float4 color1;

	float4 color2;

	float3 constForce;
	float randomPos;

	float2 size;
	float randomVel;
};
struct ParticleUpdateParameters {
	uint seed;
	uint numForcePoints;
	uint numImpulsePoints;
	uint amountToSpawn;
};
//lighting
struct PointLightData{
	float4 position;
	//float r;
	//float g;
	//float b;
	float4 color; //there is a weird bug with a broken offset if float3 is used...
	float ambient;
};
/*
struct LightingData{
	float3 dir;
	float3 color;
	float strength;
	uint num_pointLights;
	PointLightData pointLights[MAX_POINT_LIGHTS];
};*/
//SMAA
struct SMAARTMetrics {
	float invWidth;
	float invHight;
	float width;
	float hight;
};
//for text render
struct TextVertexParams {
	float2 anchorUV;
	int2 offsetPix;
	float2 invTexDim;
	float textSize;
	float textScale;
	float dstBorder;
	uint srcBorder;
};
struct TextPixelParams {
	float4 color;
	float heightRange;
};

struct RectVertexParams {
	float2 anchorUV;
	int2 offsetPix;
	int2 sizePix;
	float borderPix;
};
struct RectPixelParams {
	float4 color;
	float2 anchorUV;
	int2 offsetPix;
	int2 sizePix;
	float lineWidthPix;
	float borderWidthPix;
};

struct OrbitalElements {
	float e; // eccentricity
	float a; // semimajor axis
	float b; // semiminor axis

	//(vertical tilt of the ellipse with respect to the reference plane, measured at the ascending node)
	//float i; // inclination; THIS SHOULD ALWAYS STAY 0 -> everything stays in 2D
	//float Q; // Longitude of the ascending node; horizontally orients the ascending node of the ellipse
	float w; // Argument of periapsis defines the orientation of the ellipse in the orbital plane
	float v; // true anomaly; orbit angle; describes position in orbit
	// the true anomaly is measured from periapsis
	float ra; //distance to central body at apoapsis
	float rp; //distance to central body at periapsis
};

struct LineStyle {
	float4 color;
	float width;
};
struct OrbitalOffsets {
	float4 v;
};

struct BloomFilterParameters {
	float threshold;
};
struct BlurParameters {
	uint direction;
	float depthBias;
};

struct FogParameters {
	float height;
	float maxDensity;
	float stepSize;
	uint maxSteps;
	float blackHoleSize;
};

// Tonemapping methods
#define TM_Linear     0
#define TM_Reinhard   1
#define TM_ReinhardSq 2
#define TM_ACESFilmic 3

struct TonemapParameters{
	// The method to use to perform tonemapping.
	uint TonemapMethod;
	// Exposure should be expressed as a relative exposure value (-2, -1, 0, +1, +2 )
	float Exposure;

	// The maximum luminance to use for linear tonemapping.
	float MaxLuminance;

	// Reinhard constant. Generally this is 1.0.
	float K;

	// ACES Filmic parameters
	// See: https://www.slideshare.net/ozlael/hable-john-uncharted2-hdr-lighting/142
	float A; // Shoulder strength
	float B; // Linear strength
	float C; // Linear angle
	float D; // Toe strength
	float E; // Toe Numerator
	float F; // Toe denominator
	// Note E/F = Toe angle.
	float LinearWhite;
	float Gamma;
	float BloomStrength;
};

// VERTEX SHADER INPUTS
struct SimpleVSInput{
	float3 position : POSITION;
	float3 color : COLOR0;
};

// GEOMETRY SHADER INPUTS
struct DepthCubeGSInput {
	float4 position : SV_POSITION;
	float4 positionW : POSITION0;
};

struct ParticleGSInput {
	float4 position : SV_POSITION;
	float4 color : COLOR;
	float size : TEXCOORD0;
};


// PIXEL SHADER INPUTS
struct SimplePSInput{
	float4 position : SV_POSITION;
	float4 positionW : POSITION0;
	float4 positionV : POSITION1;
	float4 positionP : POSITION2;
	float3 normalW : NORMAL0;
	float3 normalV : NORMAL1;
	float2 uv : TEXCOORD0;
};

struct BlitPSInput{
	float4 position : SV_Position;
	float4 positionP : TEXCOORD1;
	float2 uv : TEXCOORD0;
};

struct DepthCubePSInput {
	float4 position : SV_POSITION;
	float4 positionP : POSITION0;
	uint rtvID : SV_RenderTargetArrayIndex;
};

struct ParticlePSInput {
	float4 position : SV_POSITION;
	float4 positionP : POSITION2;
	float4 color : COLOR;
	float2 uv : TEXCOORD0;
};

// PIXEL SHADER OUTPUTS

struct ColorNormalDepthPSOutput {
	float4 color : SV_TARGET0;
	float4 normalDepth : SV_TARGET1;
};