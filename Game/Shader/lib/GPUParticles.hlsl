//#define RAND_SEED_128
//#include "lib/ShaderRNG.hlsl"

#define WARP_SIZE 128

#define NUM_GLOBAL_COUNTERS 2 //number of used/available global counters
#define GC_ACTIVE 0 //index in globalCounters for active particle count
#define GC_INACTIVE 1 //index in globalCounters for inactive particle count
RWStructuredBuffer<uint> globalCounters : register(u2);

// dimension, isHalf, on/in
#define EMITTER_ON_CIRCLE 0 //0b000
#define EMITTER_IN_CIRCLE 1 //0b001
#define EMITTER_ON_SPHERE 4 //0b100
#define EMITTER_IN_SPHERE 5 //0b101
#define EMITTER_ON_HEMISPHERE 6 //0b110
#define EMITTER_IN_HEMISPHERE 7 //0b111

// use z, use y, use x, radial
#define EMISSION_VEL_R 1
#define EMISSION_VEL_X 2
#define EMISSION_VEL_Y 4
#define EMISSION_VEL_Z 8

//flags
#define PARTICLE_FLAG_COLLISION 1
#define PARTICLE_FLAG_USE_WORLD_SPACE 2
#define PARTICLE_FLAG_FORCE 4
#define PARTICLE_FLAG_WARP_TIME 8


//holds data of particles
RWStructuredBuffer<ParticleData> particleDataBuffer : register(u0);
//holds indices of active partices
RWStructuredBuffer<uint> particleStateBuffer : register(u1);

//xyz position; w force, negative is pull, positive is push
//RWStructuredBuffer<float4> forcePoints : register(b3, space1);
//RWStructuredBuffer<float4> impulsePoints : register(b4, space1);

ConstantBuffer<ParticleSystemParameters> PSparamCB : register(b1, space0);
#define HAS_PARTICLE_FLAG(flag) ((PSparamCB.flags & flag) > 0)


ConstantBuffer<ParticleUpdateParameters> PSupdateCB : register(b2, space0);

ConstantBuffer<ModelViewProjectionConstantBuffer> mvpCB : register(b0, space0);

ConstantBuffer<GloablConstantsBuffer> timeCB : register(b12, space0);
#define TIME timeCB.time.x
#define TIME_DELTA timeCB.time.y
#define WARP_TIME timeCB.time.z
#define WARP_TIME_DELTA timeCB.time.w

float getTime() {
	if (HAS_PARTICLE_FLAG(PARTICLE_FLAG_WARP_TIME)) {
		return WARP_TIME;
	}else{
		return TIME;
	}
}
float getDeltaTime() {
	if (HAS_PARTICLE_FLAG(PARTICLE_FLAG_WARP_TIME)) {
		return WARP_TIME_DELTA;
	}
	else {
		return TIME_DELTA;
	}
}