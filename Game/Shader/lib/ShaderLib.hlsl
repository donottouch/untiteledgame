
#if !defined(M_PI)
#define M_PI 3.141592653589793f
#endif
#if !defined(M_iPI)
#define M_iPI 0.318309886183790f
#endif

#if !defined(M_2PI)
#define M_2PI 6.2831853071795864f
#endif
#if !defined(M_i2PI)
#define M_i2PI 0.15915494309189f
#endif

uint argmax(const float3 arg) {
	const float vmax = max(max(arg.x, arg.y), arg.z);
	if (vmax == arg.x) return 0;
	if (vmax == arg.y) return 1;
	if (vmax == arg.z) return 2;
	return 0;
}
int argmax(const float4 arg) {
	const float vmax = max(max(arg.x, arg.y), max(arg.z, arg.w));
	if (vmax == arg.x) return 0;
	if (vmax == arg.y) return 1;
	if (vmax == arg.z) return 2;
	if (vmax == arg.w) return 3;
	return 0;
}

float mod(const float x, const float m) {
	const float r = fmod(x, m);
	return r < 0 ? r + m : r;
}

float angleDistance(const float start, const float end) {
	return mod(mod(end, M_2PI) - mod(start, M_2PI), M_2PI);
}

//coordinate space conversions
// returns (radius, theta, phi) with r>=0, theta=[0,pi], phi [0,2pi]
float3 cartesianToSpherical(const float3 pos) {
	const float r = length(pos);
	return float3(r, acos(pos.z / r), atan(pos.y / pos.x));
}
float3 sphericalToCartesian(const float3 pos) {
	const float sinTheta = sin(pos.y);
	return float3(pos.x*sinTheta*cos(pos.z), pos.x*sinTheta*sin(pos.z), pos.x*cos(pos.y));

}

//DX
float linearDepth(const float depth, const float far, const float near) {
	return (near) / (far - depth * (far - near));
}

float2 ClipToUV(const float2 clip) {
	return (clip* float2(1, -1) *0.5 + 0.5) ;
}

float2 UVToClip(const float2 uv) {
	return (uv - 0.5) * 2 * float2(1, -1);
}

int2 UVToPix(const float2 uv, const  int2 screenSizePix) {
	return int2(uv * screenSizePix);
}

float2 PixToUV(const uint2 pix, const  int2 screenSizePix) {
	return float2(pix) / float2(screenSizePix);
}

int2 ClipToPix(const float2 clip, const  int2 screenSizePix) {
	return UVToPix(ClipToUV(clip), screenSizePix);
}

float2 PixToClip(const uint2 pix, const  int2 screenSizePix) {
	return UVToClip(PixToUV(pix, screenSizePix));
}

//need RT_METRICS defined
#if defined(RT_METRICS)
#if !defined(RTM)
#define RTM RT_METRICS.zw
#endif
#if !defined(iRTM)
#define iRTM RT_METRICS.xy
#endif
int3 UVToPixRTM(const float2 uv) {
	return int3(int2(uv * RTM), 0);
}

float2 PixToUVRTM(const int2 pix) {
	return float2(pix) * iRTM;
}

int3 ClipToPixRTM(const float2 clip) {
	return UVToPixRTM(ClipToUV(clip));
}

float2 PixToClipRTM(const int2 pix) {
	return UVToClip(PixToUVRTM(pix));
}
#endif

float4 getClipFromWorldPos(const float4 posWorld, const matrix view, const matrix projection) {
	float4 viewPos = mul(posWorld, view);
	float4 clipPos = mul(viewPos, projection);
	return clipPos / clipPos.w;
}
float4 getWorldFromClipPos(const float4 clipPos, const matrix invView, const matrix invProjection) { //https://stackoverflow.com/questions/32227283/getting-world-position-from-depth-buffer-value
	float4 viewPos = mul(clipPos, invProjection);
	viewPos /= viewPos.w;
	return mul(viewPos, invView);
}
float4 getWorldFromUVDepth(const float2 uv, const float depth, const matrix invView, const matrix invProjection) {
	return getWorldFromClipPos(float4(UVToClip(uv), depth, 1), invView, invProjection);
}

float4 getEyePos(const matrix invView){
	return mul(float4(0,0,0,1), invView);
}

float stepmix(float edge0, float edge1, float E, float x)
{
	float T = clamp(0.5 * (x - edge0 + E) / E, 0.0, 1.0);
	return lerp(edge0, edge1, T);
}

float SmoothCircle(float distSQ, float lowerSQ, float upperSQ) {

	float E = fwidth(distSQ);
	float d = min(abs(distSQ - lowerSQ), abs(distSQ - upperSQ));//smallest distance to border
	if (d <= E) { //is close to border, antialiasing
		return smoothstep(0, E, d);
	}
	/*
	if (distSQ >= lowerSQ - E && distSQ <= lowerSQ + E) { //innner border antialiasing
		return smoothstep(lowerSQ - E, lowerSQ + E, dSq);
	}
	else if (distSQ >= upperSQ - E && distSQ <= upperSQ + E) { //outer border antialiasing
		return smoothstep(upperSQ + E, upperSQ - E, dSq);
	}
	*/
	else if (distSQ >= lowerSQ && distSQ <= upperSQ) { //fragment in bounds
		return 1;
	}
	return 0;
}

float SmoothLine(const float distSQ, const float width) {
	const float E = fwidth(distSQ);
	//width = max(E, width);

	const float widthSQ = width * width;

	const float d = widthSQ - distSQ; //distance to line border
	return smoothstep(0, E, d);
}

float CelStep(const float factor, const float threshold) {
	const float E = fwidth(factor);
	float sf = 0;
	if (factor > threshold - E && factor < threshold + E) sf = smoothstep(threshold - E, threshold + E, factor);
	else sf = step(threshold, factor);
	return sf;
}

float Fresnel(const float3 normal, const float3 viewDir, const float strength) {
	return pow(abs(1.0f - dot(normal, viewDir)), strength);
}

//get correct map: largest component of relative pos
//[0, 5]: PX, NX, PY, NY, PZ, NZ
uint getCubeMapIndex(const float3 relPos){
	const float3 absPos = abs(relPos);
	const uint amax = argmax(absPos);
	const uint sig = sign(relPos[amax])<0;
	return amax * 2 + sig;
}

float getCubeMapValue1(const float3 relPos, const Texture2DArray<float> cubeMap, const matrix viewArray[6], const matrix projection, out float4 clipPos){
	uint map = getCubeMapIndex(relPos);
	uint3 cubeSizePix = uint3(1024, 1024, 6);
	cubeMap.GetDimensions(cubeSizePix.x, cubeSizePix.y, cubeSizePix.z);
	clipPos = getClipFromWorldPos(float4(relPos, 1), viewArray[map], projection);
	
	uint4 pixPos = uint4(ClipToPix(clipPos.xy, cubeSizePix.xy), map, 0);
	return cubeMap.Load(pixPos);
}
float4 getCubeMapValue4(const float3 relPos, const Texture2DArray<float4> cubeMap, const matrix viewArray[6], const matrix projection, out float4 clipPos){
	uint map = getCubeMapIndex(relPos);
	uint3 cubeSizePix = uint3(1024, 1024, 6);
	cubeMap.GetDimensions(cubeSizePix.x, cubeSizePix.y, cubeSizePix.z);
	
	clipPos = getClipFromWorldPos(float4(relPos, 1), viewArray[map], projection);
	uint4 pixPos = uint4(ClipToPix(clipPos.xy, cubeSizePix.xy), map, 0);
	return cubeMap.Load(pixPos);
}

//discards the fragment if the alpha is too low. returns the input color.
float4 DiscardTransparent(const float4 color){
	if(color.a < 1e-5) discard;
	return color;
}

float lightIntensity(const float3 relativePos) {
	return 1 / (dot(relativePos, relativePos) + 1);
}

//n must be normalized
float3 normalComponent(const float3 v, const float3 n) {
	return dot(v, n)*n;
}
//n must be normalized
float3 reflectLossy(const float3 v, const float3 n, const float loss) {
	return normalComponent(v, n)*(2-loss);
}
//n must be normalized
float3 projectOnPlane(const float3 v, const float3 n) {
	return v - normalComponent(v, n);
}

float raySphereIntersect(const float3 center, const float radius, const float3 start, const float3 dir){
	/*
	const float3 toCenter = start - center;
	const float b = dot(dir, toCenter);
	const float c = dot(toCenter, toCenter) - radius * radius;
	const float det = b * b - c;
	if (det < 0) return -1.0f;
	if (det == 0) return -b;
	const float detSqrt = sqrt(det);
	const float q = (b > 0 ? b + detSqrt : b - detSqrt);
	return min(-q, -c/q);
	*/
	const float3 toCenter = center - start;
	const float tc = dot(toCenter, dir);

	if (tc < 0) return -1;
	const float d2 = dot(toCenter, toCenter) - (tc*tc);
	const float r2 = radius * radius;
	if (d2 > r2) return -1;

	const float t1c = sqrt(r2 - d2);
	return tc - t1c;
};

void makeScreenQuad(const uint vertID, inout float4 position, inout float2 uv) {
	uv = float2(vertID & 1, (vertID >> 1) & 1);
	position = float4(lerp(float2(-1, 1), float2(1, -1), uv), 0, 1);
}

float distToRect(const float2 relPos, const float2 rectSize) {
	float2 dist0 = abs(relPos);
	float2 dist1 = abs(relPos - rectSize);
	return 0;
}

uint isInRect(const float2 relPos, const float2 rectSize) {
	const float2 rectSizeHalf = rectSize * 0.5;
	float2 d = max(abs(relPos - rectSizeHalf) - rectSizeHalf, 0);
	return !any(d);
}

