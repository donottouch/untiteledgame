//based on https://github.com/simongeilfus/SimplexNoise

//provides simplex noise via snoise(pos) with pos: float2, float3 or float4
//#include "SimplexNoise3D.hlsl"
#include "noiseSimplex.hlsl"

//voronoi / worley
float worleyNoise(float2 pos){
	float2 p = floor(pos);
	float2 f = frac(pos);
	float res = 8.0;
	for(int j=-1;j<=1;j++){
		for(int i = -1; i <= 1; i++){
			float2 b = float2(i, j);
			float2 r = b - f + (snoise(p + b)*0.5 + 0.5);
			float d = dot(r, r);
			res = min(res, d);
		}
	}
	return sqrt(res);
}
float worleyNoise(float3 pos){
	float3 p = floor(pos);
	float3 f = frac(pos);
	float res = 8.0;
	for (int k = -1; k <= 1; k++){
		for (int j = -1; j <= 1; j++){
			for (int i = -1; i <= 1; i++){
				float3 b = float3(i,j,k);
				float3 r = b-f+(snoise(p+b)*0.5 + 0.5);
				float d = dot(r, r);
				res = min(res, d);
			}
		}
	}
	return sqrt(res);
}
float worleyNoise(float4 pos){
	float4 p = floor(pos);
	float4 f = frac(pos);
	float res = 8.0;
	for (int l = -1; l <= 1; l++){
		for (int k = -1; k <= 1; k++){
			for (int j = -1; j <= 1; j++){
				for (int i = -1; i <= 1; i++){
					float4 b = float4(i,j,k,l);
					float4 r = b-f+(snoise(p+b)*0.5 + 0.5);
					float d = dot(r, r);
					res = min(res, d);
				}
			}
		}
	}
	return sqrt(res);
}
//fractal brownian motion sum
float noisefBm(float2 pos, const uint octaves, float lacunarity, float gain){
	float sum = 0;
	float freq = 1;
	float amp = 0.5;
	for (uint i = 0; i<octaves; i++){
		float n = snoise(pos * freq);
		sum += n*amp;
		freq *= lacunarity;
		amp *= gain;
	}
	return sum;
}
float noisefBm(float3 pos, const uint octaves, float lacunarity, float gain){
	float sum = 0;
	float freq = 1;
	float amp = 0.5;
	for (uint i = 0; i<octaves; i++){
		float n = snoise(pos * freq);
		sum += n*amp;
		freq *= lacunarity;
		amp *= gain;
	}
	return sum;
}
float noisefBm(float4 pos, const uint octaves, float lacunarity, float gain){
	float sum = 0;
	float freq = 1;
	float amp = 0.5;
	for (uint i = 0; i<octaves; i++){
		float n = snoise(pos * freq);
		sum += n*amp;
		freq *= lacunarity;
		amp *= gain;
	}
	return sum;
}
//ridge
float ridge(float h, float offset){
	h = offset - abs(h);
	return h*h;
}
float ridgedNoise(float2 pos){
	return 1 - abs(snoise(pos));
}
float ridgedNoise(float3 pos){
	return 1 - abs(snoise(pos));
}
float ridgedNoise(float4 pos){
	return 1 - abs(snoise(pos));
}
//multi fractal ridge
float ridgedMF(float2 pos, float ridgeOffset, const uint octaves, float lacunarity, float gain){
	float sum = 0;
	float freq = 1;
	float amp = 0.5;
	float prev = 1;
	for (uint i = 0; i < octaves; i++){
		float n = ridge(snoise(pos * freq), ridgeOffset);
		sum += n*amp*prev;
		prev = n;
		freq *= lacunarity;
		amp *= gain;
	}
	return sum;
}
float ridgedMF(float3 pos, float ridgeOffset, const uint octaves, float lacunarity, float gain){
	float sum = 0;
	float freq = 1;
	float amp = 0.5;
	float prev = 1;
	for (uint i = 0; i < octaves; i++){
		float n = ridge(snoise(pos * freq), ridgeOffset);
		sum += n*amp*prev;
		prev = n;
		freq *= lacunarity;
		amp *= gain;
	}
	return sum;
}
float ridgedMF(float4 pos, float ridgeOffset, const uint octaves, float lacunarity, float gain){
	float sum = 0;
	float freq = 1;
	float amp = 0.5;
	float prev = 1;
	for (uint i = 0; i < octaves; i++){
		float n = ridge(snoise(pos * freq), ridgeOffset);
		sum += n*amp*prev;
		prev = n;
		freq *= lacunarity;
		amp *= gain;
	}
	return sum;
}
