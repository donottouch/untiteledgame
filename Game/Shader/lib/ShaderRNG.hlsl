#ifndef UINT_MAX
#define UINT_MAX 0xffffffff
#endif
#ifndef FLOAT_INVERSE_UINT_MAX
#define FLOAT_INVERSE_UINT_MAX (1.0f/float(UINT_MAX))
#endif
#ifndef MATH_PI
#define MATH_PI 3.141592653589793115997963468544185161590576171875
#endif

// from https://en.wikipedia.org/wiki/Xorshift
uint xorshift32(uint state) {
	uint x = state;
	x ^= x << 13;
	x ^= x >> 17;
	x ^= x << 5;
	return x;
}
uint4 xorshift128(uint4 state) {
	uint s, t = state.w;
	t ^= t << 11;
	t ^= t >> 8;
	state.w = state.z;
	state.z = state.y;
	state.y = s = state.x;
	t ^= s;
	t ^= s >> 19;
	state.x = t;
	return state;
}
/*
unsigned long xorshift64star(unsigned long state) {
	unsigned long x = state;
	x ^= x >> 12;
	x ^= x << 25;
	x ^= x >> 27;
	return x; //* 0x2545F4914F6CDD1DL;

}*/



struct Random { //https://gamedev.stackexchange.com/questions/32681/random-number-hlsl


#if defined(RAND_SEED_128)
	uint4 state;

	void SetState(const uint4 newState) {
		state = newState;
	}

	//seed: global seed
	//idx: thread id
	void Init(const uint seed, const uint idx) {
		hasSpare = false;
#ifdef RAND_CYCLE_INIT
		const uint4 s = uint4(seed, seed ^ 0xaaaaaaaa, seed ^ 0xffffffff, seed ^ 0x55555555);
		SetState(s);
		Cycle(idx);
#else //constant time
		const uint4 s = uint4(seed, seed ^ (idx * 0x23232323), (seed ^ 0xffffffff) * idx, seed ^ (idx * 0x55555555));
		SetState(s);
		Cycle(5);
#endif
	}

	void Cycle() {
		state = xorshift128(state);
	}

	uint Uint() {
		Cycle();
		return state.x;
	}

#else
	uint state;

	void SetState(const uint newState) {
		state = newState;
	}

	//seed: global seed
	//idx: thread id
	void Init(const uint seed, const uint idx) {
		hasSpare = false;
#ifdef RAND_CYCLE_INIT
		SetState(seed);
		Cycle(idx);
#else //constant time
		SetState(seed ^ (idx * 0x23232323));
		Cycle(5);
#endif
	}

	void Cycle() {
		state = xorshift32(state);
	}

	uint Uint() {
		Cycle();
		return state;
	}
#endif

	void Cycle(const uint num) {
		for (uint i = 0; i < num; i++) Cycle();
	}

	//returns random uniform float [0,1]
	float Float01() {
		return ((float)Uint() * FLOAT_INVERSE_UINT_MAX);
	}
	//returns random uniform float [-1,1]
	float Float() {
		return Float01() * 2 - 1;
	}
	//returns random uniform float [min,max]
	float Float(const float min,const float max) {
		return Float01() * (max - min) + min;
	}
	//returns random uniform float [minmax.x, minmax.y]
	float Float(const float2 minmax) {
		return Float(minmax.x, minmax.y);
	}

	//Float around 1 with max pvar% deviation from 1
	float FloatPvar(const float pvar) {
		return Float(1 - pvar, 1 + pvar);
	}

	//https://en.wikipedia.org/wiki/Marsaglia_polar_method
	float spare;
	bool hasSpare;
	//returns random normal distributed float with zero mean and unit stdDev
	float Normal() {
		if (hasSpare) {
			hasSpare = false;
			return spare;
		}
		float u, v, s;
		do {
			u = Float();
			v = Float();
			s = u*u + v*v;
		} while (s >= 1 || s == 0);
		float mul = sqrt(-2.0f * log(s) / s);
		spare = v * mul;
		hasSpare = true;
		return u * mul;
	}
	//returns random normal distributed float with given mean and stdDev
	float Normal(const float mean, const float stdDev) {
		return Normal() * stdDev + mean;
	}
	//return random uniform float2 on unit circle
	float2 OnUnitCircle() { //https://math.stackexchange.com/questions/40023/generating-a-random-point-on-the-unit-circle
		float theta = (2.0 * MATH_PI) * Float01();
		return float2(cos(theta), sin(theta));
	}
	//return random float2 in unit circle
	//NOT uniform
	float2 InUnitCircle() {
		return OnUnitCircle() * Float01();
	}
	//return random uniform float3 on unit sphere
	float3 OnUnitSphere() { //https://corysimon.github.io/articles/uniformdistn-on-sphere/
		float theta = (2.0 * MATH_PI) * Float01();
		//float phi = (MATH_PI) * rand01();
		float phi = acos(1 - 2 * Float01());
		return float3(sin(phi)*cos(theta), sin(phi)*sin(theta), cos(phi));
	}
	//return random float3 in unit sphere
	//NOT uniform
	float3 InUnitSphere() { 
		return OnUnitSphere() * Float01();
	}
	float3 InSphere(const float minR, const float maxR) {
		return OnUnitSphere() * Float(minR, maxR);
	}
	//return random float3 in unit sphere with min and max radius
	//NOT uniform
	float3 InUnitSphere(float minr, float maxr) {
		return OnUnitSphere() * Float(minr, maxr);
	}
	//return random float3 in unit sphere with minmax radius
	//NOT uniform
	float3 InUnitSphere(float2 minmaxr) {
		return OnUnitSphere() * Float(minmaxr);
	}
};