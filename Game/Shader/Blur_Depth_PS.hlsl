
#include "lib/ShaderStructures.hlsl"
#include "lib/ShaderLib.hlsl"
ConstantBuffer<BlurParameters> paramCB : register(b0);
Texture2D<float4> HDRTexture : register(t0);
Texture2D<float4> NDTexture : register(t1);


float4 main(float4 Position : SV_Position) : SV_Target0
{
	const float weight[5] = { 0.227027, 0.1945946, 0.1216216, 0.054054, 0.016216 };
	const float near = 0.01;
	const float far = 100;

	//uint width, height, levels;
	//HDRTexture.GetDimensions(0, width, height, levels);
	const uint3 step = paramCB.direction ? int3(0, 1, 0) : int3(1, 0, 0);
		

	const int3 texCoord = int3((int2)Position.xy,0);
	float4 HDR = HDRTexture.Load(texCoord) * weight[0];
	const float depth = linearDepth(NDTexture.Load(texCoord).w, far, near);
	float totalWeight = weight[0];
	
	int i = 1;
	int3 currentTexCoord = texCoord;
	for (; i < 5; ++i) {
		currentTexCoord += step;
		const float depthP = linearDepth(NDTexture.Load(currentTexCoord).w, far, near);
		if(abs(depth-depthP) > paramCB.depthBias) break;
		HDR += HDRTexture.Load(currentTexCoord) * weight[i];
		totalWeight += weight[i];
	}
	currentTexCoord = texCoord;
	for (i=1; i < 5; ++i) {
		currentTexCoord -= step;
		const float depthN = linearDepth(NDTexture.Load(currentTexCoord).w, far, near);
		if(abs(depth-depthN) > paramCB.depthBias) break;
		HDR += HDRTexture.Load(currentTexCoord) * weight[i];
		totalWeight += weight[i];
	}

	return HDR/totalWeight;
}