#include "lib/ShaderStructures.hlsl"
#include "lib/ShaderLib.hlsl"

ConstantBuffer<ModelViewProjectionConstantBuffer> objectCB : register(b0);
ConstantBuffer<MeshParams> meshCB : register(b1);
ConstantBuffer<PointLightData> lightCB : register(b10);
ConstantBuffer<CubeMapViewProjectionConstantBuffer> CubemvpCB : register(b11);
Texture2DArray<float> CubeMap : register(t1);

float shadow(const float3 posWorld) {
	float4 clipPos;
	const float depth = getCubeMapValue1(posWorld, CubeMap, CubemvpCB.view, CubemvpCB.projection, clipPos);
	const float far = 100;
	float dDepth = linearDepth(clipPos.z, far, 0.01) - linearDepth(depth, far, 0.01);
	const float lit = dDepth < 0.001 ? 1 : 0;
	return lit;
}

float3 lightDiffuse(const float3 posWorld, const float3 N, const float3 lightPos, const float3 lightColor) {
	const float3 dist = lightPos - posWorld;
	const float3 L = normalize(dist);
	const float lit = shadow(posWorld);
	return max(float3(0, 0, 0), dot(L, N) * lightColor * lightIntensity(dist) * lit);
}

float3 lightPoint(const float3 posWorld, const float3 normal) {
	float3 lightColor = (float3) 0;
	for (uint i = 0; i < min(MAX_POINT_LIGHTS, 8); ++i) {//objectCB.lightCount
		lightColor += lightDiffuse(posWorld, normal, objectCB.lightPos[i].xyz, objectCB.lightColor[i].rgb);
	}
	return lightColor;
}


ColorNormalDepthPSOutput main(SimplePSInput IN){
	ColorNormalDepthPSOutput OUT = (ColorNormalDepthPSOutput)0;

	//float3 lightColor = float3(lightCB.r, lightCB.g, lightCB.b);//lightCB.color.rgb;//float3(0.3,0.15,0.9)*4;
	const float ambient = lightCB.ambient;
	const float3 albedo = saturate(meshCB.color.rgb);

	float3 colorDiffuse = lightDiffuse(IN.positionW.xyz, IN.normalW, lightCB.position.xyz, lightCB.color.rgb);//max(float3(0,0,0),dot(L, IN.normalW) * lightColor * intensity * meshCB.color.rgb);
	colorDiffuse += lightPoint(IN.positionW.xyz, IN.normalW);
	colorDiffuse *= albedo;

	colorDiffuse += ambient * albedo;
	colorDiffuse += meshCB.colorEmissive.rgb * meshCB.colorEmissive.a;

	OUT.color = float4(colorDiffuse, meshCB.color.a);
	OUT.normalDepth = float4(IN.normalV, IN.positionP.z/ IN.positionP.w);

	return OUT;
}