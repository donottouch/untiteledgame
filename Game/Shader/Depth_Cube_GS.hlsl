#include "lib/ShaderStructures.hlsl"

ConstantBuffer<CubeMapViewProjectionConstantBuffer> vpCB : register(b11);

[maxvertexcount(18)]
void main(triangle DepthCubeGSInput IN[3], inout TriangleStream<DepthCubePSInput> outStream) {

	DepthCubePSInput OUT = (DepthCubePSInput)0;

	[unroll(6)]
	for (int i = 0; i < 6; ++i) { //generate primitives (triangles) for each side of the cube map
		[unroll(3)]
		for (int p = 0; p < 3; ++p) {
			OUT.position = mul(mul(IN[p].position, vpCB.view[i]), vpCB.projection);
			OUT.positionP = OUT.position;
			OUT.rtvID = i;
			outStream.Append(OUT);
		}
		outStream.RestartStrip();
	}
}