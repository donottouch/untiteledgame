#include "lib/ShaderStructures.hlsl"

[maxvertexcount(3)]
void main(triangle SimplePSInput IN[3], inout TriangleStream<SimplePSInput> tstream){
	//world normal
	float3 dir1 = IN[1].positionW.xyz - IN[0].positionW.xyz;
	float3 dir2 = IN[2].positionW.xyz - IN[0].positionW.xyz;
	float3 normal = -normalize(cross(dir1, dir2));
	IN[0].normalW = normal;
	IN[1].normalW = normal;
	IN[2].normalW = normal;
	//view normal
	dir1 = IN[1].positionV.xyz - IN[0].positionV.xyz;
	dir2 = IN[2].positionV.xyz - IN[0].positionV.xyz;
	normal = -normalize(cross(dir1, dir2));
	IN[0].normalV = normal;
	IN[1].normalV = normal;
	IN[2].normalV = normal;
	//output
	tstream.Append(IN[0]);
	tstream.Append(IN[1]);
	tstream.Append(IN[2]);
	tstream.RestartStrip();
}