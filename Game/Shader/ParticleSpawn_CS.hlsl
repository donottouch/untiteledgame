#include "lib/ShaderStructures.hlsl"
//#include "lib/ShaderLib.hlsl"
#include "lib/ShaderRNG.hlsl"
#include "lib/GPUParticles.hlsl"

void InitParticle(const int idx) {
	Random rand;
	rand.Init(PSupdateCB.seed, idx);
	//particleDataBuffer[idx]
	ParticleData particle = particleDataBuffer[idx];
	particle.lifeTime = rand.Float(PSparamCB.lifetime); //rand.Normal((lifetime.x + lifetime.y) * 0.5f, 0.1f);//
	particle.startTime = getTime();

	float3 emitterPosition = (float3) 0;
	const float distance = rand.Float(PSparamCB.emitterRadius.x, PSparamCB.emitterRadius.y);

	if (PSparamCB.emitterType == EMITTER_ON_CIRCLE) {
		float2 pos = rand.OnUnitCircle() * PSparamCB.emitterRadius.y;
		particle.position = float3(pos.x, 0, pos.y);
	}if (PSparamCB.emitterType == EMITTER_IN_CIRCLE) {
		float2 pos = rand.OnUnitCircle() * distance;
		particle.position = float3(pos.x, 0, pos.y);
	}if (PSparamCB.emitterType == EMITTER_ON_SPHERE) {
		particle.position = rand.OnUnitSphere() * PSparamCB.emitterRadius.y;
	}if (PSparamCB.emitterType == EMITTER_IN_SPHERE) {
		particle.position = rand.OnUnitSphere() * distance;
	}if (PSparamCB.emitterType == EMITTER_ON_HEMISPHERE) {
		float3 pos = rand.OnUnitSphere() * PSparamCB.emitterRadius.y;
		particle.position = float3(pos.x, abs(pos.y), pos.z);
	}if (PSparamCB.emitterType == EMITTER_IN_HEMISPHERE) {
		float3 pos = rand.OnUnitSphere() * distance;
		particle.position = float3(pos.x, abs(pos.y), pos.z);
	}
	particle.position += rand.InUnitSphere(0, PSparamCB.randomPos);

	const float3 velRestriction = float3((PSparamCB.emissionVelType & EMISSION_VEL_X) > 0, (PSparamCB.emissionVelType & EMISSION_VEL_Y) > 0, (PSparamCB.emissionVelType & EMISSION_VEL_Z) > 0);
	if (PSparamCB.emissionVelType & EMISSION_VEL_R) {
		particle.velocity = (normalize(particle.position - emitterPosition) * rand.Float(PSparamCB.emissionVel)) * velRestriction;
	}
	else {
		particle.velocity = rand.InUnitSphere(PSparamCB.emissionVel) * velRestriction;
	}
	particle.velocity += PSparamCB.emissionVelLin.xyz * (PSparamCB.emissionVelLin.w == 0 ? 1 : rand.FloatPvar(PSparamCB.emissionVelLin.w));
	particle.velocity += rand.InUnitSphere(0, PSparamCB.randomVel);

	if (HAS_PARTICLE_FLAG(PARTICLE_FLAG_USE_WORLD_SPACE)) {
		particle.position = mul(mvpCB.model, float4(particle.position, 1)).xyz;
		particle.velocity = mul(mvpCB.model, float4(particle.velocity, 0)).xyz;
	}
	float r = rand.Float01();
	float4 colorStrength = float4(10, 10, 10, 1);
	float4 color = PSparamCB.color1 * r + PSparamCB.color2 * (1 - r);


	particle.color = color;// *colorStrength;
	particle.size = rand.Float(PSparamCB.size.x, PSparamCB.size.y);
	particle.state = 1;
	particleDataBuffer[idx] = particle;
}

[numthreads(WARP_SIZE, 1, 1)]
void main(uint3 id : SV_DispatchThreadID) {
	if (id.x > PSparamCB.particleBufferSize) return;
	//spawn a particle
	if (id.x < PSupdateCB.amountToSpawn && id.x < globalCounters[GC_INACTIVE]) {
		uint idx = particleStateBuffer[PSparamCB.particleBufferSize - id.x - 1];
		if (idx > PSparamCB.particleBufferSize) return;
		InitParticle(idx);
	}
}