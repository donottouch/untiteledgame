
#include "lib/ShaderStructures.hlsl"
#include "lib/ShaderLib.hlsl"
#include "lib/NoiseVariations.hlsl"

ConstantBuffer<ModelViewProjectionConstantBuffer> mvpCB : register(b0);
ConstantBuffer<FogParameters> FogCB : register(b2);
ConstantBuffer<PointLightData> lightCB : register(b10);
ConstantBuffer<CubeMapViewProjectionConstantBuffer> CubemvpCB : register(b11);
ConstantBuffer<GloablConstantsBuffer> timeCB : register(b12);
Texture2D<float4> NDTexture : register(t0);
Texture2DArray<float> CubeMap : register(t1);
SamplerState LinearSampler : register (s0);
SamplerState PointSampler : register (s1);




float fogDensity(float3 posWorld) {
	const float height = abs(posWorld.y);
	const float t = saturate(height / FogCB.height);
	const float distDensity = saturate(1 - 6 / max(65 - dot(posWorld, posWorld), 0));
	const float heightDensity = lerp(FogCB.maxDensity, 0, t);
	//float noise = snoise(posWorld * 2) *0.5 + 0.5;
	//const float noise = ridgedMF(posWorld*1.6 + float3(0, timeCB.time.x*0.2, 0), 1.0, 3, 2.3, 0.75)*0.8 + 0.2;
	//const float3 noisePos = cartesianToSpherical(posWorld*1.1); //posWorld*1.1 + float3(0, timeCB.time.x*0.2, 0)
	const float noise = noisefBm(posWorld*1.1 + float3(0, timeCB.time.x*0.1, 0), 3, 2.3, 0.75) *0.5 + 0.5;
	return noise * heightDensity *distDensity; // 
}

float shadow(float3 posWorld) {
	float4 clipPos;
	const float depth = getCubeMapValue1(posWorld, CubeMap, CubemvpCB.view, CubemvpCB.projection, clipPos);
	const float lit = step(clipPos.z, depth);
	return lit;
}
//*/
float4 calculateLighting(float3 posWorld, float density) {
	const float intensity = lightIntensity(posWorld) * density;
	if (intensity < 0.01) return float4(0, 0, 0, density);
	else {
		const float3 lightWorld = lightCB.position.xyz;// (float3)0;
		const float3 lightColor = lightCB.color.rgb;//float3(lightCB.r, lightCB.g, lightCB.b);// float3(0.3, 0.15, 0.9) * 4;
		const float3 color = intensity * lightColor *shadow(posWorld);
		return float4(color, density);
	}
}

float4 rayMarch(int3 pixelCoord, float2 uv) {
	const float3 eyeWorld = getEyePos(mvpCB.inv_view).xyz;

	const float depth = saturate(NDTexture.Load(pixelCoord).w);
	const float3 endWorld = getWorldFromUVDepth(uv, depth, mvpCB.inv_view, mvpCB.inv_projection).xyz;
	const float3 dir = normalize(endWorld - eyeWorld);
	const float distanceEndWorld = distance(endWorld, eyeWorld);

	//return saturate(float4(distanceEndWorld*0.1, 0, 0, 1));

	const float blackHoleHit = raySphereIntersect(float3(0,0,0), FogCB.blackHoleSize, eyeWorld, dir);

	const float rayLength = blackHoleHit<0? distanceEndWorld : min(distanceEndWorld, blackHoleHit);

	//black hole
	const float blackHoleRadiusSq = FogCB.blackHoleSize * FogCB.blackHoleSize;

	float4 color = (float4) 0;
	//TODO handle low angles (dir.y approx 0)
	const float eyeDistanceToFogStartPlane = abs(eyeWorld.y) - FogCB.height;
	const float eyeDistanceToFogEndPlane = abs(eyeWorld.y) + FogCB.height;
	const float startOffset = min(eyeDistanceToFogStartPlane / abs(dir.y) *(eyeDistanceToFogStartPlane > 0), rayLength);
	const float endOffset = min(eyeDistanceToFogEndPlane / abs(dir.y), rayLength);


	const float sampleLength = endOffset - startOffset;
	if (sampleLength<0.005) return color;

	const float3 startPos = eyeWorld + dir * startOffset;

	const float stepSize = min(sampleLength / FogCB.maxSteps, FogCB.stepSize);
	//use smaller steps if fog starts close to camera to avoid artifacts. gives a bit the impression of a fresnel effect on the fog surface...
	const float correctedStepSize = eyeDistanceToFogStartPlane < 1 ? max(stepSize * eyeDistanceToFogStartPlane, 0.06) : stepSize;
	const float densityPerStep = stepSize * stepSize / correctedStepSize;
	const float3 step = dir * correctedStepSize;

	float3 pos = startPos - step;
	uint i;
	for (i = 0; i < FogCB.maxSteps; ++i) {
		pos += step;

		const float density =  fogDensity(pos) * densityPerStep;
		if (density < 0.01) continue;
		const float4 currentColor = calculateLighting(pos, density);
		color += (1 - color.a)*currentColor;
		if (color.a >= 1.0) break;
	}
	//return float4(dir, 1);
	return color;
}

float4 main(BlitPSInput IN) : SV_TARGET{
	int3 texCoord = int3((int2)IN.position.xy,0);
	float4 color = rayMarch(texCoord, IN.uv);

	//return float4(1, 0, 1, 1);
	return color;
}