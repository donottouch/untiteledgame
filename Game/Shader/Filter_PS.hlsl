
#include "lib/ShaderStructures.hlsl"
ConstantBuffer<BloomFilterParameters> FilterParametersCB : register(b0);
Texture2D<float4> HDRTexture : register(t0);

float4 main(float4 Position : SV_Position) : SV_Target0
{
	uint width, height, sampleCount;
	HDRTexture.GetDimensions(width, height);

	int3 texCoord = int3((int2)Position.xy,0);
	float3 HDR = HDRTexture.Load(texCoord).rgb;

	float thresholdsq = FilterParametersCB.threshold * FilterParametersCB.threshold;
	float brightness = dot(HDR, HDR) * 0.57735; //nomalized len(1,1,1) -> 1
	float4 color = float4(step(thresholdsq, brightness) * HDR, 1);

	return color;
	return float4(thresholdsq, brightness, 1, 1);
}