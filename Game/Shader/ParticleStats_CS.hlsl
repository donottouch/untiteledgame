#include "lib/ShaderStructures.hlsl"
//#include "lib/ShaderLib.hlsl"
#include "lib/GPUParticles.hlsl"

void UpdateParticleStateBuffer(const uint idx) {
	int stateBufferIdx = 0;
	if (particleDataBuffer[idx].state) {
		//InterlockedAdd(particleActiveCount, 1, stateBufferIdx);
		InterlockedAdd(globalCounters[GC_ACTIVE], 1, stateBufferIdx);
		particleStateBuffer[stateBufferIdx] = idx;
	}
	else {
		//InterlockedAdd(particleInactiveCount, 1, stateBufferIdx);
		InterlockedAdd(globalCounters[GC_INACTIVE], 1, stateBufferIdx);
		particleStateBuffer[PSparamCB.particleBufferSize - stateBufferIdx - 1] = idx;
	}

}


[numthreads(WARP_SIZE, 1, 1)]
void main(uint3 id : SV_DispatchThreadID) {
	if (id.x < PSparamCB.particleBufferSize){
		UpdateParticleStateBuffer(id.x);
	}
	//if (id.x == 0) {
		//globalCounters[GC_ACTIVE] = PSparamCB.emitterType;
		//globalCounters[GC_INACTIVE] = PSupdateCB.seed;
	//}
}