#include "lib/ShaderStructures.hlsl"
#include "lib/ShaderLib.hlsl"
#include "lib/ShaderRNG.hlsl"

ConstantBuffer<ModelViewProjectionConstantBuffer> mvpCB : register(b0);
ConstantBuffer<GloablConstantsBuffer> timeCB : register(b12);

//holds data of particles
RWStructuredBuffer<ParticleData> particleDataBuffer : register(u0);
//holds indices of active partices
RWStructuredBuffer<uint> particleStateBuffer : register(u1);

void initParticle(const uint idx, const float remainingTime, out float4 position, out float4 color, out float size) {
	const float maxDist = 75;
	const float minDist = 25;
	Random rand;
	rand.Init(42, idx);

	const float distance = clamp(rand.Float(minDist, maxDist) - 1 / (remainingTime*remainingTime), 0, maxDist);
	position = float4(rand.OnUnitSphere() * distance, 1);

	const float3 color1 = float3(1, 0.8, 0.5);
	const float3 color2 = float3(0.8, 0.4, 1);
	const float intensity = rand.Float(3, 8);
	color = float4(lerp(color1, color2, rand.Float01())*intensity, 1);

	size = rand.Float(0.03, 0.10);

}

ParticleGSInput main(uint VertexID : SV_VertexID) {
	ParticleGSInput OUT = (ParticleGSInput)0;

	int particleID = particleStateBuffer[VertexID];
	//get point in view space
	OUT.position = float4(particleDataBuffer[particleID].position, 1);
	OUT.color = particleDataBuffer[particleID].color;// *particleDataBuffer[particleID].state;
	OUT.size = particleDataBuffer[particleID].size;
	//*/
	//initParticle(VertexID, (904.0f - timeCB.time.x) * 0.05f, OUT.position, OUT.color, OUT.size);
	OUT.position = mul(OUT.position, mvpCB.view);

	return OUT;
}