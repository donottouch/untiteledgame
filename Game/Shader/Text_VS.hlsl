
//source https://github.com/Microsoft/DirectX-Graphics-Samples/blob/master/MiniEngine/Core/Shaders/TextVS.hlsl

struct VertexShaderInput
{
	float2 pos : POSITION;
	uint4 glyph : TEXCOORD;
};

struct VertexShaderOutput {
	float4 pos : SV_POSITION;
	float2 tex : TEXCOORD0;
};

#include "lib/ShaderStructures.hlsl"

ConstantBuffer<TextVertexParams> pCB : register(b0);
ConstantBuffer<RTMetrics> rtmCB : register(b2);
#define RT_METRICS rtmCB.rtmetrics
#include "lib/ShaderLib.hlsl"

VertexShaderOutput main(VertexShaderInput input, uint vertID : SV_VertexID)
{
	
	const float2 xy0 = input.pos - pCB.dstBorder;
	const float2 xy1 = input.pos + pCB.dstBorder + pCB.textScale * input.glyph.zw;//float2(pCB.textScale * input.glyph.z, pCB.textSize);
	const uint2 uv0 = input.glyph.xy - pCB.srcBorder;
	const uint2 uv1 = input.glyph.xy + pCB.srcBorder + input.glyph.zw;

	float2 uv = float2(vertID & 1, (vertID >> 1) & 1);

	VertexShaderOutput output;
	//Clip space
	float2 posClip = UVToClip(PixToUVRTM(lerp(xy0, xy1, uv) + pCB.offsetPix) + pCB.anchorUV);
	output.pos = float4(posClip, 0, 1);
	output.tex = lerp(uv0, uv1, uv) * pCB.invTexDim;
	return output;
}