#include "lib/ShaderStructures.hlsl"
#include "lib/ShaderLib.hlsl"

ConstantBuffer<OrbitalElements> orbitCB : register (b0, space1);
ConstantBuffer<LineStyle> styleCB : register (b1, space1);
ConstantBuffer<OrbitalOffsets> offsetCB : register (b2, space1);

struct PixelShaderInput
{
	float4 pos : SV_POSITION;
	float3 normal : NORMAL;
	float2 uv : TEXCOORD0;
	float3 wPos : TEXCOORD1;
};

float distanceInOrbit(const float trueAnomaly){
	//return (2.0f * orbitCB.ra * orbitCB.rp) / (orbitCB.ra + orbitCB.rp + ((orbitCB.ra - orbitCB.rp) * cos(trueAnomaly)));
	return orbitCB.a * (1 - pow(orbitCB.e, 2)) / (1 + orbitCB.e * cos(trueAnomaly));
}

float fragmentTrueAnomaly(const float3 relPos) {
	return atan2(relPos.x, relPos.z);
}

float distanceToOrbit(const float3 relPos, const float3 relPosN, const float v) {
	float3 orbitPos = relPosN * distanceInOrbit(v);
	return length(relPos - orbitPos);
}

float4 main(PixelShaderInput IN) : SV_TARGET0{
	const float3 bodyCenter = (float3) 0;
	const float3 relPos = IN.wPos - bodyCenter;
	const float3 relPosN = normalize(relPos);
	
	const float v = fragmentTrueAnomaly(relPos) - orbitCB.w;
	const float dist = distanceToOrbit(relPos, relPosN, v);

	const float fragmentAngleDistance = angleDistance(orbitCB.v, v);
	const float4 inRange = float4(
		fragmentAngleDistance < angleDistance(orbitCB.v, offsetCB.v.x),
		fragmentAngleDistance < angleDistance(orbitCB.v, offsetCB.v.y),
		fragmentAngleDistance < angleDistance(orbitCB.v, offsetCB.v.z),
		fragmentAngleDistance < angleDistance(orbitCB.v, offsetCB.v.w));

	float lineIntensity = 0.25;
	lineIntensity += 0.25*inRange.z;
	lineIntensity += 0.25*inRange.y;
	lineIntensity += 0.25*inRange.x;

	float width = styleCB.width;
	width += styleCB.width*0.4*inRange.z;
	width += styleCB.width*0.4*inRange.y;
	width += styleCB.width*0.4*inRange.x;

	const float lineStrength = SmoothLine(dist*dist, width);
	if (lineStrength > 0.0f)
		return float4(styleCB.color.rgb, styleCB.color.a *lineStrength * lineIntensity);
	else discard;

	return saturate(float4(dist,-dist,0,0.5f));
}

