//Pass 1: Edge detection
//Pixel Shader

Texture2D<float4> colorTex : register (t0);
#define RT_MAIN colorTex

#include "SMAA_INI.hlsl"
#include "SMAA.hlsl"

float4 main(float4 position : SV_POSITION,
			float2 texcoord : TEXCOORD0,
			float4 offset[3] : TEXCOORD1) : SV_Target0{
	return float4(SMAAColorEdgeDetectionPS(texcoord, offset, colorTex),0,0);
}
