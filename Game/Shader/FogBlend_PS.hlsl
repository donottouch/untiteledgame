
#include "lib/ShaderStructures.hlsl"
#include "lib/ShaderLib.hlsl"

Texture2D<float4> HDRTexture : register(t0);
Texture2D<float4> FogTexture : register(t1);

float4 main( BlitPSInput IN ) : SV_Target0{
	int3 texCoord = int3((int2)IN.position.xy,0);
	float4 HDR = HDRTexture.Load(texCoord);
	float4 Fog = FogTexture.Load(texCoord);
	//return Fog.raba ;// float4(Fog.a, 0, 0, 1);
	return lerp(HDR, Fog, Fog.a);
}