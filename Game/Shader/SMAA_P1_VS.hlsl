//Pass 1: Edge detection
//Vertex Shader

#include "SMAA_INI.hlsl"
#include "SMAA.hlsl"
#include "lib/ShaderLib.hlsl"

void main(uint vertID : SV_VertexID,
			out float4 svPos : SV_POSITION,
			out float2 texcoord : TEXCOORD0,
			out float4 offset[3] : TEXCOORD1){
	//svPos = float4(pos,1);
	//texcoord = mad(pos.xy, float2(0.5f, -0.5f), float2(0.5f,0.5f) );
	makeScreenQuad(vertID, svPos, texcoord);
	SMAAEdgeDetectionVS(texcoord, offset);
}
