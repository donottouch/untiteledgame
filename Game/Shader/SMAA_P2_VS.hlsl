//Pass 2: Blend Weight Calculation
//Vertex Shader

#include "SMAA_INI.hlsl"
#include "SMAA.hlsl"
#include "lib/ShaderLib.hlsl"

void main(uint vertID : SV_VertexID,
			out float4 svPos : SV_POSITION,
			out float2 texcoord : TEXCOORD0,
			out float2 pixcoord : TEXCOORD1,
			out float4 offset[3] : TEXCOORD2){
	makeScreenQuad(vertID, svPos, texcoord);
	SMAABlendingWeightCalculationVS(texcoord, pixcoord, offset);
}
