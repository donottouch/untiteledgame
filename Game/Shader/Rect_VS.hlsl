
#include "lib/ShaderStructures.hlsl"

ConstantBuffer<RectVertexParams> pCB : register(b0);
ConstantBuffer<RTMetrics> rtmCB : register(b2);
#define RT_METRICS rtmCB.rtmetrics
#include "lib/ShaderLib.hlsl"

BlitPSInput main(uint vertID : SV_VertexID)
{
	//move to CPU?
	const float2 offsetUV = PixToUVRTM(pCB.offsetPix);
	const float2 sizeUV = PixToUVRTM(pCB.sizePix);
	const float2 borderUV = PixToUVRTM(uint2(pCB.borderPix, pCB.borderPix));
	const float2 posUV = pCB.anchorUV + offsetUV;
	//posClip
	const float2 xy0 = UVToClip(posUV - borderUV);//input.pos - pCB.dstBorder;
	const float2 xy1 = UVToClip(posUV + borderUV + sizeUV);//input.pos + pCB.dstBorder + pCB.textScale * input.glyph.zw;//float2(pCB.textScale * input.glyph.z, pCB.textSize);
	//TODO handle border
	const float2 uv0 = float2(0, 0);// input.glyph.xy - pCB.srcBorder;
	const float2 uv1 = float2(1, 1);//input.glyph.xy + pCB.srcBorder + input.glyph.zw;

	float2 uv = float2(vertID & 1, (vertID >> 1) & 1);

	BlitPSInput output = (BlitPSInput)0;
	output.position = float4(lerp(xy0, xy1, uv), 0, 1);
	output.uv = lerp(uv0, uv1, uv);
	return output;
}