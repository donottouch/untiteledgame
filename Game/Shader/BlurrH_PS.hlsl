struct BloomParameters {
	float strength;
};
ConstantBuffer<BloomParameters> BloomParametersCB : register(b0);
Texture2D<float4> HDRTexture : register(t0);


float4 main(float4 Position : SV_Position) : SV_Target0
{
	const float weight[5] = { 0.227027, 0.1945946, 0.1216216, 0.054054, 0.016216 };

	uint width, height, levels;
	HDRTexture.GetDimensions(0, width, height, levels);

	int3 texCoord = int3((int2)Position.xy,0);
	float4 HDR = HDRTexture.Load(texCoord) * weight[0];
	int i = 1;
	for (; i < 5; ++i) {
		HDR += HDRTexture.Load(texCoord + int3(i, 0, 0)) * weight[i];
		HDR += HDRTexture.Load(texCoord - int3(i, 0, 0)) * weight[i];
	}

	return HDR;
}