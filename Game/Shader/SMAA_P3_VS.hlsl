//PAss 3: Neighborhood Blending
//Vertex Shader

#include "SMAA_INI.hlsl"
#include "SMAA.hlsl"
#include "lib/ShaderLib.hlsl"

void main(uint vertID : SV_VertexID,
			out float4 svPos : SV_POSITION,
			out float2 texcoord : TEXCOORD2,
			out float4 offset : TEXCOORD1){
	makeScreenQuad(vertID, svPos, texcoord);
	SMAANeighborhoodBlendingVS(texcoord, offset);
}
