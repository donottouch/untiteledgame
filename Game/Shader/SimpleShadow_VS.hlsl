
#include "ShaderStructures.hlsl"

SimplePSInput main(SimpleVSInput IN){
	SimplePSInput OUT = (SimplePSInput)0;
	
	float4 pos = float4(input.pos, 1.0f);
	OUT.uv = pos.xy; //TODO

	pos = mul(pos, model);
	OUT.wPos = pos.xyz;

	pos = mul(pos, view);
	pos = mul(pos, projection);
	OUT.pos = pos;

	return OUT;
}