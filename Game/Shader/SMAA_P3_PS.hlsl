//PAss 3: Neighborhood Blending
//Pixel Shader

Texture2D<float4> colorTex : register (t0);
Texture2D<float4> blendTex : register (t1);
#define RT_MAIN colorTex

#include "SMAA_INI.hlsl"
#include "SMAA.hlsl"

float4 main(float4 position : SV_POSITION,
			float2 texcoord : TEXCOORD2,
			float4 offset : TEXCOORD1) : SV_Target0{
	return SMAANeighborhoodBlendingPS(texcoord, offset, colorTex, blendTex);
}
