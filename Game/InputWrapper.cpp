#include "pch.h"
#include "InputWrapper.h"

using namespace Windows::UI::Core;
using namespace Windows::UI::Input;
using namespace Windows::System;
using namespace Windows::Foundation;

InputEngine::InputWrapper::InputWrapper(Windows::UI::Core::CoreWindow^  window)
{
	window->PointerPressed +=
		ref new TypedEventHandler<CoreWindow^, PointerEventArgs^>(this, &InputWrapper::OnPointerPressed);

	window->PointerMoved +=
		ref new TypedEventHandler<CoreWindow^, PointerEventArgs^>(this, &InputWrapper::OnPointerMoved);

	window->PointerReleased +=
		ref new TypedEventHandler<CoreWindow^, PointerEventArgs^>(this, &InputWrapper::OnPointerReleased);

	window->PointerExited +=
		ref new TypedEventHandler<CoreWindow^, PointerEventArgs^>(this, &InputWrapper::OnPointerExited);

	window->KeyDown +=
		ref new TypedEventHandler<CoreWindow^, KeyEventArgs^>(this, &InputWrapper::OnKeyDown);

	window->KeyUp +=
		ref new TypedEventHandler<CoreWindow^, KeyEventArgs^>(this, &InputWrapper::OnKeyUp);

	window->PointerWheelChanged +=
		ref new TypedEventHandler<CoreWindow^, PointerEventArgs^>(this, &InputWrapper::OnWheelChanged);

}

void InputEngine::InputWrapper::OnPointerPressed(Windows::UI::Core::CoreWindow ^ sender, Windows::UI::Core::PointerEventArgs ^ args)
{
	//set last click on screen
	//Set clicks for all rects
	//notify klicked rects
	tmpMouseClicked = args->CurrentPoint->Properties->IsLeftButtonPressed;
	if (args->CurrentPoint->Properties->IsLeftButtonPressed) {
		setKeyDown(VirtualKey::LeftButton);
	}
	if (args->CurrentPoint->Properties->IsRightButtonPressed) {
		setKeyDown(VirtualKey::RightButton);
	}
	if (args->CurrentPoint->Properties->IsMiddleButtonPressed) {
		setKeyDown(VirtualKey::MiddleButton);
	}

	mouseDeltaLast = XMFLOAT2(args->CurrentPoint->Position.X - tmpMousePosition.x, args->CurrentPoint->Position.Y - tmpMousePosition.y);
	tmpMousePosition = XMFLOAT2(args->CurrentPoint->Position.X, args->CurrentPoint->Position.Y);
	tmpClicks.emplace_back(args);
	XMStoreFloat2(&tmpMouseDeltaFrame, XMLoadFloat2(&tmpMouseDeltaFrame) + XMLoadFloat2(&mouseDeltaLast));

}

void InputEngine::InputWrapper::OnPointerMoved(Windows::UI::Core::CoreWindow ^ sender, Windows::UI::Core::PointerEventArgs ^ args)
{ 
	//append movemnent on screen
	//append movement onrects
	//notify rects

	//tmpMouseClicked = args->CurrentPoint->Properties->IsLeftButtonPressed;
	mouseDeltaLast = XMFLOAT2(args->CurrentPoint->Position.X - tmpMousePosition.x, args->CurrentPoint->Position.Y - tmpMousePosition.y);
	tmpMousePosition = XMFLOAT2(args->CurrentPoint->Position.X, args->CurrentPoint->Position.Y);
	tmpClicks.emplace_back(args);
	XMStoreFloat2(&tmpMouseDeltaFrame, XMLoadFloat2(&tmpMouseDeltaFrame) + XMLoadFloat2(&mouseDeltaLast));

}

void InputEngine::InputWrapper::OnPointerReleased(Windows::UI::Core::CoreWindow ^ sender, Windows::UI::Core::PointerEventArgs ^ args)
{
	//finish movement?
	tmpMouseClicked = args->CurrentPoint->Properties->IsLeftButtonPressed;
	if (!args->CurrentPoint->Properties->IsLeftButtonPressed) {
		setKeyUp(VirtualKey::LeftButton);
	}
	if (!args->CurrentPoint->Properties->IsRightButtonPressed) {
		setKeyUp(VirtualKey::RightButton);
	}
	if (!args->CurrentPoint->Properties->IsMiddleButtonPressed) {
		setKeyUp(VirtualKey::MiddleButton);
	}

	
	mouseDeltaLast = XMFLOAT2(args->CurrentPoint->Position.X - tmpMousePosition.x, args->CurrentPoint->Position.Y - tmpMousePosition.y);
	tmpMousePosition = XMFLOAT2(args->CurrentPoint->Position.X, args->CurrentPoint->Position.Y);
	tmpClicks.emplace_back(args);
	XMStoreFloat2(&tmpMouseDeltaFrame, XMLoadFloat2(&tmpMouseDeltaFrame) + XMLoadFloat2(&mouseDeltaLast));

}

void InputEngine::InputWrapper::OnPointerExited(Windows::UI::Core::CoreWindow ^ sender, Windows::UI::Core::PointerEventArgs ^ args)
{
	// notify game?
}

void InputEngine::InputWrapper::OnWheelChanged(Windows::UI::Core::CoreWindow ^ sender, Windows::UI::Core::PointerEventArgs ^ args) {
	tmpWheelDelta += args->CurrentPoint->Properties->MouseWheelDelta;
}

void InputEngine::InputWrapper::OnKeyDown(Windows::UI::Core::CoreWindow ^ sender, Windows::UI::Core::KeyEventArgs ^ args)
{

	//if key was aleady pushed in last fame, hold now
	if (keys[args->VirtualKey] == KeyState::Pushed) {
		tmpKeys[args->VirtualKey] = KeyState::Hold;
	}
	else {
		tmpKeys[args->VirtualKey] = KeyState::Pushed;
	}
}

void InputEngine::InputWrapper::OnKeyUp(Windows::UI::Core::CoreWindow ^ sender, Windows::UI::Core::KeyEventArgs ^ args)
{
	tmpKeys[args->VirtualKey] =  KeyState::Released;
}

void InputEngine::InputWrapper::reset()
{
	//make info of last frame public
	mouseDeltaFrame = tmpMouseDeltaFrame;
	mousePosition = tmpMousePosition;
	mouseClicked = tmpMouseClicked;
	wheelDeltaFrame = tmpWheelDelta;
	keys = tmpKeys;

	//reset temps
	//tmpMousePosition = XMFLOAT2();
	tmpMouseDeltaFrame = XMFLOAT2();
	tmpWheelDelta = 0;
	//tmpMouseClicked = false;
	tmpKeys.clear();

	clicks = tmpClicks;
	tmpClicks.clear(); 

	
	for each (auto k in keys)
	{
		if (k.second == KeyState::Hold) {
			tmpKeys[k.first] = KeyState::Hold;
		}
		if (k.second == KeyState::Pushed) {
			tmpKeys[k.first] = KeyState::Pushed;
		}
	}
	
}

void InputEngine::InputWrapper::setKeyDown(VirtualKey key) {
	if (keys[key] == KeyState::Pushed) {
		tmpKeys[key] = KeyState::Hold;
	}
	else {
		tmpKeys[key] = KeyState::Pushed;
	}
}

void InputEngine::InputWrapper::setKeyUp(VirtualKey key) {
	tmpKeys[key] = KeyState::Released;
}
