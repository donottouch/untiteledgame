﻿#pragma once

#include <wrl.h>
#include <wrl/client.h>
#include <dxgi1_4.h>
#include <d3d12.h>
#include "Common\d3dx12.h"
#include <pix.h>
#include <DirectXColors.h>
#include <DirectXMath.h>
//#include <math.h>
//#include <algorithm>
#include <memory>
#include <agile.h>
#include <concrt.h>

#include <list> 
#include <map>
#include <vector>
#include <unordered_set> 
#include <iterator> 
#include <sstream>
#include <iostream>
#include <fstream>
#include <algorithm>

#if defined(_DEBUG)
#include <dxgidebug.h>
#endif


// The min/max macros conflict with like-named member functions.
// Only use std::min and std::max defined in <algorithm>.
#ifdef min
#undef min
#endif

#ifdef max
#undef max
#endif