﻿#pragma once

#define CREATE_HDR_RESOURCES

namespace DX
{
	static const FLOAT clearColor[] = { 0.002f,0.002f,0.004f,1.0f };
	static const FLOAT clearColorBlack[] = { 0.0f,0.0f,0.0f,0.0f };
	static const FLOAT clearColorNormalDepth[] = { 0.0f,0.0f,0.0f,1.0f };
	static const FLOAT clearColorDepth[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	static const UINT c_frameCount = 3;		// Dreifache Pufferung verwenden.
	enum SAMPLER {
		SAMPLER_BILINEAR,
		SAMPLER_TRILINEAR,
		SAMPLER_NN,
		SAMPLER_AMOUNT
	};
	// for HDR pipeline
	enum HDR_RTV {
		HDR_RTV_MAIN,
		HDR_RTV_MAIN_NORMAL_DEPTH,
		HDR_RTV_FOG,
		HDR_RTV_SSAA,
		HDR_RTV_SMAA_EDGE,
		HDR_RTV_SMAA_WEIGHT,
		HDR_RTV_SMAA,
		HDR_RTV_FILTER,
		HDR_RTV_BLOOM1,
		HDR_RTV_BLOOM2,
		HDR_RTV_TONEMAPPING,
		HDR_RTV_AMOUNT,
	};
	enum SHADOW_RTV {
		SHADOW_RTV_MAIN,
		SHADOW_RTV_CUBE_1,
//		SHADOW_RTV_CUBE_PX,
//		SHADOW_RTV_CUBE_NX,
//		SHADOW_RTV_CUBE_PY,
//		SHADOW_RTV_CUBE_NY,
//		SHADOW_RTV_CUBE_PZ,
//		SHADOW_RTV_CUBE_NZ,
		SHADOW_RTV_AMOUNT
	};
	static const UINT c_AdditionalSrvDescriptorSpace = 32;
	template<class T>
	static inline void UpdateConstantBuffer(UINT8* destination, T* data) {
		memcpy(destination, data, sizeof(*data));
	}
	// Steuert alle DirectX-Geräteressourcen.
	class DeviceResources
	{
	public:
		DeviceResources(DXGI_FORMAT backBufferFormat = DXGI_FORMAT_B8G8R8A8_UNORM, DXGI_FORMAT depthBufferFormat = DXGI_FORMAT_D32_FLOAT);
		void SetWindow(Windows::UI::Core::CoreWindow^ window);
		void SetLogicalSize(Windows::Foundation::Size logicalSize);
		void SetCurrentOrientation(Windows::Graphics::Display::DisplayOrientations currentOrientation);
		void SetDpi(float dpi);
		void ValidateDevice();
		void Present();
		void WaitForGpu();

		void WaitForGPUCompute();

		// Die Größe des Renderziels in Pixeln.
		Windows::Foundation::Size	GetOutputSize() const				{ return m_outputSize; }
		DirectX::XMFLOAT4			GetRTMetrics() const { return DirectX::XMFLOAT4(1 / m_outputSize.Width, 1 / m_outputSize.Height, m_outputSize.Width, m_outputSize.Height); }

		// Die Größe des Renderziels in DIPs.
		Windows::Foundation::Size	GetLogicalSize() const				{ return m_logicalSize; }

		float						GetDpi() const						{ return m_effectiveDpi; }
		bool						IsDeviceRemoved() const				{ return m_deviceRemoved; }

		// D3D-Accessoren.
		ID3D12Device*				GetD3DDevice() const				{ return m_d3dDevice.Get(); }
		IDXGISwapChain3*			GetSwapChain() const				{ return m_swapChain.Get(); }
		ID3D12Resource*				GetRenderTarget() const				{ return m_renderTargets[m_currentFrame].Get(); }
		ID3D12Resource*				GetDepthStencil() const				{ return m_depthStencil.Get(); }
		ID3D12CommandQueue*			GetCommandQueue() const				{ return m_commandQueue.Get(); }
		ID3D12CommandAllocator*		GetCommandAllocator() const			{ return m_commandAllocators[m_currentFrame].Get(); }
		DXGI_FORMAT					GetBackBufferFormat() const			{ return m_backBufferFormat; }
		DXGI_FORMAT					GetDepthBufferFormat() const		{ return m_depthBufferFormat; }
		D3D12_VIEWPORT				GetScreenViewport() const			{ return m_screenViewport; }
		DirectX::XMFLOAT4X4			GetOrientationTransform3D() const	{ return m_orientationTransform3D; }
		UINT						GetCurrentFrameIndex() const		{ return m_currentFrame; }
		DXGI_FORMAT					GetShadowBufferFormat() const		{ return m_ShadowrtvFormat; }

		ID3D12CommandQueue*			GetComputeCommandQueue() const		{ return m_ComputeCommandQueue.Get(); }
		ID3D12CommandAllocator*		GetComputeCommandAllocator() const	{ return m_ComputeCommandAllocator.Get(); }



		CD3DX12_CPU_DESCRIPTOR_HANDLE GetRenderTargetView() const
		{
			return CD3DX12_CPU_DESCRIPTOR_HANDLE(m_rtvHeap->GetCPUDescriptorHandleForHeapStart(), m_currentFrame, m_rtvDescriptorSize);
		}
		CD3DX12_CPU_DESCRIPTOR_HANDLE GetDepthStencilView() const
		{
			return CD3DX12_CPU_DESCRIPTOR_HANDLE(m_dsvHeap->GetCPUDescriptorHandleForHeapStart());
		}
		//sampler
		CD3DX12_CPU_DESCRIPTOR_HANDLE GetSamplerView(SAMPLER n) const
		{
			return CD3DX12_CPU_DESCRIPTOR_HANDLE(m_SamplerHeap->GetCPUDescriptorHandleForHeapStart(), n, m_SamplerDescriptorSize);
		}
		CD3DX12_GPU_DESCRIPTOR_HANDLE GetSamplerViewGPU(SAMPLER n) const
		{
			return CD3DX12_GPU_DESCRIPTOR_HANDLE(m_SamplerHeap->GetGPUDescriptorHandleForHeapStart(), n, m_SamplerDescriptorSize);
		}
		ID3D12DescriptorHeap* GetSamplerDescriptorHeap() {
			return m_SamplerHeap.Get();
		}
		ID3D12DescriptorHeap* GetSRVDescriptorHeap() {
			return m_HDRsrvHeap.Get();
		}
		//HDR
#ifdef CREATE_HDR_RESOURCES
		ID3D12Resource*				GetHDRRenderTarget(HDR_RTV n) const { return m_HDRRenderTargets[n].Get(); }
		CD3DX12_CPU_DESCRIPTOR_HANDLE GetHDRRenderTargetView(HDR_RTV n) const
		{
			return CD3DX12_CPU_DESCRIPTOR_HANDLE(m_HDRrtvHeap->GetCPUDescriptorHandleForHeapStart(), n, m_HDRrtvDescriptorSize);
		}
		CD3DX12_CPU_DESCRIPTOR_HANDLE GetHDRShaderResourceView(HDR_RTV n) const
		{
			return CD3DX12_CPU_DESCRIPTOR_HANDLE(m_HDRsrvHeap->GetCPUDescriptorHandleForHeapStart(), n, m_HDRsrvDescriptorSize);
		}
		CD3DX12_GPU_DESCRIPTOR_HANDLE GetHDRRenderTargetViewGPU(HDR_RTV n) const
		{
			return CD3DX12_GPU_DESCRIPTOR_HANDLE(m_HDRrtvHeap->GetGPUDescriptorHandleForHeapStart(), n, m_HDRrtvDescriptorSize);
		}
		CD3DX12_GPU_DESCRIPTOR_HANDLE GetHDRShaderResourceViewGPU(HDR_RTV n) const
		{
			return CD3DX12_GPU_DESCRIPTOR_HANDLE(m_HDRsrvHeap->GetGPUDescriptorHandleForHeapStart(), n, m_HDRsrvDescriptorSize);
		}
		//return: offset from heap start or -1 if no more descripors available
		INT RequestSrvDescriptor(Microsoft::WRL::ComPtr<ID3D12DescriptorHeap>* heap) {
			INT descriptorOffset = GetNextSRVDescriptor();
			if (descriptorOffset > -1) {
				heap->Attach(m_HDRsrvHeap.Get());
				return descriptorOffset;
			}
			else return -1;
		}
		INT CreateConstantBuffer(Microsoft::WRL::ComPtr<ID3D12Resource>& resource, const UINT alignedSize, UINT8** mappedBuffer);
		CD3DX12_CPU_DESCRIPTOR_HANDLE Get_SR_CB_UA_View(UINT n) const {
			return CD3DX12_CPU_DESCRIPTOR_HANDLE(m_HDRsrvHeap->GetCPUDescriptorHandleForHeapStart(), n, m_ShadowrtvDescriptorSize);
		}
		CD3DX12_GPU_DESCRIPTOR_HANDLE Get_SR_CB_UA_ViewGPU(UINT n) const {
			return CD3DX12_GPU_DESCRIPTOR_HANDLE(m_HDRsrvHeap->GetGPUDescriptorHandleForHeapStart(), n, m_ShadowrtvDescriptorSize);
		}
#endif
		//Shadow
		D3D12_VIEWPORT				GetShadowViewport() const { return m_ShadowViewport; }
		ID3D12Resource*				GetShadowRenderTarget(SHADOW_RTV n) const { return m_ShadowRenderTargets[n].Get(); }
		ID3D12Resource*				GetShadowDepthStencil() const { return m_ShadowDepthStencil.Get(); }
		ID3D12Resource*				GetShadowCubeDepthStencil() const { return m_ShadowCubeDepthStencil.Get(); }
		CD3DX12_CPU_DESCRIPTOR_HANDLE GetShadowRenderTargetView(SHADOW_RTV n) const {
			return CD3DX12_CPU_DESCRIPTOR_HANDLE(m_ShadowrtvHeap->GetCPUDescriptorHandleForHeapStart(), n, m_ShadowrtvDescriptorSize);
		}
		CD3DX12_CPU_DESCRIPTOR_HANDLE GetShadowShaderResourceView(SHADOW_RTV n) const {
			return CD3DX12_CPU_DESCRIPTOR_HANDLE(m_HDRsrvHeap->GetCPUDescriptorHandleForHeapStart(), HDR_RTV_AMOUNT + n, m_ShadowrtvDescriptorSize);
		}
		CD3DX12_GPU_DESCRIPTOR_HANDLE GetShadowRenderTargetViewGPU(SHADOW_RTV n) const {
			return CD3DX12_GPU_DESCRIPTOR_HANDLE(m_ShadowrtvHeap->GetGPUDescriptorHandleForHeapStart(), n, m_ShadowrtvDescriptorSize);
		}
		CD3DX12_GPU_DESCRIPTOR_HANDLE GetShadowShaderResourceViewGPU(SHADOW_RTV n) const {
			return CD3DX12_GPU_DESCRIPTOR_HANDLE(m_HDRsrvHeap->GetGPUDescriptorHandleForHeapStart(), HDR_RTV_AMOUNT + n, m_ShadowrtvDescriptorSize);
		}
		CD3DX12_CPU_DESCRIPTOR_HANDLE GetShadowDepthStencilView() const {
			return CD3DX12_CPU_DESCRIPTOR_HANDLE(m_dsvHeap->GetCPUDescriptorHandleForHeapStart(), 1, m_dsvDescriptorSize);
		}
		CD3DX12_CPU_DESCRIPTOR_HANDLE GetShadowCubeDepthStencilView() const {
			return CD3DX12_CPU_DESCRIPTOR_HANDLE(m_dsvHeap->GetCPUDescriptorHandleForHeapStart(), 2, m_dsvDescriptorSize);
		}


	private:
		void CreateDeviceIndependentResources();
		void CreateDeviceResources();
		void CreateComputeResources();
		void CreateWindowSizeDependentResources();
		void CreateShadowResources();
		void UpdateRenderTargetSize();
		void MoveToNextFrame();
		DXGI_MODE_ROTATION ComputeDisplayRotation();
		void GetHardwareAdapter(IDXGIAdapter1** ppAdapter);

		INT GetNextSRVDescriptor() {
			if (m_AllocatedSrvDescriptors < c_AdditionalSrvDescriptorSpace) {
				++m_AllocatedSrvDescriptors;
				return HDR_RTV_AMOUNT + SHADOW_RTV_AMOUNT + m_AllocatedSrvDescriptors - 1;
			}
			else return -1;
		}

		UINT											m_currentFrame;

		// Direct3D-Objekte.
		Microsoft::WRL::ComPtr<ID3D12Device>			m_d3dDevice;
		Microsoft::WRL::ComPtr<IDXGIFactory4>			m_dxgiFactory;
		Microsoft::WRL::ComPtr<IDXGISwapChain3>			m_swapChain;
		Microsoft::WRL::ComPtr<ID3D12Resource>			m_renderTargets[c_frameCount];
		Microsoft::WRL::ComPtr<ID3D12Resource>			m_depthStencil;
		Microsoft::WRL::ComPtr<ID3D12DescriptorHeap>	m_rtvHeap;
		Microsoft::WRL::ComPtr<ID3D12DescriptorHeap>	m_dsvHeap; 
		Microsoft::WRL::ComPtr<ID3D12CommandQueue>		m_commandQueue;
		Microsoft::WRL::ComPtr<ID3D12CommandAllocator>	m_commandAllocators[c_frameCount];
		DXGI_FORMAT										m_backBufferFormat;
		DXGI_FORMAT										m_depthBufferFormat;
		D3D12_VIEWPORT									m_screenViewport;
		UINT											m_rtvDescriptorSize;
		UINT											m_dsvDescriptorSize;
		bool											m_deviceRemoved;

		//Sampler
		Microsoft::WRL::ComPtr<ID3D12DescriptorHeap>	m_SamplerHeap;
		UINT											m_SamplerDescriptorSize;

		//HDR resources
		Microsoft::WRL::ComPtr<ID3D12Resource>			m_HDRRenderTargets[HDR_RTV_AMOUNT];
		DXGI_FORMAT										m_HDRrtvFormat;
		Microsoft::WRL::ComPtr<ID3D12DescriptorHeap>	m_HDRrtvHeap;
		UINT											m_HDRrtvDescriptorSize;
		DXGI_FORMAT										m_HDRsrvFormat;
		Microsoft::WRL::ComPtr<ID3D12DescriptorHeap>	m_HDRsrvHeap;
		UINT											m_HDRsrvDescriptorSize;

		UINT											m_AllocatedSrvDescriptors;

		//shadow mapping
		bool											m_RecreateShadowResources;
		Microsoft::WRL::ComPtr<ID3D12Resource>			m_ShadowRenderTargets[SHADOW_RTV_AMOUNT];
		Microsoft::WRL::ComPtr<ID3D12Resource>			m_ShadowDepthStencil;
		Microsoft::WRL::ComPtr<ID3D12Resource>			m_ShadowCubeDepthStencil;
		DXGI_FORMAT										m_ShadowrtvFormat;
		Microsoft::WRL::ComPtr<ID3D12DescriptorHeap>	m_ShadowrtvHeap;
		UINT											m_ShadowrtvDescriptorSize;
		UINT											m_ShadowrtvResolution;
		D3D12_VIEWPORT									m_ShadowViewport;

		// CPU/GPU-Synchronisierung.
		Microsoft::WRL::ComPtr<ID3D12Fence>				m_fence;
		UINT64											m_fenceValues[c_frameCount];
		HANDLE											m_fenceEvent;

		//compute pipeline
		Microsoft::WRL::ComPtr<ID3D12CommandQueue>		m_ComputeCommandQueue;
		Microsoft::WRL::ComPtr<ID3D12CommandAllocator>	m_ComputeCommandAllocator;
		Microsoft::WRL::ComPtr<ID3D12Fence>				m_ComputeFence;
		UINT64											m_ComputeFenceValue;
		HANDLE											m_ComputeFenceEvent;

		// Zwischengespeicherter Verweis auf das Fenster.
		Platform::Agile<Windows::UI::Core::CoreWindow>	m_window;

		// Zwischengespeicherte Geräteeigenschaften.
		Windows::Foundation::Size						m_d3dRenderTargetSize;
		Windows::Foundation::Size						m_outputSize;
		Windows::Foundation::Size						m_logicalSize;
		Windows::Graphics::Display::DisplayOrientations	m_nativeOrientation;
		Windows::Graphics::Display::DisplayOrientations	m_currentOrientation;
		float											m_dpi;

		// Dies ist der DPI-Wert, der an die Anwendung zurückgegeben wird. Es wird berücksichtigt, ob die App Bildschirme mit hoher Auflösung unterstützt.
		float											m_effectiveDpi;

		// Transformationen für die Bildschirmausrichtung.
		DirectX::XMFLOAT4X4								m_orientationTransform3D;
	};
}
