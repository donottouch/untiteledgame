#pragma once

#ifndef RT_COORD_CONV
#define RT_COORD_CONV
#include "pch.h"

namespace Renderer {
	static inline DirectX::XMFLOAT2 RTM(DirectX::XMFLOAT4 rtMetrics) {
		return DirectX::XMFLOAT2(rtMetrics.z, rtMetrics.w);
	}
	static inline DirectX::XMFLOAT2 iRTM(DirectX::XMFLOAT4 rtMetrics) {
		return DirectX::XMFLOAT2(rtMetrics.x, rtMetrics.y);
	}

	static inline DirectX::XMINT2 UVToPix(DirectX::XMFLOAT2 uv, DirectX::XMFLOAT4 rtMetrics) {
		DirectX::XMVECTOR v_uv = DirectX::XMLoadFloat2(&uv);
		DirectX::XMFLOAT2 rtm = RTM(rtMetrics);
		DirectX::XMVECTOR v_rtm = DirectX::XMLoadFloat2(&rtm);
		DirectX::XMINT2 pix;
		DirectX::XMStoreSInt2(&pix, DirectX::XMVectorMultiply(v_uv,v_rtm));
		return pix;
	}
}

#endif