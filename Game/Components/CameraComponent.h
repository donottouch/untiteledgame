#pragma once

#ifndef CAMERA_COMPONENT
#define CAMERA_COMPONENT

#include "Component.h"
#include "pch.h"

namespace Renderer {

	class CameraComponent : public Component {
	public:
		CameraComponent();
		~CameraComponent();

		//Events
		void OnUpdate(float dT);
		void OnStart();
		void OnDestroy();
		void OnCollision(GameObject::CollisionObject collision);

		XMFLOAT4X4 GetViewMatrix()const;
		//void GetProjectionMatric()const; //golbal for now, handeled by render engine


	};
}
#endif