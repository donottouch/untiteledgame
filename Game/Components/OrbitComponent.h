#pragma once
#include "Component.h"

class OrbitManager;

//Describes an orbit
class OrbitComponent : public Component
{
public:
	OrbitComponent(double _e, double _a, double _b, double _i,
		double _Q, double _w, double _v, double _T, double _n,
		OrbitManager* _orbitManager) {
		e = _e;
		a = _a;
		b = _b;
		i = _i;
		Q = _Q;
		w = _w;
		v = _v;
		T = _T;
		n = _n;

		ra = a * (1 + e);
		rp = a * (1 - e);

		orbitManager = _orbitManager;
	}

	OrbitComponent(GameObject* _rotationCenter,
		double _e, double _a, double _b, double _i,
		double _Q, double _w, double _v, double _T, double _n) :
		e(_e), a(_a), b(_b), i(_i), Q(_Q), w(_w), v(_v), T(_T), n(_n) {
		rotationCenter = _rotationCenter;
	}

	OrbitComponent(GameObject* _rotationCenter, GameObject* periapsisCenterGO, GameObject* periapsisGO,
		double _e, double _a, double _b, double _i,
		double _Q, double _w, double _v, double _T, double _n) :
		e(_e), a(_a), b(_b), i(_i), Q(_Q), w(_w), v(_v), T(_T), n(_n) {
		rotationCenter = _rotationCenter;
		periapsisCenter = periapsisCenterGO;
		periapsis = periapsisGO;
	}

	OrbitComponent(OrbitManager* _orbitManager) {
		orbitManager = _orbitManager;
	}

	~OrbitComponent();


	const double OrbitComponent::g = 9.81; // acceleration due to gravity at sea level
	// set these values before orbit computation
	const double OrbitComponent::G = 6.67408e-11; // gravitational constant

	const double OrbitComponent::Mmass = 5.972e24; // mass of central body (currently the earths mass)
	const double OrbitComponent::mu = G * Mmass;

	double scalingFactor = 1.0 / 3000000.0; // uesed to scale the orbit
	double timeScalingFactor = 100;


	double e = 0; // eccentricity
	double a = 0; // semimajor axis
	double b = 0; // semiminor axis

	//(vertical tilt of the ellipse with respect to the reference plane, measured at the ascending node)
	double i = 0; // inclination; THIS SHOULD ALWAYS STAY 0 -> everything stays in 2D
	double Q = 0; // Longitude of the ascending node; horizontally orients the ascending node of the ellipse
	double w = 0; // Argument of periapsis defines the orientation of the ellipse in the orbital plane
	double v = 0; // true anomaly; orbit angle; describes position in orbit
	// the true anomaly is measured from periapsis

	double T = 1; // Orbital period
	double n = 1; // Mean orbit motion

	double t = 0; // Time in current orbit

	double ra = 1; //distance to central body at apoapsis
	double rp = 1; //distance to central body at periapsis



	void OnUpdate(float dT);
	void OnStart();
	void OnDestroy();
	void OnCollision(GameObject::CollisionObject collision);

	void setRotationCenter(GameObject* rotCenter);
	GameObject* getRotationCenter();

	void setPeriapsisCenterGO(GameObject* periapsisCenterGO);
	GameObject* getPeriapsisCenterGO();

	void setPeriapsisGO(GameObject* periapsisGO);
	GameObject* getPeriapsisGO();

	void setOrbitManager(OrbitManager* _orbitManager) { orbitManager = _orbitManager; }

private:
	GameObject* rotationCenter;
	GameObject* periapsisCenter;
	GameObject* periapsis;

	OrbitManager* orbitManager;
};


