#include "pch.h"
#include "Components/Behaviours/OrbitManager.h"

#include <random>

OrbitManager::OrbitManager(Renderer::Mesh* _MeshSphere, Renderer::Mesh* _MeshPlane, Renderer::Mesh* _AsteroidMesh, Renderer::Mesh* _BaseMesh, Renderer::Material* _MatWireframe,
	Renderer::Material* _MatFlat, Renderer::Material* _MatOrbit) {
	m_MeshSphere = _MeshSphere;
	m_MeshPlane = _MeshPlane;
	m_AsteroidMesh = _AsteroidMesh;
	m_BaseMesh = _BaseMesh;
	m_MatWireframe = _MatWireframe;
	m_MatFlat = _MatFlat;
	m_MatOrbit = _MatOrbit;
}

OrbitManager::~OrbitManager() {
}

void OrbitManager::OnStart() {
	m_gameMain = m_master->getGameMain();
	ConfigParser::Config* config = m_gameMain->getConfig();
	config->GetInt("iAsteroidMax", &maxAsteroids);
	config->GetInt("iAsteroidRadiusMin", &minOrbit);
	config->GetInt("iAsteroidRadiusMax", &maxOrbit);
	config->GetFloat("fAsteroidExcentricityMax", &m_AsteroidExcentricityMax);
	config->GetFloat("fAsteroidSizeMin", &m_AsteroidSizeMin);
	config->GetFloat("fAsteroidSizeMax", &m_AsteroidSizeMax);

	config->GetColor("cPlayerColorDiffuse", &m_PlayerColorDiffuse);
	config->GetColor("cPlayerColorEmissive", &m_PlayerColorEmissive);

	// create time warp component
	timeWarper = new TimeWarp();
	m_master->addComponent(timeWarper);

	// create two arbitrary asteroids
	float speedMul = 1.0f;
	//createAsteroid(XMFLOAT3(6060860, 24750457, 0), XMFLOAT3(-2142 * speedMul, 1186 * speedMul, 0 * speedMul), 0.3, m_SceneRoot);

	//speedMul = 1.1f;
	//createAsteroid(XMFLOAT3(6000000, 0, 0), XMFLOAT3(0 * speedMul, 8149.5151432462535 * speedMul, 0 * speedMul), 0.5, m_master);
	createPlayer(XMFLOAT3(6000000, 0, 0), XMFLOAT3(0 * speedMul, 8149.5151432462535f * speedMul, 0 * speedMul), 0.2f);

	// spawn initial asteroids
	spawnRandomAsteroid(4);

	
	// TODO why is this line here
	srand(time(NULL));

}

void OrbitManager::OnUpdate(float dT) {
	float DTWarped = dT * timeWarper->getTimeWarpFactor();

	//count asteroids
	int count = 0;
	for each (GameObject* child in m_master->getAllChildren())
	{
		if (child->getName() == "oBContainer")
		{
			count++;
		}
	}

	if (count < maxAsteroids) { //only cheeck if necessarry
		float randomNumber = (rand() % 10000 + 1) / 10000.0f; //rand btw 0 and 1
		float limit = (cos(count / ((float) maxAsteroids) * g_XMPi[0])+1) * DTWarped * 0.5f; //make limit bigger if close to 0, but scale with time to make independent of framerate
		if (limit > randomNumber)
		{
			spawnRandomAsteroid(1);
		}
	}

	// advance orbits
	advanceAllOrbits(orbitComps, DTWarped);
}

void OrbitManager::OnDestroy() {

}

void OrbitManager::OnCollision(GameObject::CollisionObject collision) {
}

void OrbitManager::addOrbitComponent(OrbitComponent* obComp) {
	orbitComps.push_back(obComp);
}
void OrbitManager::removeOrbitComponent(OrbitComponent* obComp) {
	// if orbitComps contains obComp
	if (std::find(orbitComps.begin(), orbitComps.end(), obComp) != orbitComps.end()) {
		orbitComps.remove(dynamic_cast<OrbitComponent*>(obComp));
	}
}

#pragma region Orbit_Handling
// ORBIT RELATED STUFF
OrbitComponent* OrbitManager::calculateOrbitalElementsFromVelocity(XMFLOAT3 posF3, XMFLOAT3 velocityF3) {
	OrbitComponent* orbitElements = new OrbitComponent(this);
	calculateOrbitalElementsFromVelocity(orbitElements, posF3, velocityF3);

	return orbitElements;
}

void OrbitManager::calculateOrbitalElementsFromVelocity(OrbitComponent* orbitElements, XMFLOAT3 posF3, XMFLOAT3 velocityF3) {
	// based on https://space.stackexchange.com/questions/1904/how-to-programmatically-calculate-orbital-elements-using-position-velocity-vecto
	// and https://github.com/RazerM/orbital/blob/0.7.0/orbital/utilities.py#L252

	XMVECTOR r = XMLoadFloat3(&posF3); // position in orbit
	XMVECTOR v = XMLoadFloat3(&velocityF3); // velocity relative to main body

	XMVECTOR h = XMVector3Cross(r, v); //angular momentum
	XMVECTOR n = XMVector3Cross(XMVectorSet(0, 0, 1, 0), h); // node vector
	double normN = XMVector3Length(n).m128_f32[0];

	double velLengthSq = XMVector3LengthSq(v).m128_f32[0];
	double posLength = XMVector3Length(r).m128_f32[0];

	// eccentricity vector, from apoapsis to periapsis, length is eccentricity
	XMVECTOR eVec = XMVector3Cross(v, h) / M_MU - r / posLength;
	double e = XMVector3Length(eVec).m128_f32[0]; // eccentricity
	double energy = (velLengthSq / 2) - (M_MU / posLength);

	double a = 1; // semimajor axis
	double b = 1; // semiminor axis

	if (abs(e - 1) > 0) {
		a = -M_MU / (2 * energy);
		b = a * (1 - e * e);
	}
	else {
		b = XMVector3LengthSq(h).m128_f32[0] / M_MU;
		a = INFINITE;
	}

	// inclination
	double i = safeAcos(h.m128_f32[2] / XMVector3Length(h).m128_f32[0]);

	// used for checks
	double smallNumber = 1e-15;

	// arg_pe = w
	// raan = Q
	double Q; // right ascencion of ascending node
	double w; // argument of periapsis (angle measured from the ascending node to the periapsis)

	//XMVECTOR eDir = XMVector3Normalize(eVec);



	if (abs(i) < smallNumber) {
		// For non - inclined orbits, raan is undefined;
		Q = 0;
		if (abs(e) < smallNumber) {
			//For circular orbits, place periapsis at ascending node by convention
			w = 0;
		}
		else {
			//Argument of periapsis is the angle between eccentricity vector and its x component
			w = atan2(eVec.m128_f32[1], eVec.m128_f32[0]);//acos(eVec.m128_f32[0] / e);
		}
	}
	else {
		// Right ascension of ascending node is the angle between the node vector and its x component.
		Q = safeAcos(n.m128_f32[0] / normN);
		if (v.m128_f32[1] < 0) {
			Q = 2 * M_PI - Q;
		}

		// Argument of periapsis is angle between node and eccentricity vectors
		w = safeAcos(dot(n, eVec) / (normN * e));

		// i dont really know which values go give Q and w here
		if (isnan(Q)) {
			Q = 0;
		}
		if (isnan(w)) {
			w = 0;
		}
	}

	// i use f for this variable since v is already taken by the velocity
	double f; // true anomaly
	// compute true anomaly
	if (abs(e) < smallNumber) {
		if (abs(i) < smallNumber) {

			f = safeAcos(posF3.x / posLength);

			if (velocityF3.x > 0) {
				f = 2 * M_PI - f;
			}
		}
		else {
			//True anomaly is angle between node vector and position vector.
			f = safeAcos(dot(n, r) / (normN * posLength));

			//if (dot(n, v).m128_f32[0] > 0) {
			if (posF3.z < 0) {
				f = 2 * M_PI - f;
			}
		}
	}
	else {
		if (abs(i) < smallNumber) {
			if (h.m128_f32[3] < 0) //clockwise orbits
				w = 2 * M_PI - w;
		}
		else if (eVec.m128_f32[3] < 0) {
			w = 2 * M_PI - w;
		}
		//True anomaly is angle between eccentricity vector and position vector.
		f = safeAcos(dot(eVec, r) / (e * posLength));

		if (dot(r, v) < 0) {
			f = 2 * M_PI - f;
		}
	}

	// reduce size of a since a^3 creates a ridiculous integeroverflow...
	double smalla = a / 1000;
	double smallm = M_MU / (1000 * 1000 * 1000);
	double T = 2 * M_PI / sqrt(smallm / (smalla * smalla * smalla)); // orbital period
	double mOM = (2 * M_PI) / T; // mean orbit motion

	orbitElements->e = e;
	orbitElements->a = a;
	orbitElements->b = b;
	orbitElements->i = i;
	orbitElements->Q = Q;
	orbitElements->w = w;
	orbitElements->v = f;
	orbitElements->T = T;
	orbitElements->n = mOM;
	orbitElements->ra = a * (1 + e);
	orbitElements->rp = a * (1 - e);

	//Eccentric anomaly
	double alpha = tan(f / 2) / sqrt((1 + e) / (1 - e));
	double radius = (a * (1 - e * e)) / (1 + e * cos(f));

	double E = safeAcos((1 - (radius / a)) / e); // computes acos(paul)

	// the results of acos are only correct in the first and second quarter of the circle.
	// this corrects for the other half
	if (f > M_PI) {
		E = M_PI + (M_PI - E);
	}

	// recomputes f to check if E is correct
	//double correctedF = 2 * atan2(sqrt(1 + e) * sin(E / 2), sqrt(1 - e) * cos(E / 2));

	//Compute time in orbit since periapsis
	double t = (E - (e * sin(E))) / mOM;
	orbitElements->t = t;
}

void OrbitManager::computeOrbitStateVectorsfromOrbitElem(OrbitComponent* orbitElem, XMFLOAT3* pos, XMFLOAT3* vel) {
	// based on https://space.stackexchange.com/questions/14095/what-is-this-algorithm-to-calculate-orbital-state-vectors
	// based on a doc file from a stackoverflow question i cant find right now.
	double w = orbitElem->w; // Argument of periapsis
	double Q = orbitElem->Q; // Longitude of the ascending node;
	double i = orbitElem->i; // inclination
	double a = orbitElem->a; // semimajor axis
	double e = orbitElem->e; // ecentricity
	double v = orbitElem->v; // true anomaly
	double r = (a * (1 - e * e)) / (1 + e * cos(v)); // radius at true anomaly

	double M = orbitElem->n * orbitElem->t; // mean anomaly
	double E = computeEccentricAnomalyFromMeanAnomaly(M, e); // Eccentric anomaly

	double h = sqrt(M_MU * a * (1 - e * e)); // angular momentum

	double x = r * (cos(Q) * cos(w + v) - sin(Q) * sin(w + v) * cos(i));
	double y = r * (sin(Q) * cos(w + v) + cos(Q) * sin(w + v) * cos(i));
	double z = r * (sin(i) * sin(w + v));

	double herp = (h * e) / (r * a * (1 - e * e));
	double hr = h / r;

	double vx = x * herp * sin(v) - hr * (cos(Q) * sin(w + v) + sin(Q) * cos(w + v) * cos(i));
	double vy = y * herp * sin(v) - hr * (sin(Q) * sin(w + v) - cos(Q) * cos(w + v) * cos(i));
	double vz = z * herp * sin(v) + hr * (sin(i) * cos(w + v));

	pos->x = (float)x;
	pos->y = (float)y;
	pos->z = (float)z;

	vel->x = (float)vx;
	vel->y = (float)vy;
	vel->z = (float)vz;

}

double OrbitManager::computeMeanAnomaly(OrbitComponent* orbitElem) {
	return orbitElem->n * orbitElem->t;
}

double OrbitManager::computeEccentricAnomalyFromMeanAnomaly(double M, double e) {
	double E = M; // eccentric anomaly

	// Copied from https://github.com/RazerM/orbital/blob/0.7.0/orbital/utilities.py
	double Mnorm = fmod(M, 2 * M_PI);
	double tolerance = 1e-14;
	double E0 = M + (-1 / 2 * e * e * e + e + (e * e + 3 / 2 * cos(M) * e * e * e) * cos(M)) * sin(M);
	double dE = tolerance + 1;
	int count = 0;
	while (dE > tolerance) {
		double t1 = cos(E0);
		double t2 = -1 + e * t1;
		double t3 = sin(E0);
		double t4 = e * t3;
		double t5 = -E0 + t4 + Mnorm;
		double t6 = t5 / (1 / 2 * t5 * t4 / t2 + t2);
		E = E0 - t5 / ((1 / 2 * t3 - 1 / 6 * t1 * t6) * e * t6 + t2);
		dE = abs(E - E0);
		E0 = E;
		count += 1;
		if (count == 999) {
			break;
		}
	}

	return E;
}

double OrbitManager::computeOrbitRadiusTrueAnomaly(double dT, OrbitComponent* orbitElem) {
	dT = dT * orbitElem->timeScalingFactor;

	double newT = orbitElem->t + dT;
	if (newT > orbitElem->T) {
		newT = newT - orbitElem->T;
	}

	orbitElem->t = newT;

	double M = orbitElem->n * orbitElem->t; // mean anomaly

	double E = computeEccentricAnomalyFromMeanAnomaly(M, orbitElem->e);

	// compute true anomaly
	orbitElem->v = 2 * atan2(sqrt(1 + orbitElem->e) * sin(E / 2), sqrt(1 - orbitElem->e) * cos(E / 2));

	// compute radius of orbiting body (distance between central body and orbiting body)
	return ((orbitElem->a * (1 - orbitElem->e * orbitElem->e)) / (1 + orbitElem->e * cos(orbitElem->v))) * orbitElem->scalingFactor;
}

void OrbitManager::advanceAllOrbits(list<OrbitComponent*> orbits, double dT) {
	std::list<OrbitComponent*>::iterator it;
	for (it = orbits.begin(); it != orbits.end(); ++it) {
		advanceOrbit(*it, dT);
	}
}

void OrbitManager::advanceOrbit(OrbitComponent* orbitElem, double dT) {
	double oldrot = orbitElem->v;
	double newRadius = computeOrbitRadiusTrueAnomaly(dT, orbitElem);

	// advance asteroid orbit
	orbitElem->getRotationCenter()->getTransform()->SetLocalRotation(XMFLOAT3(0, orbitElem->v + orbitElem->w, 0));
	orbitElem->getMasterGO()->getTransform()->setLocalPosition(XMFLOAT3(0, 0, newRadius));
	orbitElem->getMasterGO()->getTransform()->RotateLocal(XMFLOAT3(0, oldrot - orbitElem->v, 0));

	// advance periapsis orbit
	if (orbitElem->getPeriapsisCenterGO() != NULL && orbitElem->getPeriapsisGO() != NULL) {
		// position periapsis
		orbitElem->getPeriapsisCenterGO()->getTransform()->SetLocalRotation(XMFLOAT3(0, orbitElem->w, 0));
		orbitElem->getPeriapsisGO()->getTransform()->setLocalPosition(XMFLOAT3(0, 0, orbitElem->rp * orbitElem->scalingFactor));
	}
}
#pragma endregion Orbit_Handling

#pragma region Asteroid_Spawn

void OrbitManager::spawnRandomAsteroid() {
	spawnRandomAsteroid(1);
}


void OrbitManager::spawnRandomAsteroid(int count) {

	// creates range for orbit radius
	std::random_device rd;     // only used once to initialise (seed) engine
	std::mt19937 rng(rd());    // random-number engine used (Mersenne-Twister in this case)
	std::uniform_int_distribution<int> radiusDist(minOrbit, maxOrbit); // guaranteed unbiased

	std::uniform_real_distribution<double> vDist(0, 2 * M_PI); // guaranteed unbiased

	std::uniform_real_distribution<float> sizeDist(m_AsteroidSizeMin, m_AsteroidSizeMax); // guaranteed unbiased

	std::uniform_real_distribution<float> excentricityDist(0.f, m_AsteroidExcentricityMax); // guaranteed unbiased
	

	for (int i = 0; i < count; i++) {
		// get random orbit
		int a = radiusDist(rng);
		double v = vDist(rng);
		double e = excentricityDist(rng);

		// reduce size of a since a^3 creates a ridiculous integeroverflow...
		double smalla = a / 1000;
		double smallm = M_MU / (1000 * 1000 * 1000);
		double T = 2 * M_PI / sqrt(smallm / (smalla * smalla * smalla)); // orbital period
		double n = (2 * M_PI) / T; // mean orbit motion

		//OrbitComponent(e, a, b, i, Q, w, v, T, n)
		OrbitComponent* orbitElem = new OrbitComponent(
			e, a, a, 0, 0, vDist(rng), v, T, n, this);

		// recomputes f to check if E is correct
		//double correctedF = 2 * atan2(sqrt(1 + e) * sin(E / 2), sqrt(1 - e) * cos(E / 2));

		//Compute time in orbit since periapsis
		double t = (v - (e * sin(v))) / n;

		orbitElem->t = t;

		// create asteroid game object
		createAsteroid(orbitElem, sizeDist(rng), m_master);

		// advance asteroid orbit
		orbitElem->getRotationCenter()->getTransform()->SetLocalRotation(XMFLOAT3(0, orbitElem->v + orbitElem->w, 0));
		orbitElem->getMasterGO()->getTransform()->setLocalPosition(XMFLOAT3(0, 0, a));

		// advance periapsis orbit
		if (orbitElem->getPeriapsisCenterGO() != NULL && orbitElem->getPeriapsisGO() != NULL) {
			// position periapsis
			orbitElem->getPeriapsisCenterGO()->getTransform()->SetLocalRotation(XMFLOAT3(0, orbitElem->w, 0));
			orbitElem->getPeriapsisGO()->getTransform()->setLocalPosition(XMFLOAT3(0, 0, orbitElem->rp * orbitElem->scalingFactor));
		}
	}

}

GameObject* OrbitManager::createObritingBody(XMFLOAT3 startPos, XMFLOAT3 startVel, float size, GameObject* globalParent, Renderer::Mesh* mesh) {
	OrbitComponent* obElem = calculateOrbitalElementsFromVelocity(
		startPos, startVel);
	return createObritingBody(obElem, size, globalParent, mesh);
}

GameObject* OrbitManager::createObritingBody(OrbitComponent* obElem, float size, GameObject* globalParent, Renderer::Mesh* mesh){
	// create rotation center game object
	GameObject* oBContainer = new GameObject(m_gameMain, XMFLOAT3(0, 0, 0));
	oBContainer->setName("oBContainer");
	globalParent->addChild(oBContainer);


	// create periapsis rotation center game object
	GameObject* periCenter = new GameObject(m_gameMain, XMFLOAT3(0, 0, 0));
	periCenter->setName("periCenter");
	oBContainer->addChild(periCenter);


	// create periapsis game object
	GameObject* periapsis = new GameObject(m_gameMain, XMFLOAT3(0, 0, 0));
	periapsis->getTransform()->setLocalScale(XMFLOAT3(size*0.25f, size*0.25f, size*0.25f));
	periapsis->setName("periapsis");
	periCenter->addChild(periapsis);

	// create periapsis renderer
	Renderer::MeshParams stylePeri;
	stylePeri.colorDiffuse = XMFLOAT4(1.8f, 0, 0, 1);
	stylePeri.colorEmissive = XMFLOAT4(0, 0, 0, 0);
	Renderer::RenderComponent* rCp = new Renderer::MeshRenderComponent(*m_MeshSphere, *m_MatWireframe, stylePeri);
	periapsis->addComponent(rCp);



	// create rotation center game object for orbiting body
	GameObject* rotCenter = new GameObject(m_gameMain, XMFLOAT3(0, 0, 0));
	rotCenter->setName("orbitingBodyRotCenter");
	oBContainer->addChild(rotCenter);


	// create asteroid game object
	GameObject* orbitingBody = new GameObject(m_gameMain, XMFLOAT3(3, 0, 3));
	orbitingBody->getTransform()->setLocalScale(XMFLOAT3(size, size, size));
	orbitingBody->setName("orbitingBody");
	rotCenter->addChild(orbitingBody);

	//alternate asteroid object (rotate it randomly)
	orbitingBody->getTransform()->RotateLocal(XMFLOAT3((rand() % 1000) / 1000.0f*g_XMPi[0] * 2,
												(rand() % 1000) / 1000.0f*g_XMPi[0] * 2,
												(rand() % 1000) / 1000.0f*g_XMPi[0] * 2));

	// create sphere collider for orbitingBody
	SphereCollider* sphereColl = new SphereCollider();
	orbitingBody->addComponent(sphereColl);

	// Set values for orbit element
	obElem->setRotationCenter(rotCenter);
	obElem->setPeriapsisCenterGO(periCenter);
	obElem->setPeriapsisGO(periapsis);
	orbitingBody->addComponent(obElem);

	// create orbiting body renderer
	Renderer::MeshParams style;
	style.colorDiffuse = XMFLOAT4(1.8f, 1.8f, 2.0f, 1);
	style.colorEmissive = XMFLOAT4(0.f, 0.f, 0.f, 1);
	Renderer::RenderComponent* rC = new Renderer::MeshRenderComponent(*mesh, *m_MatFlat, style);
	orbitingBody->addComponent(rC);

	// create orbit line render component
	Renderer::Material* matOrbit = new Renderer::OrbitMaterial(L"SampleVertexShader.cso", L"Orbit_PS.cso");
	Renderer::LineStyle orbitLineStyle;
	orbitLineStyle.width = 0.018f;
	orbitLineStyle.color = XMFLOAT4(1.6f, 1.9f, 2.0f, 1);
	Renderer::RenderComponent* orc = new Renderer::OrbitRenderComponent(obElem, this, *m_MeshPlane, *matOrbit, orbitLineStyle);

	// create orbit render plane game object
	GameObject* orbitPlane = new GameObject(m_gameMain);
	orbitPlane->setName("orbitPlane");
	orbitPlane->getTransform()->setLocalScale(XMFLOAT3(10, 10, 10));
	//Rotate uses radians
	orbitPlane->getTransform()->Rotate(XMFLOAT3(static_cast<float>(M_PI * 0.5), 0, 0));
	orbitPlane->addComponent(orc);
	oBContainer->addChild(orbitPlane);


	// Add orbit component to list
	addOrbitComponent(obElem);

	return oBContainer;
}

void OrbitManager::createAsteroid(OrbitComponent* obElem, float size, GameObject* globalParent) {
	createAsteroid(createObritingBody(obElem, size, globalParent,m_AsteroidMesh), size);
}

void OrbitManager::createAsteroid(GameObject* asteroidContainer, float size) {
	// set asteroid to asteroidContainer so it is always initialized later on
	GameObject* asteroid = asteroidContainer;
	for (GameObject* go : asteroidContainer->getAllChildren()) {
		if (go->getName() == "orbitingBody") {
			asteroid = go;
			asteroid->setName("asteroid");
			break;
		}
	}

	// create Asteroid component for asteroid
	float mass = 20 * size;
	Asteroid* asteroidComp = new Asteroid(mass, asteroidContainer);
	m_master->logToConsole(mass);
	asteroid->addComponent(asteroidComp);
}

// creates a new asteroid at startPos with start velocity and size. globalParent is the GO it will be attached to
void OrbitManager::createAsteroid(XMFLOAT3 startPos, XMFLOAT3 startVel, float size, GameObject* globalParent) {

	createAsteroid(createObritingBody(startPos, startVel, size, globalParent, m_AsteroidMesh), size);

	
}

#pragma endregion Asteroid_Spawn

#pragma region Player_creation

void OrbitManager::createPlayer(XMFLOAT3 startPos, XMFLOAT3 startVel, float size) {
	createPlayer(startPos, startVel, size, m_master);
}

// creates a new asteroid at startPos with start velocity and size. globalParent is the GO it will be attached to
void OrbitManager::createPlayer(XMFLOAT3 startPos, XMFLOAT3 startVel, float size, GameObject* globalParent) {

	GameObject* playerContainerGO = createObritingBody(startPos, startVel, size, globalParent, m_BaseMesh);

	// find player go
	GameObject* playerGO = playerContainerGO;
	for (GameObject* go : playerContainerGO->getAllChildren()) {
		if (go->getName() == "orbitingBody") {
			playerGO = go;
			playerGO->setName("player");
			Renderer::MeshParams style;
			style.colorDiffuse = m_PlayerColorDiffuse;
			style.colorEmissive = m_PlayerColorEmissive;
			playerGO->getComponent<Renderer::MeshRenderComponent>()->SetStyle(style);
			break;
		}

		if (go->getName() == "orbitPlane") {
			Renderer::LineStyle orbitLineStyle;
			orbitLineStyle.width = 0.018f;
			orbitLineStyle.color = m_PlayerColorDiffuse;
			go->getComponent<Renderer::OrbitRenderComponent>()->setStyle(orbitLineStyle);
		}
	}

	// create player component (Player also creates BaseManager)
	Player* player = new Player(this);
	playerGO->addComponent(player);
}
#pragma endregion Player_creation

float OrbitManager::dot(XMVECTOR a, XMVECTOR b) {
	return XMVector3Dot(a, b).m128_f32[0];
}

double OrbitManager::safeAcos(double value) {
	if (value > 1) {
		return acos(1);
	}
	else if (value < -1) {
		return acos(-1);
	}
	else {
		return acos(value);
	}
}

