#pragma once
#include "Components/Behaviour.h"
#include "Components/Behaviours/BaseManager.h"

class TimeWarp : public Behaviour
{
public:
	TimeWarp();
	~TimeWarp();

	//Events
	void OnUpdate(float dT);
	void OnStart();
	void OnDestroy();
	void OnCollision(GameObject::CollisionObject collision);

	float getTimeWarpFactor() {
		return timeWarp;
	}

	void setTimeWarpFactor(float newFactor) { timeWarp = newFactor; };
private:
	void handleUserInput(float dT);

	// increase time warp (time moves faster)
	void increaseTimeWarp();
	// decrease time warp (time moves slower)
	void decreaseTimeWarp();

	float timeWarp = 0;
	float changeFaktor = 0.1;
	float m_WarpMax = 10;
	float m_WarpMin = 0;
};
