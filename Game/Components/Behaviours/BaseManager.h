#pragma once
#include "Components/Behaviour.h"
#include "Components/Behaviours/Buidlings/Building.h"
#include "Components/Behaviours/Buidlings/BuildingTypes.h"
#include "Components/Behaviours/Buidlings/Engine.h"
#include "Components/Behaviours/Buidlings/Generator.h"
#include "Components/Behaviours/Buidlings/ScienceLab.h"

#include "Components/UI/UIButtonComponent.h"
#include "Components/Rendering/UIRenderComponent.h"
#include "GameObject.h"

#include "Components/Transform.h"

#include <functional>

class TimeWarp;

class BaseManager : public Behaviour
{
public:
	BaseManager(TimeWarp* _timeWarp);
	~BaseManager();

	//Events
	void OnUpdate(float dT);
	void OnStart();
	void OnDestroy();
	void OnCollision(GameObject::CollisionObject collision);

	// constructs a building on first free slot; Checks if the required resources are available; returns false if building can't be built.
	bool constructBuilding(BuildingTypes type);
	// constructs a building on specified slot; Checks if the required resources are available; returns false if building can't be built.
	bool constructBuilding(BuildingTypes type, int slot);

	// removes the building on the specified slot
	void removeBuildingOnSlot(int slot);
	// removes the nth building (skips empty slots)
	void removeNthBuilding(int n);
	//removes the first building of the specified type
	void removeBuildingByType(BuildingTypes type);



	void addEnergy(float amount);
	void addMass(float amount);
	void addScience(float amount);

	float getMass() { return mass; };
	float getEnergy() { return energy; };
	float getScience() { return science; };

	// removes amount of energy. energy can't get negative
	void removeEnergy(float amount);
	// removes amount of mass. mass can't get negative
	void removeMass(float amount);
	//removes amount of science, for future use
	void removeScience(float amount);


	float getThrust() const { return thrust*10; }
	float applyThrust();

	// callback for the buttons to construct a building
	void constructBuildingCallback(UIButtonComponent* sender, UIButtonComponent* p_sender, BuildingTypes building, int index);

	void setTimeWarper(TimeWarp* newTimeWarper) { timeWarper = newTimeWarper; }
private:
	// resources
	float energy = 10;
	float mass = 10;

	float science = 0;

	// how much thrust is provided (by engines)
	float thrust = 100;
	float efficiency = 1.0f;

	//time to finish game in seconds, if it reaches zero you loose
	double timeLimit = 900.0f;

	// general building related stuff
	int buildingSlots = 8;
	int freeSlots = buildingSlots;
	int usedSlots = 0;
	vector<Building*> buildingsVec;

	// UI related stuff
	// contains all the building slot ui related components
	GameObject* UIBuildingSlotsGO;
	// contains the build stuff UI
	GameObject* UIavailableBuildingsGO;
	// UI dimension of a building slot
	XMINT2 UIbuildSlotDim;
	// true -> menu is open
	bool buildingMenuOpen = false;



	DirectX::XMFLOAT4 m_UIColor;
	// displays the current resources
	Renderer::UIRenderComponent* UIResourcesScreen;
	Renderer::UIRenderComponent* m_OrbitUI;
	Renderer::UIRectComponent* m_OrbitUIFrame;

	Renderer::UIRenderComponent* ThrusterScreen;

	// initilizes the base UI
	void initializeBaseUI();
	void initializeThrustUI();
	void changeEfficieny(UIButtonComponent * sender, float delta);
	void initializeOrbitUI();

	void UpdateOrbitUI();

	void createStartScreen();
	void closeStartScreen();
	GameObject* m_StartMessageGO;
	UIButtonComponent* m_StartMessage;

	void createControlsUI();
	// creates a building slot UI element for each slot
	void createAllBuildingSlotsUI();
	// creates a UIButton at the specified position; cb_funtion is called on button click
	void createUIButton(XMINT2 upperLeft, XMINT2 buttonSize, std::basic_string<wchar_t> txt, float fontSize, GameObject* parentGO, function<void(UIButtonComponent*)> cb_funtion, bool active, XMFLOAT2 anchor);
	void createBuildingSlot(XMINT2 upperLef0t, int index);
	void createAvailableBuildingsUI(UIButtonComponent* sender, int index);

	void closeBuildingMenu(UIButtonComponent* sender);

	void updateBuildingResourcesUI();

	// creates a building component of the specified type.
	Building* createBuildingComponent(BuildingTypes type);
	// returns the costs for the type of building;
	BuildCost getBuildCostByType(BuildingTypes type);

	// decreases the building count by one
	void decreaseBuildingCount();
	
	// loops over all generators to gather their produced energy and updates the global energy with it
	void updateEnergy(float dT);
	// loops over all engines to gather their provided thrust; Computes MAX POSSIBLE thrust not actual available thrust!!!
	void updateEngineThrust();
	void updateScience(float dT);

	void checkWinCondition();

	// checks if the available resources are >= the costs.
	bool checkIfResourcesAreSufficient(BuildCost availabe, BuildCost costs);

	// returns first free slot. Returns -1 if no slot was found.
	int findFirstFreeSlot();
	// check if the slot is free
	bool isBuildSlotFree(int slot);

	// adds resources on pressing X
	void cheatResources();

	TimeWarp* timeWarper;


	// returns a vector with pointers to all buildings of type. (don't pass a pointer as generic argument!!!)
	template <class T>
	vector<T*> getAllBuildingsOfType(BuildingTypes type) {
		vector<T*> buildings = vector<T*>();
		for (int i = 0; i < buildingsVec.size(); ++i) {
			if (buildingsVec[i] != nullptr) {
				if (buildingsVec[i]->type == type) {
					buildings.push_back(dynamic_cast<T*>(buildingsVec[i]));
				}
			}
		}
		return buildings;
	}


};

