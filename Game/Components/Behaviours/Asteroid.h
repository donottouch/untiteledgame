#pragma once
#include "Components/Behaviour.h"

class Asteroid : public Behaviour
{
public:
	Asteroid();
	Asteroid(double mass, GameObject* asteroidContainer);
	~Asteroid();

	//Events
	void OnUpdate(float dT);
	void OnStart();
	void OnDestroy();
	void OnCollision(GameObject::CollisionObject collision);

	void setMass(double newMass);
	double getMass();

	GameObject* getAsteroidContainer();
private:
	// used for building/ propulsion
	double mass = 1000;

	GameObject* asteroidContainer;
};
