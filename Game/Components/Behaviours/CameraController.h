#pragma once

#ifndef CAMERA_CONTROLLER_BEHAVIOR
#define CAMERA_CONTROLLER_BEHAVIOR

#include "Components/Behaviour.h"
#include "ConfigParser.h"

namespace Behavior {
	class CameraController : public Behaviour {
	public:
		CameraController(std::shared_ptr<ConfigParser::Config>& config);
		~CameraController() {}

		//Events
		void OnUpdate(float dT);
		void OnStart() {}
		void OnDestroy() {}
		void OnCollision(GameObject::CollisionObject collision) {}

	private:

		XMFLOAT4		m_PanBounds;
		XMFLOAT4		m_RotateBounds;
		XMFLOAT2		m_ZoomBounds;
		float			m_PanSpeed;
		float			m_RotateSpeed;
		float			m_ZoomSpeed;

		XMFLOAT2		m_Rotation;

		inline Transform* getPivot() const { return m_master->getParent()->getParent()->getTransform(); }
		inline Transform* getPivotX() const { return m_master->getParent()->getTransform(); }

		Windows::System::VirtualKey m_ResetKey;
		void ResetView();
	};
}

#endif