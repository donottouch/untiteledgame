#pragma once

#ifndef MESSAGE_BOX_BEHAVIOR
#define MESSAGE_BOX_BEHAVIOR

#include "pch.h"
#include "Components/Behaviour.h"

namespace Behavior {
	class MessageBox : public Behaviour {
	public:
		MessageBox(UINT maxTextLength);
		~MessageBox();

		void SetText();

		void Show(bool active);

		//Events
		void OnUpdate(float dT) {}
		void OnStart();
		void OnDestroy() {}
		void OnCollision(GameObject::CollisionObject collision) {}
	private:


		bool m_PauseOnShow;
		bool m_IsActive;
		//close button

		//close callback

		//default close callback
	};
}

#endif
