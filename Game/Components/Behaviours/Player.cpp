#include "pch.h"
#include "Components/Behaviours/Player.h"
#include "Components/Behaviours/Asteroid.h"
#include "InputWrapper.h"
#include "GameMain.h"

using namespace Windows::System;

Player::Player() {
}

Player::Player(OrbitManager* _orbitManager) {
	orbitManager = _orbitManager;
}

Player::~Player() {

}

void Player::OnStart() {
	timeWarper = m_master->getGameMain()->getSceneRoot()->getComponent<TimeWarp>();
	baseManager = new BaseManager(timeWarper);
	m_master->addComponent(baseManager);
}

void Player::OnUpdate(float dT) {
	handleUserInput(dT * timeWarper->getTimeWarpFactor());
}

void Player::OnDestroy() {

}

void Player::OnCollision(GameObject::CollisionObject collision) {
	// everything the player collides with is an asteroid (this might actually change in the future)
	// i check the name anyways
	if (collision.colliderTwo->getName() == "asteroid") {
		handleAsteroidCollision(collision.colliderTwo);
	}
	else {
		// delete everything else
		m_master->logToConsole("collision with non asteroid and player object detected (in player.cpp)");
		collision.colliderTwo->flagForDeletion();
	}
}

void Player::handleAsteroidCollision(GameObject* asteroidGO) {
	Asteroid* asteroid = asteroidGO->getComponent<Asteroid>();
	
	// delete asteroid at end of frame
	asteroid->getAsteroidContainer()->flagForDeletion();

	double mass = 0;
	if (asteroid == nullptr) {
		mass = 3;
	}
	else {
		mass = asteroid->getMass();
	}

	baseManager->addMass(mass);
}

void Player::handleUserInput(float dT) {
	// prograde thrust
	switch (m_master->getGameMain()->getInput()->getKey(VirtualKey::W)) {
	case 2:
	case 3:
		//m_master->logToConsole("w pressed case 3\n");
		updateOrbit(dT);
		break;

	// if not pressed?
	default:
		break;
	}

	// retrograde thrust
	switch (m_master->getGameMain()->getInput()->getKey(VirtualKey::S)) {
	case 2:
	case 3:
		//m_master->logToConsole("S pressed case 3\n");
		updateOrbit(-dT);
		break;

	// if not pressed?
	default:
		break;
	}
}

void Player::updateOrbit(float scale) {
	if (orbitManager == nullptr) {
		m_master->logToConsole("Player.updateOrbit: orbitManager not set!! Orbit not updated!!");
		return;
	}

	float thrust = baseManager->applyThrust();

	scale = scale * thrust;

	XMFLOAT3 pos = XMFLOAT3();
	XMFLOAT3 vel = XMFLOAT3();
	OrbitComponent* orbitComp = m_master->getComponent<OrbitComponent>();
	// compute orbit state vectors
	orbitManager->computeOrbitStateVectorsfromOrbitElem(orbitComp, &pos, &vel);

	//direction of current velocity should be prograde vector
	XMVECTOR progradeVec = XMVector3Normalize( XMLoadFloat3(&vel));

	progradeVec = XMVectorScale(progradeVec, scale);

	// add prograde thrust vector to velocity vector
	XMStoreFloat3(&vel, XMLoadFloat3(&vel) + progradeVec);

	// update orbital elements
	orbitManager->calculateOrbitalElementsFromVelocity(orbitComp, pos, vel);
}
