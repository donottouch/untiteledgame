#include "pch.h"
#include "Components/Behaviours/BaseManager.h"
#include "GameMain.h"


using namespace Windows::System;

BaseManager::BaseManager(TimeWarp* _timeWarp) {
	buildingsVec = vector<Building*>(buildingSlots);
	timeWarper = _timeWarp;
}

BaseManager::~BaseManager() {

}

void BaseManager::OnStart() {
	ConfigParser::Config* config = m_master->getGameMain()->getConfig();
	config->GetColor("cUIColor", &m_UIColor);
	config->GetFloat("fStartMass", &mass);
	config->GetFloat("fStartEnergy", &energy);
	double gameTime = config->GetFloat("fGameTime");
	timeLimit = max(60.0, min(gameTime, 3600.0));

	initializeBaseUI();
	initializeOrbitUI();

	createControlsUI();

	initializeThrustUI();
	createStartScreen();
}
void BaseManager::OnUpdate(float dT) {
	float dTWarped = dT * timeWarper->getTimeWarpFactor();
	timeLimit -= dTWarped;

	cheatResources();
	
	updateEnergy(dTWarped);
	updateEngineThrust();
	updateScience(dTWarped);
	updateBuildingResourcesUI();
	UpdateOrbitUI();
	checkWinCondition();
}
void BaseManager::OnDestroy() {
}
void BaseManager::OnCollision(GameObject::CollisionObject collision) {
}

#pragma region UI_Initialisation

void BaseManager::initializeBaseUI() {

	// Setup UI game objects
	UIBuildingSlotsGO = new GameObject(m_master->getGameMain(), XMFLOAT3(0, 0, 0));
	m_master->addChild(UIBuildingSlotsGO);
	UIavailableBuildingsGO = new GameObject(m_master->getGameMain(), XMFLOAT3(0, 0, 0));
	UIBuildingSlotsGO->addChild(UIavailableBuildingsGO);


	// fill ui with components
	createAllBuildingSlotsUI();

	//init resources UI
	UIResourcesScreen = new Renderer::UIRenderComponent(256);
	UIResourcesScreen->SetColor(m_UIColor);
	UIResourcesScreen->SetOffset(XMINT2(16, -16));
	UIResourcesScreen->SetAnchor(XMFLOAT2(0,1));
	UIResourcesScreen->SetLocalAnchor(XMFLOAT2(0,1));
	UIResourcesScreen->SetSize(20.0f);
	m_master->addComponent(UIResourcesScreen);

	Renderer::UIRectComponent* UIResourcesFrame = new Renderer::UIRectComponent();
	UIResourcesFrame->SetLineWidth(2);
	UIResourcesFrame->SetColor(m_UIColor);
	UIResourcesScreen->SetFrame(UIResourcesFrame);

	m_master->addComponent(UIResourcesFrame);

	updateBuildingResourcesUI();
}

void BaseManager::initializeThrustUI() {
	//XMFLOAT4 UIColor = XMFLOAT4(0.9f, 0.9f, 1, 1);

	//init resources UI
	ThrusterScreen = new Renderer::UIRenderComponent(256);
	ThrusterScreen->SetColor(m_UIColor);
	ThrusterScreen->SetOffset(XMINT2(300, -16));
	ThrusterScreen->SetAnchor(XMFLOAT2(0, 1));
	ThrusterScreen->SetLocalAnchor(XMFLOAT2(0, 1));
	ThrusterScreen->SetSize(20.0f);
	m_master->addComponent(ThrusterScreen);

	Renderer::UIRectComponent* ThrusterFrame = new Renderer::UIRectComponent();
	ThrusterFrame->SetLineWidth(2);
	ThrusterFrame->SetColor(m_UIColor);
	ThrusterScreen->SetFrame(ThrusterFrame);

	m_master->addComponent(ThrusterFrame);

	WCHAR buf[256];
	UINT len = swprintf(buf, 256, L" Tweak:     Engines:\n\n       Mass use:   %.2f\n\n       Enegy use:  %.2f", 1.0f / efficiency, efficiency);
	ThrusterScreen->SetText(buf, len);


	std::function<void(UIButtonComponent*)> callbackFunction = std::bind(&BaseManager::changeEfficieny, this, std::placeholders::_1, 0.1f);

	UIComponent::Rect r = UIComponent::Rect();
	r.upperLeft = XMINT2(310, -18 - 22 - 22 - 22 - 10);
	r.lowerRight = XMINT2(r.upperLeft.x + 55, r.upperLeft.y + 22);
	r.anchorUV = XMFLOAT2(0, 1);
	UIButtonComponent* upButton = new UIButtonComponent(r, L"  +  ", 6, 18.0f, XMFLOAT2(0, 1));
	upButton->setClickedCallback(callbackFunction);

	callbackFunction = std::bind(&BaseManager::changeEfficieny, this, std::placeholders::_1, -0.1f);
	r.upperLeft = XMINT2(310, -16 - 22 - 10);
	r.lowerRight = XMINT2(r.upperLeft.x + 55, r.upperLeft.y + 22);
	UIButtonComponent* downButton = new UIButtonComponent(r, L"  -  ", 6, 18.0f, XMFLOAT2(0, 1));
	downButton->setClickedCallback(callbackFunction);

	m_master->addComponent(upButton);
	m_master->addComponent(downButton);

}

void BaseManager::changeEfficieny(UIButtonComponent* sender, float delta) {
	efficiency += delta;
	if (efficiency < 0.1f) {
		efficiency = 0.1f;
	}

	WCHAR buf[256];
	UINT len = swprintf(buf, 256, L" Tweak:     Engines:\n\n       Mass use:   %.2f\n\n       Enegy use:  %.2f", 1.0f / efficiency, efficiency);
	ThrusterScreen->SetText(buf, len);

}

void BaseManager::initializeOrbitUI() {
	//XMFLOAT4 UIColor = XMFLOAT4(0.9f, 0.9f, 1, 1);
	m_OrbitUI = new Renderer::UIRenderComponent(256);
	m_OrbitUI->SetColor(m_UIColor);
	m_OrbitUI->SetOffset(XMINT2(16, 16));
	m_OrbitUI->SetSize(20.0f);
	std::wstring str(L"Orbit Parameters:");
	m_OrbitUI->SetText(str);
	m_master->addComponent(m_OrbitUI);

	m_OrbitUIFrame = new Renderer::UIRectComponent();
	m_OrbitUIFrame->SetLineWidth(2);
	m_OrbitUIFrame->SetColor(m_UIColor);
	m_OrbitUI->SetFrame(m_OrbitUIFrame);

	m_master->addComponent(m_OrbitUIFrame);
}

void BaseManager::UpdateOrbitUI() {
	WCHAR buf[256];
	UINT len = swprintf(buf, 256, L"Orbit Parameters:\n%.2f seconds left!\nTime Warp x%.1f\nFPS %d", timeLimit, timeWarper->getTimeWarpFactor(), m_master->getGameMain()->getTimer().GetFramesPerSecond());
	m_OrbitUI->SetText(buf, len);

	//m_OrbitUIFrame->SetAnchor(m_OrbitUI->GetAnchor());
	//m_OrbitUIFrame->SetOffset(m_OrbitUI->GetFrameOffset()); //XMINT2(6, 8));
	//m_OrbitUIFrame->SetSize(m_OrbitUI->GetFrameSize());

}

void BaseManager::createStartScreen() {
	m_StartMessageGO = new GameObject(m_master->getGameMain(), XMFLOAT3(0, 0, 0));
	m_master->addChild(m_StartMessageGO);

	WCHAR buf[1042];
	UINT len = swprintf(buf, 1042, L"DARK RIP\n"
		"\nYour goal is to collect 1800 research and then fly into the black hole before the time runs out."
		"\nTo do so you have to build Labs."
		"\nThe player base (orange) can only accelerate prograde and retrograde,\nonce you have build an Engine."
		"Engines consume Mass and Energy, bulding more gives more thrust."
		"\nMass is gained from collisions with asteroids."
		"\nEnergy is generated by Generators."
		"\n\nPress [Space] or [P] to start. Click to dismiss."
	);

	int sizeXhalf = 550;
	int sizeYhalf = 150;

	UIComponent::Rect r = UIComponent::Rect();
	r.upperLeft = XMINT2(-sizeXhalf, -sizeYhalf);
	r.lowerRight = XMINT2(sizeXhalf, sizeYhalf);
	m_StartMessage = new UIButtonComponent(r, buf, 512, 20.0f, XMFLOAT2(0.5f, 0.5f));

	m_StartMessage->setClickedCallback(std::bind(&BaseManager::closeStartScreen, this));

	//UIButton->makeButtonFilled(true);
	m_StartMessageGO->addComponent(m_StartMessage);
}

void BaseManager::closeStartScreen() {
	//if (m_StartMessage != nullptr) {
	//	m_StartMessageGO->removeComponent(m_StartMessage);
	//}

	list<Component*> startUIButton = m_StartMessageGO->getAllComponents();
	for (auto&& component : startUIButton) {
		m_StartMessageGO->removeComponent(component);
	}
	startUIButton.clear();
}

void BaseManager::createControlsUI() {

	WCHAR buf[256];
	UINT len = swprintf(buf, 256, L"THRUST:\nPrograde: [W]\nRetrograde: [S]\n\nTIME WARP:\nFaster: [P]\nSlower: [L]\nNormal: [Space]\n\nCAMERA:\nPan: RMB\nRotate: MMB\nZoom: Scroll");

	Renderer::UIRenderComponent* controlsUI = new Renderer::UIRenderComponent(len + 2);
	controlsUI->SetColor(m_UIColor);
	controlsUI->SetAnchor(XMFLOAT2(1, 0));
	controlsUI->SetLocalAnchor(XMFLOAT2(1, 0));
	controlsUI->SetOffset(XMINT2(-16, 16));// -200, 16));
	controlsUI->SetSize(20.0f);
	controlsUI->SetText(buf);

	m_master->addComponent(controlsUI);

	Renderer::UIRectComponent* controlsUIFrame = new Renderer::UIRectComponent();
	//controlsUIFrame->SetAnchor(controlsUI->GetAnchor());
	//controlsUIFrame->SetOffset(controlsUI->GetFrameOffset()); //XMINT2(6, 8));
	//controlsUIFrame->SetSize(XMINT2(190, 304)); //XMINT2(260, 24));
	controlsUIFrame->SetLineWidth(2);
	controlsUIFrame->SetColor(m_UIColor);
	controlsUI->SetFrame(controlsUIFrame);

	m_master->addComponent(controlsUIFrame);

}

void BaseManager::createAllBuildingSlotsUI() {
	// TODO center buttons on screen
	XMINT2 upperLeft = XMINT2(-160, -55);
	UIbuildSlotDim = XMINT2(150, 45);

	int slotUIoffset = 10;
	// create all building slots
	for (int i = 0; i < buildingSlots; ++i) {
		createBuildingSlot(upperLeft, i);
		upperLeft.x -= UIbuildSlotDim.x + slotUIoffset;
	}
}

void BaseManager::createUIButton(XMINT2 upperLeft, XMINT2 buttonSize, std::basic_string<wchar_t> txt, float fontSize, GameObject* parentGO, function<void(UIButtonComponent*)> cb_funtion, bool active, XMFLOAT2 anchor) {
	UIComponent::Rect r = UIComponent::Rect();
	r.upperLeft = upperLeft;
	r.lowerRight = XMINT2( upperLeft.x + buttonSize.x, upperLeft.y + buttonSize.y);
	UIButtonComponent* UIButton = new UIButtonComponent(r, txt, 50, fontSize,anchor, XMINT2(10,10));

	UIButton->setClickedCallback(cb_funtion);

	if (!active)
	{
		UIButton->deactivate();
	}

	//UIButton->makeButtonFilled(true);
	parentGO->addComponent(UIButton);
}

void BaseManager::createBuildingSlot(XMINT2 upperLeft, int index) {
	std::function<void(UIButtonComponent*)> callbackFunction = std::bind(&BaseManager::createAvailableBuildingsUI, this, std::placeholders::_1, index); //Bind object to callback non static callback
	createUIButton(upperLeft, UIbuildSlotDim, L"Empty", 25.0f, UIBuildingSlotsGO, callbackFunction, true, XMFLOAT2(1, 1));
}

void BaseManager::createAvailableBuildingsUI(UIButtonComponent* sender, int index) {
	if (buildingMenuOpen) {
		closeBuildingMenu(sender);
		return;
	}

	//std::function<void(UIButtonComponent*)> callbackFunction = std::bind(&BaseManager::closeBuildingMenu, this, std::placeholders::_1); //Bind object to callback non static callback
	//std::function<void(UIButtonComponent*)> callb = std::bind(&BaseManager::constructBuildingCallback, this, std::placeholders::_1, sender, BuildingTypes::GENERATOR);
	XMINT2 senderpos = sender->getRect().upperLeft;
	XMINT2 size = XMINT2(150, 85);
	XMINT2 offset = XMINT2(0, 90);
	XMINT2 pos = XMINT2(senderpos.x, senderpos.y - 4*offset.y);//XMINT2(800, 400);
	float leftoverMass = 0;
	if (buildingsVec[index] != NULL) {
		leftoverMass = buildingsVec[index]->getCosts().mass;
	}

	WCHAR buf[256];
	UINT len = swprintf(buf, 256, L"Destroy\n%.0f Mass\n%.0f Energy", -leftoverMass, 0);

	createUIButton(pos, size, buf, 20.0f, UIavailableBuildingsGO, std::bind(&BaseManager::constructBuildingCallback, this, std::placeholders::_1, sender, BuildingTypes::DEFAULT, index),
		buildingsVec[index] != NULL, XMFLOAT2(1, 1));


	pos.x = pos.x + offset.x;
	pos.y = pos.y + offset.y;


	len = swprintf(buf, 256, L"Generator\n%.0f Mass\n%.0f Energy", getBuildCostByType(BuildingTypes::GENERATOR).mass - leftoverMass, getBuildCostByType(BuildingTypes::GENERATOR).energy);

	createUIButton(pos, size, buf, 20.0f, UIavailableBuildingsGO, std::bind(&BaseManager::constructBuildingCallback, this, std::placeholders::_1, sender, BuildingTypes::GENERATOR, index),
		getBuildCostByType(BuildingTypes::GENERATOR).mass - leftoverMass <= mass && getBuildCostByType(BuildingTypes::GENERATOR).energy <= energy, XMFLOAT2(1,1));


	pos.x = pos.x + offset.x;
	pos.y = pos.y + offset.y;


	len = swprintf(buf, 256, L"Engine\n%.0f Mass\n%.0f Energy", getBuildCostByType(BuildingTypes::ENGINE).mass - leftoverMass, getBuildCostByType(BuildingTypes::ENGINE).energy);

	createUIButton(pos, size, buf, 20.0f, UIavailableBuildingsGO, std::bind(&BaseManager::constructBuildingCallback, this, std::placeholders::_1, sender, BuildingTypes::ENGINE, index),
		getBuildCostByType(BuildingTypes::ENGINE).mass - leftoverMass <= mass && getBuildCostByType(BuildingTypes::ENGINE).energy <= energy, XMFLOAT2(1, 1));

	
	pos.x = pos.x + offset.x;
	pos.y = pos.y + offset.y;


	len = swprintf(buf, 256, L"Lab\n%.0f Mass\n%.0f Energy", getBuildCostByType(BuildingTypes::SCIENCELAB).mass - leftoverMass, getBuildCostByType(BuildingTypes::SCIENCELAB).energy);

	createUIButton(pos, size, buf, 20.0f, UIavailableBuildingsGO, std::bind(&BaseManager::constructBuildingCallback, this, std::placeholders::_1, sender, BuildingTypes::SCIENCELAB, index),
		getBuildCostByType(BuildingTypes::SCIENCELAB).mass - leftoverMass <= mass && getBuildCostByType(BuildingTypes::SCIENCELAB).energy <= energy, XMFLOAT2(1, 1));


	buildingMenuOpen = true;
}

#pragma endregion UI_Initialisation

void BaseManager::closeBuildingMenu(UIButtonComponent* sender) {
	list<Component*> buildingsUIButton = UIavailableBuildingsGO->getAllComponents();
	for (auto&& component : buildingsUIButton) {
		UIavailableBuildingsGO->removeComponent(component);
	}
	buildingsUIButton.clear();

	buildingMenuOpen = false;
}


void BaseManager::updateBuildingResourcesUI() {
	WCHAR buf[256];
	UINT len = swprintf(buf, 256, L"Current resources:\nMass %.2f\nEnergy %.2f\nThrust %.2f\nScience %.2f", mass, energy, thrust, science);
	UIResourcesScreen->SetText(buf, len);
}

void BaseManager::updateEnergy(float dT) {
	vector<Generator*> generators = getAllBuildingsOfType<Generator>(BuildingTypes::GENERATOR);

	float energySum = 0;
	for (int i = 0; i < generators.size(); ++i) {
		generators[i]->updateEnergy(dT);
		energySum += generators[i]->getEnergy();
	}

	addEnergy(energySum);
}

void BaseManager::updateEngineThrust() {
	// no thrust available if there are no resources
	if (mass <= 0 || energy <= 0) {
		thrust = 0;
		return;
	}

	vector<Engine*> engines = getAllBuildingsOfType<Engine>(BuildingTypes::ENGINE);

	// compute max possible thrust
	float thrustSum = 0;
	for (int i = 0; i < engines.size(); ++i) {
		thrustSum += engines[i]->getThrust();
	}

	thrust = thrustSum;
}

void BaseManager::updateScience(float dT)
{
	vector<ScienceLab*> labs = getAllBuildingsOfType<ScienceLab>(BuildingTypes::SCIENCELAB);

	for (int i = 0; i < labs.size(); ++i) {
		// check if science is done
		if (labs[i]->doScience(energy, dT)) {
			removeEnergy(labs[i]->getResearchCost() * dT);
		}
		science += labs[i]->getScience();
	}

}

void BaseManager::checkWinCondition()
{
	//TODO:
	if(timeLimit >= 0){
		if (science > 1800) //collected for over 30min if sci/sec = 1
		{
			float squaredDistance = XMVector3LengthSq(XMLoadFloat3(&m_master->getTransform()->getPosition())).m128_f32[0];
			if (squaredDistance < 0.5f) //check if base is near enough at black hole
			{
				timeWarper->setTimeWarpFactor(0);

				XMFLOAT4 UIColor = XMFLOAT4(0.2f, 1.0f, 0.2f, 1);
				Renderer::UIRenderComponent* gameOver = new Renderer::UIRenderComponent(256);
				gameOver->SetColor(UIColor);
				gameOver->SetOffset(XMINT2(800, 400));
				gameOver->SetSize(80.0f);
				std::wstring str(L"You Win!");
				gameOver->SetText(str);
				m_master->addComponent(gameOver);

			}
			else {
				//show info what to do now
				WCHAR buf[256];
				UINT len = swprintf(buf, 256, L"Current resources:\nMass %.2f\nEnergy %.2f\nThrust %.2f\nScience %.2f\n-Enter the black hole to WIN!-", mass, energy, thrust, science);
				UIResourcesScreen->SetText(buf, len);
			}
		}
	}
	else //lost
	{
		timeWarper->setTimeWarpFactor(0);

		XMFLOAT4 UIColor = XMFLOAT4(1.f, 0.2f, 0.2f, 1);
		Renderer::UIRenderComponent* gameOver = new Renderer::UIRenderComponent(256);
		gameOver->SetColor(UIColor);
		gameOver->SetOffset(XMINT2(800,400));
		gameOver->SetSize(80.0f);
		std::wstring str(L"Game Over");
		gameOver->SetText(str);
		m_master->addComponent(gameOver);
	}
}

float BaseManager::applyThrust() {
	vector<Engine*> engines = getAllBuildingsOfType<Engine>(BuildingTypes::ENGINE);

	float thrustSum = 0;
	float energyCost = 0;
	float massCost = 0;
	for (int i = 0; i < engines.size(); ++i) {
		thrustSum += engines[i]->getThrust();
		energyCost += engines[i]->getThrustEnergyCost();
		massCost += engines[i]->getThrustMassCost();
	}

	massCost  /= efficiency;
	energyCost  *= efficiency;


	// if enough resources are available
	if (mass >= massCost && energy >= energyCost) {
		// remove thrust costs
		removeMass(massCost);
		removeEnergy(energyCost);
	}
	else {
		// compute percentage of available thrust
		float massPercentage = mass / massCost;
		float energyPercentage = energy / energyCost;

		// if mass is the limiting factor
		if (mass < energyCost) {
			// compute available thrust
			thrustSum = thrustSum * massPercentage;
			removeMass(mass);
			removeEnergy(energyCost * massPercentage);

		// if energy is the limiting factor
		}
		else {
			thrustSum = thrustSum * energyPercentage;
			removeMass(massCost * energyPercentage);
			removeEnergy(energy);
		}
	}

	thrust = thrustSum;
	return thrustSum;
}

#pragma region Building_Construction_Code

// constructs building on first free slot
bool BaseManager::constructBuilding(BuildingTypes type) {
	if (freeSlots <= 0) {
		// no free slots
		return false;
	}

	// find first free slot
	int slot = findFirstFreeSlot();
	if (slot > -1) {
		return constructBuilding(type, slot);
	}
	else {
		return false;
	}

}

// constructs a building on specified slot; Checks if the required resources are available; returns false if building cant be built.
bool BaseManager::constructBuilding(BuildingTypes type, int slot) {
	if (slot < 0) { return false; }
	if (buildingsVec.size() < slot) {
		return false;
	}
	if (isBuildSlotFree(slot) == false) {
		// there is already a building here
		// destroy building, give mass back
		Building* b = buildingsVec[slot];
		addMass(b->getCosts().mass);
		m_master->removeComponent(b);
		if (type == BuildingTypes::DEFAULT)
		{
			buildingsVec[slot] = nullptr;
			return true; //Deconstructed , dont build a new one
		}
	}

	Building* building = createBuildingComponent(type);

	// Building could not be created
	if (building == nullptr) {
		return false;
	}

	buildingsVec[slot] = building;

	// remove resources
	BuildCost costs = getBuildCostByType(type);
	removeMass(costs.mass);
	removeEnergy(costs.energy);

	m_master->addComponent(building);

	freeSlots--;
	usedSlots++;
}

Building* BaseManager::createBuildingComponent(BuildingTypes type) {
	BuildCost availabeResources = BuildCost(energy, mass);
	BuildCost costs = getBuildCostByType(type);
	// if not enough resources are available
	if (checkIfResourcesAreSufficient(availabeResources, costs) == false) {
		return nullptr;
	}

	Building* building = nullptr;
	switch (type)
	{
	case BuildingTypes::SCIENCELAB:
		building = new ScienceLab();
		break;

	case BuildingTypes::ENGINE:
		building = new Engine();
		break;

	case BuildingTypes::GENERATOR:
		building = new Generator();
		break;

	default:
		break;
	}
	return building;
}

BuildCost BaseManager::getBuildCostByType(BuildingTypes type) {
	switch (type)
	{
	case BuildingTypes::SCIENCELAB:
		return ScienceLab::getBaseCosts();

	case BuildingTypes::ENGINE:
		return Engine::getBaseCosts();

	case BuildingTypes::GENERATOR:
		return Generator::getBaseCosts();
	default:
		return Building::getBaseCosts();
	}
}

// checks if the available resources are >= the costs.
bool BaseManager::checkIfResourcesAreSufficient(BuildCost availabe, BuildCost costs) {
	if (availabe.energy >= costs.energy && availabe.mass >= costs.mass) {
		return true;
	}
	else {
		return false;
	}
}

int BaseManager::findFirstFreeSlot() {
	if (freeSlots <= 0) {
		return -1;
	}

	if (buildingsVec.size() == 0) {
		return -1;
	}

	for (int i = 0; i < buildingSlots; ++i) {
		if (buildingsVec[i] == nullptr) {
			return i;
		}
	}
	return -1;
}

bool BaseManager::isBuildSlotFree(int slot) {
	if (slot < 0 || slot >= buildingSlots) {
		return false;
	}
	if (buildingsVec[slot] == nullptr) {
		return true;
	}
	return false;
}

// removes the building on the specified slot
void BaseManager::removeBuildingOnSlot(int slot) {
	if (buildingsVec.size() < slot) {
		return;
	}

	if (buildingsVec.at(slot) != nullptr) {
		buildingsVec.at(slot)->destoryBuilding();
		buildingsVec.at(slot) = nullptr;

		decreaseBuildingCount();
	}
}

// removes the nth building (skips empty slots)
void BaseManager::removeNthBuilding(int n) {
	if (buildingsVec.size() < n) {
		return;
	}


	int count = 0;
	for (int i = 0; i < buildingSlots && count < n; i++) {
		// count buildings
		if (buildingsVec.at(i) != nullptr) {
			count++;
			if (count == n) {
				buildingsVec.at(count)->destoryBuilding();
				buildingsVec.at(count) = nullptr;

				decreaseBuildingCount();
				return;
			}
		}
	}
}

//removes the first building of the specified type
void BaseManager::removeBuildingByType(BuildingTypes type) {
	for (int i = 0; i < buildingSlots; i++) {
		Building* building = buildingsVec.at(i);
		if (building != nullptr) {
			if (building->type == type) {
				building->destoryBuilding();
				building = nullptr;

				decreaseBuildingCount();
				return;
			}
		}
	}
}

#pragma endregion Building_Construction_Code

void BaseManager::decreaseBuildingCount() {
	freeSlots++;
	usedSlots--;

	if (usedSlots < 0) {
		usedSlots = 0;
	}
	if (freeSlots > buildingSlots) {
		freeSlots = buildingSlots;
	}
}

void BaseManager::addEnergy(float amount) {
	energy += amount;
}
void BaseManager::addMass(float amount) {
	mass += amount;
}

void BaseManager::addScience(float amount)
{
	science += amount;
}

// removes amount of energy. energy cant get negative
void BaseManager::removeEnergy(float amount) {
	energy -= amount;
	if (energy < 0) {
		energy = 0;
	}
}
// removes amount of mass. mass cant get negative
void BaseManager::removeMass(float amount) {
	mass -= amount;
	if (mass < 0) {
		mass = 0;
	}
}

void BaseManager::removeScience(float amount)
{
	science -= amount;
	if (science < 0) {
		science = 0;
	}
}

void BaseManager::constructBuildingCallback(UIButtonComponent * sender, UIButtonComponent * p_sender, BuildingTypes building, int index)
{
	
	if (constructBuilding(building, index)) {
		std::wstring s;
		switch (building)
		{
		case GENERATOR:
			s = L"Generator";
			break;
		case SCIENCELAB:
			s = L"Lab";
			break;
		case ENGINE:
			s = L"Engine";
			break;
		case DEFAULT:
			s = L"Empty";
			break;
		default:
			break;
		}
		p_sender->setText(s);
	}
	closeBuildingMenu(sender);
}

void BaseManager::cheatResources() {
	switch (m_master->getGameMain()->getInput()->getKey(VirtualKey::X)) {
	case 2:
	case 3:
		addMass(10);
		addEnergy(10);
		break;

		// if not pressed?
	default:
		break;
	}
}
