#pragma once
#include "Components/Behaviour.h"

#include "GameObject.h"
#include "GameMain.h"
#include "Components/Transform.h"
#include "Components/Behaviours/Player.h"
#include "Components/Behaviours/Asteroid.h"
#include "Components/Behaviours/TimeWarp.h"
#include "Components/Physics/PhysicsComponent.h"
#include "Components/Rendering/RenderComponent.h"
#include "Components/Rendering/LightComponent.h"
#include "Components/Rendering/MeshRenderComponent.h"
#include "Components/Rendering/OrbitRenderComponent.h"
#include "Components/Rendering/UIRenderComponent.h"
#include "Components/Rendering/Mesh.h"
#include "Components/Rendering/Material.h"
#include "Components/Rendering/OrbitMaterial.h"
#include <iostream>
#include "InputEngine.h"
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <math.h>       /* cos */

#include "Components/UI/UIButtonComponent.h"

#include "Components/OrbitComponent.h"


class OrbitManager : public Behaviour
{
public:
	OrbitManager(Renderer::Mesh* _MeshSphere, Renderer::Mesh* _MeshPlane, Renderer::Mesh* _AsteroidMesh, Renderer::Mesh* _BaseMesh, Renderer::Material* _MatWireframe,
		Renderer::Material* _MatFlat, Renderer::Material* _MatOrbit);
	~OrbitManager();

	//Events
	void OnUpdate(float dT);
	void OnStart();
	void OnDestroy();
	void OnCollision(GameObject::CollisionObject collision);

	// spawn count asteroids on random orbits
	void spawnRandomAsteroid(int count);
	// spawns one asteroid on a random orbit
	void spawnRandomAsteroid();

	// creates an asteroid
	void createAsteroid(XMFLOAT3 startPos, XMFLOAT3 startVel, float size, GameObject* globalParent);
	// creates the player object
	void createPlayer(XMFLOAT3 startPos, XMFLOAT3 startVel, float size);

	float getTimeWarpFactor() {
		return timeWarper->getTimeWarpFactor();
	}

	// returns a new struct holding all the data for the an orbit, based on the position and velocity vectors
	// of the object relative to the main body
	OrbitComponent* calculateOrbitalElementsFromVelocity(XMFLOAT3 posF3, XMFLOAT3 velocityF3);
	// updates the passed OrbitComponent
	void calculateOrbitalElementsFromVelocity(OrbitComponent* orbitElements, XMFLOAT3 posF3, XMFLOAT3 velocityF3);
	double computeOrbitRadiusTrueAnomaly(double dT, OrbitComponent* orbitElements);

	/*
	Computes the orbit state vector from the orbit elements.
	Stores the results in the passed XMFLOAT3s.
	*/
	void computeOrbitStateVectorsfromOrbitElem(OrbitComponent* orbitElem, XMFLOAT3* posF3, XMFLOAT3* velocityF3);

	void advanceAllOrbits(list<OrbitComponent*> orbits, double dT);

	void addOrbitComponent(OrbitComponent* obComp);
	void removeOrbitComponent(OrbitComponent* obComp);

private:
	int maxAsteroids = 6;
	int minOrbit = 3500000;
	int maxOrbit = 15000000;
	float m_AsteroidSizeMin = 0.08f;
	float m_AsteroidSizeMax = 0.2;
	float m_AsteroidExcentricityMax = 0.7f;

	list<OrbitComponent*> orbitComps;

	Game::GameMain* m_gameMain;

	//meshes
	Renderer::Mesh* m_MeshSphere;
	Renderer::Mesh* m_MeshPlane;

	Renderer::Mesh* m_AsteroidMesh;
	Renderer::Mesh* m_BaseMesh;

	//materials
	Renderer::Material* m_MatWireframe;
	Renderer::Material* m_MatFlat;
	Renderer::Material* m_MatOrbit;

	XMFLOAT4 m_PlayerColorDiffuse = XMFLOAT4(1.f, 0.7f, 0.04f, 1);
	XMFLOAT4 m_PlayerColorEmissive = XMFLOAT4(1.f, 0.7f, 0.04f, 0.2f);

	TimeWarp* timeWarper;

	// creates an orbiting body; returns a pointer to the body container game object. The orbiting body GO has the name orbitingBody;
	GameObject* createObritingBody(XMFLOAT3 startPos, XMFLOAT3 startVel, float size, GameObject* globalParent, Renderer::Mesh* mesh);
	GameObject* createObritingBody(OrbitComponent* obElem, float size, GameObject* globalParent, Renderer::Mesh* mesh);

	// creates an asteroid
	void createAsteroid(OrbitComponent* obElem, float size, GameObject* globalParent);
	void createAsteroid(GameObject* asteroidContainer, float size);
	// creates a player object (game object including all components)
	void createPlayer(XMFLOAT3 startPos, XMFLOAT3 startVel, float size, GameObject* globalParent);

	double computeEccentricAnomalyFromMeanAnomaly(double M, double e);
	double computeMeanAnomaly(OrbitComponent* orbitElem);

	void advanceOrbit(OrbitComponent* orbitElem, double dT);

	// TODO These are the same functions as in the physics engine. Move to general misc class
	float dot(XMVECTOR a, XMVECTOR b);
	// computes acos; clamps value to be safe
	double safeAcos(double value);
};
