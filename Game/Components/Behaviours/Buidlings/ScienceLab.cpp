#include "pch.h"
#include "Components/Behaviours/Buidlings/ScienceLab.h"
#include "Components/Behaviours/Buidlings/Building.h"
#include "GameMain.h"

using namespace Windows::System;

ScienceLab::ScienceLab() {
	type = BuildingTypes::SCIENCELAB;
	_myCosts = getBaseCosts();
}

ScienceLab::~ScienceLab() {

}


void ScienceLab::OnStart() {
}
void ScienceLab::OnUpdate(float dT) {
}
void ScienceLab::OnDestroy() {
}
void ScienceLab::OnCollision(GameObject::CollisionObject collision) {
}

BuildCost ScienceLab::getBaseCosts() {
	return BuildCost(5, 1);
}

bool ScienceLab::doScience(float energy, float dT) {
	if (energy >= researchEnergyCost) {
		science = dT * sciencePerSecond;
		return true;
	}
	else {
		return false;
	}
}

bool ScienceLab::upgrade()
{
	return false;
}

float ScienceLab::getScience()
{
	float scienceTmp = science;
	science = 0;
	return scienceTmp;
}

