#pragma once
#include "Components/Behaviour.h"
#include "Components/Behaviours/Buidlings/BuildingTypes.h"

struct BuildCost {
	BuildCost(float _mass, float _energy) : energy(_energy), mass(_mass) {}
	BuildCost() {}
	float energy;
	float mass;
};

class Building : public Behaviour
{
public:

	Building();
	~Building();

	//Events
	void OnUpdate(float dT); //calulate effect, respect level
	void OnStart();
	void OnDestroy();
	void OnCollision(GameObject::CollisionObject collision);

	static BuildCost getBaseCosts();

	virtual void destoryBuilding();
	virtual BuildCost getCosts();
	virtual bool upgrade(); //level++, update sum of costs, ppossibly increase effect

	BuildingTypes type = BuildingTypes::DEFAULT;


protected:
	BuildCost _myCosts;
	int Level = 1;

private:
};
