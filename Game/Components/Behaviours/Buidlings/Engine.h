#pragma once
#include "Components/Behaviour.h"
#include "Components/Behaviours/Buidlings/Building.h"

class Engine : public Building
{
public:
	Engine();
	~Engine();

	//Events
	void OnUpdate(float dT);
	void OnStart();
	void OnDestroy();
	void OnCollision(GameObject::CollisionObject collision);

	float getThrust() { return thrust; }
	float getThrustEnergyCost() { return thrustEnergyCost; }
	float getThrustMassCost() { return thrustMassCost; }

	static BuildCost getBaseCosts();
	bool upgrade();
private:
	float thrust = 40;
	const float thrustEnergyCost = 0.01;
	const float thrustMassCost = 0.005;
};
