#include "pch.h"
#include "Components/Behaviours/Buidlings/Generator.h"
#include "GameMain.h"

using namespace Windows::System;

Generator::Generator() {
	type = BuildingTypes::GENERATOR;
	_myCosts = getBaseCosts();
}

Generator::~Generator() {

}

void Generator::OnStart() {
}

void Generator::OnUpdate(float dT) {
}
void Generator::OnDestroy() {
}
void Generator::OnCollision(GameObject::CollisionObject collision) {
}

BuildCost Generator::getBaseCosts() {
	return BuildCost(2, 1);
}

bool Generator::upgrade()
{
	return false;
}

float Generator::getEnergy() {
	float tmp = availableEnergy;
	availableEnergy = 0;
	return tmp;
}

void Generator::updateEnergy(float dT) {
	timeSinceLastFullTick += dT;
	if (timeSinceLastFullTick >= tickLength) {
		int multiplier = (int) (timeSinceLastFullTick / tickLength);
		availableEnergy += multiplier * energyPerTick;

		timeSinceLastFullTick = fmod(timeSinceLastFullTick, tickLength);
	}
}



