#pragma once
#include "Components/Behaviours/Buidlings/Building.h"

class ScienceLab : public Building
{
public:
	ScienceLab();
	~ScienceLab();

	//Events
	void OnUpdate(float dT);
	void OnStart();
	void OnDestroy();
	void OnCollision(GameObject::CollisionObject collision);

	static BuildCost getBaseCosts();
	bool upgrade();

	// returns science; sets it to zero
	float getScience();
	// produces science; if not enough energy was passed no science is done (very sad); returns if any sciencing was done
	bool doScience(float energy, float dT);

	float getResearchCost() { return researchEnergyCost; }
private:
	float science = 0;
	float sciencePerSecond = 1;

	float researchEnergyCost = 0.001;
};
