#include "pch.h"
#include "Components/Behaviours/Buidlings/Engine.h"
#include "GameMain.h"

using namespace Windows::System;

Engine::Engine() {
	type = BuildingTypes::ENGINE;
	_myCosts = getBaseCosts();
}

Engine::~Engine() {

}

void Engine::OnStart() {
	ConfigParser::Config* config = m_master->getGameMain()->getConfig();
	config->GetFloat("fThrusterStrength", &thrust);
}
void Engine::OnUpdate(float dT) {
}
void Engine::OnDestroy() {
}
void Engine::OnCollision(GameObject::CollisionObject collision) {
}

BuildCost Engine::getBaseCosts() {
	return BuildCost(3, 1);
}

bool Engine::upgrade()
{
	return false;
}

