#pragma once
#include "Components/Behaviour.h"
#include "Components/Behaviours/Buidlings/Building.h"

class Generator : public Building
{
public:
	Generator();
	~Generator();

	//Events
	void OnUpdate(float dT);
	void OnStart();
	void OnDestroy();
	void OnCollision(GameObject::CollisionObject collision);


	// returns the available energy and sets it to zero.
	float getEnergy();

	// get costs to build this building
	static BuildCost getBaseCosts();
	bool upgrade();

	void updateEnergy(float dT);
private:
	float availableEnergy = 0;
	const float energyPerTick = 1;
	// tick length is in seconds
	const float tickLength = 1;
	float timeSinceLastFullTick = 0;

};
