#include "pch.h"
#include "Components/Behaviours/Asteroid.h"

Asteroid::Asteroid() {

}

Asteroid::~Asteroid() {

}

Asteroid::Asteroid(double mass, GameObject* asteroidContainer) {
	this->mass = mass;
	this->asteroidContainer = asteroidContainer;
}

void Asteroid::OnStart() {

}

void Asteroid::OnUpdate(float dT) {

}

void Asteroid::OnDestroy() {

}

void Asteroid::OnCollision(GameObject::CollisionObject collision) {
}

void Asteroid::setMass(double newMass) {
	mass = newMass;
}

double Asteroid::getMass() {
	return mass;
}

GameObject* Asteroid::getAsteroidContainer() {
	return asteroidContainer;
}
