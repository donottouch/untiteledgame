#include "pch.h"
#include "Components/Behaviours/TimeWarp.h"
#include "Components/Behaviours/TimeWarp.h"
#include "InputWrapper.h"
#include "GameMain.h"
//#include "ConfigParser.h"

using namespace Windows::System;

TimeWarp::TimeWarp() {
}

TimeWarp::~TimeWarp() {

}

void TimeWarp::OnStart() {
	ConfigParser::Config* config = m_master->getGameMain()->getConfig();
	config->GetFloat("fTimeWarpMax", &m_WarpMax);
	config->GetFloat("fTimeWarpMin", &m_WarpMin);
	config->GetFloat("fTimeWarpStep", &changeFaktor);
}

void TimeWarp::OnUpdate(float dT) {
	handleUserInput(dT);
}

void TimeWarp::OnDestroy() {

}

void TimeWarp::OnCollision(GameObject::CollisionObject collision) {
}

void TimeWarp::handleUserInput(float dT) {
	// increase time warp (time moves faster)
	switch (m_master->getGameMain()->getInput()->getKey(VirtualKey::P)) {
	case 2:
	case 3:
		increaseTimeWarp();
		break;

		// if not pressed?
	default:
		break;
	}

	// decrease time warp (time moves slower)
	switch (m_master->getGameMain()->getInput()->getKey(VirtualKey::L)) {
	case 2:
	case 3:
		decreaseTimeWarp();
		break;

		// if not pressed?
	default:
		break;
	}

	// reset time warp
	switch (m_master->getGameMain()->getInput()->getKey(VirtualKey::Space)) {
	case 2:
	case 3:
		timeWarp = 1;
		break;

		// if not pressed?
	default:
		break;
	}
}

void TimeWarp::increaseTimeWarp() {
		//m_master->logToConsole("increase time warp\n");

		timeWarp += changeFaktor;
		if (timeWarp > m_WarpMax) {
			timeWarp = m_WarpMax;
		}
}
void TimeWarp::decreaseTimeWarp() {
		//m_master->logToConsole("decrease time warp\n");
		timeWarp -= changeFaktor;
		if (timeWarp < m_WarpMin) {
			timeWarp = m_WarpMin;
		}
}
