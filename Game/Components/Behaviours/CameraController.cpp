#include "pch.h"
#include "CameraController.h"
#include "InputWrapper.h"
#include "GameMain.h"

Behavior::CameraController::CameraController(std::shared_ptr<ConfigParser::Config>& config) :
	m_PanSpeed(0.8f),
	m_PanBounds(XMFLOAT4(-10,10,-10,10)),
	m_RotateSpeed(0.25f),
	m_RotateBounds(XMFLOAT4(-0.45f * M_PI, 0, 0, 0)),
	m_ZoomSpeed(0.3f),
	m_ZoomBounds(XMFLOAT2(2.5f, 10)),
	m_Rotation(XMFLOAT2(0,0)),
	m_ResetKey(Windows::System::VirtualKey::Home)
{
	//m_PanSpeed = 0.8f;
	config->GetFloat("fPanSpeed", &m_PanSpeed);
	config->GetFloat("fRotateSpeed", &m_RotateSpeed);
	config->GetFloat("fZoomSpeed", &m_ZoomSpeed);
	config->GetFloat("fZoomMin", &m_ZoomBounds.x);
	config->GetFloat("fZoomMax", &m_ZoomBounds.y);
	config->GetInt("iKeyCameraReset", reinterpret_cast<int*>(&m_ResetKey));
}

inline float clamp(float value, float min, float max) {
	return value < min ? min : max < value ? max : value;
}

void Behavior::CameraController::OnUpdate(float dT) {
	InputEngine::InputWrapper^ input = m_master->getGameMain()->getInput();
	Transform* pivot = getPivot();
	Transform* pivotX = getPivotX();
	const XMFLOAT2 mouseDelta = XMFLOAT2(input->mouseDeltaX(), input->mouseDeltaY());

	if (input->getKey(Windows::System::VirtualKey::RightButton)>1) {
		const XMVECTOR dirX_v = XMLoadFloat3(&m_master->getTransform()->getRight());
		const XMVECTOR dirY_v = XMVector3Cross(dirX_v, XMVectorSet(0, 1, 0, 0));
		const XMVECTOR pivotPosition_v = XMLoadFloat3(&pivot->getPosition());
		const XMVECTOR translation_v = XMVectorScale(dirX_v, mouseDelta.x * m_PanSpeed * dT) + XMVectorScale(dirY_v, mouseDelta.y * m_PanSpeed * dT);
		//XMFLOAT3 translation = XMFLOAT3(mouseDelta.x * m_PanSpeed * dT, 0, mouseDelta.y * m_PanSpeed * dT);
		XMFLOAT3 translation;
		XMStoreFloat3(&translation, pivotPosition_v - translation_v);
		//clamp to bounds
		translation.x = clamp(translation.x, m_PanBounds.x, m_PanBounds.y);
		translation.z = clamp(translation.z, m_PanBounds.z, m_PanBounds.w);

		pivot->setPosition(translation);
		//m_master->logToConsole("camera control translate\n");
	}
	//*
	else if (input->getKey(Windows::System::VirtualKey::MiddleButton)>1) {
		float rotateY = mouseDelta.x * m_RotateSpeed * dT;
		m_Rotation.y = m_Rotation.y - rotateY;// clamp(m_Rotation.y + rotateY, m_RotateBounds.z, m_RotateBounds.w);
		pivot->SetLocalRotation(XMFLOAT3(0,m_Rotation.y,0));

		float rotateRight = mouseDelta.y * m_RotateSpeed * dT;
		m_Rotation.x = clamp(m_Rotation.x + rotateRight, m_RotateBounds.x, m_RotateBounds.y);
		pivotX->SetLocalRotation(XMFLOAT3(m_Rotation.x, 0, 0));
	}//*/
	int zoomDelta;
	if (zoomDelta=input->mouseWheelDelta()) {
		float zoom = -zoomDelta * m_ZoomSpeed * dT;
		const XMVECTOR position = XMLoadFloat3(&m_master->getTransform()->getLocalPosition());
		XMFLOAT3 translation = XMFLOAT3(0, zoom, 0);
		XMStoreFloat3(&translation, XMLoadFloat3(&translation) + position);
		translation.y = clamp(translation.y,m_ZoomBounds.x, m_ZoomBounds.y);
		m_master->getTransform()->setLocalPosition(translation);
	}
	if (input->getKey(m_ResetKey) > 1) {
		pivot->setPosition(XMFLOAT3(0, 0, 0));
		pivot->SetLocalRotation(XMFLOAT3(0, 0, 0));
		pivotX->SetLocalRotation(XMFLOAT3(0, 0, 0));
	}
}
