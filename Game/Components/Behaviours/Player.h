#pragma once
#include "Components/Behaviour.h"
#include "Components/Behaviours/BaseManager.h"
#include "Components/Behaviours/OrbitManager.h"
#include "Components/Behaviours/TimeWarp.h"

class Player : public Behaviour
{
public:
	Player();
	Player(OrbitManager* _orbitManager);
	~Player();

	//Events
	void OnUpdate(float dT);
	void OnStart();
	void OnDestroy();
	void OnCollision(GameObject::CollisionObject collision);

private:
	void handleAsteroidCollision(GameObject* asteroid);

	void handleUserInput(float dT);
	void updateOrbit(float scale);

	BaseManager* baseManager;
	OrbitManager* orbitManager;
	TimeWarp* timeWarper;
};
