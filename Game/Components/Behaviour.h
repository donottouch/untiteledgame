#pragma once
#include "Component.h"

class Behaviour : public Component
{
public:
	virtual void OnUpdate(float dT) = 0;
	virtual void OnStart() = 0;
	virtual void OnDestroy() = 0;
	virtual void OnCollision(GameObject::CollisionObject collision) = 0;
};
