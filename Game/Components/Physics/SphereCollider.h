#pragma once
#include "../Physics/Collider.h"

class SphereCollider : public Collider
{
public:
	SphereCollider();
	~SphereCollider();

	//Events
	void OnUpdate(float dT);
	void OnStart();
	void OnDestroy();
	void OnCollision(GameObject::CollisionObject collision);

	XMFLOAT3 getPosition();

	float getRadius() { return radius; };

private:
	float radius = 1;

};
