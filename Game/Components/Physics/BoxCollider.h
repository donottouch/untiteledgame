#pragma once
#include "../Physics/Collider.h"

class BoxCollider : public Collider
{
public:
	BoxCollider();
	~BoxCollider();

	//Events
	void OnUpdate(float dT);
	void OnStart();
	void OnDestroy();
	void OnCollision(GameObject::CollisionObject collision);

};
