#include "pch.h"
#include "../Physics/SphereCollider.h"
#include "../Components/Transform.h"

SphereCollider::SphereCollider() {

}

SphereCollider::~SphereCollider() {
}

void SphereCollider::OnStart() {

}

void SphereCollider::OnUpdate(float dT) {

}

void SphereCollider::OnDestroy() {

}

void SphereCollider::OnCollision(GameObject::CollisionObject collision) {

}

XMFLOAT3 SphereCollider::getPosition() {
	return m_master->getTransform()->getPosition();
}