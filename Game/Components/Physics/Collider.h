#pragma once
#include "../Physics/PhysicsComponent.h"

class Collider : public PhysicsComponent
{
public:
	//Events
	virtual void OnUpdate(float dT) = 0;
	virtual void OnStart() = 0;
	virtual void OnDestroy() = 0;
	virtual void OnCollision(GameObject::CollisionObject collision) = 0;

};
