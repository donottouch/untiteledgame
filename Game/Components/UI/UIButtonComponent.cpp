#include "pch.h"
#include "UIButtonComponent.h"


UIButtonComponent::UIButtonComponent(Rect r, std::basic_string<wchar_t> txt, size_t maxTextLength, float textSize) : UIComponent(r)
{
	dimensions = r;
	//text
	text = new Renderer::UIRenderComponent(maxTextLength);
	text->SetColor(textColorNormal);
	text->SetOffset(r.upperLeft);
	text->SetSize(textSize);
	text->SetAnchor(XMFLOAT2(0,0));
	std::wstring str(txt);
	text->SetText(str);


	//frame
	frame = new Renderer::UIRectComponent();
	frame->SetAnchor(text->GetAnchor());
	frame->SetOffset(r.upperLeft);
	frame->SetSize(r.getSize());
	frame->SetLineWidth(2);
	frame->SetColor(frameColorNormal);

	r.anchorUV = XMFLOAT2(0, 0);
	//text->SetFrame(frame);
	setRect(r);
}


UIButtonComponent::UIButtonComponent(Rect r, std::basic_string<wchar_t> txt, size_t maxTextLength, float textSize, XMFLOAT2 anchor) : UIComponent(r)
{
	dimensions = r;
	//text
	text = new Renderer::UIRenderComponent(maxTextLength);
	text->SetColor(textColorNormal);
	text->SetOffset(r.upperLeft);
	text->SetSize(textSize);
	text->SetAnchor(anchor);
	std::wstring str(txt);
	text->SetText(str);


	//frame
	frame = new Renderer::UIRectComponent();
	frame->SetAnchor(text->GetAnchor());
	frame->SetOffset(r.upperLeft);
	frame->SetSize(r.getSize());
	frame->SetLineWidth(2);
	frame->SetColor(frameColorNormal);

	text->SetFrame(frame);

	r.anchorUV = anchor;
	setRect(r);
}


UIButtonComponent::UIButtonComponent(Rect r, std::basic_string<wchar_t> txt, size_t maxTextLength, float textSize, XMFLOAT2 anchor, XMINT2 textOffset) : UIComponent(r)
{
	dimensions = r;
	//text
	text = new Renderer::UIRenderComponent(maxTextLength);
	text->SetColor(textColorNormal);
	text->SetOffset(XMINT2(r.upperLeft.x + textOffset.x, r.upperLeft.y+textOffset.y));
	text->SetSize(textSize);
	text->SetAnchor(anchor);
	std::wstring str(txt);
	text->SetText(str);


	//frame
	frame = new Renderer::UIRectComponent();
	frame->SetAnchor(anchor);
	frame->SetOffset(r.upperLeft);
	frame->SetSize(r.getSize());
	frame->SetLineWidth(2);
	frame->SetColor(frameColorNormal);


	r.anchorUV = anchor;
	setRect(r);
}

UIButtonComponent::UIButtonComponent(Rect r, std::basic_string<wchar_t> txt, size_t maxTextLength, float textSize, function<void(UIButtonComponent*)> cb_funtion, bool filled)
{
	UIButtonComponent(r, txt, maxTextLength, textSize);
	setClickedCallback(cb_funtion);
	makeButtonFilled(filled);
}


UIButtonComponent::~UIButtonComponent()
{

	Rect re = Rect();
	re.anchorUV = text->GetAnchor();
	re.upperLeft = text->GetFrameOffset();
	re.lowerRight = XMINT2(re.upperLeft.x + 10, re.upperLeft.y);

}


void UIButtonComponent::OnPointerEnter(Windows::UI::Core::PointerEventArgs ^ args)
{
	if (active) {
		frame->SetColor(frameColorActive);
		text->SetColor(textColorActive);
	}
}

void UIButtonComponent::OnPointerExit(Windows::UI::Core::PointerEventArgs ^ args)
{
	if (active) {
		frame->SetColor(frameColorNormal);
		text->SetColor(textColorNormal);
	}
}

void UIButtonComponent::OnPointerMove(Windows::UI::Core::PointerEventArgs ^ args)
{
	if (active) {
		frame->SetColor(frameColorActive);
		text->SetColor(textColorActive);
	}
}

void UIButtonComponent::OnClick(Windows::UI::Core::PointerEventArgs ^ args) 
{
	if (active) {
		frame->SetColor(XMFLOAT4(0.9f, 0.1f, 0.1f, 1));
		if (clickCallback != NULL)
		{
			clickCallback(this);
		}
	}
}

void UIButtonComponent::handleResize(Windows::Foundation::Rect bonds)
{
	Rect nr = Rect();
	nr.anchorUV = r.anchorUV;
	XMINT2 topleft =  text->GetFrameOffset();
	XMINT2 s = r.getSize();
	nr.upperLeft = XMINT2(nr.anchorUV.x * bonds.Width + topleft.x+4, nr.anchorUV.y * bonds.Height+ topleft.y+4);
	nr.lowerRight = XMINT2(nr.upperLeft.x+s.x, nr.upperLeft.y+s.y);

	setRect(nr);
}


void UIButtonComponent::OnUpdate(float dT)
{
}

void UIButtonComponent::OnStart()
{
	m_master->addComponent(frame); //add frame first to have it in background
	m_master->addComponent(text);
}

void UIButtonComponent::OnDestroy()
{
}

void UIButtonComponent::OnCollision(GameObject::CollisionObject collision)
{
}

void UIButtonComponent::setText(std::basic_string<wchar_t> txt)
{
	text->SetText(txt);
}

void UIButtonComponent::makeButtonFilled(bool filled)
{
	if (filled)
	{
		XMINT2 size = dimensions.getSize();
		int thickness = size.x < size.y ? size.x : size.y; //use smaller value, then fill with 2 of these lines, account for sides too
		XMINT2 offset = XMINT2(dimensions.upperLeft.x + thickness/2, 
								dimensions.upperLeft.y + thickness /2); //upper left of Button is displaced by thickness
		size = XMINT2(size.x - thickness, size.y - thickness); //size of button shrinks depending on button


		frame->SetOffset(offset);
		frame->SetSize(size);
		frame->SetLineWidth(thickness);
	}
	else {
		frame->SetOffset(dimensions.upperLeft);
		frame->SetSize(dimensions.getSize());
		frame->SetLineWidth(2);
	}
}

void UIButtonComponent::toggleActive()
{
	if (active)
	{
		deactivate();
	}
	else {
		activate();
	}
}

void UIButtonComponent::deactivate()
{
	active = false;
	frame->SetColor(frameColorDeactivated);
	text->SetColor(textColorDeactivated);
}

void UIButtonComponent::activate()
{
	active = true;
	frame->SetColor(frameColorNormal);
	text->SetColor(textColorNormal);
}
