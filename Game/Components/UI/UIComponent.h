#pragma once
#include "Component.h"
class UIComponent :
	public Component
{
public:
	struct Rect
	{
		XMINT2 upperLeft;
		XMINT2 lowerRight;
		XMFLOAT2 anchorUV;
		XMINT2 getSize() { return XMINT2(lowerRight.x - upperLeft.x, lowerRight.y - upperLeft.y); }
	};

	enum State {
		out,
		enter,
		in,
		exit
	};

	UIComponent(Rect eventArea) { r = eventArea; };
	UIComponent() { r = Rect(); };


	virtual void OnPointerEnter(Windows::UI::Core::PointerEventArgs^ args) = 0;
	virtual void OnPointerExit(Windows::UI::Core::PointerEventArgs^ args) = 0;
	virtual void OnPointerMove(Windows::UI::Core::PointerEventArgs^ args) = 0;
	virtual void OnClick(Windows::UI::Core::PointerEventArgs^ args) = 0;
	virtual void handleResize(Windows::Foundation::Rect bonds) = 0; //recalc rect


	Rect getRect() { return r; }; //hilarious name btw
	void setRect(Rect newR) { r = newR; };
	State currState = out;

	
protected:
	Rect r;
};

