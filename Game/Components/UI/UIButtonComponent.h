#pragma once
#include "UIComponent.h"
#include "Common\DirectXHelper.h"
#include "Content/ShaderStructures.h"
#include "Components/Rendering/StandardMaterial.h"
#include "Components/Rendering/OrbitMaterial.h"
#include "Components/Rendering/UIRenderComponent.h"
#include "Components/Rendering/UIRectComponent.h"
#include <functional>

class UIButtonComponent :
	public UIComponent
{
public:
	UIButtonComponent(Rect r, std::basic_string<wchar_t> txt, size_t maxTextLength, float textSize);
	UIButtonComponent(Rect r, std::basic_string<wchar_t> txt, size_t maxTextLength, float textSize, XMFLOAT2 anchor);
	UIButtonComponent(Rect r, std::basic_string<wchar_t> txt, size_t maxTextLength, float textSize, XMFLOAT2 anchor, XMINT2 textOffset);
	UIButtonComponent(Rect r, std::basic_string<wchar_t> txt, size_t maxTextLength, float textSize, function<void(UIButtonComponent*)> cb_funtion, bool filled);
	~UIButtonComponent();

	void OnPointerEnter(Windows::UI::Core::PointerEventArgs^ args) override;
	void OnPointerExit(Windows::UI::Core::PointerEventArgs^ args)override;
	void OnPointerMove(Windows::UI::Core::PointerEventArgs^ args)override;
	void OnClick(Windows::UI::Core::PointerEventArgs^ args)override;
	void handleResize(Windows::Foundation::Rect bonds)override;


	void SetAnchor(XMFLOAT2 anchor);

	//Events
	virtual void OnUpdate(float dT);
	virtual void OnStart();
	virtual void OnDestroy();
	virtual void OnCollision(GameObject::CollisionObject collision);

	void setTextColor(XMFLOAT4 norm, XMFLOAT4 activ, XMFLOAT4 clicked, XMFLOAT4 deactive) { textColorNormal = norm; textColorActive = activ; textColorClicked = clicked; textColorDeactivated = deactive; };
	void setFrameColor(XMFLOAT4 norm, XMFLOAT4 activ, XMFLOAT4 clicked, XMFLOAT4 deactive) { frameColorNormal = norm; frameColorActive = activ; frameColorClicked = clicked; frameColorDeactivated = deactive; };
	
	void setClickedCallback(function<void(UIButtonComponent*)> cb_funtion) { clickCallback = cb_funtion; };
	void setText(std::basic_string<wchar_t> txt);

	void makeButtonFilled(bool filled);
	void toggleActive();
	void deactivate();
	void activate();
	bool isActive() {return active;	};
private:
	Renderer::UIRenderComponent* text;
	Renderer::UIRectComponent* frame;

	bool active= true;

	Rect dimensions;

	XMFLOAT4 textColorNormal = XMFLOAT4(0.9f, 0.9f, 1, 1);
	XMFLOAT4 textColorActive = XMFLOAT4(0.9f, 0.9f, 1, 1);
	XMFLOAT4 textColorClicked = XMFLOAT4(0.9f, 0.9f, 1, 1);
	XMFLOAT4 textColorDeactivated = XMFLOAT4(0, 0, 0, 0);


	XMFLOAT4 frameColorNormal = XMFLOAT4(0.6f, 0.6f, 0.6f, 1);
	XMFLOAT4 frameColorActive = XMFLOAT4(0.9f, 0.9f, 1, 1);
	XMFLOAT4 frameColorClicked = XMFLOAT4(0.4f, 0.4f, 1, 1);
	XMFLOAT4 frameColorDeactivated = XMFLOAT4(0, 0, 0, 0.8f);


	function<void(UIButtonComponent*)> clickCallback;
};

