#include "pch.h"
#include "../Components/Transform.h"
#include "GameObject.h"



XMMATRIX Transform::calculateLocalToParentMatrix_()
{
	isDirty_ = false;
	isInvDirty_ = true;

	// return Transformation matrix from Base Transform: Translation*Rotation*Scaling
	return XMMatrixAffineTransformation(XMLoadFloat3(&localScale),g_XMZero, XMLoadFloat4(&localRotation), XMLoadFloat3(&localPosition)) ;
}

Transform::Transform(GameObject* parent)
{
	this->m_master = parent;
}

Transform::Transform(GameObject * master, XMFLOAT3 globalposition)
{
	this->m_master = master; 
	if (master->getParent() == NULL) {
		localPosition = globalposition;
	}
	else { 
		//calculate local position as local position relative to parent
		XMStoreFloat3(&localPosition, master->getParent()->getTransform()->transformPointToLocal(XMLoadFloat3(&globalposition)));
	}
}

Transform::Transform(GameObject * master, XMFLOAT3 localposition, XMFLOAT3 localscale, XMFLOAT4 localrotation)
{
	//Remember loocalPosition is relative to parent, not to own coordinate system
	this->m_master = master;
	this->localPosition = localposition;
	this->localScale = localscale;
	this->localRotation = localrotation;
}

//After changes own Matrix has to be recalculated, therefore all children too
void Transform::setAllChildrenDitry()
{
	isDirty_ = true;
	isInvDirty_ = true;
	for each (GameObject* child in m_master->getChildren())
	{
		child->getTransform()->setAllChildrenDitry();
	}
}

Transform::~Transform() {
	//No need to delete anything, master is responsible for transform not vice versa
}

XMVECTOR Transform::transformPointToWorld(XMVECTOR localpoint)
{
	//w = 1 -> translates
	return XMVector3Transform(localpoint, XMLoadFloat4x4(&getLocalToWorldMatrix()));
}

XMVECTOR Transform::transformDirectionToWorld(XMVECTOR localdirection)
{
	//w = 0 -> does not translate, only sclae and rotation
	return XMVector3TransformNormal(localdirection, XMLoadFloat4x4(&getLocalToWorldMatrix()));
}

XMVECTOR Transform::transformPointToLocal(XMVECTOR globalpoint)
{
	//w = 1 -> translates
	return XMVector3Transform(globalpoint, XMLoadFloat4x4(&getWorldToLocalMatrix()));
}

XMVECTOR Transform::transformDirectionToLocal(XMVECTOR globaldirection)
{
	//w = 0 -> does not translate, only sclae and rotation
	return XMVector3TransformNormal(globaldirection, XMLoadFloat4x4(&getWorldToLocalMatrix()));
}


void Transform::Translate(XMFLOAT3 globaldirection, float distance)
{
	//Moves position in parent transform but with local rotation
	XMVECTOR localdirection = transformDirectionToLocal(XMLoadFloat3(&globaldirection)); //not normalized due to scaling
	XMStoreFloat3(&localPosition, (XMLoadFloat3(&localPosition) + (XMVector3Normalize(localdirection) * distance)));
	setAllChildrenDitry();
}

void Transform::TranslateLocal(XMFLOAT3 localdirection, float distance)
{
	XMStoreFloat3(&localPosition, (XMLoadFloat3(&localPosition) + (XMVector3Normalize(XMLoadFloat3(&localdirection)) * distance)));
	setAllChildrenDitry();
}

void Transform::Rotate(XMFLOAT3 globalEulerAngles)
{
	if (m_master->getParent() == NULL) {
		RotateLocal(globalEulerAngles);
		return;
	}
	XMVECTOR localAxis = m_master->getParent()->getTransform()->transformDirectionToLocal(XMLoadFloat3(&globalEulerAngles));// XMVector3Rotate(XMLoadFloat3(&globalEulerAngles), XMLoadFloat4(&getLoacalRotation()));
	XMStoreFloat4(&localRotation, XMQuaternionMultiply(XMLoadFloat4(&localRotation) ,XMQuaternionRotationRollPitchYawFromVector(localAxis)));
	setAllChildrenDitry();
}

void Transform::RotateLocal(XMFLOAT3 localEulerAngles)
{
	XMStoreFloat4(&localRotation, XMQuaternionMultiply(XMLoadFloat4(&localRotation), XMQuaternionRotationRollPitchYawFromVector(XMLoadFloat3(&localEulerAngles))));
	setAllChildrenDitry();
}

void Transform::SetLocalRotation(XMFLOAT3 localRadians) {
	XMStoreFloat4(&localRotation, XMQuaternionRotationRollPitchYawFromVector(XMLoadFloat3(&localRadians)));
	setAllChildrenDitry();
}

void Transform::RotateAround(XMFLOAT3 worldAxis, float angle)
{
	XMVECTOR localAxis = transformDirectionToLocal(XMLoadFloat3(&worldAxis));
	XMStoreFloat4(&localRotation, XMQuaternionMultiply(XMLoadFloat4(&localRotation), XMQuaternionRotationAxis(localAxis, angle)));
	setAllChildrenDitry();
}

void Transform::RotateAroundLocal(XMFLOAT3 localAxis, float angle)
{
	XMStoreFloat4(&localRotation, XMQuaternionMultiply(XMLoadFloat4(&localRotation), XMQuaternionRotationAxis(XMLoadFloat3(&localAxis), angle)));
	setAllChildrenDitry();
}



XMFLOAT4X4 Transform::getLocalToWorldMatrix()
{
	if (isDirty_)
	{
		if (m_master->getParent() == NULL)
		{
			XMStoreFloat4x4(&localToWorldMatrix_, calculateLocalToParentMatrix_());
		}
		else
		{
			XMStoreFloat4x4(&localToWorldMatrix_, calculateLocalToParentMatrix_() * XMLoadFloat4x4(&m_master->getParent()->getTransform()->getLocalToWorldMatrix()));
		}
	}
	return localToWorldMatrix_;
}

XMFLOAT4X4 Transform::getWorldToLocalMatrix()
{
	if (isInvDirty_)
	{
		XMMATRIX toInvert = XMLoadFloat4x4(&getLocalToWorldMatrix());
		XMStoreFloat4x4(&worldToLocalMatrix_, XMMatrixInverse(&XMMatrixDeterminant(toInvert), toInvert));
		isInvDirty_ = false;
	}
	return worldToLocalMatrix_;
}

void Transform::setLocalPosition(XMFLOAT3 localposition)
{
	setAllChildrenDitry();
	localPosition = localposition;
}

void Transform::setPosition(XMFLOAT3 globalposition)
{

	if (m_master->getParent() == NULL) {
		setLocalPosition(globalposition);
		return;
	}
	//Tranform global position to coordinates of parent to get local position
	XMStoreFloat3(&localPosition, m_master->getParent()->getTransform()->transformPointToLocal(XMLoadFloat3(&globalposition)));
	setAllChildrenDitry();
}

void Transform::setLocalScale(XMFLOAT3 localscale)
{
	localScale = localscale;
	setAllChildrenDitry();
}

void Transform::setScale(XMFLOAT3 globalscale)
{

	if (m_master->getParent() == NULL) {
		scale = globalscale;
		return;
	}
	
	XMFLOAT3 parentGlobalScale = m_master->getParent()->getTransform()->getScale();
	localScale.x = globalscale.x / parentGlobalScale.x;
	localScale.y = globalscale.y / parentGlobalScale.y;
	localScale.z = globalscale.z / parentGlobalScale.z;
	setAllChildrenDitry();
}

void Transform::setLocalRotation(XMFLOAT4 localrotation)
{
	localRotation = localrotation;
	setAllChildrenDitry();
}

void Transform::setRotation(XMFLOAT4 globalrotation)
{
	if (m_master->getParent() == NULL) {
		setLocalRotation(globalrotation);
		return;
	}
	XMStoreFloat4( &localRotation , m_master->getParent()->getTransform()->transformDirectionToLocal(XMLoadFloat4(&globalrotation)));
	setAllChildrenDitry();
}

XMFLOAT3 Transform::getForward()//x-Direction
{
	XMFLOAT3 forward = XMFLOAT3(1, 0, 0);
	XMStoreFloat3(&forward, transformDirectionToWorld(XMLoadFloat3(&forward)));
	return forward;
}

XMFLOAT3 Transform::getUp()//y-Direction
{
	XMFLOAT3 up = XMFLOAT3(0, 1, 0);
	XMStoreFloat3(&up, transformDirectionToWorld(XMLoadFloat3(&up)));
	return up;
}

XMFLOAT3 Transform::getRight()//z-Direction
{
	XMFLOAT3 right = XMFLOAT3(0, 0, 1);
	XMStoreFloat3(&right, transformDirectionToWorld(XMLoadFloat3(&right)));
	return right;
}

XMFLOAT3 Transform::getPosition()
{
	if (m_master->getParent() == NULL) {
		position = localPosition;
		return position;
	}
	XMStoreFloat3(&position, m_master->getParent()->getTransform()->transformPointToWorld(XMLoadFloat3(&localPosition)));
	return position;
}

XMFLOAT3 Transform::getLocalPosition()
{
	return localPosition;
}

XMFLOAT3 Transform::getScale()
{
	if (m_master->getParent() == NULL)
	{
		return localScale;
	}
	XMStoreFloat3(&scale, XMLoadFloat3(&m_master->getParent()->getTransform()->getScale()) * XMLoadFloat3(&localScale));
	return scale;
}

XMFLOAT3 Transform::getLocalScale()
{
	return localScale;
}

XMFLOAT4 Transform::getRotation()
{
	if (m_master->getParent() == NULL) {
		rotation = localRotation;
		return rotation;
	}
	XMStoreFloat4(&rotation, m_master->getParent()->getTransform()->transformDirectionToWorld(XMLoadFloat4(&localRotation)));
	return rotation;
}

XMFLOAT4 Transform::getLoacalRotation()
{
	return localRotation;
}

void Transform::OnStart() {

}

void Transform::OnUpdate(float dT) {

}

void Transform::OnDestroy() {

}

void Transform::OnCollision(GameObject::CollisionObject collision) {

}