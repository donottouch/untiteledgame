#pragma once
#include "Component.h"

using namespace DirectX;

//Transform repreesents the CENTER of a coordinate system
class Transform : public Component
{
private:
	XMFLOAT4X4 localToWorldMatrix_, worldToLocalMatrix_;
	bool isDirty_ = true;
	bool isInvDirty_ = true;

	XMFLOAT3 forward, up, right;
	XMFLOAT3 localPosition = XMFLOAT3(0,0,0); //position relative to parent, position in "own" coordinate system is ALWAYS 0,0,0
	XMFLOAT3 position = XMFLOAT3(0,0,0); //global position
	XMFLOAT3 localScale = XMFLOAT3(1,1,1); //local scale of coordinate system, relative to parent
	XMFLOAT3 scale = XMFLOAT3(1,1,1); //global scale, resuling from all parent transforms
	XMFLOAT4 localRotation = XMFLOAT4(0,0,0,1); //local rotation of coordinate system
	XMFLOAT4 rotation = XMFLOAT4(0,0,0,1); //global rotation

	void setAllChildrenDitry();
	XMMATRIX calculateLocalToParentMatrix_();

public:
	Transform(GameObject* master);
	Transform(GameObject* master, XMFLOAT3 globalposition);
	Transform(GameObject* master, XMFLOAT3 localposition, XMFLOAT3 localscale, XMFLOAT4 localrotation);
	~Transform();

	XMVECTOR transformPointToWorld(XMVECTOR point);
	XMVECTOR transformDirectionToWorld(XMVECTOR direction);	
	XMVECTOR transformPointToLocal(XMVECTOR point);
	XMVECTOR transformDirectionToLocal(XMVECTOR direction);

	void OnUpdate(float dT);
	void OnStart();
	void OnDestroy();
	void OnCollision(GameObject::CollisionObject collision);

	XMFLOAT3 getForward(), getUp(), getRight(), getLocalPosition(), getPosition(), getLocalScale(), getScale();
	XMFLOAT4 getLoacalRotation(), getRotation();

	void setLocalPosition(XMFLOAT3 localposition), setPosition(XMFLOAT3 globalposition), setLocalScale(XMFLOAT3 localscale), setScale(XMFLOAT3 globalscale), setLocalRotation(XMFLOAT4 localrotation), setRotation(XMFLOAT4 globalrotation);

	//public Methodes
	void Translate(XMFLOAT3 globalDirection, float distance);
	void TranslateLocal(XMFLOAT3 localDirection, float distance);
	void Rotate(XMFLOAT3 eulerAngles);
	void RotateLocal(XMFLOAT3 localEulerAngles);
	void SetLocalRotation(XMFLOAT3 localRadians);
	void RotateAround(XMFLOAT3 worldAxis, float angle);
	void RotateAroundLocal(XMFLOAT3 localAxis, float angle);
	XMFLOAT4X4 getLocalToWorldMatrix(), getWorldToLocalMatrix();
};