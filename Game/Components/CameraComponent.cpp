#include "pch.h"
#include "../Components/CameraComponent.h"
#include "Components/Transform.h"

Renderer::CameraComponent::CameraComponent() {

}

Renderer::CameraComponent::~CameraComponent() {

}

void Renderer::CameraComponent::OnStart() {

}

void Renderer::CameraComponent::OnUpdate(float dT) {

}

void Renderer::CameraComponent::OnDestroy() {

}

void Renderer::CameraComponent::OnCollision(GameObject::CollisionObject collision) {

}

XMFLOAT4X4 Renderer::CameraComponent::GetViewMatrix() const {
	Transform* transform = getMasterGO()->getTransform();

	XMFLOAT3 position_F = transform->getPosition();
	XMFLOAT3 forward_F = transform->getForward();
	XMFLOAT3 up_F = transform->getUp();

	const XMVECTORF32 eye = { position_F.x, position_F.y, position_F.z, 0.0f };
	const XMVECTORF32 fwd = { forward_F.x, forward_F.y, forward_F.z, 0.0f };
	const XMVECTORF32 up = { up_F.x, up_F.y, up_F.z, 0.0f };

	XMMATRIX view = XMMatrixTranspose(XMMatrixLookAtRH(eye, eye + fwd, up));
	XMFLOAT4X4 view_F;
	XMStoreFloat4x4(&view_F, view);

	return view_F;
}
