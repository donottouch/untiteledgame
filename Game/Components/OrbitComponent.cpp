#include "pch.h"
#include "OrbitComponent.h"
#include "Components/Behaviours/OrbitManager.h"


OrbitComponent::~OrbitComponent()
{
	if (orbitManager != nullptr) {
		orbitManager->removeOrbitComponent(this);
	}
}

void OrbitComponent::OnStart() {

}

void OrbitComponent::OnUpdate(float dT) {

}

void OrbitComponent::OnDestroy() {

}

void OrbitComponent::OnCollision(GameObject::CollisionObject collision) {

}

void OrbitComponent::setRotationCenter(GameObject* rotCenter) {
	rotationCenter = rotCenter;
}
GameObject* OrbitComponent::getRotationCenter() {
	return rotationCenter;
}

void OrbitComponent::setPeriapsisCenterGO(GameObject* periapsisCenterGO) {
	periapsisCenter = periapsisCenterGO;
}
GameObject* OrbitComponent::getPeriapsisCenterGO() {
	return periapsisCenter;
}

void OrbitComponent::setPeriapsisGO(GameObject* periapsisGO) {
	periapsis = periapsisGO;
}
GameObject* OrbitComponent::getPeriapsisGO() {
	return periapsis;
}
