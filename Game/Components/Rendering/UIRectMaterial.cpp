#pragma once
#include "pch.h"
#include "UIRectMaterial.h"
#include "Content/ShaderStructures.h"

Renderer::UIRectMaterial::UIRectMaterial(LPCWSTR vertexShader, LPCWSTR pixelShader) {
	m_VertexShader = vertexShader;
	m_GeometryShader = nullptr;
	m_PixelShader = pixelShader;
	m_FillMode = D3D12_FILL_MODE_SOLID;
	m_CullMode = D3D12_CULL_MODE_NONE;
	m_Topology = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	m_BlendState = transparent;
	m_BlendStateNormalDepth = discardNormalDepth;
	m_DepthTest = FALSE;
	m_DepthWrite = FALSE;
	
}

void Renderer::UIRectMaterial::CreateRootSignature(const std::shared_ptr<DX::DeviceResources>& deviceResources) {

	auto d3dDevice = deviceResources->GetD3DDevice();

	CD3DX12_ROOT_PARAMETER1 parameter[3];
	parameter[0].InitAsConstants(NUM_32BIT_CONSTANTS(RectVertexParams), 0, 0, D3D12_SHADER_VISIBILITY_VERTEX);
	parameter[1].InitAsConstants(NUM_32BIT_CONSTANTS(RectPixelParams), 1, 0, D3D12_SHADER_VISIBILITY_PIXEL);
	parameter[2].InitAsConstants(NUM_32BIT_CONSTANTS(SMAARTMetrics), 2, 0, D3D12_SHADER_VISIBILITY_ALL);


	D3D12_ROOT_SIGNATURE_FLAGS rootSignatureFlags =
		D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS;
	//D3D12_ROOT_SIGNATURE_FLAG_DENY_VERTEX_SHADER_ROOT_ACCESS;

	CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC descRootSignature;
	descRootSignature.Init_1_1(3, parameter, 0, nullptr, rootSignatureFlags);

	Microsoft::WRL::ComPtr<ID3DBlob> pSignature;
	Microsoft::WRL::ComPtr<ID3DBlob> pError;
	DX::ThrowIfFailed(D3D12SerializeVersionedRootSignature(&descRootSignature, pSignature.GetAddressOf(), pError.GetAddressOf()));//(&descRootSignature, D3D_ROOT_SIGNATURE_VERSION_1, pSignature.GetAddressOf(), pError.GetAddressOf()));
	DX::ThrowIfFailed(d3dDevice->CreateRootSignature(0, pSignature->GetBufferPointer(), pSignature->GetBufferSize(), IID_PPV_ARGS(&m_RootSignature)));//mat.m_RootSignature)));
}
D3D12_INPUT_LAYOUT_DESC Renderer::UIRectMaterial::GetInputLayout() {
	return { 0,0 };
}
