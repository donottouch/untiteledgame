#pragma once
#ifndef UI_RENDER_COMPONENT
#define UI_RENDER_COMPONENT

#include "pch.h"
#include "RenderComponent.h"
#include "UITextMaterial.h"
#include "Texture.h"
//#include "Fonts.h"

//based on https://github.com/Microsoft/DirectX-Graphics-Samples/blob/master/MiniEngine/Core/TextRenderer.cpp

namespace Renderer {
	class Font;
	class UIRectComponent;

	class UIRenderComponent : public RenderComponent
	{
	public:
		UIRenderComponent(UINT bufferSize);
		~UIRenderComponent();

		//Events
		void OnUpdate(float dT);
		void OnStart();
		void OnDestroy();
		void OnCollision(GameObject::CollisionObject collision);
		void CreateResources(const std::shared_ptr<DX::DeviceResources>& deviceResources);
		void UpdateConstantBuffer(Renderer::ModelViewProjectionConstantBuffer* viewProjection, const std::shared_ptr<DX::DeviceResources>& deviceResources);

		void SetText(std::wstring str);
		//length of the string without \0
		void SetText(LPCWSTR str, size_t length);

		void Renderer::UIRenderComponent::SetAnchor(XMFLOAT2 anchorUV) { m_AnchorUV = anchorUV; m_DirtyPosition = true; }
		void Renderer::UIRenderComponent::SetOffset(XMINT2 offsetPix) { m_OffsetPix = offsetPix; m_DirtyPosition = true; }
		//void SetPosition(XMFLOAT2 pos);
		void SetSize(float size);
		void SetLocalAnchor(XMFLOAT2 anchorUV) { m_LocalAnchorUV = anchorUV; m_DirtyPosition = true; }
		void SetFrame(UIRectComponent* frame) { m_Frame = frame; m_DirtyPosition = true; }
		void SetFrameMargins(INT offsetPix) { SetFrameMargins(XMINT2(offsetPix, offsetPix)); }
		void SetFrameMargins(XMINT2 offsetPix) { m_FrameMargins = offsetPix; m_DirtyPosition = true; }

		void SetColor(XMFLOAT4 color);
		void ClearText() {
			m_Text.strLen = 0;
			m_Text.drawStrLen = 0;
			m_TextUpdated = false;
		}

		XMINT2 GetFrameOffset() const {
			return XMINT2(static_cast<int32_t>(m_VSParams.offsetPix.x) - 4,
				static_cast<int32_t>(m_VSParams.offsetPix.y) - 2 - static_cast<int32_t>(m_VSParams.dstBorder));
		}
		XMINT2 GetFrameSize() const { return m_FrameSize; }

		XMFLOAT2 GetAnchor() const { return m_VSParams.anchorUV; }

	protected:
		friend class RenderEngine;
		void Initialize(UITextMaterial* mat, const Font* font, const std::shared_ptr<DX::DeviceResources>& deviceResources);
		void SetMaterial(UITextMaterial* mat) { m_Material = mat; }
		void SetFont(const Font* font);
		void CreateVB(const std::shared_ptr<DX::DeviceResources>& deviceResources);
		void UpdateVB(const std::shared_ptr<DX::DeviceResources>& deviceResources);
		void Draw(const std::shared_ptr<DX::DeviceResources>& deviceResources, Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& commandList);
		void DrawDepth(const std::shared_ptr<DX::DeviceResources>& deviceResources, Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& commandList) {}

	private:
		struct Text {
			LPCWSTR buffer;
			size_t strLen;
			UINT bufferSize, drawStrLen;
			float x, y, size, lineHight;
			XMFLOAT4 color;
		};

		//void SetFont(LPCWSTR fontName);
		//void DrawText(Text* pText);
		void SetFont(Font* font) { m_Font = font; }

		UINT FillVB();

		void UpdatePosition();
		void UpdateFrame();
		bool m_DirtyPosition;

		TextInstanceData* m_TextData = NULL;
		Microsoft::WRL::ComPtr<ID3D12Resource> m_vertexBuffer;
		D3D12_VERTEX_BUFFER_VIEW m_vertexBufferView;
		Text m_Text;
		TextVertexParams m_VSParams;
		TextPixelParams m_PSParams;
		RTMetrics m_RTMetrics;

		XMFLOAT2 m_AnchorUV;
		XMINT2 m_OffsetPix;
		XMFLOAT2 m_LocalAnchorUV;

		UIRectComponent* m_Frame;
		XMINT2 m_FrameMargins;
		XMINT2 m_FrameOffset;
		XMINT2 m_FrameSize;
		bool m_TextUpdated;
		const Font* m_Font;
		//Material* m_Material;

	};

	class Font {
	public:
		Font();
		~Font() { m_Dictionary.clear(); }

		// Each character has an XY start offset, a width, and they all share the same height
		struct Glyph {
			uint16_t x, y, w;
			int16_t bearing;
			uint16_t advance;
		};

		const Glyph* GetGlyph(WCHAR ch) const {
			auto it = m_Dictionary.find(ch);
			return it == m_Dictionary.end() ? nullptr : &it->second;
		}
		void LoadFromBinary(LPCWSTR fontName, const uint8_t* binary, const size_t binarySize, const std::shared_ptr<DX::DeviceResources>& deviceResources);

		/*const static Font* LoadDefaultFont() {
			Font* newFont = new Font();
			newFont->LoadFromBinary(L"default", g_pconsola24, sizeof(g_pconsola24));
			return newFont;
		}*/
	private:
		friend class UIRenderComponent;
		float m_NormalizeXCoord;
		float m_NormalizeYCoord;
		float m_FontLineSpacing;
		float m_AntialiasRange;
		uint16_t m_FontHeight;
		uint16_t m_BorderSize;
		uint16_t m_TextureWidth;
		uint16_t m_TextureHeight;
		Texture* m_Texture;
		std::map<wchar_t, Glyph> m_Dictionary;

		bool m_Initialized;
	};
}

#endif
