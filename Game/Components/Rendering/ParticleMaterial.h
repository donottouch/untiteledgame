#pragma once
#ifndef PARTICLE_MATERIAL
#define PARTICLE_MATERIAL

#include "pch.h"
#include "Material.h"

namespace Renderer {
	class ParticleMaterial : public Material {
	public:
		ParticleMaterial(LPCWSTR vertexShader, LPCWSTR geometryShader, LPCWSTR pixelShader, bool lit = false);
		ParticleMaterial(LPCWSTR vertexShader, LPCWSTR pixelShader, bool lit = false);
		~ParticleMaterial();
	protected:
		friend class RenderEngine;
		void CreateRootSignature(const std::shared_ptr<DX::DeviceResources>& deviceResources);
		D3D12_INPUT_LAYOUT_DESC GetInputLayout();
	private:
	};
}

#endif