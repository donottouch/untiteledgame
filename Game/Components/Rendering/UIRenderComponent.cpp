#include "pch.h"
#include "UIRenderComponent.h"
#include "UIRectComponent.h"

using namespace Renderer;

UIRenderComponent::UIRenderComponent(UINT bufferSize) : m_Frame(nullptr), m_DirtyPosition(true) {
	m_Text.bufferSize = bufferSize;
	m_Text.buffer = (LPCWSTR) malloc(sizeof(WCHAR) * bufferSize);
	ZeroMemory((void*) m_Text.buffer, sizeof(WCHAR) * bufferSize);
	m_Text.strLen = 0;
	m_Text.drawStrLen = 0;

	m_TextData = (TextInstanceData*) malloc(sizeof(TextInstanceData) * bufferSize);
	ZeroMemory((void*)m_TextData, sizeof(TextInstanceData) * bufferSize);

	m_Visible = true;

	m_AnchorUV = XMFLOAT2(0, 0);
	m_OffsetPix = XMINT2(0, 0);
	m_LocalAnchorUV = XMFLOAT2(0, 0);

	m_FrameSize = XMINT2(0, 0);
	m_FrameMargins = XMINT2(6, 6);
}

UIRenderComponent::~UIRenderComponent() {
	delete[] m_Text.buffer;
	delete[] m_TextData;
}

void UIRenderComponent::OnStart() {

}

void UIRenderComponent::OnUpdate(float dT) {
	if (m_DirtyPosition) UpdatePosition();
}

void UIRenderComponent::OnDestroy() {

}

void UIRenderComponent::OnCollision(GameObject::CollisionObject collision) {

}

void Renderer::UIRenderComponent::CreateResources(const std::shared_ptr<DX::DeviceResources>& deviceResources) {}

void Renderer::UIRenderComponent::UpdateConstantBuffer(Renderer::ModelViewProjectionConstantBuffer * viewProjection, const std::shared_ptr<DX::DeviceResources>& deviceResources) {
	UpdateVB(deviceResources);
}

void Renderer::UIRenderComponent::SetText(std::wstring wstr) {
	SetText(wstr.c_str(), wstr.size());
}

void UIRenderComponent::SetText(LPCWSTR str, size_t length) {
	memcpy((void*)m_Text.buffer, str, length < m_Text.bufferSize - 1 ? sizeof(WCHAR) * length : sizeof(WCHAR) * (m_Text.bufferSize - 1));
	m_Text.strLen = length;
	m_TextUpdated = true;
}


void Renderer::UIRenderComponent::SetSize(float size) {
	if (size == m_VSParams.textSize) return;

	m_VSParams.textSize = size;

	if (m_Font != NULL && m_Font->m_Initialized) {
		m_VSParams.textScale = m_VSParams.textSize / m_Font->m_FontHeight;
		m_VSParams.dstBorder = m_Font->m_BorderSize * m_VSParams.textScale;
		m_PSParams.heightRange = max(1.0f, m_Font->m_AntialiasRange * m_VSParams.textSize);
		m_Text.lineHight = m_Font->m_FontLineSpacing * m_VSParams.textSize;
	}
	else m_Text.lineHight = 0.0f;

	m_DirtyPosition = true;
}

void Renderer::UIRenderComponent::SetColor(XMFLOAT4 color) {
	m_PSParams.color = color;
}

void Renderer::UIRenderComponent::SetFont(const Font* font) {
	if (m_Font == font) return;
	m_Font = font;

	m_Text.lineHight = m_Font->m_FontLineSpacing * m_VSParams.textSize;
	m_VSParams.invTexDim = XMFLOAT2( m_Font->m_NormalizeXCoord, m_Font->m_NormalizeYCoord);
	m_VSParams.textScale = m_VSParams.textSize / m_Font->m_FontHeight;
	m_VSParams.dstBorder = m_Font->m_BorderSize * m_VSParams.textScale;
	m_VSParams.srcBorder = m_Font->m_BorderSize;
	m_PSParams.heightRange = max(1.0f, m_Font->m_AntialiasRange * m_VSParams.textSize);

}

UINT UIRenderComponent::FillVB() {
	UINT chars = 0;
	const float UVtoPixel = m_VSParams.textScale;

	float curX = 0, curY = 0;

	float maxX = 0;
	//m_FrameOffset = XMINT2(m_VSParams.offsetPix.x - 2, m_VSParams.offsetPix.y - 2 - m_VSParams.dstBorder);

	const UINT height = m_Font->m_FontHeight;

	const WCHAR* iter = m_Text.buffer;
	TextInstanceData* instanceIter = m_TextData;

	for (size_t i = 0; i < m_Text.strLen; ++i) {
		WCHAR c = *iter;
		iter++;
		if (c == L'\0') break;
		if (c == L'\n') {
			curX = 0;
			curY += m_Text.lineHight; //TODO line hight
			continue;
		}

		const Font::Glyph* glyph = m_Font->GetGlyph(c);

		if (glyph == NULL) continue;

		instanceIter->pos = XMFLOAT2(curX + (float)glyph->bearing * UVtoPixel, curY);
		instanceIter->texX = glyph->x;
		instanceIter->texY = glyph->y;
		instanceIter->texW = glyph->w;
		instanceIter->texH = height;

		instanceIter++;

		curX += (float)glyph->advance * UVtoPixel;
		maxX = curX > maxX ? curX : maxX;
		chars++;
	}

	m_FrameSize = XMINT2(static_cast<int32_t>(maxX), static_cast<int32_t>(curY + m_Text.lineHight));

	UpdatePosition();

	return chars;
}

void Renderer::UIRenderComponent::UpdatePosition() {
	m_VSParams.anchorUV = m_AnchorUV;

	XMINT2 offset2 = XMINT2(
		m_OffsetPix.x - m_FrameSize.x * m_LocalAnchorUV.x,
		m_OffsetPix.y - m_FrameSize.y * m_LocalAnchorUV.y
	);
	m_VSParams.offsetPix = offset2;

	m_DirtyPosition = false;
	UpdateFrame();
}

void Renderer::UIRenderComponent::UpdateFrame() {
	if (m_Frame) {
		m_Frame->SetAnchor(m_AnchorUV);
		XMINT2 offset = XMINT2(
			m_VSParams.offsetPix.x - m_FrameMargins.x,
			m_VSParams.offsetPix.y - m_FrameMargins.y
		);
		m_Frame->SetOffset(offset);
		XMINT2 frameSize = XMINT2(
			m_FrameSize.x + 2*m_FrameMargins.x,
			m_FrameSize.y + 2*m_FrameMargins.y
		);
		m_Frame->SetSize(frameSize);
	}
}

void Renderer::UIRenderComponent::Draw(const std::shared_ptr<DX::DeviceResources>& deviceResources, Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& commandList) {

	//TODO: this should not need a mesh, replace for vertex buffer
	if (m_Text.drawStrLen<1 || m_Material == nullptr || m_Font == nullptr || !m_Material->IsInitialized() || !m_Font->m_Initialized) return;

	commandList->SetGraphicsRootSignature(m_Material->GetRootSignature());
	commandList->SetPipelineState(m_Material->GetPSO());

	ID3D12DescriptorHeap* ppHeaps[] = { m_Font->m_Texture->GetSRVDescriptorHeap(), deviceResources->GetSamplerDescriptorHeap() }; //, m_Font->m_Texture->GetSRVDescriptorHeap() , deviceResources->GetSamplerDescriptorHeap()
	commandList->SetDescriptorHeaps(_countof(ppHeaps), ppHeaps);

	// Bindet den Konstantenpuffer des aktuellen Frames an die Pipeline.
	commandList->SetGraphicsRoot32BitConstants(0, NUM_32BIT_CONSTANTS(TextVertexParams), &m_VSParams, 0);
	commandList->SetGraphicsRoot32BitConstants(1, NUM_32BIT_CONSTANTS(TextPixelParams), &m_PSParams, 0);
	commandList->SetGraphicsRootDescriptorTable(2, m_Font->m_Texture->GetShaderResourceViewGPU());
	commandList->SetGraphicsRootDescriptorTable(3, deviceResources->GetSamplerViewGPU(DX::SAMPLER_TRILINEAR));
	m_RTMetrics.rtmetrics = deviceResources->GetRTMetrics();
	commandList->SetGraphicsRoot32BitConstants(4, NUM_32BIT_CONSTANTS(RTMetrics), &m_RTMetrics, 0);

	commandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
	commandList->IASetVertexBuffers(0, 1, &m_vertexBufferView);

	commandList->DrawInstanced(4, m_Text.drawStrLen, 0, 0);

}

void Renderer::UIRenderComponent::Initialize(UITextMaterial * mat, const Font * font, const std::shared_ptr<DX::DeviceResources>& deviceResources) {
	SetMaterial(mat);
	SetFont(font);
	CreateVB(deviceResources);
	m_LoadingComplete = true;
}

void Renderer::UIRenderComponent::CreateVB(const std::shared_ptr<DX::DeviceResources>& deviceResources) {
	auto d3dDevice = deviceResources->GetD3DDevice();
	//Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> cL;
	//deviceResources->WaitForGpu();
	//DX::ThrowIfFailed(d3dDevice->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, deviceResources->GetCommandAllocator(), m_Material->GetPSO(), IID_PPV_ARGS(&cL)));

	CD3DX12_HEAP_PROPERTIES defaultHeapProperties(D3D12_HEAP_TYPE_DEFAULT);
	CD3DX12_RESOURCE_DESC vertexBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(m_Text.bufferSize * sizeof(TextInstanceData));
	DX::ThrowIfFailed(d3dDevice->CreateCommittedResource(
		&defaultHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&vertexBufferDesc,
		D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER,
		nullptr,
		IID_PPV_ARGS(&m_vertexBuffer)));

}

void Renderer::UIRenderComponent::UpdateVB(const std::shared_ptr<DX::DeviceResources>& deviceResources) {
	if (!m_TextUpdated) return;
	m_Text.drawStrLen = FillVB();
	m_TextUpdated = false;

	if (m_Text.drawStrLen < 1) return;

	Microsoft::WRL::ComPtr<ID3D12Resource> vertexBufferUpload;

	auto d3dDevice = deviceResources->GetD3DDevice();
	Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> cL;
	deviceResources->WaitForGpu();
	DX::ThrowIfFailed(d3dDevice->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, deviceResources->GetCommandAllocator(), m_Material->GetPSO(), IID_PPV_ARGS(&cL)));

	CD3DX12_RESOURCE_DESC vertexBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(m_Text.bufferSize * sizeof(TextInstanceData));
	CD3DX12_HEAP_PROPERTIES uploadHeapProperties(D3D12_HEAP_TYPE_UPLOAD);
	DX::ThrowIfFailed(d3dDevice->CreateCommittedResource(
		&uploadHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&vertexBufferDesc,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&vertexBufferUpload)));

	// upload vertex buffer to GPU.
	{
		CD3DX12_RESOURCE_BARRIER vertexBufferResourceBarrier =
			CD3DX12_RESOURCE_BARRIER::Transition(m_vertexBuffer.Get(), D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, D3D12_RESOURCE_STATE_COPY_DEST);
		cL->ResourceBarrier(1, &vertexBufferResourceBarrier);

		D3D12_SUBRESOURCE_DATA vertexData = {};
		vertexData.pData = reinterpret_cast<BYTE*>(m_TextData);
		vertexData.RowPitch = m_Text.bufferSize * sizeof(TextInstanceData);
		vertexData.SlicePitch = vertexData.RowPitch;

		UpdateSubresources(cL.Get(), m_vertexBuffer.Get(), vertexBufferUpload.Get(), 0, 0, 1, &vertexData);

		vertexBufferResourceBarrier =
			CD3DX12_RESOURCE_BARRIER::Transition(m_vertexBuffer.Get(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER);
		cL->ResourceBarrier(1, &vertexBufferResourceBarrier);
	}


	DX::ThrowIfFailed(cL->Close());
	ID3D12CommandList* ppCommandLists[] = { cL.Get() };
	deviceResources->GetCommandQueue()->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);

	m_vertexBufferView.BufferLocation = m_vertexBuffer->GetGPUVirtualAddress();
	m_vertexBufferView.StrideInBytes = sizeof(TextInstanceData);
	m_vertexBufferView.SizeInBytes = m_Text.drawStrLen * sizeof(TextInstanceData);

	deviceResources->WaitForGpu();
	
}

Font::Font() {
	m_NormalizeXCoord = 0.0f;
	m_NormalizeYCoord = 0.0f;
	m_FontLineSpacing = 0.0f;
	m_AntialiasRange = 0.0f;
	m_FontHeight = 0;
	m_BorderSize = 0;
	m_TextureWidth = 0;
	m_TextureHeight = 0;
	m_Initialized = false;

	m_Texture = new Texture;

}

void Renderer::Font::LoadFromBinary(LPCWSTR fontName, const uint8_t * binary, const size_t binarySize, const std::shared_ptr<DX::DeviceResources>& deviceResources) {
	(fontName);

	// We should at least use this to assert that we have a complete file
	(binarySize);

	struct FontHeader {
		char FileDescriptor[8];        // "SDFFONT\0"
		uint8_t  majorVersion;        // '1'
		uint8_t  minorVersion;        // '0'
		uint16_t borderSize;        // Pixel empty space border width
		uint16_t textureWidth;        // Width of texture buffer
		uint16_t textureHeight;        // Height of texture buffer
		uint16_t fontHeight;        // Font height in 12.4
		uint16_t advanceY;            // Line height in 12.4
		uint16_t numGlyphs;            // Glyph count in texture
		uint16_t searchDist;        // Range of search space 12.4
	};

	FontHeader* header = (FontHeader*)binary;
	m_NormalizeXCoord = 1.0f / (header->textureWidth * 16);
	m_NormalizeYCoord = 1.0f / (header->textureHeight * 16);
	m_FontHeight = header->fontHeight;
	m_FontLineSpacing = (float)header->advanceY / (float)header->fontHeight;
	m_BorderSize = header->borderSize * 16;
	m_AntialiasRange = (float)header->searchDist / header->fontHeight;
	uint16_t textureWidth = header->textureWidth;
	uint16_t textureHeight = header->textureHeight;
	uint16_t NumGlyphs = header->numGlyphs;

	const wchar_t* wcharList = (wchar_t*)(binary + sizeof(FontHeader));
	const Glyph* glyphData = (Glyph*)(wcharList + NumGlyphs);
	const void* texelData = glyphData + NumGlyphs;

	for (uint16_t i = 0; i < NumGlyphs; ++i)
		m_Dictionary[wcharList[i]] = glyphData[i];

	m_Texture->CreateFromBinary(textureWidth, textureHeight, DXGI_FORMAT_R8_SNORM, textureWidth, texelData, deviceResources);

	m_Initialized = true;
}
