#pragma once
#ifndef UI_RECT_COMPONENT
#define UI_RECT_COMPONENT

#include "pch.h"
#include "RenderComponent.h"
#include "Texture.h"
//#include "Fonts.h"

//based on https://github.com/Microsoft/DirectX-Graphics-Samples/blob/master/MiniEngine/Core/TextRenderer.cpp

namespace Renderer {

	class UIRectComponent : public RenderComponent
	{
	public:
		UIRectComponent();
		~UIRectComponent() {}

		//Events
		void OnUpdate(float dT) {}
		void OnStart() {}
		void OnDestroy() {}
		void OnCollision(GameObject::CollisionObject collision) {}
		void CreateResources(const std::shared_ptr<DX::DeviceResources>& deviceResources);
		void UpdateConstantBuffer(Renderer::ModelViewProjectionConstantBuffer* viewProjection, const std::shared_ptr<DX::DeviceResources>& deviceResources);

		void SetAnchor(XMFLOAT2 anchorUV);
		void SetOffset(XMINT2 offsetPix);
		void SetSize(XMINT2 sizePix);
		void SetLineWidth(UINT lineWidthPix);
		void SetColor(XMFLOAT4 color);

	protected:
		friend class RenderEngine;
		void Initialize(std::shared_ptr<Material> mat, const std::shared_ptr<DX::DeviceResources>& deviceResources);
		void SetMaterial(std::shared_ptr<Material> mat) { m_Material = mat; }
		void Draw(const std::shared_ptr<DX::DeviceResources>& deviceResources, Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& commandList);
		void DrawDepth(const std::shared_ptr<DX::DeviceResources>& deviceResources, Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& commandList) {}

	private:

		RectVertexParams m_VSParams;
		RectPixelParams m_PSParams;
		RTMetrics m_RTMetrics;
		std::shared_ptr<Material> m_Material;

	};
}

#endif
