#pragma once

#include "pch.h"
#include "UITextMaterial.h"

Renderer::UITextMaterial::UITextMaterial(LPCWSTR vertexShader, LPCWSTR pixelShader, D3D12_FILL_MODE fillMode, D3D12_CULL_MODE cullMode) {
	m_VertexShader = vertexShader;
	m_GeometryShader = nullptr;
	m_PixelShader = pixelShader;
	m_FillMode = fillMode;
	m_CullMode = cullMode;
	m_Topology = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	m_BlendState = transparent;
	m_BlendStateNormalDepth = discardNormalDepth;
	m_DepthTest = FALSE;
	m_DepthWrite = FALSE;
}

void Renderer::UITextMaterial::CreateRootSignature(const std::shared_ptr<DX::DeviceResources>& deviceResources) {

	auto d3dDevice = deviceResources->GetD3DDevice();

	CD3DX12_DESCRIPTOR_RANGE1 range[2];
	CD3DX12_ROOT_PARAMETER1 parameter[5];
	range[0].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0);
	range[1].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SAMPLER, 1, 0);
	parameter[0].InitAsConstants(NUM_32BIT_CONSTANTS(TextVertexParams), 0, 0, D3D12_SHADER_VISIBILITY_VERTEX);
	parameter[1].InitAsConstants(NUM_32BIT_CONSTANTS(TextPixelParams), 1, 0, D3D12_SHADER_VISIBILITY_PIXEL);
	parameter[2].InitAsDescriptorTable(1, range, D3D12_SHADER_VISIBILITY_PIXEL);
	parameter[3].InitAsDescriptorTable(1, range+1, D3D12_SHADER_VISIBILITY_PIXEL);
	parameter[4].InitAsConstants(NUM_32BIT_CONSTANTS(RTMetrics), 2, 0, D3D12_SHADER_VISIBILITY_ALL);


	D3D12_ROOT_SIGNATURE_FLAGS rootSignatureFlags =
		D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS;
	//D3D12_ROOT_SIGNATURE_FLAG_DENY_VERTEX_SHADER_ROOT_ACCESS;

	CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC descRootSignature;
	descRootSignature.Init_1_1(5, parameter, 0, nullptr, rootSignatureFlags);

	Microsoft::WRL::ComPtr<ID3DBlob> pSignature;
	Microsoft::WRL::ComPtr<ID3DBlob> pError;
	DX::ThrowIfFailed(D3D12SerializeVersionedRootSignature(&descRootSignature, pSignature.GetAddressOf(), pError.GetAddressOf()));//(&descRootSignature, D3D_ROOT_SIGNATURE_VERSION_1, pSignature.GetAddressOf(), pError.GetAddressOf()));
	DX::ThrowIfFailed(d3dDevice->CreateRootSignature(0, pSignature->GetBufferPointer(), pSignature->GetBufferSize(), IID_PPV_ARGS(&m_RootSignature)));//mat.m_RootSignature)));
}


D3D12_INPUT_LAYOUT_DESC Renderer::UITextMaterial::GetInputLayout() {
	return { Renderer::TextInstanceDataLayout, _countof(Renderer::TextInstanceDataLayout) };
}
