#pragma once
#ifndef TEXTURE
#define TEXTURE

#include "pch.h"
#include "..\Common\DeviceResources.h"
#include "..\Common\DirectXHelper.h"

namespace Renderer {
	class Texture {
	public:
		Texture();
		~Texture();
		void CreateFromBinary(UINT width, UINT height, DXGI_FORMAT format, UINT pitch, const void* data, const std::shared_ptr<DX::DeviceResources>& deviceResources);


		CD3DX12_CPU_DESCRIPTOR_HANDLE GetShaderResourceView() const {
			return CD3DX12_CPU_DESCRIPTOR_HANDLE(m_SrvHeap->GetCPUDescriptorHandleForHeapStart(), m_HeapOffset, m_SrvDescriptorSize);
		}
		CD3DX12_GPU_DESCRIPTOR_HANDLE GetShaderResourceViewGPU() const {
			return CD3DX12_GPU_DESCRIPTOR_HANDLE(m_SrvHeap->GetGPUDescriptorHandleForHeapStart(), m_HeapOffset, m_SrvDescriptorSize);
		}
		ID3D12DescriptorHeap* GetSRVDescriptorHeap() {
			return m_SrvHeap.Get();
		}
		bool IsLoaded() { return m_LoadingComplete; }
	private:
		Microsoft::WRL::ComPtr<ID3D12Resource>			m_Resource;
		Microsoft::WRL::ComPtr<ID3D12DescriptorHeap>	m_SrvHeap;
		UINT											m_HeapOffset;
		UINT											m_SrvDescriptorSize;

		bool m_LoadingComplete;
	};
}

#endif
