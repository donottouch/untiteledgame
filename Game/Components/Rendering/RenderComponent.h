#pragma once

#ifndef RENDER_COMPONENT
#define RENDER_COMPONENT

#include "pch.h"
#include "Component.h"
#include "Mesh.h"
#include "Material.h"
#include "Content/ShaderStructures.h"
//#include "ShaderStructures.h"

using namespace DirectX;

namespace Renderer {
	class RenderComponent : public Component {
	public:
		RenderComponent() {
			m_LoadingComplete = false;
			m_Mesh = nullptr;
			m_Material = nullptr;
			m_Visible = true;
		}
		virtual ~RenderComponent() {}
		//Events
		virtual void OnUpdate(float dT) = 0;
		virtual void OnStart() = 0;
		virtual void OnDestroy() = 0;
		virtual void OnCollision(GameObject::CollisionObject collision) = 0;

		virtual void CreateResources(const std::shared_ptr<DX::DeviceResources>& deviceResources) = 0;
		virtual void UpdateConstantBuffer(Renderer::ModelViewProjectionConstantBuffer* viewProjection, const std::shared_ptr<DX::DeviceResources>& deviceResources) = 0;
		virtual void Draw(const std::shared_ptr<DX::DeviceResources>& deviceResources, Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& commandList) = 0;
		virtual void DrawDepth(const std::shared_ptr<DX::DeviceResources>& deviceResources, Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& commandList) = 0;
		Mesh* GetMesh() { return m_Mesh; }
		Material* GetMaterial() { return m_Material; }
		bool IsLoadingComplete() { return m_LoadingComplete; }
		void SetVisible(bool visible) { m_Visible = visible; }

	protected:
		friend class RenderEngine;
		Microsoft::WRL::ComPtr<ID3D12Resource>				m_constantBuffer; //object
		Renderer::ModelViewProjectionConstantBuffer			m_constantBufferData; //object
		UINT8*												m_mappedConstantBuffer; //object
		Microsoft::WRL::ComPtr<ID3D12DescriptorHeap>		m_cbDescriptorHeap; //object
		UINT												m_cbDescriptorHeapSize; //object

		bool m_LoadingComplete;

		Renderer::Mesh* m_Mesh;
		Renderer::Material* m_Material;
		bool m_Visible;
	private:
	};
}

#endif