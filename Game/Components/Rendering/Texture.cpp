#pragma once
#include "pch.h"
#include "Texture.h"

Renderer::Texture::Texture() : m_LoadingComplete(false){
	
}

Renderer::Texture::~Texture() {
	//m_Resource.
}

void Renderer::Texture::CreateFromBinary(UINT width, UINT height, DXGI_FORMAT format, UINT pitch, const void * data, const std::shared_ptr<DX::DeviceResources>& deviceResources) {
	auto d3dDevice = deviceResources->GetD3DDevice();

	Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> cL;
	deviceResources->WaitForGpu();
	DX::ThrowIfFailed(d3dDevice->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, deviceResources->GetCommandAllocator(), NULL, IID_PPV_ARGS(&cL)));

	CD3DX12_HEAP_PROPERTIES defaultHeapProperties(D3D12_HEAP_TYPE_DEFAULT);
	CD3DX12_RESOURCE_DESC textureResourceDesc = CD3DX12_RESOURCE_DESC::Tex2D(format, width, height);
	DX::ThrowIfFailed(d3dDevice->CreateCommittedResource(
		&defaultHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&textureResourceDesc,
		D3D12_RESOURCE_STATE_COPY_DEST,
		nullptr,
		IID_PPV_ARGS(&m_Resource)));

	Microsoft::WRL::ComPtr<ID3D12Resource> textureResourceUpload;
	CD3DX12_RESOURCE_DESC textureUploadDesc = CD3DX12_RESOURCE_DESC::Buffer(GetRequiredIntermediateSize(m_Resource.Get(),0,1));

	CD3DX12_HEAP_PROPERTIES uploadHeapProperties(D3D12_HEAP_TYPE_UPLOAD);
	DX::ThrowIfFailed(d3dDevice->CreateCommittedResource(
		&uploadHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&textureUploadDesc,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&textureResourceUpload)));

	// upload vertex buffer to GPU.
	{
		D3D12_SUBRESOURCE_DATA textureData = {};
		textureData.pData = (data);
		textureData.RowPitch = pitch;
		textureData.SlicePitch = textureData.RowPitch;

		UpdateSubresources(cL.Get(), m_Resource.Get(), textureResourceUpload.Get(), 0, 0, 1, &textureData);

		CD3DX12_RESOURCE_BARRIER vertexBufferResourceBarrier =
			CD3DX12_RESOURCE_BARRIER::Transition(m_Resource.Get(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
		cL->ResourceBarrier(1, &vertexBufferResourceBarrier);
	}


	DX::ThrowIfFailed(cL->Close());
	ID3D12CommandList* ppCommandLists[] = { cL.Get() };
	deviceResources->GetCommandQueue()->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);

	//TODO SRV heap
	/*
	D3D12_DESCRIPTOR_HEAP_DESC HDRsrvHeapDesc = {};
	HDRsrvHeapDesc.NumDescriptors = 1;
	HDRsrvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	HDRsrvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	DX::ThrowIfFailed(d3dDevice->CreateDescriptorHeap(&HDRsrvHeapDesc, IID_PPV_ARGS(&m_SrvHeap)));
	NAME_D3D12_OBJECT(m_SrvHeap);
	*/
	m_SrvDescriptorSize = d3dDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
	if ((m_HeapOffset = deviceResources->RequestSrvDescriptor(&m_SrvHeap)) > -1) {

	}
	else {
		//TODO handle error
	}

	D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc;
	ZeroMemory(&srvDesc, sizeof(D3D12_SHADER_RESOURCE_VIEW_DESC));
	srvDesc.Format = format;
	srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;

	d3dDevice->CreateShaderResourceView(m_Resource.Get(), nullptr, GetShaderResourceView()); //crashes if srvDesc is used?

	deviceResources->WaitForGpu();
}
