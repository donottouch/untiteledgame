#include "pch.h"
#include "LightComponent.h"
#include "Components/Transform.h"

Renderer::LightComponent::LightComponent() {
	m_color = DirectX::XMFLOAT3(1,1,1);
	m_master = nullptr;
}

Renderer::LightComponent::~LightComponent() {}

DirectX::XMFLOAT4 Renderer::LightComponent::GetPosition() const {
	if (m_master != nullptr) {
		DirectX::XMFLOAT3 pos = m_master->getTransform()->getPosition();
		return DirectX::XMFLOAT4(pos.x, pos.y, pos.z, 1);
	}
	else
		return DirectX::XMFLOAT4(0, 0, 0, 1);
}
