//mesh representation and related resources, descriptors, etc.
#pragma once
#ifndef MESH
#define MESH

#include "pch.h"
#include "..\Content\ShaderStructures.h"
#include "Common/DeviceResources.h"
#include <string>
#include <set>
#include <algorithm>
#include <utility>

using namespace DirectX;

namespace Renderer {
	class Mesh {
	public:
		Mesh();
		~Mesh();
		void LoadFromFile(const char* path, bool flip);
		void LoadToGPU(const std::shared_ptr<DX::DeviceResources>& deviceResources, Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& cL, bool);
		bool IsLoaded() const { return m_isLoaded; };
		UINT GetIndexBufferSize() const { return m_indexBufferSize; };
		void Draw(Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& commandList, UINT instanceCount = 1);
		static Mesh* GetCubeMesh() {
			Mesh* mesh = new Mesh;
			mesh->m_vertexData = CubeVertexData();
			mesh->m_vertexDataSize = CubeVertexDataSize();
			mesh->m_triangleIndices = CubeTriangleList();
			mesh->m_triangleListSize = CubeTriangleListSize();
			mesh->m_lineIndices = CubeLineList();
			mesh->m_lineListSize = CubeTriangleListSize();
			return mesh;
		}
		static Mesh* GetQuadMesh() {
			Mesh* mesh = new Mesh;
			mesh->m_vertexData = QuadVertexData();
			mesh->m_vertexDataSize = QuadVertexDataSize();
			mesh->m_triangleIndices = QuadTriangleList();
			mesh->m_triangleListSize = QuadTriangleListSize();
			mesh->m_lineIndices = QuadLineList();
			mesh->m_lineListSize = QuadTriangleListSize();
			return mesh;
		}
		static Mesh* GetSphereMesh(size_t tessellation);
	protected:
		friend class RenderEngine;
		bool m_isLoaded = false;
		UINT m_indexBufferSize = 0;
		UINT m_indexCount = 0;
		UINT m_vertexDataSize = 0;
		UINT m_triangleListSize = 0;
		UINT m_lineListSize = 0;
		VertexData* m_vertexData;
		unsigned short* m_triangleIndices;
		unsigned short* m_lineIndices;

		Microsoft::WRL::ComPtr<ID3D12Resource>				m_vertexBuffer; //mesh
		Microsoft::WRL::ComPtr<ID3D12Resource>				m_indexBuffer; //mesh
		D3D12_VERTEX_BUFFER_VIEW							m_vertexBufferView; //mesh
		D3D12_INDEX_BUFFER_VIEW								m_indexBufferView; //mesh
	private:
		//Cube Data
		static Renderer::VertexData* CubeVertexData() {
			static Renderer::VertexData cubeVertices[] =
			{
				{ XMFLOAT3(-0.5f, -0.5f, -0.5f), XMFLOAT3(0.0f, 0.0f, 0.0f) },
				{ XMFLOAT3(-0.5f, -0.5f,  0.5f), XMFLOAT3(0.0f, 0.0f, 1.0f) },
				{ XMFLOAT3(-0.5f,  0.5f, -0.5f), XMFLOAT3(0.0f, 1.0f, 0.0f) },
				{ XMFLOAT3(-0.5f,  0.5f,  0.5f), XMFLOAT3(0.0f, 1.0f, 1.0f) },
				{ XMFLOAT3(0.5f, -0.5f, -0.5f), XMFLOAT3(1.0f, 0.0f, 0.0f) },
				{ XMFLOAT3(0.5f, -0.5f,  0.5f), XMFLOAT3(1.0f, 0.0f, 1.0f) },
				{ XMFLOAT3(0.5f,  0.5f, -0.5f), XMFLOAT3(1.0f, 1.0f, 0.0f) },
				{ XMFLOAT3(0.5f,  0.5f,  0.5f), XMFLOAT3(1.0f, 1.0f, 1.0f) },
			};
			return cubeVertices;
		}
		static UINT CubeVertexDataSize() {
			return 8 * 2 * sizeof(XMFLOAT3);
		}

		static unsigned short* CubeTriangleList() {
			static unsigned short cubeIndices[] =
			{
				0, 2, 1, // -x
				1, 2, 3,

				4, 5, 6, // +x
				5, 7, 6,

				0, 1, 5, // -y
				0, 5, 4,

				2, 6, 7, // +y
				2, 7, 3,

				0, 4, 6, // -z
				0, 6, 2,

				1, 3, 7, // +z
				1, 7, 5,
			};
			return cubeIndices;
		}

		static UINT CubeTriangleListSize()
		{
			return 36 * sizeof(unsigned short);
		}

		static unsigned short* CubeLineList() {
			static unsigned short cubeIndices[] =
			{
				0,1,
				0,2,
				1,3,
				2,3,

				4,5,
				4,6,
				5,7,
				6,7,

				0,4,
				1,5,
				2,6,
				3,7,
			};
			return cubeIndices;
		}

		static UINT CubeLineListSize()
		{
			return 24 * sizeof(unsigned short);
		}
		//Quad Data
		static Renderer::VertexData* QuadVertexData() {
			static Renderer::VertexData vertices[] =
			{
				{ XMFLOAT3(-1, -1, 0), XMFLOAT3(-1.0f, -1.0f, 0.0f) },
				{ XMFLOAT3(1, -1,  0), XMFLOAT3(1.0f, -1.0f, 0.0f) },
				{ XMFLOAT3(-1,  1, 0), XMFLOAT3(-1.0f, 1.0f, 0.0f) },
				{ XMFLOAT3(1,  1,  0), XMFLOAT3(1.0f, 1.0f, 0.0f) },
			};
			return vertices;
		}
		static UINT QuadVertexDataSize() {
			return 4 * 2 * sizeof(XMFLOAT3);
		}

		static unsigned short* QuadTriangleList() {
			static unsigned short indices[] =
			{
				0, 2, 1, // -x
				1, 2, 3,
			};
			return indices;
		}

		static UINT QuadTriangleListSize()
		{
			return 6 * sizeof(unsigned short);
		}

		static unsigned short* QuadLineList() {
			static unsigned short indices[] =
			{
				0,1,
				0,2,
				1,3,
				2,3,
			};
			return indices;
		}

		static UINT QuadLineListSize()
		{
			return 8 * sizeof(unsigned short);
		}

		
	};


	
}

#endif