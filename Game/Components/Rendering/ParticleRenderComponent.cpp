#include "pch.h"
#include "ParticleRenderComponent.h"
#include "Content/ShaderStructures.h"

Renderer::ParticleRenderComponent::ParticleRenderComponent(Material & material, std::shared_ptr<ParticleSystem>& particleSystem, ParticleSystemParameters & params) {
	m_ParticleBufferSize = max(1u, params.particleBufferSize);
	m_Mesh = nullptr;
	m_Material = &material;
	m_Visible = true;
	m_Active = true;
	m_WarpSize = 128;

	m_ParticleSystem = particleSystem;
	m_PSParametersBufferData = params;
	m_PSParametersBufferData.particleBufferSize = m_ParticleBufferSize;
	m_PSParametersBufferDirty = true;
	m_UpdateParametersBufferData = {};

	m_StatisticsBufferData.inactive = m_ParticleBufferSize;
	m_StatisticsBufferUploadData.active = 0;
	m_StatisticsBufferUploadData.inactive = 0;
}

void Renderer::ParticleRenderComponent::OnUpdate(float dT) {
	if (m_Active) {
		m_PendingSpawns += m_SpawnRate * dT;

	}
}

void Renderer::ParticleRenderComponent::CreateResources(const std::shared_ptr<DX::DeviceResources>& deviceResources) {

	auto d3dDevice = deviceResources->GetD3DDevice();
	//Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> commandList;
	//DX::ThrowIfFailed(d3dDevice->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, deviceResources->GetCommandAllocator(), NULL, IID_PPV_ARGS(&commandList)));
	{
		CD3DX12_HEAP_PROPERTIES uploadHeapProperties(D3D12_HEAP_TYPE_UPLOAD);
		CD3DX12_RESOURCE_DESC constantBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(c_alignedPSParamsBufferSize);
		DX::ThrowIfFailed(d3dDevice->CreateCommittedResource(
			&uploadHeapProperties,
			D3D12_HEAP_FLAG_NONE,
			&constantBufferDesc,
			D3D12_RESOURCE_STATE_GENERIC_READ,
			nullptr,
			IID_PPV_ARGS(&m_PSParametersBuffer)));
		CD3DX12_RANGE readRange(0, 0);		// Wir beabsichtigen nicht, aus dieser Ressource f�r die CPU zu lesen.
		DX::ThrowIfFailed(m_PSParametersBuffer->Map(0, &readRange, reinterpret_cast<void**>(&m_mappedPSParametersBuffer)));
		ZeroMemory(m_mappedPSParametersBuffer, c_alignedPSParamsBufferSize);
	}
	{
		CD3DX12_HEAP_PROPERTIES uploadHeapProperties(D3D12_HEAP_TYPE_UPLOAD);
		CD3DX12_RESOURCE_DESC constantBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(c_alignedParticleUpdateParametersBufferSize);
		DX::ThrowIfFailed(d3dDevice->CreateCommittedResource(
			&uploadHeapProperties,
			D3D12_HEAP_FLAG_NONE,
			&constantBufferDesc,
			D3D12_RESOURCE_STATE_GENERIC_READ,
			nullptr,
			IID_PPV_ARGS(&m_UpdateParametersBuffer)));
		CD3DX12_RANGE readRange(0, 0);		// Wir beabsichtigen nicht, aus dieser Ressource f�r die CPU zu lesen.
		DX::ThrowIfFailed(m_UpdateParametersBuffer->Map(0, &readRange, reinterpret_cast<void**>(&m_mappedUpdateParametersBuffer)));
		ZeroMemory(m_mappedUpdateParametersBuffer, c_alignedParticleUpdateParametersBufferSize);
	}
	{
		CD3DX12_HEAP_PROPERTIES defaultHeapProperties(D3D12_HEAP_TYPE_DEFAULT);
		CD3DX12_RESOURCE_DESC particleBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(sizeof(ParticleData) * m_ParticleBufferSize, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);
		DX::ThrowIfFailed(d3dDevice->CreateCommittedResource(
			&defaultHeapProperties,
			D3D12_HEAP_FLAG_NONE,
			&particleBufferDesc,
			D3D12_RESOURCE_STATE_UNORDERED_ACCESS,
			nullptr,
			IID_PPV_ARGS(&m_ParticleDataBuffer)));
	}
	{
		CD3DX12_HEAP_PROPERTIES defaultHeapProperties(D3D12_HEAP_TYPE_DEFAULT);
		CD3DX12_RESOURCE_DESC particleBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(sizeof(ParticleState) * m_ParticleBufferSize, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);
		DX::ThrowIfFailed(d3dDevice->CreateCommittedResource(
			&defaultHeapProperties,
			D3D12_HEAP_FLAG_NONE,
			&particleBufferDesc,
			D3D12_RESOURCE_STATE_UNORDERED_ACCESS,
			nullptr,
			IID_PPV_ARGS(&m_ParticleStatusBuffer)));
	}
	{
		CD3DX12_HEAP_PROPERTIES defaultHeapProperties(D3D12_HEAP_TYPE_UPLOAD);
		CD3DX12_RESOURCE_DESC particleBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(sizeof(ParticleStatistics));
		DX::ThrowIfFailed(d3dDevice->CreateCommittedResource(
			&defaultHeapProperties,
			D3D12_HEAP_FLAG_NONE,
			&particleBufferDesc,
			D3D12_RESOURCE_STATE_GENERIC_READ,
			nullptr,
			IID_PPV_ARGS(&m_StatisticsBufferUpload)));
		CD3DX12_RANGE readRange(0, 0);		// Wir beabsichtigen nicht, aus dieser Ressource f�r die CPU zu lesen.
		DX::ThrowIfFailed(m_StatisticsBufferUpload->Map(0, &readRange, reinterpret_cast<void**>(&m_mappedStatisticsBufferUpload)));
		ZeroMemory(m_mappedStatisticsBufferUpload, sizeof(ParticleStatistics));
		DX::UpdateConstantBuffer(m_mappedStatisticsBufferUpload, &m_StatisticsBufferUploadData);
	}
	{
		CD3DX12_HEAP_PROPERTIES defaultHeapProperties(D3D12_HEAP_TYPE_DEFAULT);
		CD3DX12_RESOURCE_DESC particleBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(sizeof(ParticleStatistics), D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);
		DX::ThrowIfFailed(d3dDevice->CreateCommittedResource(
			&defaultHeapProperties,
			D3D12_HEAP_FLAG_NONE,
			&particleBufferDesc,
			D3D12_RESOURCE_STATE_UNORDERED_ACCESS,
			nullptr,
			IID_PPV_ARGS(&m_StatisticsBuffer)));
	}
	{
		CD3DX12_HEAP_PROPERTIES heapProperties(D3D12_HEAP_TYPE_READBACK);
		CD3DX12_RESOURCE_DESC particleBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(sizeof(ParticleStatistics));
		DX::ThrowIfFailed(d3dDevice->CreateCommittedResource(
			&heapProperties,
			D3D12_HEAP_FLAG_NONE,
			&particleBufferDesc,
			D3D12_RESOURCE_STATE_COPY_DEST,
			nullptr,
			IID_PPV_ARGS(&m_StatisticsBufferReadback)));
	}
	//UpdateParticleConstantBuffer();
	//deviceResources->WaitForGpu();
}

void Renderer::ParticleRenderComponent::UpdateConstantBuffer(Renderer::ModelViewProjectionConstantBuffer * viewProjection, const std::shared_ptr<DX::DeviceResources>& deviceResources) {
	m_constantBufferData = *viewProjection;


	// Die Konstantenpufferressource aktualisieren.
	UINT currentFrame = deviceResources->GetCurrentFrameIndex();
	UINT8* destination = m_mappedConstantBuffer + (currentFrame * Renderer::c_alignedConstantBufferSize);
	//memcpy(destination, &m_constantBufferData, sizeof(m_constantBufferData));
	DX::UpdateConstantBuffer(destination, &m_constantBufferData);
}

void Renderer::ParticleRenderComponent::Draw(const std::shared_ptr<DX::DeviceResources>& deviceResources, Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& commandList) {
	if (!m_Visible || !m_LoadingComplete || m_Material == nullptr || !m_Material->IsInitialized() || m_StatisticsBufferData.active==0) return;

	commandList->SetGraphicsRootSignature(m_Material->GetRootSignature());
	commandList->SetPipelineState(m_Material->GetPSO());

	ID3D12DescriptorHeap* ppHeaps[] = { m_cbDescriptorHeap.Get() };
	commandList->SetDescriptorHeaps(_countof(ppHeaps), ppHeaps);

	// Bindet den Konstantenpuffer des aktuellen Frames an die Pipeline.
	CD3DX12_GPU_DESCRIPTOR_HANDLE gpuHandle(m_cbDescriptorHeap->GetGPUDescriptorHandleForHeapStart(), deviceResources->GetCurrentFrameIndex(), m_cbDescriptorHeapSize);
	commandList->SetGraphicsRootDescriptorTable(0, gpuHandle);
	CD3DX12_GPU_DESCRIPTOR_HANDLE gpuHandleTimer(m_cbDescriptorHeap->GetGPUDescriptorHandleForHeapStart(), DX::c_frameCount + 3, m_cbDescriptorHeapSize);
	commandList->SetGraphicsRootDescriptorTable(1, gpuHandleTimer);
	commandList->SetGraphicsRootUnorderedAccessView(2, m_ParticleDataBuffer->GetGPUVirtualAddress());
	commandList->SetGraphicsRootUnorderedAccessView(3, m_ParticleStatusBuffer->GetGPUVirtualAddress());

	commandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_POINTLIST);
	commandList->DrawInstanced(m_StatisticsBufferData.active, 1, 0, 0);
}

void Renderer::ParticleRenderComponent::UpdateParticleConstantBuffer() {
	DX::UpdateConstantBuffer(m_mappedUpdateParametersBuffer, &m_UpdateParametersBufferData);
	if (m_PSParametersBufferDirty) {
		DX::UpdateConstantBuffer(m_mappedPSParametersBuffer, &m_PSParametersBufferData);
		m_PSParametersBufferDirty = false;
	}
}

void Renderer::ParticleRenderComponent::SpawnParticles(const uint32_t numParticles) {
	m_PendingSpawns += numParticles;
}

void Renderer::ParticleRenderComponent::UpdateParticles(const std::shared_ptr<DX::DeviceResources>& deviceResources, Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& computeCommandList) {
	if (!m_LoadingComplete) return;


	uint32_t numParticles = static_cast<uint32_t>(m_PendingSpawns);
	m_PendingSpawns -= numParticles;

	if (numParticles == 0 && !m_Active) return;

	m_UpdateParametersBufferData.amountToSpawn = numParticles;
	m_UpdateParametersBufferData.seed = rand();
	UpdateParticleConstantBuffer();

	//auto d3dDevice = deviceResources->GetD3DDevice();
	//Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> commandList;
	//DX::ThrowIfFailed(d3dDevice->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_COMPUTE, deviceResources->GetComputeCommandAllocator(), NULL, IID_PPV_ARGS(&commandList)));

	computeCommandList->SetComputeRootSignature(m_ParticleSystem->GetRootSignature());

	ID3D12DescriptorHeap* ppHeaps[] = { m_cbDescriptorHeap.Get() };
	computeCommandList->SetDescriptorHeaps(_countof(ppHeaps), ppHeaps);

	CD3DX12_GPU_DESCRIPTOR_HANDLE gpuHandle(m_cbDescriptorHeap->GetGPUDescriptorHandleForHeapStart(), deviceResources->GetCurrentFrameIndex(), m_cbDescriptorHeapSize);
	computeCommandList->SetComputeRootDescriptorTable(0, gpuHandle);
	CD3DX12_GPU_DESCRIPTOR_HANDLE gpuHandleTimer(m_cbDescriptorHeap->GetGPUDescriptorHandleForHeapStart(), DX::c_frameCount + 3, m_cbDescriptorHeapSize);
	computeCommandList->SetComputeRootDescriptorTable(3, gpuHandleTimer);

	//commandList->SetComputeRootConstantBufferView(0, m_constantBuffer->GetGPUVirtualAddress());
	//commandList->SetComputeRootConstantBufferView(3, m_PSParametersBuffer->GetGPUVirtualAddress());

	computeCommandList->SetComputeRootConstantBufferView(1, m_PSParametersBuffer->GetGPUVirtualAddress());
	computeCommandList->SetComputeRootConstantBufferView(2, m_UpdateParametersBuffer->GetGPUVirtualAddress());

	computeCommandList->SetComputeRootUnorderedAccessView(4, m_ParticleDataBuffer->GetGPUVirtualAddress());
	computeCommandList->SetComputeRootUnorderedAccessView(5, m_ParticleStatusBuffer->GetGPUVirtualAddress());
	computeCommandList->SetComputeRootUnorderedAccessView(6, m_StatisticsBuffer->GetGPUVirtualAddress());

	//reset statistics
	{
		CD3DX12_RESOURCE_BARRIER statisticsResourceBarrier =
			CD3DX12_RESOURCE_BARRIER::Transition(m_StatisticsBuffer.Get(), D3D12_RESOURCE_STATE_UNORDERED_ACCESS, D3D12_RESOURCE_STATE_COPY_DEST);
		computeCommandList->ResourceBarrier(1, &statisticsResourceBarrier);
	}
	computeCommandList->CopyResource(m_StatisticsBuffer.Get(), m_StatisticsBufferUpload.Get());
	{
		CD3DX12_RESOURCE_BARRIER statisticsResourceBarrier =
			CD3DX12_RESOURCE_BARRIER::Transition(m_StatisticsBuffer.Get(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_UNORDERED_ACCESS);
		computeCommandList->ResourceBarrier(1, &statisticsResourceBarrier);
	}

	//update statistics
	uint32_t numGroups = GetThreadGroups(m_ParticleBufferSize);
	computeCommandList->SetPipelineState(m_ParticleSystem->GetStatsPSO());
	computeCommandList->Dispatch(numGroups, 1, 1);

	{
		CD3DX12_RESOURCE_BARRIER statisticsResourceBarrier = CD3DX12_RESOURCE_BARRIER::UAV(m_StatisticsBuffer.Get());
		computeCommandList->ResourceBarrier(1, &statisticsResourceBarrier);
	}

	if (m_Active) {
		{
			CD3DX12_RESOURCE_BARRIER statisticsResourceBarrier = CD3DX12_RESOURCE_BARRIER::UAV(m_ParticleDataBuffer.Get());
			//commandList->ResourceBarrier(1, &statisticsResourceBarrier);
		}
		//update active particles
		computeCommandList->SetPipelineState(m_ParticleSystem->GetUpdatePSO());
		computeCommandList->Dispatch(GetThreadGroups(m_StatisticsBufferData.active), 1, 1);
	}

	if (numParticles > 0) {
		{
			CD3DX12_RESOURCE_BARRIER statisticsResourceBarrier = CD3DX12_RESOURCE_BARRIER::UAV(m_ParticleDataBuffer.Get());
			//commandList->ResourceBarrier(1, &statisticsResourceBarrier);
		}
		//spawn new particles
		computeCommandList->SetPipelineState(m_ParticleSystem->GetSpawnPSO());
		computeCommandList->Dispatch(GetThreadGroups(min(numParticles, m_StatisticsBufferData.inactive)), 1, 1);
	}

	//reset statistics
	{
		CD3DX12_RESOURCE_BARRIER statisticsResourceBarrier =
			CD3DX12_RESOURCE_BARRIER::Transition(m_StatisticsBuffer.Get(), D3D12_RESOURCE_STATE_UNORDERED_ACCESS, D3D12_RESOURCE_STATE_COPY_DEST);
		computeCommandList->ResourceBarrier(1, &statisticsResourceBarrier);
	}
	computeCommandList->CopyResource(m_StatisticsBuffer.Get(), m_StatisticsBufferUpload.Get());
	{
		CD3DX12_RESOURCE_BARRIER statisticsResourceBarrier =
			CD3DX12_RESOURCE_BARRIER::Transition(m_StatisticsBuffer.Get(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_UNORDERED_ACCESS);
		computeCommandList->ResourceBarrier(1, &statisticsResourceBarrier);
	}
	//update statistics
	computeCommandList->SetPipelineState(m_ParticleSystem->GetStatsPSO());
	computeCommandList->Dispatch(GetThreadGroups(m_ParticleBufferSize), 1, 1);
	//*/
	//copy statistics to readback buffer
	{
		CD3DX12_RESOURCE_BARRIER statisticsResourceBarrier =
			CD3DX12_RESOURCE_BARRIER::Transition(m_StatisticsBuffer.Get(), D3D12_RESOURCE_STATE_UNORDERED_ACCESS, D3D12_RESOURCE_STATE_COPY_SOURCE);
		computeCommandList->ResourceBarrier(1, &statisticsResourceBarrier);
	}
	computeCommandList->CopyResource(m_StatisticsBufferReadback.Get(), m_StatisticsBuffer.Get());
	{
		CD3DX12_RESOURCE_BARRIER statisticsResourceBarrier =
			CD3DX12_RESOURCE_BARRIER::Transition(m_StatisticsBuffer.Get(), D3D12_RESOURCE_STATE_COPY_SOURCE, D3D12_RESOURCE_STATE_UNORDERED_ACCESS);
		computeCommandList->ResourceBarrier(1, &statisticsResourceBarrier);
	}



}

void Renderer::ParticleRenderComponent::ReadParticleStatistics() {
	CD3DX12_RANGE readRange(0, sizeof(ParticleStatistics)); //read all, set explicitly to avoid warnings
	DX::ThrowIfFailed(m_StatisticsBufferReadback->Map(0, &readRange, reinterpret_cast<void**>(&m_mappedStatisticsBuffer)));
	m_StatisticsBufferData = *reinterpret_cast<ParticleStatistics*>(m_mappedStatisticsBuffer);
	CD3DX12_RANGE writeRange(0, 0); //nothing written
	m_StatisticsBufferReadback->Unmap(0, &writeRange);
	//m_master->logToConsole(m_StatisticsBufferData.active);
}
