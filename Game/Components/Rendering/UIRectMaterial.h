#pragma once
#ifndef UI_RECT_MAT
#define UI_RECT_MAT

#include "pch.h"
#include "Material.h"

namespace Renderer {
	class UIRectMaterial : public Material {
	public:
		UIRectMaterial(LPCWSTR vertexShader, LPCWSTR pixelShader);
		~UIRectMaterial() {}
	protected:
		friend class RenderEngine;
		void CreateRootSignature(const std::shared_ptr<DX::DeviceResources>& deviceResources);
		D3D12_INPUT_LAYOUT_DESC GetInputLayout();
	private:
	};
}

#endif