#pragma once
#ifndef MATERIAL
#define MATERIAL

#include "pch.h"
#include "..\Content\ShaderStructures.h"
#include "..\Common\DeviceResources.h"
#include "..\Common\DirectXHelper.h"

namespace Renderer {
	enum BlendState {
		opague,
		transparent, //normal alpha blending
		alphaAdditive, //color buffer += color*color.a
		writeNormalDepth,
		discardNormalWriteDepth,
		discardNormalDepth
	};

	const static D3D12_RENDER_TARGET_BLEND_DESC BlendStates[] = {
		{	FALSE, FALSE,
			D3D12_BLEND_SRC_ALPHA, D3D12_BLEND_INV_SRC_ALPHA, D3D12_BLEND_OP_ADD,
			D3D12_BLEND_ONE, D3D12_BLEND_ZERO, D3D12_BLEND_OP_ADD,
			D3D12_LOGIC_OP_NOOP,
			D3D12_COLOR_WRITE_ENABLE_ALL
		},
		{	TRUE, FALSE,
			D3D12_BLEND_SRC_ALPHA, D3D12_BLEND_INV_SRC_ALPHA, D3D12_BLEND_OP_ADD,
			D3D12_BLEND_ONE, D3D12_BLEND_ZERO, D3D12_BLEND_OP_ADD,
			D3D12_LOGIC_OP_NOOP,
			D3D12_COLOR_WRITE_ENABLE_ALL
		},
		{	TRUE, FALSE,
			D3D12_BLEND_SRC_ALPHA, D3D12_BLEND_ONE, D3D12_BLEND_OP_ADD,
			D3D12_BLEND_ONE, D3D12_BLEND_ZERO, D3D12_BLEND_OP_ADD,
			D3D12_LOGIC_OP_NOOP,
			D3D12_COLOR_WRITE_ENABLE_ALL
		},
		{	FALSE ,FALSE,
			D3D12_BLEND_ONE, D3D12_BLEND_ZERO, D3D12_BLEND_OP_ADD,
			D3D12_BLEND_ONE, D3D12_BLEND_ZERO, D3D12_BLEND_OP_ADD,
			D3D12_LOGIC_OP_NOOP,
			D3D12_COLOR_WRITE_ENABLE_ALL
		},
		{	TRUE ,FALSE,
			D3D12_BLEND_ZERO, D3D12_BLEND_ONE, D3D12_BLEND_OP_ADD,
			D3D12_BLEND_ONE, D3D12_BLEND_ONE, D3D12_BLEND_OP_MIN,
			D3D12_LOGIC_OP_NOOP,
			D3D12_COLOR_WRITE_ENABLE_ALL
		},
		{	TRUE ,FALSE,
			D3D12_BLEND_ZERO, D3D12_BLEND_ONE, D3D12_BLEND_OP_ADD,
			D3D12_BLEND_ZERO, D3D12_BLEND_ONE, D3D12_BLEND_OP_ADD,
			D3D12_LOGIC_OP_NOOP,
			D3D12_COLOR_WRITE_ENABLE_ALL
		},
	};

	class Material {
	public:
		bool IsInitialized() const{ return m_IsInitialized; }
		ID3D12RootSignature * GetRootSignature() const { return m_RootSignature.Get(); }
		ID3D12PipelineState * GetPSO() const { return m_PipelineState.Get(); }
		bool CastsShadows() const { return m_CastsShadows; }
		bool Lit() const { return m_Lit; }
		ID3D12PipelineState * GetPSODepth() const{
			if (m_CastsShadows)
				return m_PipelineStateDepth.Get();
			else
				return nullptr;
		}
	protected:
		friend class RenderEngine;
		virtual void CreateRootSignature(const std::shared_ptr<DX::DeviceResources>& deviceResources) = 0;

		virtual D3D12_INPUT_LAYOUT_DESC GetInputLayout() = 0;
		//PipelineStateConfiguration		m_PipelineState;
		LPCWSTR m_VertexShader;
		LPCWSTR m_GeometryShader;
		LPCWSTR m_PixelShader;
		D3D12_FILL_MODE m_FillMode;
		D3D12_CULL_MODE m_CullMode;
		BlendState m_BlendState;
		BlendState m_BlendStateNormalDepth;
		BOOL m_DepthTest;
		BOOL m_DepthWrite;
		D3D12_PRIMITIVE_TOPOLOGY_TYPE m_Topology;
		bool m_IsInitialized = false;
		Microsoft::WRL::ComPtr<ID3D12PipelineState>			m_PipelineState; //pipeline config
		Microsoft::WRL::ComPtr<ID3D12RootSignature>			m_RootSignature;
		bool m_CastsShadows = false;
		bool m_Lit = false;
		Microsoft::WRL::ComPtr<ID3D12PipelineState>			m_PipelineStateDepth; //uses normal root sig

		
	private:
	};
}

#endif
