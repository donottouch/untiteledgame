#pragma once
#include "pch.h"
#include "Mesh.h"
#include "Common/DirectXHelper.h"


using namespace Renderer;

Mesh::Mesh()
{
	//TODO
}

Mesh::~Mesh()
{
}

/*
loads a obj file https://en.wikipedia.org/wiki/Wavefront_.obj_file,  but presumes triangular faces!
generrates a cube if eg file not found
*/
void Renderer::Mesh::LoadFromFile(const char* path, bool flip)
{
	std::string line;
	std::ifstream objFile(path, std::ifstream::in); //Execution dir is btw \untiteledgame\x64\Debug\Game\AppX

	std::vector<VertexData>* vertices = new std::vector<VertexData>();
	std::vector<unsigned short>* triangles = new std::vector<unsigned short>();
	std::set<std::pair<unsigned short, unsigned short>> lines = std::set<std::pair<unsigned short, unsigned short>>();


	if (objFile.is_open())
	{
		while (getline(objFile, line)) //split for lines
		{
			std::vector<std::string> tokens;
			 
			std::string token;
			std::istringstream tokenStream(line);

			std::string type;
			std::getline(tokenStream, type, ' '); //get type of line



			if (type == "v") //vertex, possibly contains color
			{
				VertexData v = VertexData();

				//position
				std::getline(tokenStream, token, ' '); //look for tokens
				v.pos.x = std::stof(token);
				std::getline(tokenStream, token, ' ');
				v.pos.y = std::stof(token);
				std::getline(tokenStream, token, ' ');
				v.pos.z = std::stof(token);

				//optional: colors
				if (std::getline(tokenStream, token, ' '))
				{
					v.color.x = std::stof(token);
					std::getline(tokenStream, token, ' ');
					v.color.y = std::stof(token);
					std::getline(tokenStream, token, ' ');
					v.color.z = std::stof(token);
				}
				else {
					v.color = XMFLOAT3(0.9687f, 0.09375f, 0.5781f); //DUBUG color
				}
					
				vertices->emplace_back(v);
			}
			else if (type == "f") //face
			{
				//parse only first 3 elements, assuming we have an triangularr shape. easy extendable at this point replace for with stream
				//index can be negative to mark counting from back of CURRENTLY declarated vertices, map back with (size+index)%size
				//index is 1 based, recalc with index-1
				
				for (size_t i = 0; i < 3; i++)
				{

					std::getline(tokenStream, token, ' ');
					size_t pos = 0;
					triangles->emplace_back(static_cast<unsigned short>((vertices->size() + std::stoi(token, &pos) -1) % vertices->size()));

					//TODO: make data usable
					if (pos + 1 <= token.length()) //input of form v1/...
					{
						token = token.substr(pos + 1); //erase v1/
						try //check wether v1/vt1.. or v1//..
						{
							unsigned short vt1 = std::stoi(token, &pos);
						}
						catch (const std::invalid_argument&) {
							pos = 0;
						}

						if (pos + 1 <= token.length()) //input of form v1/vt1/vn1 or v1//vn1
						{
							token = token.substr(pos + 1);
							unsigned short vn1 = std::stoi(token, &pos);
						}

					}
				}

				if (flip)
				{
					std::swap(triangles->at(triangles->size()-1), triangles->at(triangles->size()- 3));
				}

				//add lines of triangle to list (thats stupid btw) 
				
				/*
				const int m = triangles->size();
				if (triangles->at(m - 1) < triangles->at(m - 2))
				{
					std::pair<unsigned short, unsigned short> nPair = std::make_pair(triangles->at(m - 1), triangles->at(m - 2));
					lines.emplace(nPair);
				}
				else {
					std::pair<unsigned short, unsigned short> nPair = std::make_pair(triangles->at(m - 2), triangles->at(m - 1));
					lines.emplace(nPair);
				}
				if (triangles->at(m - 1) < triangles->at(m - 3))
				{
					std::pair<unsigned short, unsigned short> nPair = std::make_pair(triangles->at(m - 1), triangles->at(m - 3));
					lines.emplace(nPair);
				}
				else {
					std::pair<unsigned short, unsigned short> nPair = std::make_pair(triangles->at(m - 3), triangles->at(m - 1));
					lines.emplace(nPair);
				}
				if (triangles->at(m - 3) < triangles->at(m - 2))
				{
					std::pair<unsigned short, unsigned short> nPair = std::make_pair(triangles->at(m - 3), triangles->at(m - 2));
					lines.emplace(nPair);
				}
				else {
					std::pair<unsigned short, unsigned short> nPair = std::make_pair(triangles->at(m - 2), triangles->at(m - 3));
					lines.emplace(nPair);
				}
				*/
			}
			else if (type == "vt") //vertex texture
			{
				//TODO make usable, like type == "v" nut without color case
			}
			else if (type == "vn") // vertex normal
			{
				//TODO make usable, like type == "v" nut without color case
			}
			else if (type == "vp") //vertex parameter
			{
				//TODO make usable, like type == "v" nut without color case
			}
			else if (type == "l") //line
			{
				//TODO make usable, used for splines - maybe not neccessary
			}
			else {
				//TODO: eg material
			}
			
		}

		objFile.close();

		//set data
		m_vertexDataSize = static_cast<UINT>(vertices->size() *2* sizeof(XMFLOAT3));
		m_vertexData = static_cast<VertexData*>(malloc(m_vertexDataSize));
		memcpy(m_vertexData, vertices->data(), m_vertexDataSize);//m_vertexData = vertices->data();

		m_triangleListSize = static_cast<UINT>(triangles->size() * sizeof(uint16_t));
		m_triangleIndices = static_cast<unsigned short*>(malloc(m_triangleListSize));
		memcpy(m_triangleIndices, triangles->data(), m_triangleListSize);//m_triangleIndices = triangles->data();
		
		//currently not used as we never render lines, save the memory
		//m_lineListSize = static_cast<UINT>(lines.size() * 2 * sizeof(uint16_t));
		//m_lineIndices = new unsigned short[lines.size() * 2];

		
		int c = 0;
		for each (std::pair<unsigned short, unsigned short> var in lines)
		{
			m_lineIndices[c++] = var.first;
			m_lineIndices[c++] = var.second;
		}
		
		delete(vertices);
		delete(triangles);
	}
	else {
		//TODO: say something... file not opened
		m_vertexData = CubeVertexData();
		m_vertexDataSize = CubeVertexDataSize();
		m_triangleIndices = CubeTriangleList();
		m_triangleListSize = CubeTriangleListSize();
		m_lineIndices = CubeLineList();
		m_lineListSize = CubeTriangleListSize();
	}
}

void Mesh::LoadToGPU(const std::shared_ptr<DX::DeviceResources>& deviceResources, Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& cL, bool loadTriangles = true)
{	
	if (m_isLoaded) return;
	auto device = deviceResources->GetD3DDevice();

	
	//auto cl = deviceResources->
	// Vertexpufferressource im Standardheap der GPU erstellen und Indexdaten mit dem Uploadheap dort hinein kopieren.
	// Die Uploadressource darf erst freigegeben werden, wenn sie von der GPU nicht mehr verwendet wird.
	Microsoft::WRL::ComPtr<ID3D12Resource> vertexBufferUpload;

	CD3DX12_HEAP_PROPERTIES defaultHeapProperties(D3D12_HEAP_TYPE_DEFAULT);
	CD3DX12_RESOURCE_DESC vertexBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(m_vertexDataSize);
	DX::ThrowIfFailed(device->CreateCommittedResource(
		&defaultHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&vertexBufferDesc,
		D3D12_RESOURCE_STATE_COPY_DEST,
		nullptr,
		IID_PPV_ARGS(&m_vertexBuffer)));

	CD3DX12_HEAP_PROPERTIES uploadHeapProperties(D3D12_HEAP_TYPE_UPLOAD);
	DX::ThrowIfFailed(device->CreateCommittedResource(
		&uploadHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&vertexBufferDesc,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&vertexBufferUpload)));

	//NAME_D3D12_OBJECT(m_vertexBuffer);

	// Vertexpuffer in die GPU hochladen.
	{
		D3D12_SUBRESOURCE_DATA vertexData = {};
		vertexData.pData = reinterpret_cast<BYTE*>(m_vertexData);
		vertexData.RowPitch = m_vertexDataSize;
		vertexData.SlicePitch = vertexData.RowPitch;

		UpdateSubresources(cL.Get(), m_vertexBuffer.Get(), vertexBufferUpload.Get(), 0, 0, 1, &vertexData);

		CD3DX12_RESOURCE_BARRIER vertexBufferResourceBarrier =
			CD3DX12_RESOURCE_BARRIER::Transition(m_vertexBuffer.Get(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER);
		cL->ResourceBarrier(1, &vertexBufferResourceBarrier);
	}

	// Mesh-Indizes laden. Jede Indexdreiergruppe stellt ein Dreieck zum Rendern auf dem Bildschirm dar.


	m_indexBufferSize = loadTriangles ? m_triangleListSize : m_lineListSize;//sizeof(renderComponent->GetTriangleList());
	m_indexCount = loadTriangles ? 36 : 24; //TODO

	// Indexpufferressource im Standardheap der GPU erstellen und Indexdaten mit dem Uploadheap dort hinein kopieren.
	// Die Uploadressource darf erst freigegeben werden, wenn sie von der GPU nicht mehr verwendet wird.
	Microsoft::WRL::ComPtr<ID3D12Resource> indexBufferUpload;

	CD3DX12_RESOURCE_DESC indexBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(m_indexBufferSize);
	DX::ThrowIfFailed(device->CreateCommittedResource(
		&defaultHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&indexBufferDesc,
		D3D12_RESOURCE_STATE_COPY_DEST,
		nullptr,
		IID_PPV_ARGS(&m_indexBuffer)));

	DX::ThrowIfFailed(device->CreateCommittedResource(
		&uploadHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&indexBufferDesc,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&indexBufferUpload)));

	//NAME_D3D12_OBJECT(m_indexBuffer);

	// Indexpuffer in die GPU hochladen.
	{
		D3D12_SUBRESOURCE_DATA indexData = {};
		indexData.pData = reinterpret_cast<BYTE*>(loadTriangles? m_triangleIndices : m_lineIndices);
		indexData.RowPitch = m_indexBufferSize;
		indexData.SlicePitch = indexData.RowPitch;

		UpdateSubresources(cL.Get(), m_indexBuffer.Get(), indexBufferUpload.Get(), 0, 0, 1, &indexData);

		CD3DX12_RESOURCE_BARRIER indexBufferResourceBarrier =
			CD3DX12_RESOURCE_BARRIER::Transition(m_indexBuffer.Get(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_INDEX_BUFFER);
		cL->ResourceBarrier(1, &indexBufferResourceBarrier);
	}

	

	// Befehlsliste schlie�en und ausf�hren, um mit dem Kopieren des Vertex-/Indexpuffers in den Standardheap der GPU zu beginnen.
	DX::ThrowIfFailed(cL->Close());
	ID3D12CommandList* ppCommandLists[] = { cL.Get() };
	deviceResources->GetCommandQueue()->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);

	// Vertex-/Indexpufferansichten erstellen.
	m_vertexBufferView.BufferLocation = m_vertexBuffer->GetGPUVirtualAddress();
	m_vertexBufferView.StrideInBytes = sizeof(VertexData);
	m_vertexBufferView.SizeInBytes = m_vertexDataSize;

	m_indexBufferView.BufferLocation = m_indexBuffer->GetGPUVirtualAddress();
	m_indexBufferView.SizeInBytes = m_indexBufferSize;
	m_indexBufferView.Format = DXGI_FORMAT_R16_UINT;

	// Auf das Beenden der Ausf�hrung der Befehlsliste warten. Die Vertex-/Indexpuffer m�ssen in die GPU hochgeladen werden, bevor die Uploadressourcen den Bereich �berschreiten.
	deviceResources->WaitForGpu();
	m_isLoaded = true;
}

void Mesh::Draw(Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& commandList, UINT instanceCount)
{
	if (!m_isLoaded) return;
	commandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST); //TODO
	commandList->IASetVertexBuffers(0, 1, &m_vertexBufferView);
	commandList->IASetIndexBuffer(&m_indexBufferView);
	commandList->DrawIndexedInstanced(m_indexCount, instanceCount, 0, 0, 0);
}
//https://github.com/jpvanoosten/LearningDirectX12/blob/master/DX12Lib/inc/Mesh.h
Mesh * Renderer::Mesh::GetSphereMesh(size_t tessellation)
{
	std::vector<VertexData> vertices;
	std::vector<uint16_t> indices;

	if (tessellation < 3)
		throw std::out_of_range("tessellation parameter out of range");

	float radius = 1.0f; //0.5f
	size_t verticalSegments = tessellation;
	size_t horizontalSegments = tessellation * 2;

	// Create rings of vertices at progressively higher latitudes.
	for (size_t i = 0; i <= verticalSegments; i++)
	{
		float v = 1 - (float)i / verticalSegments;

		float latitude = (i * XM_PI / verticalSegments) - XM_PIDIV2;
		float dy, dxz;

		XMScalarSinCos(&dy, &dxz, latitude);

		// Create a single ring of vertices at this latitude.
		for (size_t j = 0; j <= horizontalSegments; j++)
		{
			float u = (float)j / horizontalSegments;

			float longitude = j * XM_2PI / horizontalSegments;
			float dx, dz;

			XMScalarSinCos(&dx, &dz, longitude);

			dx *= dxz;
			dz *= dxz;

			XMVECTOR normal = XMVectorSet(dx, dy, dz, 0);
			XMVECTOR textureCoordinate = XMVectorSet(u, v, 0, 0);
			XMFLOAT3 pos, normalF;
			XMStoreFloat3(&normalF, normal);
			XMStoreFloat3(&pos, normal * radius );
			VertexData vert = { pos, normalF };
			vertices.push_back(vert);
		}
	}

	// Fill the index buffer with triangles joining each pair of latitude rings.
	size_t stride = horizontalSegments + 1;

	for (size_t i = 0; i < verticalSegments; i++)
	{
		for (size_t j = 0; j <= horizontalSegments; j++)
		{
			size_t nextI = i + 1;
			size_t nextJ = (j + 1) % stride;

			indices.push_back(static_cast<uint16_t>(i * stride + j));
			indices.push_back(static_cast<uint16_t>(nextI * stride + j));
			indices.push_back(static_cast<uint16_t>(i * stride + nextJ));

			indices.push_back(static_cast<uint16_t>(i * stride + nextJ));
			indices.push_back(static_cast<uint16_t>(nextI * stride + j));
			indices.push_back(static_cast<uint16_t>(nextI * stride + nextJ));
		}
	}

	Mesh* mesh = new Mesh;
	mesh->m_vertexDataSize = static_cast<UINT>(vertices.size() * 2 * sizeof(XMFLOAT3));
	mesh->m_vertexData = static_cast<VertexData*>(malloc(mesh->m_vertexDataSize)); //vertices.data();
	memcpy(mesh->m_vertexData, vertices.data(), mesh->m_vertexDataSize);
	mesh->m_triangleListSize = static_cast<UINT>(indices.size() * sizeof(uint16_t));
	mesh->m_triangleIndices = static_cast<unsigned short*>(malloc(mesh->m_triangleListSize));//indices.data();
	memcpy(mesh->m_triangleIndices, indices.data(), mesh->m_triangleListSize);
	//mesh->m_lineIndices = CubeLineList();
	//mesh->m_lineListSize = CubeTriangleListSize();
	return mesh;
}
