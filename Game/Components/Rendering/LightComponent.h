#pragma once
#ifndef Light_COMPONENT
#define Light_COMPONENT
#include "pch.h"
#include "Component.h"

//class GameObject;

namespace Renderer {
	class LightComponent : public Component {
	public:
		LightComponent();
		~LightComponent();
		//Events
		void OnUpdate(float dT) {}
		void OnStart() {}
		void OnDestroy() {}
		void OnCollision(GameObject::CollisionObject collision) {}

		void SetActive(const bool active) { m_active = active; }
		void SetColor(const DirectX::XMFLOAT3 color) { m_color = color; }

		DirectX::XMFLOAT4 GetColor() const { return DirectX::XMFLOAT4(m_color.x, m_color.y, m_color.z, 1); }
		DirectX::XMFLOAT4 GetPosition() const;

	private:
		bool m_active;
		DirectX::XMFLOAT3 m_color;
	};
}

#endif 