#pragma once
#ifndef ORBIT_MATERIAL
#define ORBIT_MATERIAL

#include "pch.h"
#include "Material.h"

namespace Renderer {
	class OrbitMaterial : public Material {
	public:
		OrbitMaterial(LPCWSTR vertexShader, LPCWSTR pixelShader);
	protected:
		friend class RenderEngine;
		void CreateRootSignature(const std::shared_ptr<DX::DeviceResources>& deviceResources);
		D3D12_INPUT_LAYOUT_DESC GetInputLayout();
	};
}

#endif