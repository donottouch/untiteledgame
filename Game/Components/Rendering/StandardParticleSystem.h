#pragma once

#ifndef STANDARD_PARTICLE_SYSTEM
#define STANDARD_PARTICLE_SYSTEM

#include "pch.h"
#include "ParticleSystem.h"

namespace Renderer {

	class StandardParticleSystem : public ParticleSystem {
	public:
		StandardParticleSystem(std::wstring spawnShader, std::wstring updateShader, std::wstring statsShader);
		~StandardParticleSystem() {}
	protected:
		friend class RenderEngine;
		void Create(const std::shared_ptr<DX::DeviceResources>& deviceResources);
		void CreateRootSignature(const std::shared_ptr<DX::DeviceResources>& deviceResources);
		void CreateComputePSO(const LPCWSTR shader, Microsoft::WRL::ComPtr<ID3D12PipelineState>& PSO, const std::shared_ptr<DX::DeviceResources>& deviceResources);

		std::wstring m_SpawnShader;
		std::wstring m_UpdateShader;
		std::wstring m_StatsShader;
	};
}


#endif