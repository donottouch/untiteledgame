#include "pch.h"
#include "OrbitRenderComponent.h"
#include "../OrbitComponent.h"
#include "GameObject.h"
#include "Components/Transform.h"

#include "GameMain.h"
#include "PhysicEngine.h"


void Renderer::OrbitRenderComponent::CreateResources(const std::shared_ptr<DX::DeviceResources>& deviceResources) {}

void Renderer::OrbitRenderComponent::UpdateConstantBuffer(Renderer::ModelViewProjectionConstantBuffer * viewProjection, const std::shared_ptr<DX::DeviceResources>& deviceResources) {
	if (m_LoadingComplete) {
		UINT currentFrame = deviceResources->GetCurrentFrameIndex();
		XMMATRIX model = XMLoadFloat4x4(&m_master->getTransform()->getLocalToWorldMatrix());
		XMStoreFloat4x4(&m_constantBufferData.model, XMMatrixTranspose(model));

		//m_constantBufferData.model = m_master->getTransform()->getLocalToWorldMatrix();// XMFLOAT4X4(model_test);
		//copy view and projection
		m_constantBufferData.view = viewProjection->view;
		m_constantBufferData.projection = viewProjection->projection;

		// Die Konstantenpufferressource aktualisieren.
		UINT8* destination = m_mappedConstantBuffer + (currentFrame * Renderer::c_alignedConstantBufferSize);
		memcpy(destination, &m_constantBufferData, sizeof(m_constantBufferData));

		float scale = (float)m_OrbitC->scalingFactor;
		//update orbit elements
		m_Orbit.e = (float)m_OrbitC->e;
		m_Orbit.a = (float)m_OrbitC->a * scale;
		m_Orbit.b = (float)m_OrbitC->b * scale;
		m_Orbit.w = (float)m_OrbitC->w;
		m_Orbit.v = (float)m_OrbitC->v;
		m_Orbit.ra = (float) m_OrbitC->ra * scale;
		m_Orbit.rp = (float) m_OrbitC->rp * scale;

		std::unique_ptr<OrbitComponent> orbitAdvance = std::make_unique<OrbitComponent>(*m_OrbitC);
		m_orbitManager->computeOrbitRadiusTrueAnomaly(m_OffsetTimestep, orbitAdvance.get());
		m_OrbitOffsets.v.x = orbitAdvance->v;
		m_orbitManager->computeOrbitRadiusTrueAnomaly(m_OffsetTimestep, orbitAdvance.get());
		m_OrbitOffsets.v.y = orbitAdvance->v;
		m_orbitManager->computeOrbitRadiusTrueAnomaly(m_OffsetTimestep, orbitAdvance.get());
		m_OrbitOffsets.v.z = orbitAdvance->v;
		m_OrbitOffsets.v.w = 0;

	}
}

void Renderer::OrbitRenderComponent::Draw(const std::shared_ptr<DX::DeviceResources>& deviceResources, Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& commandList) {

	if (!m_Visible || !m_LoadingComplete || m_Material == nullptr || m_Mesh == nullptr || !m_Material->IsInitialized() || !m_Mesh->IsLoaded()) return;

	commandList->SetGraphicsRootSignature(m_Material->GetRootSignature());
	commandList->SetPipelineState(m_Material->GetPSO());

	ID3D12DescriptorHeap* ppHeaps[] = { m_cbDescriptorHeap.Get() }; //get from all components?
	commandList->SetDescriptorHeaps(_countof(ppHeaps), ppHeaps);

	// Bindet den Konstantenpuffer des aktuellen Frames an die Pipeline.
	CD3DX12_GPU_DESCRIPTOR_HANDLE gpuHandle(m_cbDescriptorHeap->GetGPUDescriptorHandleForHeapStart(), deviceResources->GetCurrentFrameIndex(), m_cbDescriptorHeapSize);
	commandList->SetGraphicsRootDescriptorTable(0, gpuHandle);
	commandList->SetGraphicsRoot32BitConstants(1, NUM_32BIT_CONSTANTS(OrbitalElements), &m_Orbit, 0);
	commandList->SetGraphicsRoot32BitConstants(2, NUM_32BIT_CONSTANTS(LineStyle), &m_LineStyle, 0);
	commandList->SetGraphicsRoot32BitConstants(3, NUM_32BIT_CONSTANTS(OrbitalOffsets), &m_OrbitOffsets, 0);

	m_Mesh->Draw(commandList);
}
