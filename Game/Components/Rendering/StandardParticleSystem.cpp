#include "pch.h"
#include "StandardParticleSystem.h"

Renderer::StandardParticleSystem::StandardParticleSystem(std::wstring spawnShader, std::wstring updateShader, std::wstring statsShader) {
	m_SpawnShader = spawnShader;
	m_UpdateShader = updateShader;
	m_StatsShader = statsShader;
}

void Renderer::StandardParticleSystem::Create(const std::shared_ptr<DX::DeviceResources>& deviceResources) {
	CreateRootSignature(deviceResources);
	CreateComputePSO(m_UpdateShader.c_str(), m_ComputePSOUpdate, deviceResources);
	CreateComputePSO(m_SpawnShader.c_str(), m_ComputePSOSpawn, deviceResources);
	CreateComputePSO(m_StatsShader.c_str(), m_ComputePSOStats, deviceResources);
	m_IsInitialized = true;
}

void Renderer::StandardParticleSystem::CreateRootSignature(const std::shared_ptr<DX::DeviceResources>& deviceResources) {

	auto d3dDevice = deviceResources->GetD3DDevice();

	CD3DX12_DESCRIPTOR_RANGE1 range[2];
	range[0].Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 0);
	range[1].Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 12); //util, time
	CD3DX12_ROOT_PARAMETER1 parameter[7];
	int p = -1;
	parameter[++p].InitAsDescriptorTable(1, range);
	parameter[++p].InitAsConstantBufferView(1);
	parameter[++p].InitAsConstantBufferView(2);
	parameter[++p].InitAsDescriptorTable(1, range + 1);
	parameter[++p].InitAsUnorderedAccessView(0);
	parameter[++p].InitAsUnorderedAccessView(1);
	parameter[++p].InitAsUnorderedAccessView(2);


	D3D12_ROOT_SIGNATURE_FLAGS rootSignatureFlags =
		D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_PIXEL_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_VERTEX_SHADER_ROOT_ACCESS;

	CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC descRootSignature;
	descRootSignature.Init_1_1(p + 1, parameter, 0, nullptr, rootSignatureFlags);

	Microsoft::WRL::ComPtr<ID3DBlob> pSignature;
	Microsoft::WRL::ComPtr<ID3DBlob> pError;
	DX::ThrowIfFailed(D3D12SerializeVersionedRootSignature(&descRootSignature, pSignature.GetAddressOf(), pError.GetAddressOf()));//(&descRootSignature, D3D_ROOT_SIGNATURE_VERSION_1, pSignature.GetAddressOf(), pError.GetAddressOf()));
	DX::ThrowIfFailed(d3dDevice->CreateRootSignature(0, pSignature->GetBufferPointer(), pSignature->GetBufferSize(), IID_PPV_ARGS(&m_ComputeRootSignature)));//mat.m_RootSignature)));}

}

void Renderer::StandardParticleSystem::CreateComputePSO(const LPCWSTR shader, Microsoft::WRL::ComPtr<ID3D12PipelineState>& PSO, const std::shared_ptr<DX::DeviceResources>& deviceResources) {
	auto d3dDevice = deviceResources->GetD3DDevice();

	//load shaders
	Microsoft::WRL::ComPtr<ID3D10Blob> cs;
	DX::ThrowIfFailed(D3DReadFileToBlob(shader, &cs)); //m_TonemapPS

	D3D12_COMPUTE_PIPELINE_STATE_DESC pipelineStateDesc;
	ZeroMemory(&pipelineStateDesc, sizeof(pipelineStateDesc));
	pipelineStateDesc.pRootSignature = m_ComputeRootSignature.Get();
	pipelineStateDesc.CS = CD3DX12_SHADER_BYTECODE(cs.Get());

	DX::ThrowIfFailed(d3dDevice->CreateComputePipelineState(&pipelineStateDesc, IID_PPV_ARGS(&PSO)));
}
