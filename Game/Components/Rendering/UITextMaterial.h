#pragma once
#ifndef UI_TEXT_MAT
#define UI_TEXT_MAT

#include "pch.h"
#include "Material.h"

namespace Renderer {
	class UITextMaterial : public Material {
	public:
		UITextMaterial(LPCWSTR vertexShader, LPCWSTR pixelShader, D3D12_FILL_MODE fillMode, D3D12_CULL_MODE cullMode);
		~UITextMaterial() {}
	protected:
		friend class RenderEngine;
		void CreateRootSignature(const std::shared_ptr<DX::DeviceResources>& deviceResources);
		D3D12_INPUT_LAYOUT_DESC GetInputLayout();
	private:
	};
}

#endif