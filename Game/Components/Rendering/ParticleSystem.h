#pragma once

#ifndef PARTICLE_SYSTEM
#define PARTICLE_SYSTEM

#include "pch.h"
#include "..\Common\DirectXHelper.h"
#include "..\Common\DeviceResources.h"
#include <d3dcompiler.h>

namespace Renderer {

	class ParticleSystem {
	public:
		virtual ~ParticleSystem(){}

		ID3D12RootSignature* GetRootSignature() const { return m_ComputeRootSignature.Get(); }
		ID3D12PipelineState* GetUpdatePSO() const { return m_ComputePSOUpdate.Get(); }
		ID3D12PipelineState* GetSpawnPSO() const { return m_ComputePSOSpawn.Get(); }
		ID3D12PipelineState* GetStatsPSO() const { return m_ComputePSOStats.Get(); }
	protected:
		friend class RenderEngine;
		virtual void Create(const std::shared_ptr<DX::DeviceResources>& deviceResources) = 0;

		virtual void CreateRootSignature(const std::shared_ptr<DX::DeviceResources>& deviceResources) = 0;
		virtual void CreateComputePSO(const LPCWSTR shader, Microsoft::WRL::ComPtr<ID3D12PipelineState>& PSO, const std::shared_ptr<DX::DeviceResources>& deviceResources) = 0;

		Microsoft::WRL::ComPtr<ID3D12PipelineState>			m_ComputePSOUpdate;
		Microsoft::WRL::ComPtr<ID3D12PipelineState>			m_ComputePSOSpawn;
		Microsoft::WRL::ComPtr<ID3D12PipelineState>			m_ComputePSOStats;
		Microsoft::WRL::ComPtr<ID3D12RootSignature>			m_ComputeRootSignature;

		bool m_IsInitialized = false;

	};
}

#endif