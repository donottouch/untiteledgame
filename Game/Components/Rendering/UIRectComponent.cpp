#include "pch.h"
#include "UIRectComponent.h"

Renderer::UIRectComponent::UIRectComponent() {
	SetAnchor(XMFLOAT2(0, 0));
	SetOffset(XMINT2(0, 0));
	SetSize(XMINT2(40, 20));
	SetLineWidth(1);
	SetColor(XMFLOAT4(1, 0, 1, 1));
}

void Renderer::UIRectComponent::CreateResources(const std::shared_ptr<DX::DeviceResources>& deviceResources) {}

void Renderer::UIRectComponent::UpdateConstantBuffer(Renderer::ModelViewProjectionConstantBuffer * viewProjection, const std::shared_ptr<DX::DeviceResources>& deviceResources) {
	
}

void Renderer::UIRectComponent::SetAnchor(XMFLOAT2 anchorUV) {
	m_VSParams.anchorUV = anchorUV;
	m_PSParams.anchorUV = anchorUV;
}

void Renderer::UIRectComponent::SetOffset(XMINT2 offsetPix) {
	m_VSParams.offsetPix = offsetPix;
	m_PSParams.offsetPix = offsetPix;
}

void Renderer::UIRectComponent::SetSize(XMINT2 sizePix) {
	m_VSParams.sizePix = sizePix;
	m_PSParams.sizePix = sizePix;
}

void Renderer::UIRectComponent::SetLineWidth(UINT lineWidthPix) {
	m_VSParams.border = lineWidthPix*0.5f;
	m_PSParams.lineWidth = static_cast<float>(lineWidthPix);
}

void Renderer::UIRectComponent::SetColor(XMFLOAT4 color) {
	m_PSParams.color = color;
}

void Renderer::UIRectComponent::Initialize(std::shared_ptr<Material> mat, const std::shared_ptr<DX::DeviceResources>& deviceResources) {
	m_Material = mat;
	m_LoadingComplete = true;
}

void Renderer::UIRectComponent::Draw(const std::shared_ptr<DX::DeviceResources>& deviceResources, Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& commandList) {

	if (m_Material == nullptr || !m_Material->IsInitialized() ) return;

	commandList->SetGraphicsRootSignature(m_Material->GetRootSignature());
	commandList->SetPipelineState(m_Material->GetPSO());

	//ID3D12DescriptorHeap* ppHeaps[] = { }; //, m_Font->m_Texture->GetSRVDescriptorHeap() , deviceResources->GetSamplerDescriptorHeap()
	//commandList->SetDescriptorHeaps(_countof(ppHeaps), ppHeaps);

	// Bindet den Konstantenpuffer des aktuellen Frames an die Pipeline.
	commandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
	commandList->SetGraphicsRoot32BitConstants(0, NUM_32BIT_CONSTANTS(RectVertexParams), &m_VSParams, 0);
	commandList->SetGraphicsRoot32BitConstants(1, NUM_32BIT_CONSTANTS(RectPixelParams), &m_PSParams, 0);
	m_RTMetrics.rtmetrics = deviceResources->GetRTMetrics();
	commandList->SetGraphicsRoot32BitConstants(2, NUM_32BIT_CONSTANTS(SMAARTMetrics), &m_RTMetrics, 0);

	commandList->DrawInstanced(4, 1, 0, 0);
}
