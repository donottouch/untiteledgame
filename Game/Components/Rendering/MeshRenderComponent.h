#pragma once

#ifndef MESH_RENDER_COMPONENT
#define MESH_RENDER_COMPONENT

#include "pch.h"
#include "RenderComponent.h"

namespace Renderer {
	class MeshRenderComponent : public RenderComponent {
	public:
		MeshRenderComponent(Mesh& mesh, Material& material) {
			m_Mesh = &mesh;
			m_Material = &material;
			m_ShaderParams.colorDiffuse = XMFLOAT4(1, 0, 1, 1);
			m_Visible = true;
		}
		MeshRenderComponent(Mesh& mesh, Material& material, MeshParams params) {
			m_Mesh = &mesh;
			m_Material = &material;
			m_ShaderParams = params;
			m_Visible = true;
		}
		~MeshRenderComponent() {}
		void OnUpdate(float dT);
		void OnStart();
		void OnDestroy();
		void OnCollision(GameObject::CollisionObject collision);

		void SetStyle(MeshParams style) { m_ShaderParams = style; }
		void CreateResources(const std::shared_ptr<DX::DeviceResources>& deviceResources);
		void UpdateConstantBuffer(Renderer::ModelViewProjectionConstantBuffer* viewProjection, const std::shared_ptr<DX::DeviceResources>& deviceResources);
		void Draw(const std::shared_ptr<DX::DeviceResources>& deviceResources, Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& commandList);
		void DrawDepth(const std::shared_ptr<DX::DeviceResources>& deviceResources, Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& commandList);
	private:
		MeshParams m_ShaderParams;
	};
}

#endif