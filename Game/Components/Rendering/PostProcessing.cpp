#pragma once
#include "pch.h"
#include "PostProcessing.h"

Renderer::PostProcessing::PostProcessing(LPCWSTR vertexShader, LPCWSTR pixelShader) : 
	m_VertexShader(vertexShader),
	m_PixelShader(pixelShader)
{
}

Renderer::PostProcessing::~PostProcessing()
{
}
