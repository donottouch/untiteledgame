#pragma once
#include "pch.h"
#include "StandardMaterial.h"

Renderer::StandardMaterial::StandardMaterial(LPCWSTR vertexShader, LPCWSTR geometryShader, LPCWSTR pixelShader, D3D12_FILL_MODE fillMode, D3D12_CULL_MODE cullMode, bool castsShadows, bool lit) {
	m_VertexShader = vertexShader;
	m_GeometryShader = geometryShader;
	m_PixelShader = pixelShader;
	m_FillMode = fillMode;
	m_CullMode = cullMode;
	m_Topology = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	m_BlendState = opague;
	m_BlendStateNormalDepth = writeNormalDepth;
	m_DepthTest = TRUE;
	m_DepthWrite = TRUE;
	m_CastsShadows = castsShadows;
	m_Lit = lit;
}Renderer::StandardMaterial::StandardMaterial(LPCWSTR vertexShader, LPCWSTR pixelShader, D3D12_FILL_MODE fillMode, D3D12_CULL_MODE cullMode, bool castsShadows, bool lit) {
	StandardMaterial(vertexShader, nullptr, pixelShader, fillMode, cullMode, castsShadows, lit);
}

Renderer::StandardMaterial::~StandardMaterial() {}

void Renderer::StandardMaterial::CreateRootSignature(const std::shared_ptr<DX::DeviceResources>& deviceResources) {

	auto d3dDevice = deviceResources->GetD3DDevice();

	CD3DX12_DESCRIPTOR_RANGE1 range[5];
	CD3DX12_ROOT_PARAMETER1 parameter[6];

	range[0].Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 0); //object data
	range[1].Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 10); //global light
	range[2].Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 11); //cubemap
	range[3].Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 12); //util, time
	range[4].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 1); //cube array
	parameter[0].InitAsDescriptorTable(1, range, D3D12_SHADER_VISIBILITY_ALL);
	parameter[1].InitAsConstants(NUM_32BIT_CONSTANTS(MeshParams), 1, 0, D3D12_SHADER_VISIBILITY_PIXEL);
	parameter[2].InitAsDescriptorTable(1, range+1, D3D12_SHADER_VISIBILITY_PIXEL);
	parameter[3].InitAsDescriptorTable(1, range+2, D3D12_SHADER_VISIBILITY_ALL);
	parameter[4].InitAsDescriptorTable(1, range+3, D3D12_SHADER_VISIBILITY_ALL);
	parameter[5].InitAsDescriptorTable(1, range+4, D3D12_SHADER_VISIBILITY_PIXEL);


	D3D12_ROOT_SIGNATURE_FLAGS rootSignatureFlags =
		D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
		//D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS;
	//D3D12_ROOT_SIGNATURE_FLAG_DENY_VERTEX_SHADER_ROOT_ACCESS;

	CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC descRootSignature;
	descRootSignature.Init_1_1(6, parameter, 0, nullptr, rootSignatureFlags);

	Microsoft::WRL::ComPtr<ID3DBlob> pSignature;
	Microsoft::WRL::ComPtr<ID3DBlob> pError;
	DX::ThrowIfFailed(D3D12SerializeVersionedRootSignature(&descRootSignature, pSignature.GetAddressOf(), pError.GetAddressOf()));//(&descRootSignature, D3D_ROOT_SIGNATURE_VERSION_1, pSignature.GetAddressOf(), pError.GetAddressOf()));
	DX::ThrowIfFailed(d3dDevice->CreateRootSignature(0, pSignature->GetBufferPointer(), pSignature->GetBufferSize(), IID_PPV_ARGS(&m_RootSignature)));//mat.m_RootSignature)));
}


D3D12_INPUT_LAYOUT_DESC Renderer::StandardMaterial::GetInputLayout() {
	return { Renderer::VertexDataLayout, _countof(Renderer::VertexDataLayout) };
}
