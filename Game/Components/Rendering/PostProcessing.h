#pragma once
#ifndef POSTPROCESSING
#define POSTPROCESSING
#include "pch.h"
#include "Material.h"

namespace Renderer {

	class PostProcessing {
	public:
		PostProcessing(LPCWSTR vertexShader, LPCWSTR pixelShader);
		~PostProcessing();
		ID3D12RootSignature* GetRootSignature() const { return m_RootSignature.Get(); }
		ID3D12PipelineState* GetPSO() const { return m_PipelineState.Get(); }
		bool IsInitialized() const { return m_IsInitialized; }
	private:
		friend class RenderEngine;
		LPCWSTR m_VertexShader;
		LPCWSTR m_PixelShader;
		Microsoft::WRL::ComPtr<ID3D12RootSignature>					m_RootSignature;
		Microsoft::WRL::ComPtr<ID3D12PipelineState>					m_PipelineState;
		bool m_IsInitialized;

	};
}

#endif