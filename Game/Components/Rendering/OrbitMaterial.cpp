#pragma once
#include "pch.h"
#include "OrbitMaterial.h"
#include "Content/ShaderStructures.h"

Renderer::OrbitMaterial::OrbitMaterial(LPCWSTR vertexShader, LPCWSTR pixelShader) {
	m_VertexShader = vertexShader;
	m_GeometryShader = nullptr;
	m_PixelShader = pixelShader;
	m_FillMode = D3D12_FILL_MODE_SOLID;
	m_CullMode = D3D12_CULL_MODE_NONE;
	m_Topology = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	m_BlendState = transparent;
	m_BlendStateNormalDepth = discardNormalDepth;
	m_DepthTest = TRUE;
	m_DepthWrite = FALSE;
}

void Renderer::OrbitMaterial::CreateRootSignature(const std::shared_ptr<DX::DeviceResources>& deviceResources) {

	auto d3dDevice = deviceResources->GetD3DDevice();

	CD3DX12_DESCRIPTOR_RANGE1 range[1];
	CD3DX12_ROOT_PARAMETER1 parameter[4];

	range[0].Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 0);
	parameter[0].InitAsDescriptorTable(1, range, D3D12_SHADER_VISIBILITY_VERTEX);
	parameter[1].InitAsConstants(NUM_32BIT_CONSTANTS(OrbitalElements), 0, 1, D3D12_SHADER_VISIBILITY_PIXEL);
	parameter[2].InitAsConstants(NUM_32BIT_CONSTANTS(LineStyle), 1, 1, D3D12_SHADER_VISIBILITY_PIXEL);
	parameter[3].InitAsConstants(NUM_32BIT_CONSTANTS(OrbitalOffsets), 2, 1, D3D12_SHADER_VISIBILITY_PIXEL);


	D3D12_ROOT_SIGNATURE_FLAGS rootSignatureFlags =
		D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS;
	//D3D12_ROOT_SIGNATURE_FLAG_DENY_VERTEX_SHADER_ROOT_ACCESS;

	CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC descRootSignature;
	descRootSignature.Init_1_1(4, parameter, 0, nullptr, rootSignatureFlags);

	Microsoft::WRL::ComPtr<ID3DBlob> pSignature;
	Microsoft::WRL::ComPtr<ID3DBlob> pError;
	DX::ThrowIfFailed(D3D12SerializeVersionedRootSignature(&descRootSignature, pSignature.GetAddressOf(), pError.GetAddressOf()));//(&descRootSignature, D3D_ROOT_SIGNATURE_VERSION_1, pSignature.GetAddressOf(), pError.GetAddressOf()));
	DX::ThrowIfFailed(d3dDevice->CreateRootSignature(0, pSignature->GetBufferPointer(), pSignature->GetBufferSize(), IID_PPV_ARGS(&m_RootSignature)));//mat.m_RootSignature)));
}
D3D12_INPUT_LAYOUT_DESC Renderer::OrbitMaterial::GetInputLayout() {
	return { Renderer::VertexDataLayout, _countof(Renderer::VertexDataLayout) };
}
