#pragma once
#ifndef ORBIT_RENDER_COMPONENT
#define ORBIT_RENDER_COMPONENT

#include "pch.h"
#include "MeshRenderComponent.h"
#include "../OrbitComponent.h"
#include "Components/Behaviours/OrbitManager.h"

namespace Renderer {
	class OrbitRenderComponent : public RenderComponent {
	public:
		OrbitRenderComponent(OrbitComponent* orbitC, OrbitManager* _orbitManager, Mesh& mesh, Material& material, LineStyle lineStyle) {
			m_Mesh = &mesh;
			m_Material = &material;
			m_LineStyle = lineStyle;
			m_OrbitC = orbitC;
			m_Visible = true;
			m_OffsetTimestep = 5.0f;
			m_orbitManager = _orbitManager;
		}
		void OnUpdate(float dT) {}
		void OnStart() {}
		void OnDestroy() {}
		void OnCollision(GameObject::CollisionObject collision) {}
		void CreateResources(const std::shared_ptr<DX::DeviceResources>& deviceResources);
		void UpdateConstantBuffer(Renderer::ModelViewProjectionConstantBuffer* viewProjection, const std::shared_ptr<DX::DeviceResources>& deviceResources);
		void Draw(const std::shared_ptr<DX::DeviceResources>& deviceResources, Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& commandList);
		void DrawDepth(const std::shared_ptr<DX::DeviceResources>& deviceResources, Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& commandList) {}

		void setStyle(LineStyle newStyle) { m_LineStyle = newStyle; }
	private:
		OrbitalElements m_Orbit;
		LineStyle m_LineStyle;

		float m_OffsetTimestep;
		OrbitalOffsets m_OrbitOffsets;
		//TODO: get via parent
		OrbitComponent* m_OrbitC;
		OrbitManager* m_orbitManager;
	};
}

#endif