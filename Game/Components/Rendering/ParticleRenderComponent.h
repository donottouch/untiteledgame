#pragma once
#ifndef PARTICLE_RENDER_COMPONENT
#define PARTICLE_RENDER_COMPONENT

#include "pch.h"
#include "RenderComponent.h"
#include "ParticleSystem.h"
#include "../OrbitComponent.h"

namespace Renderer {
	class ParticleRenderComponent : public RenderComponent {
	public:
		ParticleRenderComponent(Material& material, std::shared_ptr<ParticleSystem>& particleSystem, ParticleSystemParameters& params);
		~ParticleRenderComponent() {}

		//Component events
		void OnUpdate(float dT);
		void OnStart() {}
		void OnDestroy() {}
		void OnCollision(GameObject::CollisionObject collision) {}
		//render component events
		void CreateResources(const std::shared_ptr<DX::DeviceResources>& deviceResources);
		void UpdateConstantBuffer(Renderer::ModelViewProjectionConstantBuffer* viewProjection, const std::shared_ptr<DX::DeviceResources>& deviceResources);
		void Draw(const std::shared_ptr<DX::DeviceResources>& deviceResources, Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& commandList);
		void DrawDepth(const std::shared_ptr<DX::DeviceResources>& deviceResources, Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& commandList) {}
		//Material events
		//void CreateRootSignature(const std::shared_ptr<DX::DeviceResources>& deviceResources) {}
		//virtual D3D12_INPUT_LAYOUT_DESC GetInputLayout() { return { 0,0 }; }

		void UpdateParticleConstantBuffer();
		void SpawnParticles(const uint32_t numParticles);
		void SetSpawnRate(const float spawnRate) { m_SpawnRate = spawnRate; }
		void UpdateParticles(const std::shared_ptr<DX::DeviceResources>& deviceResources, Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& computeCommandList);

		void Active(const bool active) { m_Active = active; };

		ParticleSystem* GetParticleSystem() const { return m_ParticleSystem.get(); }
	protected:
		friend class RenderEngine;
		void ReadParticleStatistics();

	private:

		inline uint32_t GetThreadGroups(const uint32_t threads) const { return static_cast<uint32_t>(ceil(threads / static_cast<float>(m_WarpSize))); }

		std::shared_ptr<ParticleSystem>		   m_ParticleSystem;

		Microsoft::WRL::ComPtr<ID3D12Resource> m_ParticleDataBuffer;

		Microsoft::WRL::ComPtr<ID3D12Resource> m_ParticleStatusBuffer;


		ParticleStatistics					   m_StatisticsBufferUploadData;
		Microsoft::WRL::ComPtr<ID3D12Resource> m_StatisticsBufferUpload;
		UINT8*								   m_mappedStatisticsBufferUpload;

		Microsoft::WRL::ComPtr<ID3D12Resource> m_StatisticsBufferReadback;
		ParticleStatistics					   m_StatisticsBufferData;
		UINT8*								   m_mappedStatisticsBuffer;

		Microsoft::WRL::ComPtr<ID3D12Resource> m_StatisticsBuffer;


		UINT m_WarpSize;
		uint32_t m_ParticleBufferSize;
		float m_SpawnRate;
		float m_PendingSpawns;

		ParticleUpdateParameters			   m_UpdateParametersBufferData;
		Microsoft::WRL::ComPtr<ID3D12Resource> m_UpdateParametersBuffer;
		UINT8*								   m_mappedUpdateParametersBuffer;

		ParticleSystemParameters			   m_PSParametersBufferData;
		Microsoft::WRL::ComPtr<ID3D12Resource> m_PSParametersBuffer;
		UINT8*								   m_mappedPSParametersBuffer;
		bool								   m_PSParametersBufferDirty;

		bool m_Active;


	};
}

#endif