#pragma once
#include "pch.h"
#include "MeshRenderComponent.h"
#include "GameObject.h"
#include "Components/Transform.h"

void Renderer::MeshRenderComponent::OnUpdate(float dT) {}

void Renderer::MeshRenderComponent::OnStart() {}

void Renderer::MeshRenderComponent::OnDestroy() {}

void Renderer::MeshRenderComponent::OnCollision(GameObject::CollisionObject collision) {}

void Renderer::MeshRenderComponent::CreateResources(const std::shared_ptr<DX::DeviceResources>& deviceResources) {}

void Renderer::MeshRenderComponent::UpdateConstantBuffer(Renderer::ModelViewProjectionConstantBuffer * viewProjection, const std::shared_ptr<DX::DeviceResources>& deviceResources) {
	if (m_LoadingComplete) {
		//get all global
		m_constantBufferData = *viewProjection;

		//get object specific
		XMMATRIX t_model = XMLoadFloat4x4(&m_master->getTransform()->getLocalToWorldMatrix());
		XMMATRIX model = XMMatrixTranspose(t_model);
		XMStoreFloat4x4(&m_constantBufferData.model, model);
		XMStoreFloat4x4(&m_constantBufferData.inv_model, XMMatrixInverse(nullptr, model));
		XMStoreFloat4x4(&m_constantBufferData.it_model, XMMatrixInverse(nullptr, t_model));

		// Die Konstantenpufferressource aktualisieren.
		UINT currentFrame = deviceResources->GetCurrentFrameIndex();
		UINT8* destination = m_mappedConstantBuffer + (currentFrame * Renderer::c_alignedConstantBufferSize);
		memcpy(destination, &m_constantBufferData, sizeof(m_constantBufferData));
	}
}
void Renderer::MeshRenderComponent::Draw(const std::shared_ptr<DX::DeviceResources>& deviceResources, Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& commandList) {

	if (!m_Visible || !m_LoadingComplete || m_Material == nullptr || m_Mesh == nullptr || !m_Material->IsInitialized() || !m_Mesh->IsLoaded()) return;

	commandList->SetGraphicsRootSignature(m_Material->GetRootSignature());
	commandList->SetPipelineState(m_Material->GetPSO());

	ID3D12DescriptorHeap* ppHeaps[] = { m_cbDescriptorHeap.Get() };
	commandList->SetDescriptorHeaps(_countof(ppHeaps), ppHeaps);

	// Bindet den Konstantenpuffer des aktuellen Frames an die Pipeline.
	CD3DX12_GPU_DESCRIPTOR_HANDLE gpuHandle(m_cbDescriptorHeap->GetGPUDescriptorHandleForHeapStart(), deviceResources->GetCurrentFrameIndex(), m_cbDescriptorHeapSize);
	commandList->SetGraphicsRootDescriptorTable(0, gpuHandle);
	commandList->SetGraphicsRoot32BitConstants(1, NUM_32BIT_CONSTANTS(MeshParams), &m_ShaderParams, 0);
	// global buffers
	CD3DX12_GPU_DESCRIPTOR_HANDLE gpuGlobalLightHandle(m_cbDescriptorHeap->GetGPUDescriptorHandleForHeapStart(), DX::c_frameCount, m_cbDescriptorHeapSize);
	commandList->SetGraphicsRootDescriptorTable(2, gpuGlobalLightHandle);
	CD3DX12_GPU_DESCRIPTOR_HANDLE gpuCubeHandle(m_cbDescriptorHeap->GetGPUDescriptorHandleForHeapStart(), DX::c_frameCount + 1, m_cbDescriptorHeapSize);
	commandList->SetGraphicsRootDescriptorTable(3, gpuCubeHandle);
	CD3DX12_GPU_DESCRIPTOR_HANDLE gpuCubemapHandle(m_cbDescriptorHeap->GetGPUDescriptorHandleForHeapStart(), DX::c_frameCount + 2, m_cbDescriptorHeapSize);
	commandList->SetGraphicsRootDescriptorTable(5, gpuCubemapHandle);

	m_Mesh->Draw(commandList);
}
void Renderer::MeshRenderComponent::DrawDepth(const std::shared_ptr<DX::DeviceResources>& deviceResources, Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& commandList) {

	if (!m_Visible || !m_LoadingComplete || m_Material == nullptr || m_Mesh == nullptr || !m_Material->CastsShadows() ||!m_Material->IsInitialized() || !m_Mesh->IsLoaded()) return;

	commandList->SetGraphicsRootSignature(m_Material->GetRootSignature());
	commandList->SetPipelineState(m_Material->GetPSODepth());

	ID3D12DescriptorHeap* ppHeaps[] = { m_cbDescriptorHeap.Get() };
	commandList->SetDescriptorHeaps(_countof(ppHeaps), ppHeaps);

	// Bindet den Konstantenpuffer des aktuellen Frames an die Pipeline.
	CD3DX12_GPU_DESCRIPTOR_HANDLE gpuHandle(m_cbDescriptorHeap->GetGPUDescriptorHandleForHeapStart(), deviceResources->GetCurrentFrameIndex(), m_cbDescriptorHeapSize);
	commandList->SetGraphicsRootDescriptorTable(0, gpuHandle);

	// global buffers
	CD3DX12_GPU_DESCRIPTOR_HANDLE gpuGlobalLightHandle(m_cbDescriptorHeap->GetGPUDescriptorHandleForHeapStart(), DX::c_frameCount, m_cbDescriptorHeapSize);
	commandList->SetGraphicsRootDescriptorTable(2, gpuGlobalLightHandle);
	CD3DX12_GPU_DESCRIPTOR_HANDLE gpuCubeHandle(m_cbDescriptorHeap->GetGPUDescriptorHandleForHeapStart(), DX::c_frameCount + 1, m_cbDescriptorHeapSize);
	commandList->SetGraphicsRootDescriptorTable(3, gpuCubeHandle);

	m_Mesh->Draw(commandList);
	
}
