#pragma once
#ifndef STANDARD_MATERIAL
#define STANDARD_MATERIAL

#include "pch.h"
#include "Material.h"

namespace Renderer {
	class StandardMaterial : public Material {
	public:
		StandardMaterial(LPCWSTR vertexShader, LPCWSTR geometryShader, LPCWSTR pixelShader, D3D12_FILL_MODE fillMode = D3D12_FILL_MODE_SOLID, D3D12_CULL_MODE cullMode = D3D12_CULL_MODE_BACK, bool castsShadows = true, bool lit = true);
		StandardMaterial(LPCWSTR vertexShader, LPCWSTR pixelShader, D3D12_FILL_MODE fillMode = D3D12_FILL_MODE_SOLID, D3D12_CULL_MODE cullMode = D3D12_CULL_MODE_BACK, bool castsShadows = true, bool lit = true);
		~StandardMaterial();
		void SetBlendState(const BlendState state) { m_BlendState = state; }
		void SetBlendStateNormalDepth(const BlendState state) { m_BlendStateNormalDepth = state; }
	protected:
		friend class RenderEngine;
		void CreateRootSignature(const std::shared_ptr<DX::DeviceResources>& deviceResources);
		D3D12_INPUT_LAYOUT_DESC GetInputLayout();
	private:
	};
}

#endif