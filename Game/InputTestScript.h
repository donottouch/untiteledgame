#pragma once
#include "../Components/Behaviour.h"
#include "GameMain.h"
class InputTestScript :
	public Behaviour
{
public:
	InputTestScript(Game::GameMain *gameMain);
	~InputTestScript();

	void unS();


	virtual void OnUpdate(float dT);
	virtual void OnStart();
	virtual void OnDestroy();
	virtual void OnCollision(GameObject::CollisionObject collision);

private:
	Game::GameMain* m_gMain;
};

