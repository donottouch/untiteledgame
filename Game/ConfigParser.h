#pragma once

#ifndef CONFIG_PARSER
#define CONFIG_PARSER

#include"pch.h"

namespace ConfigParser{
	
	class Config{
	public:
		Config() {}
		~Config() {}
		
		//return the value if the key is present, otherwise the default value
		bool GetBool(const std::string key, const bool default=false) const;
		int GetInt(const std::string key, const int default=0) const;
		float GetFloat(const std::string key, const float default=0.0f) const;
		DirectX::XMFLOAT4 GetColor(const std::string key, const DirectX::XMFLOAT4 default = DirectX::XMFLOAT4(0,0,0,0)) const;
		std::string GetString(const std::string key) const;

		//put the value into the value pointer if the key is present and return success
		int GetBool(const std::string key, bool* value) const;
		int GetInt(const std::string key, int* value) const;
		int GetFloat(const std::string key, float* value) const;
		int GetColor(const std::string key, DirectX::XMFLOAT4* value) const;
		
		void AddBoolProperty(const std::string key, const bool value);
		void AddIntProperty(const std::string key, const int value);
		void AddFloatProperty(const std::string key, const float value);
		void AddColorProperty(const std::string key, const DirectX::XMFLOAT4 value);
		void AddStringProperty(const std::string key, const std::string value);
		
	private:
		
		std::map<std::string, bool> m_PropertiesBool;
		std::map<std::string, int> m_PropertiesInt;
		std::map<std::string, float> m_PropertiesFloat;
		std::map<std::string, DirectX::XMFLOAT4> m_PropertiesColor;
		std::map<std::string, std::string> m_PropertiesString;
	
	};
	int Parse(const std::string fileName, Config* config);
}

#endif