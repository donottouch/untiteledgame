#pragma once
#ifndef GAMEOBJECT
#define GAMEOBJECT

#include "pch.h"
#include <list>
#include <string>

#include <iostream>
#include <fstream>


using namespace std;
using namespace DirectX;

class Component;
class Transform;

namespace Game {
	class GameMain;
}

class GameObject
{
public:
	// ja der struct sollte hier eigentlich nicht sein. Aber abgrund der dummen Eigenheiten von c++ kann ich ihn nicht in Component klatschen.
struct CollisionObject {
	CollisionObject(XMFLOAT3 _collisionPoint, GameObject* _colliderOne, GameObject* _colliderTwo) :
		collisionPoint(_collisionPoint), colliderOne(_colliderOne), colliderTwo(_colliderTwo) {}
	XMFLOAT3 collisionPoint;
	GameObject* colliderOne;
	GameObject* colliderTwo;
};


	GameObject(Game::GameMain *gameMain);
	GameObject(Game::GameMain *gameMain, XMFLOAT3 globalposition);


	~GameObject();
	// returns true if the operation was succesfull; Calls OnStart() on the component;
	bool addComponent(Component* c);
	// returns true if the operation was succesfull; Calls the deconstructor on the component;
	bool removeComponent(Component* c);
	
	// returns true if the operation was succesfull
	bool addChild(GameObject* go);
	void setPartent(GameObject*go);
	// returns true if the operation was succesfull
	bool removeChild(GameObject* go);


	
	template <class T> list<Component*> getComponentsByType() {
		list<Component*> containedElements;
		std::list<Component*>::iterator it;
		for (it = components.begin(); it != components.end(); ++it) {
			if (dynamic_cast<T*>(*it) != nullptr) {
				containedElements.push_back(*it);
			}
		}
		return containedElements;
	}

	list<Component*> getAllComponents();

	template <class T> T* getComponent() {
		std::list<Component*>::iterator it;
		for (it = components.begin(); it != components.end(); ++it) {
			T* obj = dynamic_cast<T*>(*it);
			if (obj != nullptr) {
				return obj;
			}
		}
		return nullptr;
	}

	void setName(string name);
	
	Transform* getTransform();
	string getName();
	Game::GameMain* getGameMain();

	// returns all direct children
	list<GameObject*> getChildren();
	// returns all children (also childrens children, etc.); does not contain itself
	list<GameObject*> getAllChildren();
	//returns all children recursiv; contains itself
	list<GameObject*> getAllChildrenRecursiv();
	GameObject* getParent();

	// flags this gameObject for deletion
	void flagForDeletion();

	void OnCollision(CollisionObject collision);

	void logToConsole(string input);
	void logToConsole(double input) { logToConsole(std::to_string(input) + "\n"); };

private:
	//Variables
	list<GameObject*> children;
	GameObject* parent;
	Transform* transform;
	Game::GameMain* gameMain;

	list<Component*> components;
	string name;

	// methods
	bool componentsContainComponent(Component* c);
	bool childsContainGameObject(GameObject* go);

	bool addComponentToGameMain(Component* c);
	void removeComponentFromGameMain(Component* c);

};

#endif 