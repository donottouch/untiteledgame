#pragma once
#include "pch.h"
#include <ppltasks.h>
#include <synchapi.h>
#include "RenderEngine.h"
#include "Components/Rendering/UIRenderComponent.h"
#include "Components/Rendering/UIRectComponent.h"
#include "Components/Rendering/OrbitRenderComponent.h"
#include "Components/Rendering/ParticleRenderComponent.h"

#include "Components/Rendering/UITextMaterial.h"
#include "Components/Rendering/UIRectMaterial.h"
#include "Components/Rendering/PostProcessing.h"
#include "Components/Rendering/Mesh.h"
#include "Components/Rendering/LightComponent.h"
#include "Components/CameraComponent.h"
#include "Components/Rendering/Fonts.h"

#include "Shader/AreaTex.h"
#include "Shader/SearchTex.h"


//Vergleiche mit Sample3DSceneRenderer
using namespace Renderer;
using namespace Concurrency;
using namespace DirectX;
RenderEngine::RenderEngine(std::shared_ptr<ConfigParser::Config>& config) {

	m_GlobalLightComponent = std::make_shared<LightComponent>();
	m_GlobalLightComponent->SetColor(XMFLOAT3(1.2f, 0.6f, 3.6f));

	m_RenderOpaque = true;
	m_RenderTransparent = true;
	m_RenderTransparentAdditive = true;

	m_SMAAActive = config->GetBool("bSMAA");// true;

	m_FogActive = config->GetBool("bFog");// true;
	m_FogParameters.height = config->GetFloat("fFogHeight");// 0.4f;
	m_FogParameters.maxDensity = config->GetFloat("fFogMaxDensity");// 4.0f;
	m_FogParameters.maxSteps = config->GetInt("iFogMaxSteps");// 12;
	m_FogBlurDepthBias = 0.2;

	m_ShadowsActive = config->GetBool("bShadows");// true;
	m_BloomActive = config->GetBool("bBloom");// true;
	m_BloomBlurrPasses = config->GetInt("iBloomPasses");// 5;
	m_TonemapParameters.BloomStrength = config->GetFloat("fBloomStrength");
	m_TonemappingActive = config->GetBool("bTonemapping");// true;

	m_RenderUI = config->GetBool("bUI");// true;

	m_Camera = nullptr;
}

void Renderer::RenderEngine::Initialize(const std::shared_ptr<DX::DeviceResources>& deviceResources) {
	m_deviceResources = deviceResources;
	auto d3dDevice = m_deviceResources->GetD3DDevice();

	CreateDefaultPipeline();
	CreateGlobalConstantBuffers(); //(1.2f,0.6f,3.6f) (1.64f, 1.76f, 1.86f)

	SetupAA();
	SetupPostProcessing();

	// Erstellt eine Befehlsliste.
	DX::ThrowIfFailed(d3dDevice->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, m_deviceResources->GetCommandAllocator(), m_DefaultPipelineState.Get(), IID_PPV_ARGS(&m_CommandList)));
	DX::ThrowIfFailed(d3dDevice->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_COMPUTE, m_deviceResources->GetComputeCommandAllocator(), NULL, IID_PPV_ARGS(&m_ComputeCommandList)));
	DX::ThrowIfFailed(m_ComputeCommandList->Close());
	//create default pipeline state
	//ResizeWindow();
	m_ScreenQuad = Mesh::GetQuadMesh();
	LoadMeshToGPU(m_ScreenQuad, true);

	//UI
	m_TextMat = std::make_unique<UITextMaterial>(L"Text_VS.cso", L"Text_PS.cso", D3D12_FILL_MODE_SOLID, D3D12_CULL_MODE_NONE);
	CreatePipelineState(m_TextMat.get(), m_deviceResources->GetBackBufferFormat());
	m_DefaultFont = new Font();
	m_DefaultFont->LoadFromBinary(L"default", g_pconsola24, sizeof(g_pconsola24), m_deviceResources);

	m_UIRectMat = std::make_shared<UIRectMaterial>(L"Rect_VS.cso", L"Rect_PS.cso");
	CreatePipelineState(&(*m_UIRectMat), m_deviceResources->GetBackBufferFormat());

	m_IsInitialized = true;
}

bool Renderer::RenderEngine::IsInitialized() {
	return m_IsInitialized && m_DefaultPipelineState != nullptr;
}

void Renderer::RenderEngine::Update(DX::StepTimer& timer, float timeWarp) {
	m_FrameConstantsBufferData.time.x = timer.GetTotalSeconds();
	m_FrameConstantsBufferData.time.y = timer.GetElapsedSeconds();

	m_FrameConstantsBufferData.time.w = timer.GetElapsedSeconds() * timeWarp; //todo warped
	m_FrameConstantsBufferData.time.z += m_FrameConstantsBufferData.time.w; //todo warped

	m_FrameConstantsBufferData.remainingTime = 900.f - m_FrameConstantsBufferData.time.z;

}
void Renderer::RenderEngine::UpdateParticleSystems(std::unordered_set<ParticleRenderComponent*>& PS) {

	m_deviceResources->WaitForGpu();

	DX::ThrowIfFailed(m_deviceResources->GetComputeCommandAllocator()->Reset());
	DX::ThrowIfFailed(m_ComputeCommandList->Reset(m_deviceResources->GetComputeCommandAllocator(), NULL));

	//record command list
	UpdateGlobalBuffers();
	for (auto itr = PS.begin(); itr != PS.end(); ++itr) {
		ParticleRenderComponent* c = *itr;
		c->UpdateParticles(m_deviceResources, m_ComputeCommandList);
	}

	//execute
	DX::ThrowIfFailed(m_ComputeCommandList->Close());
	ID3D12CommandList* ppCommandLists[] = { m_ComputeCommandList.Get() };
	m_deviceResources->GetComputeCommandQueue()->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);

	m_deviceResources->WaitForGPUCompute();

	for (auto itr = PS.begin(); itr != PS.end(); ++itr) {
		ParticleRenderComponent* c = *itr;
		c->ReadParticleStatistics();
	}
	
}
/*
void Renderer::RenderEngine::UpdateRenderComponents(std::unordered_set<RenderComponent*>* renderComponents, ModelViewProjectionConstantBuffer * VPBuffer) {
	for (auto itr = renderComponents->begin(); itr != renderComponents->end(); ++itr) {
		RenderComponent* renderComponent = *itr;
		//only render loaded objects
		if (!renderComponent->m_LoadingComplete) continue;
		renderComponent->UpdateConstantBuffer(VPBuffer, m_deviceResources);
	}
	
}*/
void Renderer::RenderEngine::UpdateRenderComponents(std::vector<RenderComponent*>* renderComponents, ModelViewProjectionConstantBuffer * VPBuffer, std::list<LightComponent*>& lightComponents) {
	for (auto itr = renderComponents->begin(); itr != renderComponents->end(); ++itr) {
		RenderComponent* renderComponent = *itr;
		//only render loaded objects
		if (!renderComponent->m_LoadingComplete) continue;

		//light sorting
		ModelViewProjectionConstantBuffer thisMVPBuffer = *VPBuffer;
		if (renderComponent->m_Material != nullptr && renderComponent->m_Material->m_Lit) {
			//TODO sort

			//get closest lights, up to MAX_POINT_LIGHTS
			thisMVPBuffer.lightCount = min(MAX_POINT_LIGHTS, static_cast<uint32_t>(lightComponents.size()));
			auto lightItr = lightComponents.begin();
			for (uint32_t i = 0; i < thisMVPBuffer.lightCount; ++i, ++lightItr) {
				thisMVPBuffer.lightPos[i] = (*lightItr)->GetPosition();
				thisMVPBuffer.lightColor[i] = (*lightItr)->GetColor();
			}
		}
		renderComponent->UpdateConstantBuffer(&thisMVPBuffer, m_deviceResources);
	}

}

void Renderer::RenderEngine::Render(std::unordered_set<RenderComponent*>& renderComponents, std::list<LightComponent*>& lightComponents) {
	if (!IsInitialized()) return;
	SortRenderComponents(&renderComponents);
	m_deviceResources->WaitForGpu();
	UpdateGlobalBuffers();
	UpdateRenderComponents(&m_UIRenderComponents, &m_MVPBufferData, lightComponents);
	UpdateRenderComponents(&m_UIOrbitComponents, &m_MVPBufferData, lightComponents);
	UpdateRenderComponents(&m_UIRectComponents, &m_MVPBufferData, lightComponents);

	//m_deviceResources->WaitForGpu();
	auto d3dDevice = m_deviceResources->GetD3DDevice();
	//ThrowIfFailed(d3dDevice->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, m_deviceResources->GetCommandAllocator(), m_DefaultPipelineConfig->pipelineState.Get(), IID_PPV_ARGS(&commandList)));
	DX::ThrowIfFailed(m_deviceResources->GetCommandAllocator()->Reset());
	DX::ThrowIfFailed(m_CommandList->Reset(m_deviceResources->GetCommandAllocator(), m_DefaultPipelineState.Get()));


	if (m_ShadowsActive) {
		RenderDepthCube();
	}

	m_LastRTV = DX::HDR_RTV_MAIN;
	m_MainRTV = DX::HDR_RTV_MAIN;
	PIXBeginEvent(m_CommandList.Get(), 0, L"Clear Render targets");
	{
		D3D12_VIEWPORT viewport = m_deviceResources->GetScreenViewport();
		m_CommandList->RSSetViewports(1, &viewport);
		m_CommandList->RSSetScissorRects(1, &m_scissorRect);

		//Clear render targets
		D3D12_CPU_DESCRIPTOR_HANDLE depthStencilView = m_deviceResources->GetDepthStencilView();
		m_CommandList->ClearDepthStencilView(depthStencilView, D3D12_CLEAR_FLAG_DEPTH, 1.0f, 0, 0, nullptr);
		//set render targets
		D3D12_CPU_DESCRIPTOR_HANDLE HDRRTVmain = m_deviceResources->GetHDRRenderTargetView(m_LastRTV);
		D3D12_CPU_DESCRIPTOR_HANDLE HDRRTVnormalDepth = m_deviceResources->GetHDRRenderTargetView(DX::HDR_RTV_MAIN_NORMAL_DEPTH);
		CD3DX12_RESOURCE_BARRIER renderTargetResourceBarrier[2];
		renderTargetResourceBarrier[0] = CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetHDRRenderTarget(m_LastRTV), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_RENDER_TARGET);
		renderTargetResourceBarrier[1] = CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetHDRRenderTarget(DX::HDR_RTV_MAIN_NORMAL_DEPTH), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_RENDER_TARGET);
		m_CommandList->ResourceBarrier(2, renderTargetResourceBarrier);
		m_CommandList->ClearRenderTargetView(HDRRTVmain, DX::clearColor, 0, nullptr);
		m_CommandList->ClearRenderTargetView(HDRRTVnormalDepth, DX::clearColorNormalDepth, 0, nullptr);
		m_CommandList->OMSetRenderTargets(2, &HDRRTVmain, true, &depthStencilView);

		m_CommandList->SetGraphicsRootSignature(m_DefaultRootSignature.Get());

	}
	PIXEndEvent(m_CommandList.Get());

	//render objects
	if (m_RenderOpaque) {
		PIXBeginEvent(m_CommandList.Get(), 0, L"Render opaque");
		UpdateRenderComponents(&m_OpaqueComponents, &m_MVPBufferData, lightComponents);
		RenderComponents(m_OpaqueComponents);
		PIXEndEvent(m_CommandList.Get());
	}

	if (m_RenderTransparent) {
		PIXBeginEvent(m_CommandList.Get(), 0, L"Render transparent");
		UpdateRenderComponents(&m_TransparentComponents, &m_MVPBufferData, lightComponents);
		RenderComponents(m_TransparentComponents);
		PIXEndEvent(m_CommandList.Get());
	}

	if (m_RenderTransparentAdditive) {
		PIXBeginEvent(m_CommandList.Get(), 0, L"Render additive transparent");
		UpdateRenderComponents(&m_TransparentAdditiveComponents, &m_MVPBufferData, lightComponents);
		RenderComponents(m_TransparentAdditiveComponents);
		PIXEndEvent(m_CommandList.Get());
	}

	{
		CD3DX12_RESOURCE_BARRIER renderTargetResourceBarrier[2];
		renderTargetResourceBarrier[0] = CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetHDRRenderTarget(m_LastRTV), D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
		renderTargetResourceBarrier[1] = CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetHDRRenderTarget(DX::HDR_RTV_MAIN_NORMAL_DEPTH), D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
		m_CommandList->ResourceBarrier(2, renderTargetResourceBarrier);
		ID3D12DescriptorHeap* ppHeaps[] = { m_deviceResources->GetSRVDescriptorHeap(), m_deviceResources->GetSamplerDescriptorHeap()};
		m_CommandList->SetDescriptorHeaps(_countof(ppHeaps), ppHeaps);
	}
	/*{ //Debug
		//m_LastRTV = DX::HDR_RTV_MAIN_NORMAL_DEPTH;
		//m_MainRTV = DX::HDR_RTV_MAIN_NORMAL_DEPTH;
		m_FogActive = false;
		//m_BloomActive = false;
		//m_BloomBlurrPasses = 0;
		//m_TonemappingActive = false;
		//m_SMAAActive = false;
	} //*/

#ifdef AA_METHOD_SSAA
	//SSAA
	{
		D3D12_CPU_DESCRIPTOR_HANDLE HDRRTVfilter = m_deviceResources->GetHDRRenderTargetView(DX::HDR_RTV_SSAA);
		D3D12_CPU_DESCRIPTOR_HANDLE depthStencilView = m_deviceResources->GetDepthStencilView();
		CD3DX12_RESOURCE_BARRIER renderTargetResourceBarrier = CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetHDRRenderTarget(DX::HDR_RTV_SSAA), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_RENDER_TARGET);
		m_CommandList->ResourceBarrier(1, &renderTargetResourceBarrier);
		m_CommandList->RSSetScissorRects(1, &m_scissorRect);
		m_CommandList->OMSetRenderTargets(1, &HDRRTVfilter, false, &depthStencilView);

		m_CommandList->SetGraphicsRootSignature(m_SSAA->GetRootSignature());
		m_CommandList->SetPipelineState(m_SSAA->GetPSO());

		//m_CommandList->SetGraphicsRoot32BitConstants(0, NUM_32BIT_CONSTANTS(Renderer::BloomFilterParameters), &m_BloomFilterParameters, 0);
		m_CommandList->SetGraphicsRootDescriptorTable(0, m_deviceResources->GetHDRShaderResourceViewGPU(lastRTV));
		m_ScreenQuad->Draw(m_CommandList);

		CD3DX12_RESOURCE_BARRIER srvResourceBarrier =
			CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetHDRRenderTarget(DX::HDR_RTV_SSAA), D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
		m_CommandList->ResourceBarrier(1, &srvResourceBarrier);
		lastRTV = DX::HDR_RTV_SSAA;
		mainRTV = DX::HDR_RTV_SSAA;

	}
#endif

	if (m_FogActive) {
		PIXBeginEvent(m_CommandList.Get(), 0, L"Fog");
		RenderFog();
		PIXEndEvent(m_CommandList.Get());
	}

	/*{ //Debug
		m_LastRTV = DX::HDR_RTV_BLOOM2;//DX::HDR_RTV_FOG;
		m_MainRTV = DX::HDR_RTV_FOG;
		m_BloomActive = false;
		m_TonemappingActive = false;
		m_SMAAActive = false;
	} //*/

	// Bloom
	if (m_BloomActive) {
		PIXBeginEvent(m_CommandList.Get(), 0, L"Bloom");
		RenderBloom();
		PIXEndEvent(m_CommandList.Get());
	}

	if (m_TonemappingActive) {
		PIXBeginEvent(m_CommandList.Get(), 0, L"Tonemapping");
		RenderTonemapping();
		PIXEndEvent(m_CommandList.Get());
	}

#ifdef AA_METHOD_SMAA
	if (m_SMAAActive) {
		PIXBeginEvent(m_CommandList.Get(), 0, L"SMAA");
		RenderSMAA();
		PIXEndEvent(m_CommandList.Get());
	}
#endif
	/*{ //Debug
		m_LastRTV = DX::HDR_RTV_SMAA_WEIGHT;
		//m_MainRTV = DX::HDR_RTV_MAIN_NORMAL_DEPTH;
		//m_FogActive = false;
		//m_BloomActive = false;
		//m_TonemappingActive = false;
		//m_SMAAActive = false;
	} //*/
	{
		D3D12_CPU_DESCRIPTOR_HANDLE renderTargetView = m_deviceResources->GetRenderTargetView();
		D3D12_CPU_DESCRIPTOR_HANDLE depthStencilView = m_deviceResources->GetDepthStencilView();
		CD3DX12_RESOURCE_BARRIER renderTargetResourceBarrier = CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetRenderTarget(), D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET);
		m_CommandList->ResourceBarrier(1, &renderTargetResourceBarrier);
		m_CommandList->OMSetRenderTargets(1, &renderTargetView, false, &depthStencilView);
	}

	//Blit previous to final target
	{
		m_CommandList->SetGraphicsRootSignature(m_Blit->GetRootSignature());
		m_CommandList->SetPipelineState(m_Blit->GetPSO());
		m_CommandList->SetGraphicsRootDescriptorTable(0, m_deviceResources->GetHDRShaderResourceViewGPU(m_LastRTV)); //
		//m_CommandList->SetGraphicsRootDescriptorTable(0, m_deviceResources->GetHDRShaderResourceViewGPU(DX::HDR_RTV_SMAA_EDGE)); //
		//m_CommandList->SetGraphicsRootDescriptorTable(0, m_deviceResources->GetShadowShaderResourceViewGPU(DX::SHADOW_RTV_CUBE_1)); // 
		DrawScreenQuad();
	}

	if (m_RenderUI) {
		PIXBeginEvent(m_CommandList.Get(), 0, L"Render UI");
		RenderComponents(m_UIOrbitComponents);
		RenderComponents(m_UIRectComponents);
		RenderComponents(m_UIRenderComponents);
		PIXEndEvent(m_CommandList.Get());
	}

	{
		CD3DX12_RESOURCE_BARRIER presentResourceBarrier =
			CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetRenderTarget(), D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT);
		m_CommandList->ResourceBarrier(1, &presentResourceBarrier);
	}

	DX::ThrowIfFailed(m_CommandList->Close());

	// Befehlsliste ausf�hren.
	ID3D12CommandList* ppCommandLists[] = { m_CommandList.Get() };
	m_deviceResources->GetCommandQueue()->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);

	m_OpaqueComponents.clear();
	m_TransparentComponents.clear();
	m_TransparentAdditiveComponents.clear();
	m_UIOrbitComponents.clear();
	m_UIRenderComponents.clear();
	m_UIRectComponents.clear();

	m_deviceResources->WaitForGpu();
}

void Renderer::RenderEngine::ResizeWindow() {
	SetViewport();
	//CreateAdditionalRenderTargets();
}


void Renderer::RenderEngine::InitializeMaterial(Material * mat) {
	if (!mat->IsInitialized())
		CreatePipelineState(mat);
}

void Renderer::RenderEngine::InitializeRenderComponent(RenderComponent * renderComponent) {
	UIRenderComponent* ui_text;
	if ((ui_text = dynamic_cast<UIRenderComponent*>(renderComponent)) != nullptr) {
		ui_text->Initialize(m_TextMat.get(), m_DefaultFont, m_deviceResources);
		return;
	}
	UIRectComponent* ui_rect;
	if ((ui_rect = dynamic_cast<UIRectComponent*>(renderComponent)) != nullptr) {
		ui_rect->Initialize(m_UIRectMat, m_deviceResources);
		return;
	}
	if (dynamic_cast<OrbitRenderComponent*>(renderComponent) != nullptr) {
		if (!renderComponent->GetMaterial()->IsInitialized())
			CreatePipelineState(renderComponent->GetMaterial(), m_deviceResources->GetBackBufferFormat());
		LoadComponentData(renderComponent);
		return;
	}

	InitializeMaterial(renderComponent->GetMaterial());

	ParticleRenderComponent* pc;
	if ((pc=dynamic_cast<ParticleRenderComponent*>(renderComponent)) != nullptr) {
		if (!pc->GetParticleSystem()->m_IsInitialized)
			pc->GetParticleSystem()->Create(m_deviceResources);
		pc->CreateResources(m_deviceResources);
	}

	LoadComponentData(renderComponent);
}

RenderEngine::~RenderEngine() {}

inline void Renderer::RenderEngine::DrawScreenQuad() {
	//m_ScreenQuad->Draw(m_CommandList);
	m_CommandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
	m_CommandList->DrawInstanced(4, 1, 0, 0);
}

void RenderEngine::EnableDebugLayer() {
#if defined(_DEBUG)
	// Always enable the debug layer before doing anything DX12 related
	// so all possible errors generated while creating DX12 objects
	// are caught by the debug layer.
	ComPtr<ID3D12Debug> debugInterface;
	DX::ThrowIfFailed(D3D12GetDebugInterface(IID_PPV_ARGS(&debugInterface)));
	debugInterface->EnableDebugLayer();
#endif
}


void Renderer::RenderEngine::CreateDefaultPipeline() {
	auto d3dDevice = m_deviceResources->GetD3DDevice();
	CD3DX12_DESCRIPTOR_RANGE range;
	CD3DX12_ROOT_PARAMETER parameter[2];

	range.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 0);
	parameter[0].InitAsDescriptorTable(1, &range, D3D12_SHADER_VISIBILITY_VERTEX);
	parameter[1].InitAsConstants(NUM_32BIT_CONSTANTS(MeshParams), 1, 0, D3D12_SHADER_VISIBILITY_PIXEL);

	D3D12_ROOT_SIGNATURE_FLAGS rootSignatureFlags =
		D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT | // Nur der Eingabeassemblerzustand ben�tigt Zugriff auf den Konstantenpuffer.
		D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS;
		//D3D12_ROOT_SIGNATURE_FLAG_DENY_PIXEL_SHADER_ROOT_ACCESS;

	CD3DX12_ROOT_SIGNATURE_DESC descRootSignature;
	descRootSignature.Init(2, parameter, 0, nullptr, rootSignatureFlags);

	ComPtr<ID3DBlob> pSignature;
	ComPtr<ID3DBlob> pError;
	DX::ThrowIfFailed(D3D12SerializeRootSignature(&descRootSignature, D3D_ROOT_SIGNATURE_VERSION_1, pSignature.GetAddressOf(), pError.GetAddressOf()));
	DX::ThrowIfFailed(d3dDevice->CreateRootSignature(0, pSignature->GetBufferPointer(), pSignature->GetBufferSize(), IID_PPV_ARGS(&m_DefaultRootSignature)));

	ComPtr<ID3D10Blob> vs;
	ComPtr<ID3D10Blob> ps;
	DX::ThrowIfFailed(D3DReadFileToBlob(m_DefaultVertexShader, &vs));
	DX::ThrowIfFailed(D3DReadFileToBlob(m_DefaultPixelShader, &ps));

	D3D12_RASTERIZER_DESC rasterizerDesc;
	ZeroMemory(&rasterizerDesc, sizeof(rasterizerDesc));
	rasterizerDesc.FillMode = D3D12_FILL_MODE_SOLID;
	rasterizerDesc.CullMode = D3D12_CULL_MODE_BACK;
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.DepthBias = D3D12_DEFAULT_DEPTH_BIAS;
	rasterizerDesc.DepthBiasClamp = D3D12_DEFAULT_DEPTH_BIAS_CLAMP;
	rasterizerDesc.SlopeScaledDepthBias = D3D12_DEFAULT_SLOPE_SCALED_DEPTH_BIAS;
	rasterizerDesc.DepthClipEnable = TRUE;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.AntialiasedLineEnable = FALSE;
	rasterizerDesc.ForcedSampleCount = 0;
	rasterizerDesc.ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;

	// 
	D3D12_GRAPHICS_PIPELINE_STATE_DESC pipelineStateDesc;
	ZeroMemory(&pipelineStateDesc, sizeof(pipelineStateDesc));
	pipelineStateDesc.InputLayout = { Renderer::VertexDataLayout, _countof(Renderer::VertexDataLayout) };
	pipelineStateDesc.pRootSignature = m_DefaultRootSignature.Get();
	pipelineStateDesc.VS = CD3DX12_SHADER_BYTECODE(vs.Get());
	pipelineStateDesc.PS = CD3DX12_SHADER_BYTECODE(ps.Get());
	pipelineStateDesc.RasterizerState = rasterizerDesc;
	//pipelineStateDesc.BlendState = blendDesc;
	//pipelineStateDesc.DepthStencilState = depthStencilDesc;
	pipelineStateDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	pipelineStateDesc.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
	pipelineStateDesc.SampleMask = UINT_MAX;
	pipelineStateDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	pipelineStateDesc.NumRenderTargets = 1;
	pipelineStateDesc.RTVFormats[0] = c_HDRRTVFormat;
	pipelineStateDesc.DSVFormat = m_deviceResources->GetDepthBufferFormat();
	pipelineStateDesc.SampleDesc.Count = 1;

	DX::ThrowIfFailed(d3dDevice->CreateGraphicsPipelineState(&pipelineStateDesc, IID_PPV_ARGS(&m_DefaultPipelineState)));
}

void Renderer::RenderEngine::CreatePipelineState(Material* mat, DXGI_FORMAT format) {
	//m_deviceResources->WaitForGpu();
	auto d3dDevice = m_deviceResources->GetD3DDevice();

	//create root signature
	{
		//D3D12_VERSIONED_ROOT_SIGNATURE_DESC rootDesc = mat->GetRootSignatureDesc();

		//ComPtr<ID3DBlob> pSignature;
		//ComPtr<ID3DBlob> pError;
		//DX::ThrowIfFailed(D3D12SerializeVersionedRootSignature(&rootDesc, pSignature.GetAddressOf(), pError.GetAddressOf()));//(&descRootSignature, D3D_ROOT_SIGNATURE_VERSION_1, pSignature.GetAddressOf(), pError.GetAddressOf()));
		//DX::ThrowIfFailed(d3dDevice->CreateRootSignature(0, pSignature->GetBufferPointer(), pSignature->GetBufferSize(), IID_PPV_ARGS(&mat->m_RootSignature)));//mat.m_RootSignature)));
		mat->CreateRootSignature(m_deviceResources);
	}
	{
		ComPtr<ID3D10Blob> vs;
		ComPtr<ID3D10Blob> ps;
		DX::ThrowIfFailed(D3DReadFileToBlob(mat->m_VertexShader, &vs));
		DX::ThrowIfFailed(D3DReadFileToBlob(mat->m_PixelShader, &ps));


		ComPtr<ID3D10Blob> gs;
		if (mat->m_GeometryShader != nullptr) {
			DX::ThrowIfFailed(D3DReadFileToBlob(mat->m_GeometryShader, &gs));
		}

		D3D12_RASTERIZER_DESC rasterizerDesc;
		ZeroMemory(&rasterizerDesc, sizeof(rasterizerDesc));
		rasterizerDesc.FillMode = mat->m_FillMode;
		rasterizerDesc.CullMode = mat->m_CullMode;
		rasterizerDesc.FrontCounterClockwise = FALSE;
		rasterizerDesc.DepthBias = D3D12_DEFAULT_DEPTH_BIAS;
		rasterizerDesc.DepthBiasClamp = D3D12_DEFAULT_DEPTH_BIAS_CLAMP;
		rasterizerDesc.SlopeScaledDepthBias = D3D12_DEFAULT_SLOPE_SCALED_DEPTH_BIAS;
		rasterizerDesc.DepthClipEnable = TRUE;
		rasterizerDesc.MultisampleEnable = FALSE;
		rasterizerDesc.AntialiasedLineEnable = FALSE;
		rasterizerDesc.ForcedSampleCount = 0;
		rasterizerDesc.ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;

		// 
		D3D12_BLEND_DESC blendDesc;
		ZeroMemory(&blendDesc, sizeof(blendDesc));
		blendDesc.AlphaToCoverageEnable = FALSE;
		blendDesc.IndependentBlendEnable = TRUE;
		blendDesc.RenderTarget[0] = BlendStates[mat->m_BlendState];
		blendDesc.RenderTarget[1] = BlendStates[mat->m_BlendStateNormalDepth];

		// 
		CD3DX12_DEPTH_STENCIL_DESC depthStencilDesc(D3D12_DEFAULT);
		//ZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));
		depthStencilDesc.DepthEnable = mat->m_DepthTest;
		depthStencilDesc.DepthWriteMask = mat->m_DepthWrite ? D3D12_DEPTH_WRITE_MASK_ALL : D3D12_DEPTH_WRITE_MASK_ZERO;
		depthStencilDesc.StencilEnable = FALSE;

		// 
		D3D12_GRAPHICS_PIPELINE_STATE_DESC pipelineStateDesc;
		ZeroMemory(&pipelineStateDesc, sizeof(pipelineStateDesc));
		pipelineStateDesc.InputLayout = mat->GetInputLayout();
		pipelineStateDesc.pRootSignature = mat->m_RootSignature.Get();
		pipelineStateDesc.VS = CD3DX12_SHADER_BYTECODE(vs.Get());
		if (mat->m_GeometryShader != nullptr) {
			pipelineStateDesc.GS = CD3DX12_SHADER_BYTECODE(gs.Get());
		}
		pipelineStateDesc.PS = CD3DX12_SHADER_BYTECODE(ps.Get());
		pipelineStateDesc.RasterizerState = rasterizerDesc;
		pipelineStateDesc.BlendState = blendDesc;
		pipelineStateDesc.DepthStencilState = depthStencilDesc;
		//pipelineStateDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
		//pipelineStateDesc.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
		pipelineStateDesc.SampleMask = UINT_MAX;
		pipelineStateDesc.PrimitiveTopologyType = mat->m_Topology;
		pipelineStateDesc.NumRenderTargets = 2;
		pipelineStateDesc.RTVFormats[0] = format;//m_deviceResources->GetBackBufferFormat();
		pipelineStateDesc.RTVFormats[1] = format;
		pipelineStateDesc.DSVFormat = m_deviceResources->GetDepthBufferFormat();
		pipelineStateDesc.SampleDesc.Count = 1;

		DX::ThrowIfFailed(d3dDevice->CreateGraphicsPipelineState(&pipelineStateDesc, IID_PPV_ARGS(&mat->m_PipelineState)));
	}
	//create depth pipeline
	if (mat->CastsShadows()) {

		ComPtr<ID3D10Blob> vs;
		ComPtr<ID3D10Blob> ps;
		DX::ThrowIfFailed(D3DReadFileToBlob(L"Depth_Cube_VS.cso", &vs));
		DX::ThrowIfFailed(D3DReadFileToBlob(L"Depth_Cube_PS.cso", &ps));
		ComPtr<ID3D10Blob> gs;
		//if (mat->m_GeometryShader != nullptr) {
		DX::ThrowIfFailed(D3DReadFileToBlob(L"Depth_Cube_GS.cso", &gs));
		//}

		D3D12_RASTERIZER_DESC rasterizerDesc;
		ZeroMemory(&rasterizerDesc, sizeof(rasterizerDesc));
		rasterizerDesc.FillMode = D3D12_FILL_MODE_SOLID;
		rasterizerDesc.CullMode = D3D12_CULL_MODE_BACK;
		rasterizerDesc.FrontCounterClockwise = FALSE;
		rasterizerDesc.DepthBias = D3D12_DEFAULT_DEPTH_BIAS;
		rasterizerDesc.DepthBiasClamp = D3D12_DEFAULT_DEPTH_BIAS_CLAMP;
		rasterizerDesc.SlopeScaledDepthBias = D3D12_DEFAULT_SLOPE_SCALED_DEPTH_BIAS;
		rasterizerDesc.DepthClipEnable = TRUE;
		rasterizerDesc.MultisampleEnable = FALSE;
		rasterizerDesc.AntialiasedLineEnable = FALSE;
		rasterizerDesc.ForcedSampleCount = 0;
		rasterizerDesc.ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;


		// 
		D3D12_GRAPHICS_PIPELINE_STATE_DESC pipelineStateDesc;
		ZeroMemory(&pipelineStateDesc, sizeof(pipelineStateDesc));
		pipelineStateDesc.InputLayout = mat->GetInputLayout();
		pipelineStateDesc.pRootSignature = mat->m_RootSignature.Get();
		pipelineStateDesc.VS = CD3DX12_SHADER_BYTECODE(vs.Get());
		pipelineStateDesc.PS = CD3DX12_SHADER_BYTECODE(ps.Get());
		pipelineStateDesc.GS = CD3DX12_SHADER_BYTECODE(gs.Get());
		pipelineStateDesc.RasterizerState = rasterizerDesc;
		pipelineStateDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
		pipelineStateDesc.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
		pipelineStateDesc.SampleMask = UINT_MAX;
		pipelineStateDesc.PrimitiveTopologyType = mat->m_Topology;
		pipelineStateDesc.NumRenderTargets = 2;
		pipelineStateDesc.RTVFormats[0] = m_deviceResources->GetShadowBufferFormat();//m_deviceResources->GetBackBufferFormat();
		//pipelineStateDesc.RTVFormats[1] = format;
		pipelineStateDesc.DSVFormat = m_deviceResources->GetDepthBufferFormat();
		pipelineStateDesc.SampleDesc.Count = 1;

		DX::ThrowIfFailed(d3dDevice->CreateGraphicsPipelineState(&pipelineStateDesc, IID_PPV_ARGS(&mat->m_PipelineStateDepth)));

	}

	mat->m_IsInitialized = true;
}
void Renderer::RenderEngine::SetupAA() {
#ifdef AA_METHOD_SMAA
	//Pass 1: Edge detection
	{

		const UINT numParams = 4;
		m_SMAAedge = std::make_unique<Renderer::PostProcessing>(L"SMAA_P1_VS.cso", L"SMAA_P1_PS.cso");

		CD3DX12_DESCRIPTOR_RANGE1 range[3];
		CD3DX12_ROOT_PARAMETER1 parameter[numParams];
		range[0].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0);
		range[1].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SAMPLER, 1, 0);
		range[2].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SAMPLER, 1, 1);
		parameter[0].InitAsDescriptorTable(1, range, D3D12_SHADER_VISIBILITY_PIXEL);
		parameter[1].InitAsDescriptorTable(1, range + 1, D3D12_SHADER_VISIBILITY_PIXEL);
		parameter[2].InitAsDescriptorTable(1, range + 2, D3D12_SHADER_VISIBILITY_PIXEL);
		parameter[3].InitAsConstants(NUM_32BIT_CONSTANTS(SMAARTMetrics), 0, 0, D3D12_SHADER_VISIBILITY_ALL);

		CreatePPPipeline(*m_SMAAedge, numParams, parameter);
	}
	//Pass 2: Blend Weight Calculation
	{
		const UINT numParams = 6;
		m_SMAAweight = std::make_unique<Renderer::PostProcessing>(L"SMAA_P2_VS.cso", L"SMAA_P2_PS.cso");

		CD3DX12_DESCRIPTOR_RANGE1 range[5];
		CD3DX12_ROOT_PARAMETER1 parameter[numParams];
		range[0].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0);
		range[1].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 1);
		range[2].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 2);
		range[3].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SAMPLER, 1, 0);
		range[4].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SAMPLER, 1, 1);
		for (UINT i = 0; i < numParams-1; ++i) {
			parameter[i].InitAsDescriptorTable(1, range + i, D3D12_SHADER_VISIBILITY_PIXEL);
		}
		parameter[5].InitAsConstants(NUM_32BIT_CONSTANTS(SMAARTMetrics), 0, 0, D3D12_SHADER_VISIBILITY_ALL);

		CreatePPPipeline(*m_SMAAweight, numParams, parameter);

		m_SMAAareatex = std::make_unique<Renderer::Texture>();
		m_SMAAareatex->CreateFromBinary(AREATEX_WIDTH, AREATEX_HEIGHT, DXGI_FORMAT_R8G8_UNORM, AREATEX_PITCH, areaTexBytes, m_deviceResources);
		m_SMAAsearchtex = std::make_unique<Renderer::Texture>();
		m_SMAAsearchtex->CreateFromBinary(SEARCHTEX_WIDTH, SEARCHTEX_HEIGHT, DXGI_FORMAT_R8_UNORM, SEARCHTEX_PITCH, areaTexBytes, m_deviceResources);
	}
	//PAss 3: Neighborhood Blending
	{
		const UINT numParams = 5;
		m_SMAA = std::make_unique<Renderer::PostProcessing>(L"SMAA_P3_VS.cso", L"SMAA_P3_PS.cso");

		CD3DX12_DESCRIPTOR_RANGE1 range[4];
		CD3DX12_ROOT_PARAMETER1 parameter[numParams];
		range[0].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0);
		range[1].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 1);
		range[2].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SAMPLER, 1, 0);
		range[3].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SAMPLER, 1, 1);
		for (UINT i = 0; i < numParams-1; ++i) {
			parameter[i].InitAsDescriptorTable(1, range + i, D3D12_SHADER_VISIBILITY_PIXEL);
		}
		parameter[4].InitAsConstants(NUM_32BIT_CONSTANTS(SMAARTMetrics), 0, 0, D3D12_SHADER_VISIBILITY_ALL);

		CreatePPPipeline(*m_SMAA, numParams, parameter);
	}
#endif
}

void Renderer::RenderEngine::SetupPostProcessing() {
	//SSAA downsample
	{
		m_SSAA = std::make_unique<Renderer::PostProcessing>(L"Blit_VS.cso", L"SSAA_PS.cso");

		CD3DX12_DESCRIPTOR_RANGE1 range[2];
		CD3DX12_ROOT_PARAMETER1 parameter[2];
		range[0].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0);
		range[1].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SAMPLER, 1, 0);
		parameter[0].InitAsDescriptorTable(1, range, D3D12_SHADER_VISIBILITY_PIXEL);
		parameter[1].InitAsDescriptorTable(1, range + 1, D3D12_SHADER_VISIBILITY_PIXEL);

		CreatePPPipeline(*m_SSAA, 2, parameter);
	}
	//Fog
	{
		m_Fog = std::make_unique<Renderer::PostProcessing>(L"Blit_VS.cso", L"Fog_PS.cso");
		m_FogParameters.stepSize = 0.2f;
		m_FogParameters.blackHoleSize = 0.7f;

		CD3DX12_DESCRIPTOR_RANGE1 range[8];
		CD3DX12_ROOT_PARAMETER1 parameter[9];
		UINT r = 0;
		range[r++].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0); //main
		range[r++].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 1); //cube array
		range[r++].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SAMPLER, 1, 0); //bilinear param 2
		range[r++].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SAMPLER, 1, 1); //point param 3
		range[r++].Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 0); //MVP  param 4
		range[r++].Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 10); //global light
		range[r++].Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 11); //Cube MVP
		range[r++].Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 12); //Time
		UINT i;
		for (i = 0; i < r; ++i) {
			parameter[i].InitAsDescriptorTable(1, range+i, D3D12_SHADER_VISIBILITY_PIXEL);

		}
		parameter[i].InitAsConstants(NUM_32BIT_CONSTANTS(FogParameters), 2, 0, D3D12_SHADER_VISIBILITY_PIXEL);

		CreatePPPipeline(*m_Fog, i+1, parameter);
		//CreateCubeMap();
	}
	//Fog blend
	{
		m_FogBlend = std::make_unique<Renderer::PostProcessing>(L"Blit_VS.cso", L"FogBlend_PS.cso");

		CD3DX12_DESCRIPTOR_RANGE1 range[2];
		CD3DX12_ROOT_PARAMETER1 parameter[2];
		range[0].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0);
		range[1].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 1);
		parameter[0].InitAsDescriptorTable(1, range, D3D12_SHADER_VISIBILITY_PIXEL);
		parameter[1].InitAsDescriptorTable(1, range+1, D3D12_SHADER_VISIBILITY_PIXEL);

		CreatePPPipeline(*m_FogBlend, 2, parameter);
	}
	//Bloom filtering
	{
		m_BloomFilter = std::make_unique<Renderer::PostProcessing>(L"Blit_VS.cso", L"Filter_PS.cso");
		m_BloomFilterParameters = BloomFilterParameters();
		m_BloomFilterParameters.threshold = 1.5f;

		CD3DX12_DESCRIPTOR_RANGE1 range;
		CD3DX12_ROOT_PARAMETER1 parameter[2];
		range.Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0);
		parameter[0].InitAsConstants(NUM_32BIT_CONSTANTS(BloomFilterParameters), 0, 0, D3D12_SHADER_VISIBILITY_PIXEL);
		parameter[1].InitAsDescriptorTable(1, &range, D3D12_SHADER_VISIBILITY_PIXEL);

		CreatePPPipeline(*m_BloomFilter, 2, parameter);

	}
	//Bloom Blur H
	{
		m_BloomBlurrH = std::make_unique<Renderer::PostProcessing>(L"Blit_VS.cso", L"BlurrH_PS.cso");

		CD3DX12_DESCRIPTOR_RANGE1 range;
		CD3DX12_ROOT_PARAMETER1 parameter[2];
		range.Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0);
		parameter[0].InitAsConstants(NUM_32BIT_CONSTANTS(BlurParameters), 0, 0, D3D12_SHADER_VISIBILITY_PIXEL);
		parameter[1].InitAsDescriptorTable(1, &range, D3D12_SHADER_VISIBILITY_PIXEL);

		CreatePPPipeline(*m_BloomBlurrH, 2, parameter);
	}
	//Bloom blur V
	{
		m_BloomBlurrV = std::make_unique<Renderer::PostProcessing>(L"Blit_VS.cso", L"BlurrV_PS.cso");

		CD3DX12_DESCRIPTOR_RANGE1 range;
		CD3DX12_ROOT_PARAMETER1 parameter[2];
		range.Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0);
		parameter[0].InitAsConstants(NUM_32BIT_CONSTANTS(BlurParameters), 0, 0, D3D12_SHADER_VISIBILITY_PIXEL);
		parameter[1].InitAsDescriptorTable(1, &range, D3D12_SHADER_VISIBILITY_PIXEL);

		CreatePPPipeline(*m_BloomBlurrV, 2, parameter);

	}
	//Bilateral Blur V
	{
		m_BlurBilateral = std::make_unique<Renderer::PostProcessing>(L"Blit_VS.cso", L"Blur_Depth_PS.cso");

		CD3DX12_DESCRIPTOR_RANGE1 range[2];
		CD3DX12_ROOT_PARAMETER1 parameter[3];
		range[0].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0);
		range[1].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 1);
		parameter[0].InitAsConstants(NUM_32BIT_CONSTANTS(BlurParameters), 0, 0, D3D12_SHADER_VISIBILITY_PIXEL);
		parameter[1].InitAsDescriptorTable(1, range, D3D12_SHADER_VISIBILITY_PIXEL);
		parameter[2].InitAsDescriptorTable(1, range+1, D3D12_SHADER_VISIBILITY_PIXEL);

		CreatePPPipeline(*m_BlurBilateral, 3, parameter);

	}
	//Tonemapping
	{
		m_Tonemapping = std::make_unique<Renderer::PostProcessing>(L"Blit_VS.cso", L"Tonemap_PS.cso");
		m_TonemapParameters = TonemapParameters();
		m_TonemapParameters.TonemapMethod = TM_ACESFilmic;
		m_TonemapParameters.A = 2.51f;
		m_TonemapParameters.B = 0.03f;
		m_TonemapParameters.C = 2.43f;
		m_TonemapParameters.D = 0.59f;
		m_TonemapParameters.E = 0.14f;
		m_TonemapParameters.Gamma = 1.2f;
		m_TonemapParameters.BloomStrength = 0.5f;

		CD3DX12_DESCRIPTOR_RANGE1 range[2];
		CD3DX12_ROOT_PARAMETER1 parameter[3];
		range[0].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0);
		range[1].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 1);
		parameter[0].InitAsConstants(NUM_32BIT_CONSTANTS(TonemapParameters), 0, 0, D3D12_SHADER_VISIBILITY_PIXEL);
		parameter[1].InitAsDescriptorTable(1, range, D3D12_SHADER_VISIBILITY_PIXEL);
		parameter[2].InitAsDescriptorTable(1, range + 1, D3D12_SHADER_VISIBILITY_PIXEL);

		CreatePPPipeline(*m_Tonemapping, 3, parameter);
	}
	//Blit
	{
		m_Blit = std::make_unique<Renderer::PostProcessing>(L"Blit_VS.cso", L"Blit_PS.cso");

		CD3DX12_DESCRIPTOR_RANGE1 range;
		CD3DX12_ROOT_PARAMETER1 parameter;
		range.Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0);
		parameter.InitAsDescriptorTable(1, &range, D3D12_SHADER_VISIBILITY_PIXEL);

		CreatePPPipeline(*m_Blit, 1, &parameter, m_deviceResources->GetBackBufferFormat());
	}
}
void Renderer::RenderEngine::CreatePPPipeline(Renderer::PostProcessing& pp, const UINT numParameters, const CD3DX12_ROOT_PARAMETER1 * parameters, DXGI_FORMAT format) {
	auto d3dDevice = m_deviceResources->GetD3DDevice();
	//create root signature
	{
		D3D12_ROOT_SIGNATURE_FLAGS rootSignatureFlags =
			D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
			D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
			D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS |
			D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS;
		//D3D12_ROOT_SIGNATURE_FLAG_DENY_VERTEX_SHADER_ROOT_ACCESS;

		CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC descRootSignature;
		descRootSignature.Init_1_1(numParameters, parameters, 0, nullptr, rootSignatureFlags);

		ComPtr<ID3DBlob> pSignature;
		ComPtr<ID3DBlob> pError;
		DX::ThrowIfFailed(D3D12SerializeVersionedRootSignature(&descRootSignature, pSignature.GetAddressOf(), pError.GetAddressOf()));//(&descRootSignature, D3D_ROOT_SIGNATURE_VERSION_1, pSignature.GetAddressOf(), pError.GetAddressOf()));
		DX::ThrowIfFailed(d3dDevice->CreateRootSignature(0, pSignature->GetBufferPointer(), pSignature->GetBufferSize(), IID_PPV_ARGS(&pp.m_RootSignature)));
	}
	//create pipeline state
	{
		//load shaders
		ComPtr<ID3D10Blob> vs;
		ComPtr<ID3D10Blob> ps;
		DX::ThrowIfFailed(D3DReadFileToBlob(pp.m_VertexShader, &vs)); //m_TonemapVS
		DX::ThrowIfFailed(D3DReadFileToBlob(pp.m_PixelShader, &ps)); //m_TonemapPS

		CD3DX12_RASTERIZER_DESC rasterizerDesc(D3D12_DEFAULT);
		ZeroMemory(&rasterizerDesc, sizeof(rasterizerDesc));
		rasterizerDesc.CullMode = D3D12_CULL_MODE_BACK;
		rasterizerDesc.FillMode = D3D12_FILL_MODE_SOLID;
		rasterizerDesc.FrontCounterClockwise = FALSE;
		rasterizerDesc.DepthBias = D3D12_DEFAULT_DEPTH_BIAS;
		rasterizerDesc.DepthBiasClamp = D3D12_DEFAULT_DEPTH_BIAS_CLAMP;
		rasterizerDesc.SlopeScaledDepthBias = D3D12_DEFAULT_SLOPE_SCALED_DEPTH_BIAS;
		rasterizerDesc.DepthClipEnable = TRUE;
		rasterizerDesc.MultisampleEnable = FALSE;
		rasterizerDesc.AntialiasedLineEnable = FALSE;
		rasterizerDesc.ForcedSampleCount = 0;
		rasterizerDesc.ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;


		D3D12_BLEND_DESC blendDesc;
		ZeroMemory(&blendDesc, sizeof(blendDesc));
		blendDesc.AlphaToCoverageEnable = FALSE;
		blendDesc.IndependentBlendEnable = FALSE;
		blendDesc.RenderTarget[0] = {
			TRUE ,FALSE,
			D3D12_BLEND_ONE, D3D12_BLEND_ZERO, D3D12_BLEND_OP_ADD,
			D3D12_BLEND_ONE, D3D12_BLEND_ZERO, D3D12_BLEND_OP_ADD,
			D3D12_LOGIC_OP_NOOP,
			D3D12_COLOR_WRITE_ENABLE_ALL
		};

		CD3DX12_DEPTH_STENCIL_DESC depthStencilDesc(D3D12_DEFAULT);
		//ZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));
		depthStencilDesc.DepthEnable = FALSE;
		depthStencilDesc.StencilEnable = FALSE;

		D3D12_GRAPHICS_PIPELINE_STATE_DESC pipelineStateDesc;
		ZeroMemory(&pipelineStateDesc, sizeof(pipelineStateDesc));
		pipelineStateDesc.InputLayout = { Renderer::VertexDataLayout, _countof(Renderer::VertexDataLayout) };
		pipelineStateDesc.pRootSignature = pp.m_RootSignature.Get();
		pipelineStateDesc.VS = CD3DX12_SHADER_BYTECODE(vs.Get());
		pipelineStateDesc.PS = CD3DX12_SHADER_BYTECODE(ps.Get());
		pipelineStateDesc.RasterizerState = rasterizerDesc;
		pipelineStateDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;// D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
		pipelineStateDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
		pipelineStateDesc.DepthStencilState = depthStencilDesc;// CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
		pipelineStateDesc.SampleMask = UINT_MAX;
		pipelineStateDesc.NumRenderTargets = 1;
		pipelineStateDesc.RTVFormats[0] = format;// m_deviceResources->GetBackBufferFormat();
		pipelineStateDesc.DSVFormat = m_deviceResources->GetDepthBufferFormat();
		pipelineStateDesc.SampleDesc.Count = 1;

		DX::ThrowIfFailed(d3dDevice->CreateGraphicsPipelineState(&pipelineStateDesc, IID_PPV_ARGS(&pp.m_PipelineState)));
	}

	pp.m_IsInitialized = true;
}

void Renderer::RenderEngine::LoadMeshToGPU(Mesh* mesh, bool loadTriangles = true) {
	if (mesh->m_isLoaded) return;
	auto d3dDevice = m_deviceResources->GetD3DDevice();
	Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> cL;
	//m_deviceResources->WaitForGpu();
	//bool init = IsInitialized();
	DX::ThrowIfFailed(d3dDevice->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, m_deviceResources->GetCommandAllocator(), m_DefaultPipelineState.Get(), IID_PPV_ARGS(&m_CommandList)));

	//DX::ThrowIfFailed(m_deviceResources->GetCommandAllocator()->Reset());
	//DX::ThrowIfFailed(m_CommandList->Reset(m_deviceResources->GetCommandAllocator(), m_DefaultPipelineConfig.pipelineState.Get()));

	//auto cl = deviceResources->
	// Vertexpufferressource im Standardheap der GPU erstellen und Indexdaten mit dem Uploadheap dort hinein kopieren.
	// Die Uploadressource darf erst freigegeben werden, wenn sie von der GPU nicht mehr verwendet wird.
	Microsoft::WRL::ComPtr<ID3D12Resource> vertexBufferUpload;

	CD3DX12_HEAP_PROPERTIES defaultHeapProperties(D3D12_HEAP_TYPE_DEFAULT);
	CD3DX12_RESOURCE_DESC vertexBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(mesh->m_vertexDataSize);
	DX::ThrowIfFailed(d3dDevice->CreateCommittedResource(
		&defaultHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&vertexBufferDesc,
		D3D12_RESOURCE_STATE_COPY_DEST,
		nullptr,
		IID_PPV_ARGS(&mesh->m_vertexBuffer)));

	CD3DX12_HEAP_PROPERTIES uploadHeapProperties(D3D12_HEAP_TYPE_UPLOAD);
	DX::ThrowIfFailed(d3dDevice->CreateCommittedResource(
		&uploadHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&vertexBufferDesc,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&vertexBufferUpload)));

	//NAME_D3D12_OBJECT(m_vertexBuffer);

	// Vertexpuffer in die GPU hochladen.
	{
		D3D12_SUBRESOURCE_DATA vertexData = {};
		vertexData.pData = reinterpret_cast<BYTE*>(mesh->m_vertexData);
		vertexData.RowPitch = mesh->m_vertexDataSize;
		vertexData.SlicePitch = vertexData.RowPitch;

		UpdateSubresources(m_CommandList.Get(), mesh->m_vertexBuffer.Get(), vertexBufferUpload.Get(), 0, 0, 1, &vertexData);

		CD3DX12_RESOURCE_BARRIER vertexBufferResourceBarrier =
			CD3DX12_RESOURCE_BARRIER::Transition(mesh->m_vertexBuffer.Get(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER);
		m_CommandList->ResourceBarrier(1, &vertexBufferResourceBarrier);
	}

	// Mesh-Indizes laden. Jede Indexdreiergruppe stellt ein Dreieck zum Rendern auf dem Bildschirm dar.


	mesh->m_indexBufferSize = loadTriangles ? mesh->m_triangleListSize : mesh->m_lineListSize;//sizeof(renderComponent->GetTriangleList());
	mesh->m_indexCount = mesh->m_indexBufferSize / sizeof(unsigned short); //TODO

	// Indexpufferressource im Standardheap der GPU erstellen und Indexdaten mit dem Uploadheap dort hinein kopieren.
	// Die Uploadressource darf erst freigegeben werden, wenn sie von der GPU nicht mehr verwendet wird.
	Microsoft::WRL::ComPtr<ID3D12Resource> indexBufferUpload;

	CD3DX12_RESOURCE_DESC indexBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(mesh->m_indexBufferSize);
	DX::ThrowIfFailed(d3dDevice->CreateCommittedResource(
		&defaultHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&indexBufferDesc,
		D3D12_RESOURCE_STATE_COPY_DEST,
		nullptr,
		IID_PPV_ARGS(&mesh->m_indexBuffer)));

	DX::ThrowIfFailed(d3dDevice->CreateCommittedResource(
		&uploadHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&indexBufferDesc,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&indexBufferUpload)));

	//NAME_D3D12_OBJECT(m_indexBuffer);

	// Indexpuffer in die GPU hochladen.
	{
		D3D12_SUBRESOURCE_DATA indexData = {};
		indexData.pData = reinterpret_cast<BYTE*>(loadTriangles ? mesh->m_triangleIndices : mesh->m_lineIndices);
		indexData.RowPitch = mesh->m_indexBufferSize;
		indexData.SlicePitch = indexData.RowPitch;

		UpdateSubresources(m_CommandList.Get(), mesh->m_indexBuffer.Get(), indexBufferUpload.Get(), 0, 0, 1, &indexData);

		CD3DX12_RESOURCE_BARRIER indexBufferResourceBarrier =
			CD3DX12_RESOURCE_BARRIER::Transition(mesh->m_indexBuffer.Get(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_INDEX_BUFFER);
		m_CommandList->ResourceBarrier(1, &indexBufferResourceBarrier);
	}



	// Befehlsliste schlie�en und ausf�hren, um mit dem Kopieren des Vertex-/Indexpuffers in den Standardheap der GPU zu beginnen.
	DX::ThrowIfFailed(m_CommandList->Close());
	ID3D12CommandList* ppCommandLists[] = { m_CommandList.Get() };
	m_deviceResources->GetCommandQueue()->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);

	// Vertex-/Indexpufferansichten erstellen.
	mesh->m_vertexBufferView.BufferLocation = mesh->m_vertexBuffer->GetGPUVirtualAddress();
	mesh->m_vertexBufferView.StrideInBytes = sizeof(VertexData);
	mesh->m_vertexBufferView.SizeInBytes = mesh->m_vertexDataSize;

	mesh->m_indexBufferView.BufferLocation = mesh->m_indexBuffer->GetGPUVirtualAddress();
	mesh->m_indexBufferView.SizeInBytes = mesh->m_indexBufferSize;
	mesh->m_indexBufferView.Format = DXGI_FORMAT_R16_UINT;

	// Auf das Beenden der Ausf�hrung der Befehlsliste warten. Die Vertex-/Indexpuffer m�ssen in die GPU hochgeladen werden, bevor die Uploadressourcen den Bereich �berschreiten.
	m_deviceResources->WaitForGpu();
	mesh->m_isLoaded = true;
}
void Renderer::RenderEngine::LoadComponentData(RenderComponent * renderComponent) {

	renderComponent->CreateResources(m_deviceResources);
	auto d3dDevice = m_deviceResources->GetD3DDevice();
	//Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> cL;
	//DX::ThrowIfFailed(d3dDevice->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, m_deviceResources->GetCommandAllocator(), m_DefaultPipelineConfig->pipelineState.Get(), IID_PPV_ARGS(&m_CommandList)));
	

	//load mesh if not already done
	{
		auto mesh = renderComponent->GetMesh();
		if (mesh != nullptr && !mesh->IsLoaded()) {

			LoadMeshToGPU(mesh, true);

		}
	}

	CD3DX12_HEAP_PROPERTIES uploadHeapProperties(D3D12_HEAP_TYPE_UPLOAD);
	// Erstellt einen Deskriptorheap f�r die Konstantenpuffer.
	{
		D3D12_DESCRIPTOR_HEAP_DESC heapDesc = {};
		heapDesc.NumDescriptors = DX::c_frameCount + 4; //+ num of global buffers
		heapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
		// Diese Kennzeichnung gibt an, dass dieser Deskriptorheap an die Pipeline gebunden werden kann, und dass die darin enthaltenen Deskriptoren von einer Stammtabelle referenziert werden k�nnen.
		heapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
		DX::ThrowIfFailed(d3dDevice->CreateDescriptorHeap(&heapDesc, IID_PPV_ARGS(&renderComponent->m_cbDescriptorHeap)));
		renderComponent->m_cbDescriptorHeap->SetName(L"m_cbvHeap");
	}

	CD3DX12_RESOURCE_DESC constantBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(DX::c_frameCount * c_alignedConstantBufferSize);
	DX::ThrowIfFailed(d3dDevice->CreateCommittedResource(
		&uploadHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&constantBufferDesc,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&renderComponent->m_constantBuffer)));

	//NAME_D3D12_OBJECT(m_constantBuffer);

	// Erstellt Konstantenpufferansichten f�r den Zugriff auf den Uploadpuffer.
	D3D12_GPU_VIRTUAL_ADDRESS cbvGpuAddress = renderComponent->m_constantBuffer->GetGPUVirtualAddress();
	CD3DX12_CPU_DESCRIPTOR_HANDLE cbvCpuHandle(renderComponent->m_cbDescriptorHeap->GetCPUDescriptorHandleForHeapStart());
	renderComponent->m_cbDescriptorHeapSize = d3dDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

	for (int n = 0; n < DX::c_frameCount; n++) {
		D3D12_CONSTANT_BUFFER_VIEW_DESC desc;
		desc.BufferLocation = cbvGpuAddress;
		desc.SizeInBytes = c_alignedConstantBufferSize;
		d3dDevice->CreateConstantBufferView(&desc, cbvCpuHandle);

		cbvGpuAddress += desc.SizeInBytes;
		cbvCpuHandle.Offset(renderComponent->m_cbDescriptorHeapSize);
	}
	{ // global light buffer
		D3D12_GPU_VIRTUAL_ADDRESS globalcbvGpuAddress = m_GlobalLightBuffer->GetGPUVirtualAddress();

		D3D12_CONSTANT_BUFFER_VIEW_DESC desc;
		desc.BufferLocation = globalcbvGpuAddress;
		desc.SizeInBytes = c_alignedLightBufferSize;

		d3dDevice->CreateConstantBufferView(&desc, cbvCpuHandle);
		cbvCpuHandle.Offset(renderComponent->m_cbDescriptorHeapSize);
	}
	{ // cubemap mvp buffer
		D3D12_GPU_VIRTUAL_ADDRESS globalcbvGpuAddress = m_CubeMapMVPBuffer->GetGPUVirtualAddress();

		D3D12_CONSTANT_BUFFER_VIEW_DESC desc;
		desc.BufferLocation = globalcbvGpuAddress;
		desc.SizeInBytes = c_alignedCBSizeCube;

		d3dDevice->CreateConstantBufferView(&desc, cbvCpuHandle);
		cbvCpuHandle.Offset(renderComponent->m_cbDescriptorHeapSize);
	}
	{ // cubemap
		d3dDevice->CreateShaderResourceView(m_deviceResources->GetShadowRenderTarget(DX::SHADOW_RTV_CUBE_1), nullptr, cbvCpuHandle);
		cbvCpuHandle.Offset(renderComponent->m_cbDescriptorHeapSize);
	}
	{ // timer
		D3D12_GPU_VIRTUAL_ADDRESS globalcbvGpuAddress = m_FrameConstantsBuffer->GetGPUVirtualAddress();

		D3D12_CONSTANT_BUFFER_VIEW_DESC desc;
		desc.BufferLocation = globalcbvGpuAddress;
		desc.SizeInBytes = c_alignedGlobalBufferSize;

		d3dDevice->CreateConstantBufferView(&desc, cbvCpuHandle);
		cbvCpuHandle.Offset(renderComponent->m_cbDescriptorHeapSize);
	}

	// Ordnet die Konstantenpuffer zu.
	CD3DX12_RANGE readRange(0, 0);		// Wir beabsichtigen nicht, aus dieser Ressource f�r die CPU zu lesen.
	DX::ThrowIfFailed(renderComponent->m_constantBuffer->Map(0, &readRange, reinterpret_cast<void**>(&renderComponent->m_mappedConstantBuffer)));
	ZeroMemory(renderComponent->m_mappedConstantBuffer, DX::c_frameCount * Renderer::c_alignedConstantBufferSize);

	m_deviceResources->WaitForGpu();
	renderComponent->m_LoadingComplete = true;
}

void Renderer::RenderEngine::CreateGlobalConstantBuffers() {
	//cubemap CB
	SetCubemapViewport(XMFLOAT3(0, 0, 0));
	m_CubeMapMVPBufferDescriptorOffset = m_deviceResources->CreateConstantBuffer(m_CubeMapMVPBuffer, c_alignedCBSizeCube, &m_MappedCubeMapMVPBuffer);
	//VP for main camera (used in fog)
	m_MVPBufferDescriptorOffset = m_deviceResources->CreateConstantBuffer(m_MVPBuffer, c_alignedConstantBufferSize, &m_MappedMVPBuffer);
	//global light CB
	m_GlobalLightBufferDescriptorOffset = m_deviceResources->CreateConstantBuffer(m_GlobalLightBuffer, c_alignedLightBufferSize, &m_MappedGlobalLightBuffer);
	//util CB (time, etc)
	m_FrameConstantsBufferDescriptorOffset = m_deviceResources->CreateConstantBuffer(m_FrameConstantsBuffer, c_alignedGlobalBufferSize, &m_MappedFrameConstantsBuffer);
}

void Renderer::RenderEngine::UpdateGlobalBuffers() {

	if (m_Camera != nullptr) {
		m_MVPBufferData.view = m_Camera->GetViewMatrix();
		XMMATRIX view = XMLoadFloat4x4(&m_MVPBufferData.view);
		XMStoreFloat4x4(&m_MVPBufferData.inv_view, XMMatrixInverse(nullptr, view));
	}

	DX::UpdateConstantBuffer<Renderer::CubeMapViewProjectionConstantBuffer>(m_MappedCubeMapMVPBuffer, &m_CubeMapMVPBufferData);
	DX::UpdateConstantBuffer<Renderer::ModelViewProjectionConstantBuffer>(m_MappedMVPBuffer, &m_MVPBufferData);

	m_GlobalLightBufferData.position = m_GlobalLightComponent->GetPosition();
	m_GlobalLightBufferData.color = m_GlobalLightComponent->GetColor();
	m_GlobalLightBufferData.ambient = 0.0f;
	DX::UpdateConstantBuffer<Renderer::PointLightData>(m_MappedGlobalLightBuffer, &m_GlobalLightBufferData);

	DX::UpdateConstantBuffer<Renderer::GloablConstantsBuffer>(m_MappedFrameConstantsBuffer, &m_FrameConstantsBufferData);
}

void Renderer::RenderEngine::SetViewport() {
	Windows::Foundation::Size outputSize = m_deviceResources->GetOutputSize();
	float aspectRatio = outputSize.Width / outputSize.Height;
	float fovAngleY = 70.0f * XM_PI / 180.0f;

	D3D12_VIEWPORT viewport = m_deviceResources->GetScreenViewport();
	m_scissorRect = { 0, 0, static_cast<LONG>(viewport.Width), static_cast<LONG>(viewport.Height) };
	//m_scissorRect2 = { 0, 0, static_cast<LONG>(viewport.Width*2), static_cast<LONG>(viewport.Height*2) };

	m_RTMetrics.width = outputSize.Width;
	m_RTMetrics.hight = outputSize.Height;
	m_RTMetrics.invWidth = 1 / m_RTMetrics.width;
	m_RTMetrics.invHight = 1 / m_RTMetrics.hight;
	// Dies ist ein einfaches Beispiel f�r eine �nderung, die vorgenommen werden kann, wenn die App im
	// Hochformat oder angedockte Ansicht.
	if (aspectRatio < 1.0f) {
		fovAngleY *= 2.0f;
	}


	// F�r dieses Beispiel wird ein rechtsh�ndiges Koordinatensystem mit Zeilenmatrizen verwendet.
	XMMATRIX perspectiveMatrix = XMMatrixPerspectiveFovRH(
		fovAngleY,
		aspectRatio,
		0.01f,
		100.0f
	);

	XMFLOAT4X4 orientation = m_deviceResources->GetOrientationTransform3D();
	XMMATRIX orientationMatrix = XMLoadFloat4x4(&orientation);

	XMMATRIX projection = XMMatrixTranspose(perspectiveMatrix * orientationMatrix);

	XMStoreFloat4x4(&m_MVPBufferData.projection, projection);
	XMStoreFloat4x4(&m_MVPBufferData.inv_projection, XMMatrixInverse(nullptr, projection));

	// Das Auge befindet sich bei (0,0.7,1.5) und betrachtet Punkt (0,-0.1,0) mit dem Up-Vektor entlang der Y-Achse.
	static const XMVECTORF32 eye = { 0.0f, 6.0f, -5.0f, 0.0f };
	static const XMVECTORF32 at = { 0.0f, 0.0f, 0.0f, 0.0f };
	static const XMVECTORF32 up = { 0.0f, 0.0f, 1.0f, 0.0f };

	XMMATRIX view = XMMatrixTranspose(XMMatrixLookAtRH(eye, at, up));

	XMStoreFloat4x4(&m_MVPBufferData.view, view);
	XMStoreFloat4x4(&m_MVPBufferData.inv_view, XMMatrixInverse(nullptr, view));
}

void Renderer::RenderEngine::SetCubemapViewport(XMFLOAT3 cubePosition) {

	D3D12_VIEWPORT viewport = m_deviceResources->GetShadowViewport();
	float aspectRatio = viewport.Width / viewport.Height;
	float fovAngleY = 90.0f * XM_PI / 180.0f;

	m_ShadowScissorRect = { 0, 0, static_cast<LONG>(viewport.Width), static_cast<LONG>(viewport.Height) };

	XMMATRIX perspectiveMatrix = XMMatrixPerspectiveFovRH(
		fovAngleY,
		aspectRatio,
		0.01f,
		100.0f
	);

	XMMATRIX projection = XMMatrixTranspose(perspectiveMatrix );
	//TODO use
	XMMATRIX eyePos = XMMatrixTranslation(cubePosition.x, cubePosition.y, cubePosition.z);

	static const XMVECTORF32 eye = { 0.0f, 0.0f, 0.0f, 0.0f };

	static const XMVECTORF32 upY = { 0.0f, 1.0f, 0.0f, 0.0f };
	static const XMVECTORF32 atPX = { 1.0f, 0.0f, 0.0f, 0.0f };
	static const XMVECTORF32 atNX = { -1.0f, 0.0f, 0.0f, 0.0f };
	static const XMVECTORF32 atPZ = { 0.0f, 0.0f, 1.0f, 0.0f };
	static const XMVECTORF32 atNZ = { 0.0f, 0.0f, -1.0f, 0.0f };

	static const XMVECTORF32 upZ = { 0.0f, 0.0f, 1.0f, 0.0f };
	static const XMVECTORF32 atPY = { 0.0f, 1.0f, 0.0f, 0.0f };
	static const XMVECTORF32 atNY = { 0.0f, -1.0f, 0.0f, 0.0f };


	XMMATRIX viewPX = XMMatrixTranspose(XMMatrixLookAtRH(eye, atPX, upY));
	XMMATRIX viewNX = XMMatrixTranspose(XMMatrixLookAtRH(eye, atNX, upY));
	XMMATRIX viewPY = XMMatrixTranspose(XMMatrixLookAtRH(eye, atPY, upZ));
	XMMATRIX viewNY = XMMatrixTranspose(XMMatrixLookAtRH(eye, atNY, upZ));
	XMMATRIX viewPZ = XMMatrixTranspose(XMMatrixLookAtRH(eye, atPZ, upY));
	XMMATRIX viewNZ = XMMatrixTranspose(XMMatrixLookAtRH(eye, atNZ, upY));

	XMStoreFloat4x4(&m_CubeMapMVPBufferData.projection, projection);
	XMStoreFloat4x4(&m_CubeMapMVPBufferData.viewPX, viewPX);
	XMStoreFloat4x4(&m_CubeMapMVPBufferData.viewNX, viewNX);
	XMStoreFloat4x4(&m_CubeMapMVPBufferData.viewPY, viewPY);
	XMStoreFloat4x4(&m_CubeMapMVPBufferData.viewNY, viewNY);
	XMStoreFloat4x4(&m_CubeMapMVPBufferData.viewPZ, viewPZ);
	XMStoreFloat4x4(&m_CubeMapMVPBufferData.viewNZ, viewNZ);

}

void Renderer::RenderEngine::SortRenderComponents(std::unordered_set<RenderComponent*>* renderComponents) {

	for (auto itr = renderComponents->begin(); itr != renderComponents->end(); ++itr) {
		RenderComponent* c = *itr;
		if (dynamic_cast<OrbitRenderComponent*>(c) != nullptr) {
			m_UIOrbitComponents.push_back(c);
		}
		else if (dynamic_cast<UIRenderComponent*>(c) != nullptr) {
			m_UIRenderComponents.push_back(c);
		}
		else if (dynamic_cast<UIRectComponent*>(c) != nullptr) {
			m_UIRectComponents.push_back(c);
		}
		else if (c->GetMaterial()->m_BlendState == transparent) {
			m_TransparentComponents.push_back(c);
		}
		else if (c->GetMaterial()->m_BlendState == alphaAdditive) {
			m_TransparentAdditiveComponents.push_back(c);
		}
		else {
			m_OpaqueComponents.push_back(c);
		}
	}
}


inline void Renderer::RenderEngine::RenderDepthCube() {
	//TODO refactor to single pass cubemap rendering to tex array


	for (UINT SRTV = DX::SHADOW_RTV_CUBE_1; SRTV < DX::SHADOW_RTV_AMOUNT; ++SRTV) {
		{
			//update cubmap view-projection buffer if necessary
			//m_CubeMapMVPBufferData
		}
		/*{
			m_deviceResources->WaitForGpu();
			DX::ThrowIfFailed(m_deviceResources->GetCommandAllocator()->Reset());
			DX::ThrowIfFailed(m_CommandList->Reset(m_deviceResources->GetCommandAllocator(), nullptr));
		}//*/

		DX::SHADOW_RTV currentRTV = static_cast<DX::SHADOW_RTV>(SRTV);
		{
			D3D12_CPU_DESCRIPTOR_HANDLE depthStencilView = m_deviceResources->GetShadowCubeDepthStencilView();
			D3D12_VIEWPORT viewport = m_deviceResources->GetShadowViewport();
			m_CommandList->RSSetViewports(1, &viewport);
			m_CommandList->RSSetScissorRects(1, &m_ShadowScissorRect);

			//move current RTV to RT status
			D3D12_CPU_DESCRIPTOR_HANDLE rtv = m_deviceResources->GetShadowRenderTargetView(currentRTV);
			CD3DX12_RESOURCE_BARRIER rtResourceBarrier =
				CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetShadowRenderTarget(currentRTV), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_RENDER_TARGET);
			m_CommandList->ResourceBarrier(1, &rtResourceBarrier);

			//clear and set RTV and DSV
			m_CommandList->ClearRenderTargetView(rtv, DX::clearColorDepth, 0, nullptr);
			m_CommandList->ClearDepthStencilView(depthStencilView, D3D12_CLEAR_FLAG_DEPTH, 1.0f, 0, 0, nullptr);
			m_CommandList->OMSetRenderTargets(1, &rtv, false, &depthStencilView);
		}
		//Render object Depth
		RenderComponentsDepth(m_OpaqueComponents);

		//move current RTV back to SR status
		{
			CD3DX12_RESOURCE_BARRIER srvResourceBarrier =
				CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetShadowRenderTarget(currentRTV), D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
			m_CommandList->ResourceBarrier(1, &srvResourceBarrier);
		}
		/*{
			DX::ThrowIfFailed(m_CommandList->Close());

			// Befehlsliste ausf�hren.
			ID3D12CommandList* ppCommandLists[] = { m_CommandList.Get() };
			m_deviceResources->GetCommandQueue()->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);
		}//*/
	}
}

inline void Renderer::RenderEngine::RenderComponents(std::vector<RenderComponent*>& renderComponents) {

	for (auto itr = renderComponents.begin(); itr != renderComponents.end(); ++itr) {
		RenderComponent* renderComponent = *itr;
		//only render loaded objects
		if (renderComponent == nullptr || !renderComponent->m_LoadingComplete) continue;
		renderComponent->Draw(m_deviceResources, m_CommandList);
	}
}

inline void Renderer::RenderEngine::RenderComponentsDepth(std::vector<RenderComponent*>& renderComponents) {

	for (auto itr = renderComponents.begin(); itr != renderComponents.end(); ++itr) {
		RenderComponent* renderComponent = *itr;
		//only render loaded objects
		if (renderComponent == nullptr || !renderComponent->m_LoadingComplete) continue;
		renderComponent->DrawDepth(m_deviceResources, m_CommandList);
	}
}

inline void Renderer::RenderEngine::RenderFog() {

	{
		D3D12_CPU_DESCRIPTOR_HANDLE HDRRTVfilter = m_deviceResources->GetHDRRenderTargetView(DX::HDR_RTV_FOG);
		D3D12_CPU_DESCRIPTOR_HANDLE depthStencilView = m_deviceResources->GetDepthStencilView();
		CD3DX12_RESOURCE_BARRIER renderTargetResourceBarrier = CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetHDRRenderTarget(DX::HDR_RTV_FOG), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_RENDER_TARGET);
		m_CommandList->ResourceBarrier(1, &renderTargetResourceBarrier);
		m_CommandList->OMSetRenderTargets(1, &HDRRTVfilter, false, &depthStencilView);

		m_CommandList->SetGraphicsRootSignature(m_Fog->GetRootSignature());
		m_CommandList->SetPipelineState(m_Fog->GetPSO());
		m_CommandList->SetGraphicsRootDescriptorTable(0, m_deviceResources->GetHDRShaderResourceViewGPU(DX::HDR_RTV_MAIN_NORMAL_DEPTH));
		m_CommandList->SetGraphicsRootDescriptorTable(1, m_deviceResources->GetShadowShaderResourceViewGPU(DX::SHADOW_RTV_CUBE_1));
		m_CommandList->SetGraphicsRootDescriptorTable(2, m_deviceResources->GetSamplerViewGPU(DX::SAMPLER_BILINEAR));
		m_CommandList->SetGraphicsRootDescriptorTable(3, m_deviceResources->GetSamplerViewGPU(DX::SAMPLER_NN));
		m_CommandList->SetGraphicsRootDescriptorTable(4, m_deviceResources->Get_SR_CB_UA_ViewGPU(m_MVPBufferDescriptorOffset));
		m_CommandList->SetGraphicsRootDescriptorTable(5, m_deviceResources->Get_SR_CB_UA_ViewGPU(m_GlobalLightBufferDescriptorOffset));
		m_CommandList->SetGraphicsRootDescriptorTable(6, m_deviceResources->Get_SR_CB_UA_ViewGPU(m_CubeMapMVPBufferDescriptorOffset));
		m_CommandList->SetGraphicsRootDescriptorTable(7, m_deviceResources->Get_SR_CB_UA_ViewGPU(m_FrameConstantsBufferDescriptorOffset));
		m_CommandList->SetGraphicsRoot32BitConstants(8, NUM_32BIT_CONSTANTS(Renderer::FogParameters), &m_FogParameters, 0);
		//m_ScreenQuad->Draw(m_CommandList);
		DrawScreenQuad();

		CD3DX12_RESOURCE_BARRIER srvResourceBarrier =
			CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetHDRRenderTarget(DX::HDR_RTV_FOG), D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
		m_CommandList->ResourceBarrier(1, &srvResourceBarrier);
		//
	}
	Blur(DX::HDR_RTV_FOG, DX::HDR_RTV_BLOOM2, 1); //Blurr fog in FOG into BLOOM2
	BlurBilateral(DX::HDR_RTV_FOG, DX::HDR_RTV_MAIN_NORMAL_DEPTH, DX::HDR_RTV_BLOOM2, m_FogBlurDepthBias, 1); //Blurr fog in FOG into BLOOM2
	//Fog Blend
	Blend(DX::HDR_RTV_MAIN, DX::HDR_RTV_BLOOM2, DX::HDR_RTV_FOG); //Blend (blurred) fog in BLOOM2 over MAIN back into FOG
	m_LastRTV = DX::HDR_RTV_FOG;
	m_MainRTV = DX::HDR_RTV_FOG;
}

inline void Renderer::RenderEngine::RenderBloom() {
	//Filter
	{
		D3D12_CPU_DESCRIPTOR_HANDLE HDRRTVfilter = m_deviceResources->GetHDRRenderTargetView(DX::HDR_RTV_FILTER);
		D3D12_CPU_DESCRIPTOR_HANDLE depthStencilView = m_deviceResources->GetDepthStencilView();
		CD3DX12_RESOURCE_BARRIER renderTargetResourceBarrier = CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetHDRRenderTarget(DX::HDR_RTV_FILTER), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_RENDER_TARGET);
		m_CommandList->ResourceBarrier(1, &renderTargetResourceBarrier);
		m_CommandList->OMSetRenderTargets(1, &HDRRTVfilter, false, &depthStencilView);

		m_CommandList->SetGraphicsRootSignature(m_BloomFilter->GetRootSignature());
		m_CommandList->SetPipelineState(m_BloomFilter->GetPSO());

		m_CommandList->SetGraphicsRoot32BitConstants(0, NUM_32BIT_CONSTANTS(Renderer::BloomFilterParameters), &m_BloomFilterParameters, 0);
		m_CommandList->SetGraphicsRootDescriptorTable(1, m_deviceResources->GetHDRShaderResourceViewGPU(m_MainRTV));
		//m_ScreenQuad->Draw(m_CommandList);
		DrawScreenQuad();

		CD3DX12_RESOURCE_BARRIER srvResourceBarrier =
			CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetHDRRenderTarget(DX::HDR_RTV_FILTER), D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
		m_CommandList->ResourceBarrier(1, &srvResourceBarrier);
		m_LastRTV = DX::HDR_RTV_FILTER;
	}
	Blur(DX::HDR_RTV_FILTER, DX::HDR_RTV_FILTER, m_BloomBlurrPasses);
	
	PIXEndEvent(m_CommandList.Get());
}

inline void Renderer::RenderEngine::RenderTonemapping() {

	D3D12_CPU_DESCRIPTOR_HANDLE HDRRTV = m_deviceResources->GetHDRRenderTargetView(DX::HDR_RTV_TONEMAPPING);
	D3D12_CPU_DESCRIPTOR_HANDLE depthStencilView = m_deviceResources->GetDepthStencilView();
	CD3DX12_RESOURCE_BARRIER renderTargetResourceBarrier = CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetHDRRenderTarget(DX::HDR_RTV_TONEMAPPING), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_RENDER_TARGET);
	m_CommandList->ResourceBarrier(1, &renderTargetResourceBarrier);
	m_CommandList->OMSetRenderTargets(1, &HDRRTV, false, &depthStencilView);


	m_CommandList->SetGraphicsRootSignature(m_Tonemapping->GetRootSignature());
	m_CommandList->SetPipelineState(m_Tonemapping->GetPSO());

	m_CommandList->SetGraphicsRoot32BitConstants(0, NUM_32BIT_CONSTANTS(Renderer::TonemapParameters), &m_TonemapParameters, 0);
	m_CommandList->SetGraphicsRootDescriptorTable(1, m_deviceResources->GetHDRShaderResourceViewGPU(m_MainRTV));
	m_CommandList->SetGraphicsRootDescriptorTable(2, m_deviceResources->GetHDRShaderResourceViewGPU(m_LastRTV));
	//m_ScreenQuad->Draw(m_CommandList);
	DrawScreenQuad();

	CD3DX12_RESOURCE_BARRIER presentResourceBarrier =
		CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetHDRRenderTarget(DX::HDR_RTV_TONEMAPPING), D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
	m_CommandList->ResourceBarrier(1, &presentResourceBarrier);

	m_LastRTV = DX::HDR_RTV_TONEMAPPING;
	m_MainRTV = DX::HDR_RTV_TONEMAPPING;
}

inline void Renderer::RenderEngine::RenderSMAA() {
	//SMAA
	{
		ID3D12DescriptorHeap* ppHeaps[] = { m_deviceResources->GetSRVDescriptorHeap(), m_deviceResources->GetSamplerDescriptorHeap() };
		m_CommandList->SetDescriptorHeaps(_countof(ppHeaps), ppHeaps);
	}
	//Pass 1
	{
		D3D12_CPU_DESCRIPTOR_HANDLE HDRRTV = m_deviceResources->GetHDRRenderTargetView(DX::HDR_RTV_SMAA_EDGE);
		D3D12_CPU_DESCRIPTOR_HANDLE depthStencilView = m_deviceResources->GetDepthStencilView();
		CD3DX12_RESOURCE_BARRIER renderTargetResourceBarrier = CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetHDRRenderTarget(DX::HDR_RTV_SMAA_EDGE), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_RENDER_TARGET);
		m_CommandList->ResourceBarrier(1, &renderTargetResourceBarrier);
		m_CommandList->OMSetRenderTargets(1, &HDRRTV, false, &depthStencilView);
		m_CommandList->ClearRenderTargetView(HDRRTV, DX::clearColorBlack, 0, nullptr);

		m_CommandList->SetGraphicsRootSignature(m_SMAAedge->GetRootSignature());
		m_CommandList->SetPipelineState(m_SMAAedge->GetPSO());

		//m_CommandList->SetGraphicsRoot32BitConstants(0, NUM_32BIT_CONSTANTS(Renderer::BloomFilterParameters), &m_BloomFilterParameters, 0);
		m_CommandList->SetGraphicsRootDescriptorTable(0, m_deviceResources->GetHDRShaderResourceViewGPU(m_LastRTV));
		m_CommandList->SetGraphicsRootDescriptorTable(1, m_deviceResources->GetSamplerViewGPU(DX::SAMPLER_BILINEAR));
		m_CommandList->SetGraphicsRootDescriptorTable(2, m_deviceResources->GetSamplerViewGPU(DX::SAMPLER_NN));
		m_CommandList->SetGraphicsRoot32BitConstants(3, NUM_32BIT_CONSTANTS(Renderer::SMAARTMetrics), &m_RTMetrics, 0);
		//m_ScreenQuad->Draw(m_CommandList);
		DrawScreenQuad();

		CD3DX12_RESOURCE_BARRIER srvResourceBarrier =
			CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetHDRRenderTarget(DX::HDR_RTV_SMAA_EDGE), D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
		m_CommandList->ResourceBarrier(1, &srvResourceBarrier);
		m_LastRTV = DX::HDR_RTV_SMAA_EDGE;
		//debug
		//mainRTV = DX::HDR_RTV_SMAA_EDGE;
		//m_BloomActive = false;
	}
	//Pass 2
	{
		// /* TODO load area and search textures
		D3D12_CPU_DESCRIPTOR_HANDLE HDRRTV = m_deviceResources->GetHDRRenderTargetView(DX::HDR_RTV_SMAA_WEIGHT);
		D3D12_CPU_DESCRIPTOR_HANDLE depthStencilView = m_deviceResources->GetDepthStencilView();
		CD3DX12_RESOURCE_BARRIER renderTargetResourceBarrier = CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetHDRRenderTarget(DX::HDR_RTV_SMAA_WEIGHT), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_RENDER_TARGET);
		m_CommandList->ResourceBarrier(1, &renderTargetResourceBarrier);
		m_CommandList->OMSetRenderTargets(1, &HDRRTV, false, &depthStencilView);
		m_CommandList->ClearRenderTargetView(HDRRTV, DX::clearColorBlack, 0, nullptr);

		m_CommandList->SetGraphicsRootSignature(m_SMAAweight->GetRootSignature());
		m_CommandList->SetPipelineState(m_SMAAweight->GetPSO());

		//m_CommandList->SetGraphicsRoot32BitConstants(0, NUM_32BIT_CONSTANTS(Renderer::BloomFilterParameters), &m_BloomFilterParameters, 0);
		m_CommandList->SetGraphicsRootDescriptorTable(0, m_deviceResources->GetHDRShaderResourceViewGPU(m_LastRTV));
		m_CommandList->SetGraphicsRootDescriptorTable(1, m_SMAAareatex->GetShaderResourceViewGPU()); //area
		m_CommandList->SetGraphicsRootDescriptorTable(2, m_SMAAsearchtex->GetShaderResourceViewGPU()); //search
		m_CommandList->SetGraphicsRootDescriptorTable(3, m_deviceResources->GetSamplerViewGPU(DX::SAMPLER_BILINEAR));
		m_CommandList->SetGraphicsRootDescriptorTable(4, m_deviceResources->GetSamplerViewGPU(DX::SAMPLER_NN));
		m_CommandList->SetGraphicsRoot32BitConstants(5, NUM_32BIT_CONSTANTS(Renderer::SMAARTMetrics), &m_RTMetrics, 0);
		//m_ScreenQuad->Draw(m_CommandList);
		DrawScreenQuad();

		CD3DX12_RESOURCE_BARRIER srvResourceBarrier =
			CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetHDRRenderTarget(DX::HDR_RTV_SMAA_WEIGHT), D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
		m_CommandList->ResourceBarrier(1, &srvResourceBarrier);
		m_LastRTV = DX::HDR_RTV_SMAA_WEIGHT;
		//debug
		//mainRTV = DX::HDR_RTV_SMAA_WEIGHT;
		//m_BloomActive = false;
		//*/
	}
	//Pass 3
	{
		// /* TODO load area and search textures
		D3D12_CPU_DESCRIPTOR_HANDLE HDRRTV = m_deviceResources->GetHDRRenderTargetView(DX::HDR_RTV_SMAA);
		D3D12_CPU_DESCRIPTOR_HANDLE depthStencilView = m_deviceResources->GetDepthStencilView();
		CD3DX12_RESOURCE_BARRIER renderTargetResourceBarrier = CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetHDRRenderTarget(DX::HDR_RTV_SMAA), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_RENDER_TARGET);
		m_CommandList->ResourceBarrier(1, &renderTargetResourceBarrier);
		m_CommandList->OMSetRenderTargets(1, &HDRRTV, false, &depthStencilView);
		m_CommandList->ClearRenderTargetView(HDRRTV, DX::clearColorBlack, 0, nullptr);

		m_CommandList->SetGraphicsRootSignature(m_SMAA->GetRootSignature());
		m_CommandList->SetPipelineState(m_SMAA->GetPSO());

		m_CommandList->SetGraphicsRootDescriptorTable(0, m_deviceResources->GetHDRShaderResourceViewGPU(m_MainRTV));
		m_CommandList->SetGraphicsRootDescriptorTable(1, m_deviceResources->GetHDRShaderResourceViewGPU(m_LastRTV));
		m_CommandList->SetGraphicsRootDescriptorTable(2, m_deviceResources->GetSamplerViewGPU(DX::SAMPLER_BILINEAR));
		m_CommandList->SetGraphicsRootDescriptorTable(3, m_deviceResources->GetSamplerViewGPU(DX::SAMPLER_NN));
		m_CommandList->SetGraphicsRoot32BitConstants(4, NUM_32BIT_CONSTANTS(Renderer::SMAARTMetrics), &m_RTMetrics, 0);
		DrawScreenQuad();

		CD3DX12_RESOURCE_BARRIER srvResourceBarrier =
			CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetHDRRenderTarget(DX::HDR_RTV_SMAA), D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
		m_CommandList->ResourceBarrier(1, &srvResourceBarrier);
		m_LastRTV = DX::HDR_RTV_SMAA;
		m_MainRTV = DX::HDR_RTV_SMAA;
		//debug
		//m_BloomActive = false;
		//*/
	}
}

inline void Renderer::RenderEngine::Blur(const DX::HDR_RTV src, const DX::HDR_RTV dst, const UINT passes, const DX::HDR_RTV mid) {

	DX::HDR_RTV lastRTV = src;
	BlurParameters params;
	params.depthBias = 0;
	for (UINT i = 0; i < passes; ++i) {
		//Blurr H
		{
			params.direction = 0;
			D3D12_CPU_DESCRIPTOR_HANDLE HDRRTVblurr1 = m_deviceResources->GetHDRRenderTargetView(mid);
			D3D12_CPU_DESCRIPTOR_HANDLE depthStencilView = m_deviceResources->GetDepthStencilView();
			CD3DX12_RESOURCE_BARRIER renderTargetResourceBarrier = CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetHDRRenderTarget(mid), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_RENDER_TARGET);
			m_CommandList->ResourceBarrier(1, &renderTargetResourceBarrier);
			m_CommandList->OMSetRenderTargets(1, &HDRRTVblurr1, false, &depthStencilView);

			m_CommandList->SetGraphicsRootSignature(m_BloomBlurrH->GetRootSignature());
			m_CommandList->SetPipelineState(m_BloomBlurrH->GetPSO());

			m_CommandList->SetGraphicsRoot32BitConstants(0, NUM_32BIT_CONSTANTS(Renderer::BlurParameters), &params, 0);
			m_CommandList->SetGraphicsRootDescriptorTable(1, m_deviceResources->GetHDRShaderResourceViewGPU(lastRTV));
			//m_ScreenQuad->Draw(m_CommandList);
			DrawScreenQuad();

			CD3DX12_RESOURCE_BARRIER srvResourceBarrier =
				CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetHDRRenderTarget(mid), D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
			m_CommandList->ResourceBarrier(1, &srvResourceBarrier);
			lastRTV = mid;
		}
		//Blurr V
		{
			params.direction = 1;
			D3D12_CPU_DESCRIPTOR_HANDLE HDRRTVblurr2 = m_deviceResources->GetHDRRenderTargetView(dst);
			D3D12_CPU_DESCRIPTOR_HANDLE depthStencilView = m_deviceResources->GetDepthStencilView();
			CD3DX12_RESOURCE_BARRIER renderTargetResourceBarrier = CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetHDRRenderTarget(dst), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_RENDER_TARGET);
			m_CommandList->ResourceBarrier(1, &renderTargetResourceBarrier);
			m_CommandList->OMSetRenderTargets(1, &HDRRTVblurr2, false, &depthStencilView);

			m_CommandList->SetGraphicsRootSignature(m_BloomBlurrV->GetRootSignature());
			m_CommandList->SetPipelineState(m_BloomBlurrV->GetPSO());

			m_CommandList->SetGraphicsRoot32BitConstants(0, NUM_32BIT_CONSTANTS(Renderer::BlurParameters), &params, 0);
			m_CommandList->SetGraphicsRootDescriptorTable(1, m_deviceResources->GetHDRShaderResourceViewGPU(lastRTV));
			//m_ScreenQuad->Draw(m_CommandList);
			DrawScreenQuad();

			CD3DX12_RESOURCE_BARRIER srvResourceBarrier =
				CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetHDRRenderTarget(dst), D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
			m_CommandList->ResourceBarrier(1, &srvResourceBarrier);
			lastRTV = dst;
		}
	}
}

inline void Renderer::RenderEngine::BlurBilateral(const DX::HDR_RTV src, const DX::HDR_RTV normalDepth, const DX::HDR_RTV dst, const float depthBias, const UINT passes, const DX::HDR_RTV mid) {

	DX::HDR_RTV lastRTV = src;
	BlurParameters params;
	params.depthBias = depthBias;
	for (UINT i = 0; i < passes; ++i) {
		//Blurr H
		{
			params.direction = 0;
			D3D12_CPU_DESCRIPTOR_HANDLE HDRRTVblurr1 = m_deviceResources->GetHDRRenderTargetView(mid);
			D3D12_CPU_DESCRIPTOR_HANDLE depthStencilView = m_deviceResources->GetDepthStencilView();
			CD3DX12_RESOURCE_BARRIER renderTargetResourceBarrier = CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetHDRRenderTarget(mid), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_RENDER_TARGET);
			m_CommandList->ResourceBarrier(1, &renderTargetResourceBarrier);
			m_CommandList->OMSetRenderTargets(1, &HDRRTVblurr1, false, &depthStencilView);

			m_CommandList->SetGraphicsRootSignature(m_BlurBilateral->GetRootSignature());
			m_CommandList->SetPipelineState(m_BlurBilateral->GetPSO());

			m_CommandList->SetGraphicsRoot32BitConstants(0, NUM_32BIT_CONSTANTS(Renderer::BlurParameters), &params, 0);
			m_CommandList->SetGraphicsRootDescriptorTable(1, m_deviceResources->GetHDRShaderResourceViewGPU(lastRTV));
			m_CommandList->SetGraphicsRootDescriptorTable(2, m_deviceResources->GetHDRShaderResourceViewGPU(normalDepth));
			//m_ScreenQuad->Draw(m_CommandList);
			DrawScreenQuad();

			CD3DX12_RESOURCE_BARRIER srvResourceBarrier =
				CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetHDRRenderTarget(mid), D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
			m_CommandList->ResourceBarrier(1, &srvResourceBarrier);
			lastRTV = mid;
		}
		//Blurr V
		{
			params.direction = 1;
			D3D12_CPU_DESCRIPTOR_HANDLE HDRRTVblurr2 = m_deviceResources->GetHDRRenderTargetView(dst);
			D3D12_CPU_DESCRIPTOR_HANDLE depthStencilView = m_deviceResources->GetDepthStencilView();
			CD3DX12_RESOURCE_BARRIER renderTargetResourceBarrier = CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetHDRRenderTarget(dst), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_RENDER_TARGET);
			m_CommandList->ResourceBarrier(1, &renderTargetResourceBarrier);
			m_CommandList->OMSetRenderTargets(1, &HDRRTVblurr2, false, &depthStencilView);

			m_CommandList->SetGraphicsRootSignature(m_BlurBilateral->GetRootSignature());
			m_CommandList->SetPipelineState(m_BlurBilateral->GetPSO());

			m_CommandList->SetGraphicsRoot32BitConstants(0, NUM_32BIT_CONSTANTS(Renderer::BlurParameters), &params, 0);
			m_CommandList->SetGraphicsRootDescriptorTable(1, m_deviceResources->GetHDRShaderResourceViewGPU(lastRTV));
			m_CommandList->SetGraphicsRootDescriptorTable(2, m_deviceResources->GetHDRShaderResourceViewGPU(normalDepth));
			//m_ScreenQuad->Draw(m_CommandList);
			DrawScreenQuad();

			CD3DX12_RESOURCE_BARRIER srvResourceBarrier =
				CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetHDRRenderTarget(dst), D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
			m_CommandList->ResourceBarrier(1, &srvResourceBarrier);
			lastRTV = dst;
		}
	}
}

inline void Renderer::RenderEngine::Blend(const DX::HDR_RTV src1, const DX::HDR_RTV src2, const DX::HDR_RTV dst) {

	D3D12_CPU_DESCRIPTOR_HANDLE HDRRTVfilter = m_deviceResources->GetHDRRenderTargetView(dst);
	D3D12_CPU_DESCRIPTOR_HANDLE depthStencilView = m_deviceResources->GetDepthStencilView();
	CD3DX12_RESOURCE_BARRIER renderTargetResourceBarrier = CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetHDRRenderTarget(dst), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_RENDER_TARGET);
	m_CommandList->ResourceBarrier(1, &renderTargetResourceBarrier);
	m_CommandList->OMSetRenderTargets(1, &HDRRTVfilter, false, &depthStencilView);

	m_CommandList->SetGraphicsRootSignature(m_FogBlend->GetRootSignature());
	m_CommandList->SetPipelineState(m_FogBlend->GetPSO());
	m_CommandList->SetGraphicsRootDescriptorTable(0, m_deviceResources->GetHDRShaderResourceViewGPU(src1));
	m_CommandList->SetGraphicsRootDescriptorTable(1, m_deviceResources->GetHDRShaderResourceViewGPU(src2));
	DrawScreenQuad();

	CD3DX12_RESOURCE_BARRIER srvResourceBarrier =
		CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetHDRRenderTarget(dst), D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
	m_CommandList->ResourceBarrier(1, &srvResourceBarrier);
}
