#include "pch.h"
#include "GameObject.h"
#include "Component.h"
#include "../Components/Transform.h"
#include "../Components/Rendering/RenderComponent.h"
#include "../Components/Physics/PhysicsComponent.h"
#include <algorithm>
#include "GameMain.h"

using namespace std;
using namespace DirectX;

GameObject::GameObject(Game::GameMain *gameMain)
{
	transform = new Transform(this);
	this->gameMain = gameMain;
}

GameObject::GameObject(Game::GameMain *gameMain, XMFLOAT3 globalposition)
{
	this->gameMain = gameMain;
	this->transform = new Transform(this, globalposition);
}


GameObject::~GameObject()
{
	// delete all children
	while (children.size() > 0) {
		delete *children.begin();
	}

	//delete all components
	while (components.size() > 0) {
		Component* pointer = *components.begin();
		// remove components also calls the deconstructor
		removeComponent(*components.begin());
	}

	parent->removeChild(this);

	delete transform;
}

void GameObject::setName(string name) {
	this->name = name;
}

Transform* GameObject::getTransform() {
	return this->transform;
}

string GameObject::getName() {
	return name;
}

list<GameObject*> GameObject::getChildren(){
	return children;
}

list<GameObject*> GameObject::getAllChildren() {
	list<GameObject*> childrenRecursiv;
	list<GameObject*>::iterator it;
	for (it = children.begin(); it != children.end(); ++it) {
		childrenRecursiv.splice(childrenRecursiv.begin(), (*it)->getAllChildrenRecursiv());
	}
	return childrenRecursiv;
}

list<GameObject*> GameObject::getAllChildrenRecursiv() {
	list<GameObject*> childrenRecursiv;
	list<GameObject*>::iterator it;
	for (it = children.begin(); it != children.end(); ++it) {
		childrenRecursiv.splice(childrenRecursiv.begin(), (*it)->getAllChildrenRecursiv());
	}
	childrenRecursiv.push_back(this);
	return childrenRecursiv;
}

GameObject* GameObject::getParent(){
	return parent;
}

Game::GameMain* GameObject::getGameMain() {
	return gameMain;
}

void GameObject::flagForDeletion() {
	gameMain->flagForDeletion(this);
}


bool GameObject::addComponent(Component* c) {
	if (componentsContainComponent(c)) {
		return false;
	}
	components.push_back(c);
	c->SetMaster(this);
	addComponentToGameMain(c);

	c->OnStart();

	return true;
}

bool GameObject::removeComponent(Component* c) {
	if (componentsContainComponent(c) == false) {
		// element not contained
		return false;
	}
	components.remove(c);
	removeComponentFromGameMain(c);
	
	// TODO Figure out why this check is necessary
	// security check
	if (c == nullptr) {
		return true;
	}

	// THIS COULD CREATE PROBLEMS
	delete c;

	return true;
}

bool GameObject::addChild(GameObject* go) {
	if (childsContainGameObject(go)) {
		return false;
	}
	children.push_back(go);
	go->setPartent(this);
	return true;
}

void GameObject::setPartent(GameObject * go)
{
	parent = go;
}

void GameObject::OnCollision(GameObject::CollisionObject collision) {
	std::list<Component*>::iterator it;
	for (it = components.begin(); it != components.end(); ++it) {
		(*it)->OnCollision(collision);
	}
}



bool GameObject::removeChild(GameObject* go) {
	if (childsContainGameObject(go) == false) {
		// element not contained
		return false;
	}
	children.remove(go);
	return 0;
}

list<Component*> GameObject::getAllComponents() {
	return components;
}

bool GameObject::componentsContainComponent(Component* c) {
	return (std::find(components.begin(), components.end(), c) != components.end());
}

bool GameObject::childsContainGameObject(GameObject* go) {
	return (std::find(children.begin(), children.end(), go) != children.end());
}

bool GameObject::addComponentToGameMain(Component* c) {
	return gameMain->addComponent(c);
}

void GameObject::removeComponentFromGameMain(Component* c) {
	gameMain->removeComponent(c);
}



void GameObject::logToConsole(string input) {
	gameMain->logToConsole(input);
}
