#pragma once
#ifndef COMPONENT
#define COMPONENT
#include "GameObject.h"

//class GameObject;

class Component
{
public:
	virtual ~Component() {

	}
	//Events
	virtual void OnUpdate(float dT) = 0;
	virtual void OnStart() = 0;
	virtual void OnDestroy() = 0;
	virtual void OnCollision(GameObject::CollisionObject collision) = 0;

	void SetMaster(GameObject* master) { m_master = master; };
	GameObject* getMasterGO() const { return m_master; }

protected:
	GameObject* m_master;
};

#endif 