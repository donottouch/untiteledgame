#include "pch.h"
#include "PhysicEngine.h"
#include "Components/Physics/SphereCollider.h"
#include "GameObject.h"
#include "Components/Transform.h"
#include "Component.h"
#include <math.h>

#include <string>
#include <sstream>
#include <iostream>

using namespace DirectX;

PhysicEngine::PhysicEngine()
{
}


PhysicEngine::~PhysicEngine()
{
}

// returns true if the operation was succesfull
bool PhysicEngine::addPhysicsComponent(PhysicsComponent* pc) {
	if (physicsCompsContainComponent(pc)) {
		return false;
	}
	physicsComps.push_back(pc);
	return true;
}
// returns true if the operation was succesfull
bool PhysicEngine::removePhysicsComponent(PhysicsComponent* pc) {
	physicsComps.erase(std::remove(physicsComps.begin(), physicsComps.end(), pc), physicsComps.end());
	return true;
}

void PhysicEngine::computeCollisions() {
	int size = physicsComps.size() - 1;
	for (int i = 0; i < size; ++i) {
		SphereCollider* sCOuter = dynamic_cast<SphereCollider*>(physicsComps[i]);

		// Do a check if this is a SphereCollider here
		if (sCOuter != nullptr) {
			// get radius of outer object
			Transform* transOuter = sCOuter->getMasterGO()->getTransform();
			XMVECTOR mainPos = XMLoadFloat3(&transOuter->getPosition());
			double radiusOuter = sCOuter->getRadius() * findLargestComponent(transOuter->getScale());

			// iterate all other elements
			int innerSize = physicsComps.size();
			for (int j = i + 1; j < innerSize; ++j) {
				SphereCollider* sCInner = dynamic_cast<SphereCollider*>(physicsComps[j]);
				// Do a check if this is a SphereCollider here
				if (sCInner != nullptr) {
					//get radius of inner object
					Transform* transInner = sCInner->getMasterGO()->getTransform();
					XMVECTOR innerPos = XMLoadFloat3(&transInner->getPosition());
					double radiusInner = sCInner->getRadius() * findLargestComponent(transInner->getScale());


					if (checkCollision(mainPos, innerPos, radiusOuter, radiusInner)) {
						XMFLOAT3 collPoint;
						XMStoreFloat3(&collPoint, (mainPos - innerPos) / 2);
						GameObject::CollisionObject collision(collPoint ,sCOuter->getMasterGO(), sCInner->getMasterGO());
						GameObject::CollisionObject collision2(collPoint , sCInner->getMasterGO(), sCOuter->getMasterGO());

						// call onCollision functions
						sCOuter->getMasterGO()->OnCollision(collision);
						sCInner->getMasterGO()->OnCollision(collision2);
					}

				}
			}
		}
	}
}

bool PhysicEngine::checkCollision(XMVECTOR v3_1, XMVECTOR v3_2, double radius1, double radius2) {
	double dist = XMVector3LengthSq(v3_1 - v3_2).m128_f32[0];
	double radiSqr = radius1 + radius2;
	radiSqr = radiSqr * radiSqr;
	if (radiSqr >= dist) {
		return true;
	} 
	else {
		return false;
	}
}


bool PhysicEngine::physicsCompsContainComponent(PhysicsComponent* c) {
	return (std::find(physicsComps.begin(), physicsComps.end(), c) != physicsComps.end());
}


double PhysicEngine::findLargestComponent(XMFLOAT3 vector) {
	if (vector.x >= vector.y && vector.x >= vector.z) {
		return vector.x;
	}
	else if (vector.y >= vector.x && vector.x >= vector.z) {
		return vector.y;
	}
	else {
		return vector.z;
	}
}

float PhysicEngine::dot(XMVECTOR a, XMVECTOR b) {
	return XMVector3Dot(a, b).m128_f32[0];
}

double PhysicEngine::safeAcos(double value) {
	if (value > 1) {
		return acos(1);
	}
	else if (value < -1) {
		return acos(-1);
	}
	else {
		return acos(value);
	}
}