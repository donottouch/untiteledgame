#include "pch.h"
#include "InputEngine.h"


InputEngine::InputEngine::InputEngine(Windows::UI::Core::CoreWindow ^ window)
{
	inputWrapper = ref new InputWrapper(window);
}


InputEngine::InputEngine::~InputEngine()
{
}

void InputEngine::InputEngine::tick(list<UIComponent*>* ui)
{
	inputWrapper->reset();
	const int cLength = inputWrapper->getClicksLenght();
	Windows::Foundation::Rect bonds = Windows::UI::Core::CoreWindow::GetForCurrentThread()->Bounds;

	for (int i = 0; i < cLength; i++)
	{
		Windows::UI::Core::PointerEventArgs^ click = inputWrapper->getClick(i);



		for each (UIComponent* comp in *ui)
		{
			float scaling = m_CoordianteScale;

			int upperleftx = comp->getRect().anchorUV.x * bonds.Width/scaling + comp->getRect().upperLeft.x;
			int upperlefty = comp->getRect().anchorUV.y * bonds.Height/scaling + comp->getRect().upperLeft.y;
			int lowerrightx = upperleftx + comp->getRect().getSize().x;
			int lowerrighty = upperlefty + comp->getRect().getSize().y;

			int posx1 = click->CurrentPoint->Position.X;
			int posy1 = click->CurrentPoint->Position.Y;

			if (click->CurrentPoint->Position.X > upperleftx *scaling &&
				click->CurrentPoint->Position.Y > upperlefty*scaling &&
				click->CurrentPoint->Position.X < lowerrightx*scaling &&
				click->CurrentPoint->Position.Y < lowerrighty*scaling)
			{
				if (click->CurrentPoint->Properties->IsLeftButtonPressed)
				{
					comp->OnClick(click);
				}

				switch (comp->currState)
				{
				case UIComponent::State::out:
					comp->currState = UIComponent::State::enter;
					comp->OnPointerEnter(click);
					break;
				case UIComponent::State::enter:
					comp->currState = UIComponent::State::in;
					comp->OnPointerMove(click);
					break;
				case UIComponent::State::in:
					comp->OnPointerMove(click);
					break;
				case UIComponent::State::exit:
					comp->currState = UIComponent::State::enter;
					comp->OnPointerEnter(click);
					break;
				default:
					break;
				}
			}
			else {
				switch (comp->currState)
				{
				case UIComponent::State::out:
					break;
				case UIComponent::State::enter:
					comp->currState = UIComponent::State::exit;
					comp->OnPointerExit(click);
					break;
				case UIComponent::State::in:
					comp->currState = UIComponent::State::exit;
					comp->OnPointerExit(click);
					break;
				case UIComponent::State::exit:
					comp->currState = UIComponent::State::out;
					break;
				default:
					break;
				}
			}
			if (comp == nullptr) break;
		}
	}

}

void InputEngine::InputEngine::handleResizeWindow(list<UIComponent*>* ui)
{
	auto bounds = Windows::UI::Core::CoreWindow::GetForCurrentThread()->Bounds;
	for each (UIComponent* comp in *ui)
	{
		//comp->handleResize(bounds);
	}
}

